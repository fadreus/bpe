#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"  # 0  push packetId
            "\xE8\xAB\xAB\xAB\xAB"  # 5  call CRagConnection::instanceR
            "\x8B\xC8"              # 10 mov ecx, eax
            "\xE8"                  # 12 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 12,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 6,
        }
    ],
    # 2018-05-09
    [
        (
            "\xBF\xAB\xAB\x00\x00"          # 0  mov edi, packetId
            "\x8D\xAB\x00"                  # 5  lea ecx, [ecx+0]
            "\xAB"                          # 8  push esi
            "\x6A\x08"                      # 9  push 8
            "\x8D\x85\xAB\xAB\xAB\xAB"      # 11 lea eax, [ebp+var_95C]
            "\x50"                          # 17 push eax
            "\xB9\xAB\xAB\xAB\xAB"          # 18 mov ecx, offset g_session
            "\xE8\xAB\xAB\xAB\xAB"          # 23 call func1
            "\xC7\xAB\xAB\xAB\x00\x00\x00"  # 28 mov [ebp+var_4], 9
            "\xA1\xAB\xAB\xAB\xAB"          # 35 mov eax, cchWideChar
            "\x8D\x8D\xAB\xAB\xAB\xAB"      # 40 lea  ecx, [ebp+var_95C]
            "\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 46 mov word ptr [ebp+A], di
            "\x89\x85\xAB\xAB\xAB\xAB"      # 53 mov [ebp+A+2], eax
            "\xE8\xAB\xAB\xAB\xAB"          # 59 call func2
            "\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 64 mov word ptr [ebp+A+6], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"      # 71 lea eax, [ebp+A]
            "\x50"                          # 77 push eax
            "\x0F\xAB\xAB\xAB\xAB\xAB\xAB"  # 78 movsx eax, word ptr [ebp+A]
            "\x50"                          # 85 push eax
            "\xE8\xAB\xAB\xAB\xAB"          # 86 call CRagConnection::instanceR
            "\x8B\xC8"                      # 91 mov ecx, eax
            "\xE8"                          # 93 call CRag...::GetPacketSize
        ),
        {
            "fixedOffset": 93,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 87,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB9\xAB\xAB\x00\x00"  # 0  mov ecx, packetId
            "\x66\xAB\xAB\xAB"      # 5  mov word ptr [ebp+buf], cx
            "\x8B\xAB"              # 9  mov eax, [eax]
            "\x89\xAB\xAB"          # 11 mov dword ptr [ebp+buf+2], eax
            "\x8D\xAB\xAB"          # 14 lea eax, [ebp+buf]
            "\x50"                  # 17 push eax
            "\x51"                  # 18 push ecx
            "\xE8\xAB\xAB\xAB\xAB"  # 19 call CRagConnection::instanceR
            "\x8B\xC8"              # 24 mov ecx, eax
            "\xE8"                  # 26 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 20,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"          # 0  mov eax, packetId
            "\x66\xAB\xAB\xAB"              # 5  mov word ptr [ebp+buf], ax
            "\x0F\xB6\xAB\xAB\xAB\xAB\xAB"  # 9  movzx eax, addr1
            "\x89\x85\xAB\xAB\xAB\xAB"      # 16 mov [ebp+var_120], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"      # 22 lea eax, [ebp+var_120]
            "\x50"                          # 28 push eax
            "\x8D\xAB\xAB\xAB\x00\x00"      # 29 lea ecx, [ebx+0B0h]
            "\xE8\xAB\xAB\xAB\xAB"          # 35 call func1
            "\x8B\xAB"                      # 40 mov eax, [eax]
            "\x89\xAB\xAB"                  # 42 mov dword ptr [ebp+buf+2], eax
            "\x8D\xAB\xAB"                  # 45 lea eax, [ebp+buf]
            "\x50"                          # 48 push eax
            "\x0F\xAB\xAB\xAB"              # 49 movsx eax, word ptr [ebp+buf]
            "\x50"                          # 53 push eax
            "\xE8\xAB\xAB\xAB\xAB"          # 54 call CRagConnection::instanceR
            "\x8B\xC8"                      # 59 mov ecx, eax
            "\xE8"                          # 61 call CRagCon...::GetPacketSize
        ),
        {
            "fixedOffset": 61,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 55,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"  # 0  push packetId
            "\x66\x89\xAB\xAB"      # 5  mov word ptr [ebp+buf+2], cx
            "\x66\x89\xAB\xAB"      # 9  mov word ptr [ebp+var_8], si
            "\xE8\xAB\xAB\xAB\xAB"  # 13 call CRagConnection::instanceR
            "\x8B\xC8"              # 18 mov ecx, eax
            "\xE8"                  # 20 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 14,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"              # 0  push packetId
            "\xC7\x85\xAB\xAB\xFF\xFF\xAB\x00\x00\x00"  # 5  mov [ebp+var1], 0F
            "\xC7\x85\xAB\xAB\xFF\xFF\xAB\x00\x00\x00"  # 15 mov [ebp+var2], 0
            "\xC6\xAB\xAB\xAB\xFF\xFF\xAB"      # 25 mov byte ptr [ebp+var3], 0
            "\xE8\xAB\xAB\xAB\xAB"              # 32 call ::instanceR
            "\x8B\xC8"                          # 37 mov ecx, eax
            "\xE8"                              # 39 call ::GetPacketSize
        ),
        {
            "fixedOffset": 39,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 33,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"          # 0  push packetId
            "\x66\x89\xAB\xAB\xAB\xFF\xFF"  # 5  mov word ptr [ebp+Src], bx
            "\xE8\xAB\xAB\xAB\xAB"          # 12 call CRagConnection::instanceR
            "\x8B\xC8"                      # 17 mov ecx, eax
            "\xE8"                          # 19 call CRagCon...::GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"          # 0  push packetId
            "\x89\xAB\xAB"                  # 5  mov dword ptr [ebp+buf+2], ecx
            "\x0F\x95\xAB\xAB"              # 8  setnz [ebp+var_76]
            "\xE8\xAB\xAB\xAB\xAB"          # 12 call CRagConnection::instanceR
            "\x8B\xC8"                      # 17 mov ecx, eax
            "\xE8"                          # 19 call CRagCon...::GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"  # 0  push packetId
            "\xC6\x45\xAB\xAB"      # 5  mov byte ptr [ebp+0Eh], 5
            "\xE8\xAB\xAB\xAB\xAB"  # 9  call CRagConnection::instanceR
            "\x8B\xC8"              # 14 mov ecx, eax
            "\xE8"                  # 16 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 10,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push packetId
            "\x89\x8D\xAB\xAB\xAB\xFF"  # 5  mov [ebp+var_10E], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection::instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 12,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"        # 0  push packetId
            "\x66\x0F\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  movq addr1, xmm0
            "\xE8\xAB\xAB\xAB\xAB"        # 13 call CRagConnection::instanceR
            "\x8B\xC8"                    # 18 mov ecx, eax
            "\xE8"                        # 20 call CRagCon...::GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 14,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"  # 0  push packetId
            "\x89\xAB\xAB"          # 5  mov [ebp+var_52], ebx
            "\x66\x0F\xAB\xAB\xAB"  # 8  movq [ebp+var_36], xmm0
            "\xE8\xAB\xAB\xAB\xAB"  # 13 call CRagConnection::instanceR
            "\x8B\xC8"              # 18 mov ecx, eax
            "\xE8"                  # 20 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 14,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"  # 0  push packetId
            "\x0F\xAB\xAB\xAB"      # 5  setz [ebp+var_7E]
            "\xE8\xAB\xAB\xAB\xAB"  # 9  call CRagConnection::instanceR
            "\x8B\xC8"              # 14 mov ecx, eax
            "\xE8"                  # 16 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 10,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB9\xAB\xAB\x00\x00"  # 0  mov ecx, 202h
            "\xF3\x0F\xAB\x00"      # 5  movq xmm0, qword ptr [eax]
            "\x66\x0F\xD6\xAB\xAB"  # 9  movq [ebp+var_1E], xmm0
            "\xF3\x0F\x7E\xAB\xAB"  # 14 movq xmm0, qword ptr [eax+8]
            "\x66\x0F\xD6\xAB\xAB"  # 19 movq [ebp+var_16], xmm0
            "\xF3\x0F\x7E\xAB\xAB"  # 24 movq xmm0, qword ptr [eax+10h]
            "\x8D\x45\xAB"          # 29 lea eax, [ebp+Src]
            "\x50"                  # 32 push eax
            "\x51"                  # 33 push ecx
            "\x66\xAB\xAB\xAB"      # 34 mov [ebp+Src], cx
            "\x66\x0F\xD6\xAB\xAB"  # 38 movq [ebp+var_E], xmm0
            "\xE8\xAB\xAB\xAB\xAB"  # 43 call CRagConnection::instanceR
            "\x8B\xC8"              # 48 mov ecx, eax
            "\xE8"                  # 50 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 50,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 44,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push packetId
            "\xC7\x87\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 5  mov [edi+A], 0
            "\xB3\xAB"                  # 15 mov bl, 1
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection::instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 18,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"          # 0  mov eax, packetId
            "\x6A\x10"                      # 5  push 10h
            "\x66\x89\xAB\xAB"              # 7  mov [ebp+Src], ax
            "\xE8\xAB\xAB\xAB\xAB"          # 11 call func1
            "\x50"                          # 16 push eax
            "\x8D\xAB\xAB"                  # 17 lea  eax, [ebp+Dst]
            "\x6A\x10"                      # 20 push 10h
            "\x50"                          # 22 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"      # 23 call ds:memcpy_s
            "\x66\x8B\x86\xAB\xAB\x00\x00"  # 29 mov ax, [esi+0C4h]
            "\x66\x89\xAB\xAB"              # 36 mov word ptr [ebp+var14], ax
            "\x83\xC4\xAB"                  # 40 add esp, 10h
            "\x8D\x45\xAB"                  # 43 lea eax, [ebp+Src]
            "\x50"                          # 46 push eax
            "\x0F\xBF\xAB\xAB"              # 47 movsx eax, [ebp+Src]
            "\x50"                          # 51 push eax
            "\xE8\xAB\xAB\xAB\xAB"          # 52 call CRagConnection::instanceR
            "\x8B\xC8"                      # 57 mov ecx, eax
            "\xE8"                          # 59 call CRagCon...::GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 53,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, packetId
            "\x6A\x10"                  # 5  push 10h
            "\x66\x89\xAB\xAB"          # 7  mov [ebp+Src], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call func1
            "\x50"                      # 16 push eax
            "\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            "\x6A\x10"                  # 20 push 10h
            "\x50"                      # 22 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call ds:memcpy_s
            "\x83\xC4\xAB"              # 29 add esp, 10h
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+Src]
            "\x50"                      # 35 push eax
            "\x0F\xBF\xAB\xAB"          # 36 movsx eax, [ebp+Src]
            "\x50"                      # 40 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection::instanceR
            "\x8B\xC8"                  # 46 mov ecx, eax
            "\xE8"                      # 48 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 42,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, packetId
            "\x66\x89\xAB\xAB"          # 5  mov [ebp+Src], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call func1
            "\x50"                      # 14 push eax
            "\x8D\x45\xAB"              # 15 lea eax, [ebp+Dst]
            "\x6A\xAB"                  # 18 push 18h
            "\x50"                      # 20 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 21 call ds:strcpy_s
            "\x83\xC4\xAB"              # 27 add esp, 0Ch
            "\x8D\x45\xAB"              # 30 lea eax, [ebp+Src]
            "\x50"                      # 33 push eax
            "\x0F\xBF\xAB\xAB"          # 34 movsx eax, [ebp+Src]
            "\x50"                      # 38 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection::instanceR
            "\x8B\xC8"                  # 44 mov ecx, eax
            "\xE8"                      # 46 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 40,
        }
    ],
    # 2018-05-09
    [
        (
            "\xBF\xAB\xAB\x00\x00"      # 0  mov edi, packetId
            "\xEB\x08"                  # 5  jmp +8
            "\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  unused
            "\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, cchWideChar
            "\x8D\x4E\xAB"              # 20 lea ecx, [esi+8]
            "\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 23 mov word ptr [ebp+var1], di
            "\x89\x85\xAB\xAB\xAB\xAB"  # 30 mov [ebp+var2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call func1
            "\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 41 mov word ptr [ebp+var3], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 48 lea eax, [ebp+var_142C]
            "\x50"                      # 54 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 55 movsx eax, word ptr [ebp+var3]
            "\x50"                      # 62 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 63 call CRagConnection::instanceR
            "\x8B\xC8"                  # 68 mov ecx, eax
            "\xE8"                      # 70 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 70,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 64,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, packetId
            "\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 5  mov g1, 0FFFFFFFFh
            "\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 15 mov g2, 0FFFFFFFFh
            "\xC7\x05\xAB\xAB\xAB\xAB\xFF\xFF\xFF\xFF"  # 25 mov g3, 0FFFFFFFFh
            "\x66\x89\xAB\xAB"          # 35 mov [ebp+Src], ax
            "\x8B\x46\xAB"              # 39 mov eax, [esi+4]
            "\x89\x45\xAB"              # 42 mov [ebp+var_1A], eax
            "\x8B\xAB"                  # 45 mov eax, [esi]
            "\x8D\x4E\xAB"              # 47 lea ecx, [esi+60h]
            "\x89\x45\xAB"              # 50 mov [ebp+var_16], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call func1
            "\x66\x89\xAB\xAB"          # 58 mov [ebp+var_12], ax
            "\x8D\x45\xAB"              # 62 lea eax, [ebp+Src]
            "\x50"                      # 65 push eax
            "\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+Src]
            "\x50"                      # 70 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection::instanceR
            "\x8B\xC8"                  # 76 mov ecx, eax
            "\xE8"                      # 78 call CRagConnection::GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 72,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 907h
            "\xFF\x75\xAB"              # 5  push [ebp+arg_0]
            "\x66\x89\xAB\xAB"          # 8  mov [ebp+Src], ax
            "\x66\x8B\xAB\xAB"          # 12 mov ax, [edx+4]
            "\x66\x89\xAB\xAB"          # 16 mov [ebp+var_6], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call sub_5A4CB0
            "\x83\xF8\xAB"              # 25 cmp eax, 3
            "\x8D\x45\xAB"              # 28 lea eax, [ebp+Src]
            "\x50"                      # 31 push eax
            "\x0F\xBF\xAB\xAB"          # 32 movsx eax, [ebp+Src]
            "\x50"                      # 36 push eax
            "\x0F\x95\x45\xAB"          # 37 setnz [ebp+var_4]
            "\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            "\x8B\xC8"                  # 46 mov ecx, eax
            "\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0
        },
        {
            "instanceR": 42,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            "\x6A\xAB"                  # 5  push 18h
            "\x50"                      # 7  push eax
            "\x66\x89\xAB\xAB"          # 8  mov word ptr [ebp+buf], cx
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call ds:strcpy_s
            "\x8A\x45\xAB"              # 18 mov al, [ebp+arg_4]
            "\x88\x45\xAB"              # 21 mov [ebp+var_1E], al
            "\x83\xC4\xAB"              # 24 add esp, 0Ch
            "\x8D\x45\xAB"              # 27 lea eax, [ebp+buf]
            "\x50"                      # 30 push eax
            "\x0F\xBF\x45\xAB"          # 31 movsx eax, word ptr [ebp+buf]
            "\x50"                      # 35 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            "\x8B\xC8"                  # 41 mov ecx, eax
            "\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\x8B\x47\xAB"              # 9  mov eax, [edi+34h]
            "\x83\xC0\xAB"              # 12 add eax, 4
            "\x50"                      # 15 push eax
            "\x8D\x45\xAB"              # 16 lea eax, [ebp+Dst]
            "\x6A\xAB"                  # 19 push 18h
            "\x50"                      # 21 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 22 call ds:strcpy_s
            "\x8D\x47\xAB"              # 28 lea eax, [edi+1Ch]
            "\x50"                      # 31 push eax
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1D]
            "\x6A\xAB"                  # 35 push 18h
            "\x50"                      # 37 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 38 call ds:strcpy_s
            "\x8B\x45\xAB"              # 44 mov eax, [ebp+arg_0]
            "\x83\xC4\xAB"              # 47 add esp, 18h
            "\x88\x45\xAB"              # 50 mov [ebp+var_36], al
            "\x83\xF8\xAB"              # 53 cmp eax, 2
            "\x75\xAB"                  # 56 jnz short loc_8015B1
            "\xC6\x47\xAB\xAB"          # 58 mov byte ptr [edi+38h], 0
            "\x8D\x45\xAB"              # 62 lea eax, [ebp+Src]
            "\x50"                      # 65 push eax
            "\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+Src]
            "\x50"                      # 70 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            "\x8B\xC8"                  # 76 mov ecx, eax
            "\xE8"                      # 78 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\xAB\xAB"      # 0  mov eax, 8D7h
            "\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            "\x66\x8B\x45\xAB"          # 9  mov ax, [ebp+arg_0]
            "\x6A\xAB"                  # 13 push 17h
            "\x51"                      # 15 push ecx
            "\x66\x89\x45\xAB"          # 16 mov [ebp+var_1E], ax
            "\x8D\x45\xAB"              # 20 lea eax, [ebp+Dst]
            "\x6A\xAB"                  # 23 push 18h
            "\x50"                      # 25 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 26 call ds:strncpy_s
            "\x83\xC4\xAB"              # 32 add esp, 10h
            "\x8D\x45\xAB"              # 35 lea eax, [ebp+buf]
            "\x50"                      # 38 push eax
            "\x0F\xBF\x45\xAB"          # 39 movsx eax, word ptr [ebp+buf]
            "\x50"                      # 43 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            "\x8B\xC8"                  # 49 mov ecx, eax
            "\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8DAh
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\x8B\x41\xAB"              # 9  mov eax, [ecx+34h]
            "\x83\xC0\xAB"              # 12 add eax, 4
            "\x50"                      # 15 push eax
            "\x8D\x45\xAB"              # 16 lea eax, [ebp+Dst]
            "\x6A\xAB"                  # 19 push 18h
            "\x50"                      # 21 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 22 call ds:strcpy_s
            "\x83\xC4\xAB"              # 28 add esp, 0Ch
            "\x8D\x45\xAB"              # 31 lea eax, [ebp+Src]
            "\x50"                      # 34 push eax
            "\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+Src]
            "\x50"                      # 39 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            "\x8B\xC8"                  # 45 mov ecx, eax
            "\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0D0h
            "\xC7\x83\xAB\xAB\x00\x00\xAB\x00\x00\x00"  # 5  mov [ebx+A], 1
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 15 mov byte ptr [ebp+B], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2018-05-09
    [
        (
            "\x50"                      # 0  push eax
            "\x68\xAB\xAB\x00\x00"      # 1  push 368h
            "\x89\x7D\xAB"              # 6  mov [ebp+var_6], edi
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            "\x8B\xC8"                  # 14 mov ecx, eax
            "\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 2,
            "retOffset": 1,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 360h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\xFF\xD6"                  # 9  call esi
            "\x89\x45\xAB"              # 11 mov [ebp+var_6], eax
            "\x8D\x45\xAB"              # 14 lea eax, [ebp+Src]
            "\x50"                      # 17 push eax
            "\x0F\xBF\x45\xAB"          # 18 movsx eax, [ebp+Src]
            "\x50"                      # 22 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 998h
            "\x66\x89\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0DF9Eh], bx
            "\x89\xB5\xAB\xAB\xAB\xAB"  # 12 mov [ebp-0DF9Ch], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 363h
            "\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0DE52h], bx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 12 mov [ebp-0DE50h], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0E4h
            "\x89\x9D\xAB\xAB\xAB\xAB"  # 5  mov [ebp-0DF2Ah], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A99h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp-0DDECh], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 436h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\xFF\xD3"                  # 9  call ebx
            "\x89\x45\xAB"              # 11 mov [ebp+var_2E], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 14 mov eax, cchWideChar
            "\x89\x45\xAB"              # 19 mov [ebp+var_3A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 22 mov eax, dword_100355C
            "\x89\x45\xAB"              # 27 mov [ebp+var_36], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 30 mov eax, dword_1002E1C
            "\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset g_session
            "\x89\x45\xAB"              # 40 mov [ebp+var_32], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_A27B60
            "\x88\x45\xAB"              # 48 mov [ebp+var_2A], al
            "\x8D\x45\xAB"              # 51 lea eax, [ebp+Src]
            "\x50"                      # 54 push eax
            "\x0F\xBF\xAB\xAB"          # 55 movsx eax, [ebp+Src]
            "\x50"                      # 59 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 60 call CRagConnection_instanceR
            "\x8B\xC8"                  # 65 mov ecx, eax
            "\xE8"                      # 67 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 67,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 61,
        }
    ],
    # 2018-05-09
    [
        (
            "\x6A\xAB"                  # 0  push 65h
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\x00\x00\x00"  # 2  mov A, 1
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 436h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var2], ax
            "\xFF\xD7"                  # 12 call edi
            "\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov dword ptr [ebp+var_2CC+2], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 20 mov eax, cchWideChar
            "\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov dword ptr [ebp+var_2DC+6], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword_100355C
            "\x89\x85\xAB\xAB\xAB\xAB"  # 36 mov dword ptr [ebp+var_2D4+2], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 42 mov eax, dword_1002E1C
            "\xB9\xAB\xAB\xAB\xAB"      # 47 mov ecx, offset g_session
            "\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov dword ptr [ebp+var_2D4+6], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 58 call sub_A27B60
            "\x88\x85\xAB\xAB\xAB\xAB"  # 63 mov [ebp+var_2CC+6], al
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 69 lea eax, [ebp+var_2DC+4]
            "\x50"                      # 75 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 76 movsx eax, word ptr [ebp+var1]
            "\x50"                      # 83 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 84 call CRagConnection_instanceR
            "\x8B\xC8"                  # 89 mov ecx, eax
            "\xE8"                      # 91 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 91,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 85,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 233h
            "\x89\x5D\xAB"              # 5  mov dword ptr [ebp+Src+2], ebx
            "\x89\x75\xAB"              # 8  mov dword ptr [ebp+Src+6], esi
            "\xC6\x45\xAB\x00"          # 11 mov [ebp+var_6], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 233h
            "\x89\x5D\xAB"              # 5  mov dword ptr [ebp+Src+6], ebx
            "\xC6\x45\xAB\x00"          # 8  mov [ebp+var_6], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 232h
            "\x89\x7D\xAB"              # 5  mov [ebp+var_E], edi
            "\x88\x4D\xAB"              # 8  mov [ebp+var_8], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 234h
            "\x89\x75\xAB"              # 5  mov [ebp+var_12], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2018-05-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2D8h
            "\xC7\x45\xAB\xAB\x00\x00\x00"  # 5  mov [ebp+var_E], 3
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 827h
            "\x89\x45\xAB"              # 5  mov [ebp+var_6], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_8], 82Bh
            "\x89\x55\xAB"              # 6  mov [ebp+var_10], edx
            "\xC7\x45\xAB\xAB\xAB\x00\x00"  # 9  mov [ebp+var_C], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_4191D0
            "\x8B\xD0"                  # 21 mov edx, eax
            "\x8B\x02"                  # 23 mov eax, [edx]
            "\x8B\x4A\xAB"              # 25 mov ecx, [edx+4]
            "\x8B\x50\xAB"              # 28 mov edx, [eax+10h]
            "\x89\x4D\xAB"              # 31 mov [ebp+var_1C], ecx
            "\x8D\x45\xAB"              # 34 lea eax, [ebp+var_8]
            "\x0F\xBF\xAB\xAB"          # 37 movsx ecx, [ebp+var_8]
            "\x50"                      # 41 push eax
            "\x51"                      # 42 push ecx
            "\x89\x55\xAB"              # 43 mov [ebp+var_6], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 46 call CRagConnection_instanceR
            "\x8B\xC8"                  # 51 mov ecx, eax
            "\xE8"                      # 53 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 53,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 47,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 28Ch
            "\xF3\xA5"                  # 5  rep movsd
            "\xE8\xAB\xAB\xAB\xAB"      # 7  call CRagConnection_instanceR
            "\x8B\xC8"                  # 12 mov ecx, eax
            "\xE8"                      # 14 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 14,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 8,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 802h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_34], 802h
            "\x89\x4D\xAB"              # 11 mov [ebp+var_2A], ecx
            "\x89\x55\xAB"              # 14 mov [ebp+var_26], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 83Ch
            "\x89\x4D\xAB"              # 6  mov [ebp+var_16], ecx
            "\x89\x55\xAB"              # 9  mov [ebp+var_12], edx
            "\x75\x05"                  # 12 jnz short loc_4BCDDB
            "\xB8\xAB\xAB\xAB\xAB"      # 14 mov eax, offset byte_7397F4
            "\x50"                      # 19 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call _atoi
            "\x0F\xBF\xAB\xAB"          # 25 movsx ecx, [ebp+var_18]
            "\x83\xC4\xAB"              # 29 add esp, 4
            "\x66\x89\x45\xAB"          # 32 mov [ebp+var_E], ax
            "\x8D\x45\xAB"              # 36 lea eax, [ebp+var_18]
            "\x50"                      # 39 push eax
            "\x51"                      # 40 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            "\x8B\xC8"                  # 46 mov ecx, eax
            "\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 838h
            "\x89\xBB\xAB\xAB\x00\x00"  # 5  mov [ebx+88h], edi
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_8+2], 838h
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_8], 2B6h
            "\x89\x55\xAB"              # 11 mov [ebp+var_6], edx
            "\x88\x45\xAB"              # 14 mov [ebp+var_2], al
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            "\x6A\xAB"                  # 0  push 7Dh
            "\xE8\xAB\xAB\xAB\xAB"      # 2  call CRagConnection_instanceR
            "\x8B\xC8"                  # 7  mov ecx, eax
            "\xE8"                      # 9  call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 9,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 3,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 89h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\xFF\xD6"                  # 11 call esi
            "\x0F\xBF\xAB\xAB"          # 13 movsx edx, [ebp+var_18]
            "\x8D\x4D\xAB"              # 17 lea ecx, [ebp+var_18]
            "\x89\x45\xAB"              # 20 mov [ebp+var_14+3], eax
            "\x51"                      # 23 push ecx
            "\x52"                      # 24 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            "\x8B\xC8"                  # 30 mov ecx, eax
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_14], 89h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\xFF\xD3"                  # 11 call ebx
            "\x0F\xBF\xAB\xAB"          # 13 movsx ecx, [ebp+var_14]
            "\x89\x45\xAB"              # 17 mov [ebp+var_D], eax
            "\x8D\x45\xAB"              # 20 lea eax, [ebp+var_14]
            "\x50"                      # 23 push eax
            "\x51"                      # 24 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            "\x8B\xC8"                  # 30 mov ecx, eax
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_C], 89h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\xFF\xD7"                  # 11 call edi
            "\x0F\xBF\xAB\xAB"          # 13 movsx ecx, [ebp+var_C]
            "\x89\x45\xAB"              # 17 mov [ebp+var_5], eax
            "\x8D\x45\xAB"              # 20 lea eax, [ebp+var_C]
            "\x50"                      # 23 push eax
            "\x51"                      # 24 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            "\x8B\xC8"                  # 30 mov ecx, eax
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_38], 8Ch
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\x0F\xBF\xAB\xAB"          # 11 movsx ecx, [ebp+var_38]
            "\x8B\x55\xAB"              # 15 mov edx, [ebp+arg_0]
            "\x8D\x45\xAB"              # 18 lea eax, [ebp+var_38]
            "\x50"                      # 21 push eax
            "\x51"                      # 22 push ecx
            "\x89\x55\xAB"              # 23 mov [ebp+var_2E], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            "\x8B\xC8"                  # 31 mov ecx, eax
            "\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_40], 0A2h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\x0F\xBF\xAB\xAB"          # 11 movsx edx, [ebp+var_40]
            "\x8B\x45\xAB"              # 15 mov eax, [ebp+var_10]
            "\x8D\x4D\xAB"              # 18 lea ecx, [ebp+var_40]
            "\x51"                      # 21 push ecx
            "\x52"                      # 22 push edx
            "\x89\x45\xAB"              # 23 mov [ebp+var_36], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            "\x8B\xC8"                  # 31 mov ecx, eax
            "\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_28], 0A2h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\x0F\xBF\x55\xAB"          # 11 movsx edx, [ebp+var_28]
            "\x8D\x4D\xAB"              # 15 lea ecx, [ebp+var_28]
            "\x89\x7D\xAB"              # 18 mov [ebp+var_1E], edi
            "\x51"                      # 21 push ecx
            "\x52"                      # 22 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 90h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_10], 90h
            "\x89\x45\xAB"              # 11 mov [ebp+var_E], eax
            "\xC6\x45\xAB\xAB"          # 14 mov [ebp+var_A], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0D0h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_0], 0D0h
            "\xC6\x45\xAB\xAB"          # 11 mov byte ptr [ebp+arg_0+2], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 20Fh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_1C], 20Fh
            "\x89\x45\xAB"              # 11 mov [ebp+var_18+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2C8h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_0], 2C8h
            "\x88\x4D\xAB"              # 11 mov byte ptr [ebp+arg_0+2], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0BFh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 0BFh
            "\x88\x55\xAB"              # 11 mov byte ptr [ebp+arg_8+2], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0B2h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 0B2h
            "\x88\x45\xAB"              # 11 mov byte ptr [ebp+arg_8+2], al
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2D6h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 2D6h
            "\x89\x55\xAB"              # 11 mov [ebp+var_14+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 7F5h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 7F5h
            "\x89\x4D\xAB"              # 11 mov [ebp+var_14+2], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0ABh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_4], 0ABh
            "\x66\x89\x45\xAB"          # 11 mov word ptr [ebp+arg_4+2], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0B8h
            "\x88\x55\xAB"              # 5  mov byte ptr [ebp+var_10+2], dl
            "\x89\x45\xAB"              # 8  mov [ebp+var_14+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0A9h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 0A9h
            "\x66\x89\x55\xAB"          # 11 mov word ptr [ebp+var_14+2], dx
            "\x66\x89\x45\xAB"          # 15 mov word ptr [ebp+var_10], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0A9h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_14], 0A9h
            "\x66\x89\x7D\xAB"          # 11 mov word ptr [ebp+var_14+2], di
            "\x66\x89\x75\xAB"          # 15 mov word ptr [ebp+var_10], si
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_2C], 116h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\x8D\x45\xAB"              # 11 lea eax, [ebp+var_28+4]
            "\x6A\xAB"                  # 14 push 7
            "\x50"                      # 16 push eax
            "\x8B\xCB"                  # 17 mov ecx, ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call sub_5AA240
            "\x0F\xBF\x45\xAB"          # 24 movsx eax, [ebp+var_2C]
            "\x66\x8B\x4D\xAB"          # 28 mov cx, word ptr [ebp+arg_8]
            "\x8D\x55\xAB"              # 32 lea edx, [ebp+var_2C]
            "\x52"                      # 35 push edx
            "\x50"                      # 36 push eax
            "\x66\x89\x75\xAB"          # 37 mov word ptr [ebp+var_28+2], si
            "\x66\x89\x4D\xAB"          # 41 mov word ptr [ebp+var_28+0Bh], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            "\x8B\xC8"                  # 50 mov ecx, eax
            "\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18+4], 439h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_599B90
            "\x0F\xBF\x55\xAB"          # 11 movsx edx, word ptr [ebp+var_18+4]
            "\x8D\x4D\xAB"              # 15 lea ecx, [ebp+var_18+4]
            "\x89\x45\xAB"              # 18 mov [ebp-10h], eax
            "\x51"                      # 21 push ecx
            "\x52"                      # 22 push edx
            "\x66\x89\xAB\xAB"          # 23 mov word ptr [ebp+var_18+6], di
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            "\x8B\xC8"                  # 32 mov ecx, eax
            "\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 90h
            "\x88\x55\xAB"              # 5  mov [ebp-0Eh], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0E0h
            "\xF3\xA4"                  # 5  rep movsb
            "\xB9\xAB\xAB\x00\x00"      # 7  mov ecx, 6
            "\x8D\xB5\xAB\xAB\xAB\xAB"  # 12 lea esi, [ebp+var_274]
            "\x8D\x7D\xAB"              # 18 lea edi, [ebp+var_34+2]
            "\xF3\xA5"                  # 21 rep movsd
            "\x8B\x4D\xAB"              # 23 mov ecx, [ebp+arg_8]
            "\x89\x4D\xAB"              # 26 mov [ebp+var_38+2], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            "\x8B\xC8"                  # 34 mov ecx, eax
            "\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0E8h
            "\xC7\x83\xAB\xAB\xAB\x00\xAB\xAB\x00\x00"  # 5  mov [ebx+0ECh], 1
            "\x66\xC7\x45\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_18+4], 0E8h
            "\x66\x89\xAB\xAB"          # 21 mov word ptr [ebp+var_18+6], si
            "\x89\x45\xAB"              # 25 mov [ebp-10h], eax
            "\x89\x83\xAB\xAB\xAB\xAB"  # 28 mov [ebx+0FCh], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 34 call CRagConnection_instanceR
            "\x8B\xC8"                  # 39 mov ecx, eax
            "\xE8"                      # 41 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 41,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 35,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 94h
            "\x89\x55\xAB"              # 5  mov ptr [ebp+var_28+0Bh], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0F7h
            "\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_28+9], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 126h
            "\x66\x89\xAB\xAB"          # 5  mov word ptr [ebp+var_18+6], cx
            "\x89\x55\xAB"              # 9  mov [ebp-10h], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2C7h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 2C7h
            "\x88\x45\xAB"              # 11 mov [ebp-0Eh], al
            "\x89\x4D\xAB"              # 14 mov dword ptr [ebp+var_18+6], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 1E3h
            "\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_28+2], ecx
            "\x89\x55\xAB"              # 8  mov [ebp+var_28+0Ah], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 1F7h
            "\x89\x55\xAB"              # 5  mov dword ptr [ebp+var1+2], edx
            "\x89\x45\xAB"              # 8  mov dword ptr [ebp+var1+0Ah], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 103h
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_4], 2Bh
            "\x89\x45\xAB"              # 12 mov [ebp+var_38+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 112h
            "\xC7\x83\xAB\xAB\xAB\x00\xAB\xAB\x00\x00"  # 5  mov [ebx+0F8h], 1
            "\x66\xC7\x45\xAB\xAB\xAB"  # 15 mov word ptr [ebp+arg_8], 112h
            "\x66\x89\x4D\xAB"          # 21 mov word ptr [ebp+arg_8+2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            "\x8B\xC8"                  # 30 mov ecx, eax
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 438h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18], 438h
            "\x66\x89\xAB\xAB"          # 11 mov word ptr [ebp+var_18+2], si
            "\x66\x89\xAB\xAB"          # 15 mov word ptr [ebp+var_18+4], di
            "\x89\x45\xAB"              # 19 mov dword ptr [ebp+var_18+6], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 113h
            "\x66\x89\x7D\xAB"          # 5  mov word ptr [ebp+var_28+2], di
            "\x66\x89\x45\xAB"          # 9  mov word ptr [ebp+var_28+6], ax
            "\x66\x89\x4D\xAB"          # 13 mov word ptr [ebp+var_28+0Bh], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 130h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 130h
            "\x89\x7D\xAB"              # 11 mov dword ptr [ebp+var_18+6], edi
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 143h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18], 143h
            "\x89\x75\xAB"              # 11 mov dword ptr [ebp+var_18+2], esi
            "\x89\x55\xAB"              # 14 mov dword ptr [ebp+var_18+6], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 16Eh
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_163], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 178h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 178h
            "\x66\x89\x4D\xAB"          # 11 mov word ptr [ebp+arg_8+2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 17Ah
            "\x89\x83\xAB\xAB\xAB\xAB"  # 5  mov [ebx+41Ch], eax
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_4], 17Ah
            "\x66\x89\x45\xAB"          # 17 mov word ptr [ebp+arg_4+2], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 18Ah
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_4+2], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18], 0A7h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\x8B\x4D\xAB"              # 11 mov ecx, [ebp+arg_8]
            "\x8B\x55\xAB"              # 14 mov edx, [ebp+arg_4]
            "\x8D\x45\xAB"              # 17 lea eax, [ebp+var_18+6]
            "\x50"                      # 20 push eax
            "\x6A\xAB"                  # 21 push 0
            "\x51"                      # 23 push ecx
            "\x52"                      # 24 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call sub_5C6820
            "\x0F\xBF\xAB\xAB"          # 30 movsx ecx, word ptr [ebp+var_18]
            "\x83\xC4\xAB"              # 34 add esp, 10h
            "\x8D\x45\xAB"              # 37 lea eax, [ebp+var_18]
            "\x50"                      # 40 push eax
            "\x51"                      # 41 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call CRagConnection_instanceR
            "\x8B\xC8"                  # 47 mov ecx, eax
            "\xE8"                      # 49 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 49,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 43,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18], 85h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5AA240
            "\x8D\x45\xAB"              # 11 lea eax, [ebp+var_18+6]
            "\x6A\xAB"                  # 14 push 3
            "\x50"                      # 16 push eax
            "\x8B\xCB"                  # 17 mov ecx, ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call sub_5AA240
            "\x8A\x4D\xAB"              # 24 mov cl, byte ptr [ebp+arg_4]
            "\x66\x8B\xAB\xAB"          # 27 mov dx, word ptr [ebp+arg_8]
            "\x88\x4D\xAB"              # 31 mov [ebp-0Fh], cl
            "\x8D\x45\xAB"              # 34 lea eax, [ebp+var_18]
            "\x0F\xBF\x4D\xAB"          # 37 movsx ecx, word ptr [ebp+var_18]
            "\x50"                      # 41 push eax
            "\x51"                      # 42 push ecx
            "\x66\x89\x55\xAB"          # 43 mov word ptr [ebp+var_18+4], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            "\x8B\xC8"                  # 52 mov ecx, eax
            "\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 198h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 198h
            "\x66\x89\x45\xAB"          # 11 mov [ebp-10h], ax
            "\x66\x89\x4D\xAB"          # 15 mov [ebp-0Eh], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 1A8h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+arg_8], 1A8h
            "\x66\x89\x55\xAB"          # 11 mov word ptr [ebp+arg_8+2], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 208h
            "\x89\x45\xAB"              # 5  mov dword ptr [ebp+var1+2], eax
            "\x89\x4D\xAB"              # 8  mov dword ptr [ebp+var1+6], ecx
            "\x89\x75\xAB"              # 11 mov dword ptr [ebp+var1+0Ah], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 203h
            "\x89\x45\xAB"              # 5  mov dword ptr [ebp+var_18+2], eax
            "\x89\x4D\xAB"              # 8  mov dword ptr [ebp+var_18+6], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 22Dh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 22Dh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+var_18+6], 0
            "\x88\x45\xAB"              # 17 mov [ebp-10h], al
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_34], 231h
            "\xF2\xAE"                  # 6  repne scasb
            "\xF7\xD1"                  # 8  not ecx
            "\x49"                      # 10 dec ecx
            "\x83\xF9\xAB"              # 11 cmp ecx, 18h
            "\x7D\x14"                  # 14 jge short loc_5C5715
            "\x41"                      # 16 inc ecx
            "\x8D\x7D\xAB"              # 17 lea edi, [ebp+var_34+2]
            "\x8B\xD1"                  # 20 mov edx, ecx
            "\xC1\xE9\xAB"              # 22 shr ecx, 2
            "\xF3\xA5"                  # 25 rep movsd
            "\x8B\xCA"                  # 27 mov ecx, edx
            "\x83\xE1\xAB"              # 29 and ecx, 3
            "\xF3\xA4"                  # 32 rep movsb
            "\xEB\x0E"                  # 34 jmp short loc_5C5723
            "\xB9\xAB\xAB\xAB\x00"      # 36 mov ecx, 6
            "\x8D\x7D\xAB"              # 41 lea edi, [ebp+var_34+2]
            "\xF3\xA5"                  # 44 rep movsd
            "\xC6\x45\xAB\xAB"          # 46 mov [ebp+var_28+0Dh], 0
            "\x0F\xBF\x4D\xAB"          # 50 movsx ecx, word ptr [ebp+var_34]
            "\x8D\x45\xAB"              # 54 lea eax, [ebp+var_34]
            "\x50"                      # 57 push eax
            "\x51"                      # 58 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            "\x8B\xC8"                  # 64 mov ecx, eax
            "\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 24Dh
            "\x89\x55\xAB"              # 5  mov dword ptr [ebp+var_18+6], edx
            "\x66\x89\x45\xAB"          # 8  mov [ebp-0Eh], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2BAh
            "\x88\x4D\xAB"              # 5  mov [ebp-0Eh], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2D8h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18], 2D8h
            "\xC7\x45\xAB\xAB\xAB\x00\x00"  # 11 mov dword ptr [ebp+var1], 0
            "\x89\x45\xAB"              # 18 mov dword ptr [ebp+var2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 247h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_18+4], 247h
            "\x89\x45\xAB"              # 11 mov [ebp-10h], eax
            "\xA3\xAB\xAB\xAB\xAB"      # 14 mov dword_83D0F0, eax
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 7D7h
            "\x88\x45\xAB"              # 5  mov [ebp-0Eh], al
            "\x88\x4D\xAB"              # 8  mov [ebp-0Dh], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 149h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_C], 149h
            "\x89\x45\xAB"              # 11 mov [ebp+var_A], eax
            "\xC6\x45\xAB\xAB"          # 14 mov [ebp+var_6], 2
            "\x66\xC7\x45\xAB\xAB\xAB"  # 18 mov [ebp+var_5], 0Ah
            "\xE8\xAB\xAB\xAB\xAB"      # 24 call CRagConnection_instanceR
            "\x8B\xC8"                  # 29 mov ecx, eax
            "\xE8"                      # 31 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 31,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 25,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_4C], 436h
            "\xFF\xD6"                  # 6  call esi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, dword_83B6A0
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, dword_83AFD0
            "\x89\x45\xAB"              # 20 mov [ebp+var_3E], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, dword_83B69C
            "\x89\x4D\xAB"              # 28 mov [ebp+var_46], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_839C18
            "\x89\x45\xAB"              # 36 mov [ebp+var_4A], eax
            "\x89\x55\xAB"              # 39 mov [ebp+var_42], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_6E64A0
            "\x0F\xBF\xAB\xAB"          # 47 movsx ecx, [ebp+var_4C]
            "\x88\x45\xAB"              # 51 mov [ebp+var_3A], al
            "\x8D\x45\xAB"              # 54 lea eax, [ebp+var_4C]
            "\x50"                      # 57 push eax
            "\x51"                      # 58 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            "\x8B\xC8"                  # 64 mov ecx, eax
            "\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2010-08-10
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x45\xAB"          # 2  mov [ebp+var_33], ax
            "\x66\x89\x4D\xAB"          # 6  mov [ebp+var_31], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            "\x8B\xC8"                  # 15 mov ecx, eax
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_64], 1FBh
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call _strncpy
            "\x0F\xBF\x55\xAB"          # 11 movsx edx, [ebp+var_64]
            "\x83\xC4\xAB"              # 15 add esp, 0Ch
            "\x8D\x4D\xAB"              # 18 lea ecx, [ebp+var_64]
            "\xC6\x45\xAB\xAB"          # 21 mov [ebp+var_2D], 0
            "\x51"                      # 25 push ecx
            "\x52"                      # 26 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            "\x8B\xC8"                  # 32 mov ecx, eax
            "\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": (4, 2),
            "retOffset": 0,
            "goto": (27, 1)
        },
        {
            "instanceR": 28,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+cp], 436h
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 6  call ds:timeGetTime
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 12 mov ecx, dword_83AFD0
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 18 mov edx, dword_83B69C
            "\x89\x45\xAB"              # 24 mov dword ptr [ebp+a+8], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, dword_83B6A0
            "\x89\x4D\xAB"              # 32 mov dword ptr [ebp+a+4], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_839C18
            "\x89\x55\xAB"              # 40 mov [ebp-1Eh], edx
            "\x89\x45\xAB"              # 43 mov [ebp-1Ah], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 46 call sub_6E64A0
            "\x88\x45\xAB"              # 51 mov byte ptr [ebp+var_10+2], al
            "\x8D\x55\xAB"              # 54 lea edx, [ebp+cp]
            "\x0F\xBF\x45\xAB"          # 57 movsx eax, [ebp+cp]
            "\x52"                      # 61 push edx
            "\x50"                      # 62 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 63 call CRagConnection_instanceR
            "\x8B\xC8"                  # 68 mov ecx, eax
            "\xE8"                      # 70 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 70,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 64,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 841h
            "\x88\x45\xAB"              # 5  mov byte ptr [ebp+var_2C+3], al
            "\xE8\xAB\xAB\xAB\xAB"      # 8  call CRagConnection_instanceR
            "\x8B\xC8"                  # 13 mov ecx, eax
            "\xE8"                      # 15 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 15,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 9,
        }
    ],
    # 2010-08-10
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_30], 1DDh
            "\x89\x55\xAB"              # 6  mov [ebp+var_2E], edx
            "\xF3\xA5"                  # 9  rep movsd
            "\x8D\x45\xAB"              # 11 lea eax, [ebp+var_12]
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 14 lea ecx, [ebp+var_94]
            "\x50"                      # 20 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_41AD80
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_616BF0
            "\x0F\xBF\x55\xAB"          # 31 movsx edx, [ebp+var_30]
            "\x8D\x4D\xAB"              # 35 lea ecx, [ebp+var_30]
            "\x88\x45\xAB"              # 38 mov [ebp+var_2], al
            "\x51"                      # 41 push ecx
            "\x52"                      # 42 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            "\x8B\xC8"                  # 48 mov ecx, eax
            "\xE8"                      # 50 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 50,
            "packetId": (4, 2),
            "retOffset": 0,
            "goto": (43, 4)
        },
        {
            "instanceR": 44,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 232h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_14], 232h
            "\x89\x75\xAB"              # 11 mov [ebp+var_12], esi
            "\x88\x55\xAB"              # 14 mov [ebp+var_D], dl
            "\x88\x4D\xAB"              # 17 mov [ebp+var_C], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2010-08-10
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 233h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_C], 233h
            "\x89\x5D\xAB"              # 11 mov [ebp+var_A], ebx
            "\x89\x45\xAB"              # 14 mov [ebp+var_6], eax
            "\xC6\x45\xAB\xAB"          # 17 mov [ebp+var_2], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A25h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_8], ax
            "\x89\x4D\xAB"              # 11 mov [ebp+var_6], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 974h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_4], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A13h
            "\x66\x89\x4D\xAB"          # 5  mov [ebp+var_2C], cx
            "\x8B\x8E\xAB\xAB\xAB\xAB"  # 9  mov ecx, [esi+9Ch]
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call sub_49D900
            "\x50"                      # 20 push eax
            "\x8D\x55\xAB"              # 21 lea edx, [ebp+var_2A]
            "\x6A\xAB"                  # 24 push 18h
            "\x52"                      # 26 push edx
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 27 call strcpy_s
            "\x0F\xBF\x4D\xAB"          # 33 movsx ecx, [ebp+var_2C]
            "\x83\xC4\xAB"              # 37 add esp, 0Ch
            "\x8D\x45\xAB"              # 40 lea eax, [ebp+var_2C]
            "\x50"                      # 43 push eax
            "\x51"                      # 44 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            "\x8B\xC8"                  # 50 mov ecx, eax
            "\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9E9h
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\x66\x89\x55\xAB"          # 7  mov [ebp+var_44], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            "\x88\x45\xAB"              # 5  mov byte ptr [ebp+var_1C+2], al
            "\x89\x7D\xAB"              # 8  mov [ebp+var_1C+3], edi
            "\x89\x4D\xAB"              # 11 mov [ebp+var_18+3], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0A21h
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\x66\x89\x55\xAB"          # 7  mov word ptr [ebp+arg_8], dx
            "\x88\x45\xAB"              # 11 mov byte ptr [ebp+arg_8+2], al
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9A7h
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_114], dx
            "\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_112], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9A9h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_114], cx
            "\x89\x95\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_112], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_12C], cx
            "\x8B\x10"                  # 12 mov edx, [eax]
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_12C]
            "\x50"                      # 20 push eax
            "\x51"                      # 21 push ecx
            "\x89\x95\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_12A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 28 call CRagConnection_instanceR
            "\x8B\xC8"                  # 33 mov ecx, eax
            "\xE8"                      # 35 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 35,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 29,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 82Bh
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 5  lea eax, [ebp+var_124]
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 11 mov [ebp+var_12C], cx
            "\x50"                      # 18 push eax
            "\x8D\x8E\xAB\xAB\xAB\x00"  # 19 lea ecx, [esi+110h]
            "\x89\x95\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_124], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 31 call sub_495490
            "\x8B\x08"                  # 36 mov ecx, [eax]
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 38 movsx eax, [ebp+var_12C]
            "\x8D\x95\xAB\xAB\xAB\xAB"  # 45 lea edx, [ebp+var_12C]
            "\x52"                      # 51 push edx
            "\x50"                      # 52 push eax
            "\x89\x8D\xAB\xAB\xAB\xAB"  # 53 mov [ebp+var_12A], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            "\x8B\xC8"                  # 64 mov ecx, eax
            "\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], dx
            "\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var1+2], di
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_126], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            "\x8B\xC8"                  # 31 mov ecx, eax
            "\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 844h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x4D\xAB"          # 7  mov word ptr [ebp+arg_8], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 998h
            "\x66\x89\x55\xAB"          # 5  mov [ebp+var_32], dx
            "\x89\x45\xAB"              # 9  mov [ebp+var_30], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 84Ah
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9C3h
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], cx
            "\x8D\x4F\xAB"              # 12 lea ecx, [edi+8]
            "\x89\x95\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_1440+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_4830E0
            "\x0F\xBF\x8D\xAB\xAB\xAB\xAB"  # 26 movsx ecx, ptr [ebp+var1]
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 33 mov ptr [ebp+var_143C+2], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebp+var_1440]
            "\x50"                      # 46 push eax
            "\x51"                      # 47 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 48 call CRagConnection_instanceR
            "\x8B\xC8"                  # 53 mov ecx, eax
            "\xE8"                      # 55 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 55,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 49,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 28Ch
            "\x89\x7D\xAB"              # 5  mov [ebp+var_52], edi
            "\x66\x89\x55\xAB"          # 8  mov [ebp+var_58], dx
            "\x89\x45\xAB"              # 12 mov [ebp+var_56], eax
            "\xB9\xAB\xAB\xAB\xAB"      # 15 mov ecx, 9
            "\x8D\x75\xAB"              # 20 lea esi, [ebp+var_28]
            "\x8D\x7D\xAB"              # 23 lea edi, [ebp+var_4E]
            "\xF3\xA5"                  # 26 rep movsd
            "\x8D\x4D\xAB"              # 28 lea ecx, [ebp+var_58]
            "\x51"                      # 31 push ecx
            "\x52"                      # 32 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            "\x8B\xC8"                  # 38 mov ecx, eax
            "\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9ABh
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x4D\xAB"          # 7  mov [ebp+var_C], cx
            "\x89\x55\xAB"              # 11 mov [ebp+var_A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9E8h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x4D\xAB"          # 7  mov [ebp+var_10], cx
            "\xC6\x45\xAB\xAB"          # 11 mov [ebp+var_E], 0
            "\x89\x75\xAB"              # 15 mov [ebp-0Dh], esi
            "\x89\x75\xAB"              # 18 mov [ebp+var_A+1], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8FCh
            "\xA3\xAB\xAB\xAB\xAB"      # 5  mov dword_D47024, eax
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            "\x8B\xC8"                  # 15 mov ecx, eax
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 838h
            "\x66\x89\x4D\xAB"          # 5  mov [ebp+var_1C], cx
            "\x8D\x4E\xAB"              # 9  lea ecx, [esi+60h]
            "\x89\x55\xAB"              # 12 mov [ebp+var_1A], edx
            "\x89\x45\xAB"              # 15 mov [ebp+var_16], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call sub_4830E0
            "\x0F\xBF\x55\xAB"          # 23 movsx edx, [ebp+var_1C]
            "\x8D\x4D\xAB"              # 27 lea ecx, [ebp+var_1C]
            "\x51"                      # 30 push ecx
            "\x52"                      # 31 push edx
            "\x66\x89\x45\xAB"          # 32 mov [ebp+var_12], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            "\x8B\xC8"                  # 41 mov ecx, eax
            "\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 364h
            "\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_19E], ecx
            "\x89\x95\xAB\xAB\xAB\xAB"  # 11 mov [ebp+var_19A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 83Bh
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            "\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_90+2], eax
            "\x88\x8D\xAB\xAB\xAB\xAB"  # 11 mov [ebp-8Ah], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 907h
            "\x66\x89\x55\xAB"          # 5  mov [ebp+var_8], dx
            "\x8B\x55\xAB"              # 9  mov edx, [ebp+arg_4]
            "\x66\x89\x45\xAB"          # 12 mov [ebp+var_6], ax
            "\x8B\x45\xAB"              # 16 mov eax, [ebp+arg_0]
            "\x52"                      # 19 push edx
            "\x50"                      # 20 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_4AD210
            "\x83\xF8\xAB"              # 26 cmp eax, 3
            "\x0F\xBF\x45\xAB"          # 29 movsx eax, [ebp+var_8]
            "\x8D\x55\xAB"              # 33 lea edx, [ebp+var_8]
            "\x52"                      # 36 push edx
            "\x0F\x95\xAB"              # 37 setnz cl
            "\x50"                      # 40 push eax
            "\x88\x4D\xAB"              # 41 mov [ebp+var_4], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            "\x8B\xC8"                  # 49 mov ecx, eax
            "\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            "\x89\x4D\xAB"              # 5  mov [ebp+var_A], ecx
            "\x88\x55\xAB"              # 8  mov [ebp+var_6], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A35h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1], ax
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 14 mov word ptr [ebp+var1+2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A01h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x4D\xAB"          # 7  mov word ptr [ebp+arg_0], cx
            "\x88\x55\xAB"              # 11 mov byte ptr [ebp+arg_0+2], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8D7h
            "\x6A\xAB"                  # 5  push 18h
            "\x50"                      # 7  push eax
            "\x66\x89\x4D\xAB"          # 8  mov [ebp+var_20], cx
            "\x66\x89\x55\xAB"          # 12 mov [ebp+var_1E], dx
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 16 call strncpy_s
            "\x0F\xBF\x55\xAB"          # 22 movsx edx, [ebp+var_20]
            "\x83\xC4\xAB"              # 26 add esp, 10h
            "\x8D\x4D\xAB"              # 29 lea ecx, [ebp+var_20]
            "\x51"                      # 32 push ecx
            "\x52"                      # 33 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 34 call CRagConnection_instanceR
            "\x8B\xC8"                  # 39 mov ecx, eax
            "\xE8"                      # 41 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 41,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 35,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90Ah
            "\x6A\xAB"                  # 5  push 18h
            "\x52"                      # 7  push edx
            "\x66\x89\x45\xAB"          # 8  mov [ebp+var_20], ax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call strcpy_s
            "\x0F\xBF\x4D\xAB"          # 18 movsx ecx, [ebp+var_20]
            "\x83\xC4\xAB"              # 22 add esp, 0Ch
            "\x8D\x45\xAB"              # 25 lea eax, [ebp+var_20]
            "\x50"                      # 28 push eax
            "\x51"                      # 29 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 30 call CRagConnection_instanceR
            "\x8B\xC8"                  # 35 mov ecx, eax
            "\xE8"                      # 37 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 37,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 31,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            "\x6A\xAB"                  # 5  push 18h
            "\x52"                      # 7  push edx
            "\x66\x89\x4D\xAB"          # 8  mov [ebp+var_20], cx
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 12 call strcpy_s
            "\x0F\xBF\x55\xAB"          # 18 movsx edx, [ebp+var_20]
            "\x8A\x45\xAB"              # 22 mov al, [ebp+arg_4]
            "\x83\xC4\xAB"              # 25 add esp, 0Ch
            "\x8D\x4D\xAB"              # 28 lea ecx, [ebp+var_20]
            "\x51"                      # 31 push ecx
            "\x52"                      # 32 push edx
            "\x88\x45\xAB"              # 33 mov [ebp+var_1E], al
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            "\x8B\xC8"                  # 41 mov ecx, eax
            "\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            "\x6A\xAB"                  # 5  push 18h
            "\x52"                      # 7  push edx
            "\x66\x89\x45\xAB"          # 8  mov [ebp+var_38], ax
            "\xFF\xD7"                  # 12 call edi
            "\x8D\x46\xAB"              # 14 lea eax, [esi+1Ch]
            "\x50"                      # 17 push eax
            "\x8D\x4D\xAB"              # 18 lea ecx, [ebp+var_1D]
            "\x6A\xAB"                  # 21 push 18h
            "\x51"                      # 23 push ecx
            "\xFF\xD7"                  # 24 call edi
            "\x8B\x45\xAB"              # 26 mov eax, [ebp+arg_0]
            "\x83\xC4\xAB"              # 29 add esp, 18h
            "\x88\x45\xAB"              # 32 mov [ebp+var_36], al
            "\x83\xF8\xAB"              # 35 cmp eax, 2
            "\x75\x04"                  # 38 jnz short loc_6E4FC0
            "\xC6\x46\xAB\xAB"          # 40 mov byte ptr [esi+38h], 0
            "\x0F\xBF\xAB\xAB"          # 44 movsx eax, [ebp+var_38]
            "\x8D\x55\xAB"              # 48 lea edx, [ebp+var_38]
            "\x52"                      # 51 push edx
            "\x50"                      # 52 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call CRagConnection_instanceR
            "\x8B\xC8"                  # 58 mov ecx, eax
            "\xE8"                      # 60 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 60,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 54,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 2F1h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_D8], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_C], ax
            "\x89\x4D\xAB"              # 11 mov [ebp+var_A], ecx
            "\xC6\x45\xAB\xAB"          # 14 mov [ebp+var_6], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 35Fh
            "\x89\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+318h], eax
            "\x66\x89\x55\xAB"          # 11 mov [ebp+var_8], dx
            "\xFF\xD6"                  # 15 call esi
            "\x0F\xBF\x4D\xAB"          # 17 movsx ecx, [ebp+var_8]
            "\x89\x45\xAB"              # 21 mov [ebp+var_6], eax
            "\x8D\x45\xAB"              # 24 lea eax, [ebp+var_8]
            "\x50"                      # 27 push eax
            "\x51"                      # 28 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            "\x8B\xC8"                  # 34 mov ecx, eax
            "\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0D0h
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [esi+X], 1
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 17 mov word ptr [ebp+var1], dx
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 24 mov byte ptr [ebp+var1+2], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 31 call CRagConnection_instanceR
            "\x8B\xC8"                  # 36 mov ecx, eax
            "\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 32,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 368h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_10], ax
            "\x89\x5D\xAB"              # 11 mov [ebp+var_E], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 149h
            "\x89\x55\xAB"              # 5  mov [ebp+var_E], edx
            "\xC6\x45\xF6\xAB"          # 8  mov [ebp+var_A], 2
            "\x66\x89\x45\xAB"          # 12 mov [ebp+var_9], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 998h
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1+2], cx
            "\x89\x95\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_1068], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 362h
            "\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebx+384h], 1
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var1+2], ax
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_1064], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            "\x8B\xC8"                  # 34 mov ecx, eax
            "\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 439h
            "\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_1068], eax
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 11 mov word ptr [ebp+var1+2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0E8h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov ptr [ebp+var1+2], ax
            "\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+var_14E0]
            "\x51"                      # 18 push ecx
            "\x52"                      # 19 push edx
            "\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 20 mov [ebx+108h], 1
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 30 mov [ebp+var_106C], dx
            "\x89\x85\xAB\xAB\xAB\xAB"  # 37 mov ptr [ebp+var_106C+4], eax
            "\x89\x83\xAB\xAB\xAB\xAB"  # 43 mov [ebx+118h], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            "\x8B\xC8"                  # 54 mov ecx, eax
            "\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 802h
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], dx
            "\x8D\x50\xAB"              # 12 lea edx, [eax+1]
            "\x8A\x08"                  # 15 mov cl, [eax]
            "\x40"                      # 17 inc eax
            "\x84\xC9"                  # 18 test cl, cl
            "\x75\xF9"                  # 20 jnz short loc_849027
            "\x2B\xC2"                  # 22 sub eax, edx
            "\x83\xF8\xAB"              # 24 cmp eax, 18h
            "\x0F\x83\xAB\xAB\xAB\xAB"  # 27 jnb loc_848EEE
            "\x8B\xB5\xAB\xAB\xAB\xAB"  # 33 mov esi, [ebp+Str1]
            "\x6A\xAB"                  # 39 push 18h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 41 lea eax, [ebp+var1+2]
            "\x56"                      # 47 push esi
            "\x50"                      # 48 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 49 call strncpy
            "\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 55 movsx edx, word ptr [ebp+var1]
            "\x83\xC4\xAB"              # 62 add esp, 0Ch
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 65 lea ecx, [ebp+var1]
            "\x51"                      # 71 push ecx
            "\x52"                      # 72 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            "\x8B\xC8"                  # 78 mov ecx, eax
            "\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 103h
            "\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_10A8+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1F9h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1+4], ax
            "\x89\x8D\xAB\xAB\xAB\xAB"  # 14 mov dword ptr [ebp+var1+6], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 9B4h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x89\x95\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [ebp+var1+6], edx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 13 mov word ptr [ebp+var1+4], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
            # many goto/jmp
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 22Dh
            "\x89\x43\xAB"              # 5  mov [ebx+1Ch], eax
            "\x66\x89\x55\xAB"          # 8  mov [ebp+var_54], dx
            "\xFF\xD7"                  # 12 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 14 mov ecx, dword_D43034
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 20 mov edx, dword_D4285C
            "\x89\x45\xAB"              # 26 mov [ebp+var_46], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 29 mov eax, dword_D43030
            "\x89\x4D\xAB"              # 34 mov [ebp+var_4E], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 37 mov ecx, offset dword_D41C90
            "\x89\x45\xAB"              # 42 mov [ebp+var_52], eax
            "\x89\x55\xAB"              # 45 mov [ebp+var_4A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 48 call sub_9028A0
            "\x0F\xBF\x4D\xAB"          # 53 movsx ecx, [ebp+var_54]
            "\x88\x45\xAB"              # 57 mov [ebp+var_42], al
            "\x8D\x45\xAB"              # 60 lea eax, [ebp+var_54]
            "\x50"                      # 63 push eax
            "\x51"                      # 64 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            "\x8B\xC8"                  # 70 mov ecx, eax
            "\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 9EFh
            "\x88\x55\xAB"              # 5  mov [ebp+var_E], dl
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 8  mov [ebp+var_D], 0
            "\xC7\x45\xF7\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_9], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1BFh
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_4], ax
            "\xC6\x45\xAB\xAB"          # 11 mov [ebp+var_2], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 1DDh
            "\x66\x89\x55\xAB"          # 5  mov [ebp+var_34], dx
            "\x8B\x96\xAB\xAB\xAB\xAB"  # 9  mov edx, [esi+180h]
            "\x89\x45\xAB"              # 15 mov [ebp+var_32], eax
            "\x8B\x86\xAB\xAB\xAB\xAB"  # 18 mov eax, [esi+184h]
            "\x89\x4D\xAB"              # 24 mov [ebp+var_22], ecx
            "\x8D\x4D\xAB"              # 27 lea ecx, [ebp+var_16]
            "\x89\x55\xAB"              # 30 mov [ebp+var_2A], edx
            "\x8B\x96\xAB\xAB\xAB\xAB"  # 33 mov edx, [esi+18Ch]
            "\x89\x45\xAB"              # 39 mov [ebp+var_26], eax
            "\x8B\x86\xAB\xAB\xAB\xAB"  # 42 mov eax, [esi+190h]
            "\x51"                      # 48 push ecx
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 49 lea ecx, [ebp+var_98]
            "\x89\x55\xAB"              # 55 mov [ebp+var_1E], edx
            "\x89\x45\xAB"              # 58 mov [ebp+var_1A], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 61 call sub_7C0970
            "\xE8\xAB\xAB\xAB\xAB"      # 66 call sub_8AC310
            "\x88\x45\xAB"              # 71 mov [ebp+var_6], al
            "\x0F\xBF\x45\xAB"          # 74 movsx eax, [ebp+var_34]
            "\x8D\x55\xAB"              # 78 lea edx, [ebp+var_34]
            "\x52"                      # 81 push edx
            "\x50"                      # 82 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 83 call CRagConnection_instanceR
            "\x8B\xC8"                  # 88 mov ecx, eax
            "\xE8"                      # 90 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 90,
            "packetId": 1,
            "retOffset": 0,
            "goto": (83, 4)
        },
        {
            "instanceR": 84,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 65h
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+cp], dx
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 12 mov edx, dword_D4285C
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 18 mov word ptr [ebp+var_28A], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 25 mov eax, dword_D42860
            "\x51"                      # 30 push ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_D41C90
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 36 mov A, 1
            "\x89\x95\xAB\xAB\xAB\xAB"  # 46 mov [ebp+var_292], edx
            "\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov [ebp+var_28E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 58 call sub_902880
            "\xB9\xAB\xAB\xAB\xAB"      # 63 mov ecx, offset dword_D41C90
            "\xE8\xAB\xAB\xAB\xAB"      # 68 call sub_9028A0
            "\x88\x85\xAB\xAB\xAB\xAB"  # 73 mov [ebp+var_28A+2], al
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 79 movsx eax, word ptr [ebp+cp]
            "\x8D\x95\xAB\xAB\xAB\xAB"  # 86 lea edx, [ebp+cp]
            "\x52"                      # 92 push edx
            "\x50"                      # 93 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 94 call CRagConnection_instanceR
            "\x8B\xC8"                  # 99 mov ecx, eax
            "\xE8"                      # 101 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 101,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 95,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 970h
            "\x66\x89\x4D\xAB"          # 5  mov [ebp-13h], cx
            "\x88\x55\xAB"              # 9  mov [ebp-16h], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 1FBh
            "\x56"                      # 5  push esi
            "\x50"                      # 6  push eax
            "\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_2D2], ecx
            "\x66\x89\x95\xAB\xAB\xAB\xAB"  # 13 mov [ebp+var_2D4], dx
            "\xFF\xD3"                  # 20 call ebx
            "\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 22 movsx edx, [ebp+var_2D4]
            "\x83\xC4\xAB"              # 29 add esp, 0Ch
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 32 lea ecx, [ebp+var_2D4]
            "\x51"                      # 38 push ecx
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 39 mov byte ptr [ebp+a+6], 0
            "\x52"                      # 46 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            "\x8B\xC8"                  # 52 mov ecx, eax
            "\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
            "goto": (47, 1)
        },
        {
            "instanceR": 48,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 66h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+Data], cx
            "\x88\x95\xAB\xAB\xAB\xAB"  # 14 mov [ebp+Data+2], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 22Dh
            "\x83\xC4\xAB"              # 5  add esp, 4
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 8  mov word ptr [ebp+cp], ax
            "\xFF\xD7"                  # 15 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 17 mov ecx, dword_D43030
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 23 mov edx, dword_D43034
            "\x89\x85\xAB\xAB\xAB\xAB"  # 29 mov dword ptr [ebp+var_28A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 35 mov eax, dword_D4285C
            "\x89\x8D\xAB\xAB\xAB\xAB"  # 40 mov [ebp+var_296], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 46 mov ecx, offset dword_D41C90
            "\x89\x95\xAB\xAB\xAB\xAB"  # 51 mov [ebp+var_292], edx
            "\x89\x85\xAB\xAB\xAB\xAB"  # 57 mov [ebp+var_28E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 63 call sub_9028A0
            "\x0F\xBF\x95\xAB\xAB\xAB\xAB"  # 68 movsx edx, word ptr [ebp+cp]
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 75 lea ecx, [ebp+cp]
            "\x51"                      # 81 push ecx
            "\x52"                      # 82 push edx
            "\x88\x85\xAB\xAB\xAB\xAB"  # 83 mov [ebp+var_28A+4], al
            "\xE8\xAB\xAB\xAB\xAB"      # 89 call CRagConnection_instanceR
            "\x8B\xC8"                  # 94 mov ecx, eax
            "\xE8"                      # 96 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 96,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 90,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 841h
            "\x88\x8D\xAB\xAB\xAB\xAB"  # 5  mov byte ptr [ebp+A+3], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 234h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x4D\xAB"          # 7  mov [ebp+var_14], cx
            "\x89\x75\xAB"              # 11 mov [ebp+var_12], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2015-06-24
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 232h
            "\x89\x7D\xAB"              # 5  mov [ebp+var_E], edi
            "\x88\x55\xAB"              # 8  mov [ebp+var_A], dl
            "\x88\x45\xAB"              # 11 mov [ebp+var_9], al
            "\x88\x5D\xAB"              # 14 mov [ebp+var_8], bl
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2015-06-24
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 233h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x45\xAB"          # 7  mov word ptr [ebp+var_10], ax
            "\x89\x5D\xAB"              # 11 mov dword ptr [ebp+var_10+2], ebx
            "\x89\x4D\xAB"              # 14 mov dword ptr [ebp+var_10+6], ecx
            "\xC6\x45\xAB\xAB"          # 17 mov [ebp+var_6], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2015-06-24
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 21Dh
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\x66\x89\x55\xAB"          # 7  mov [ebp+var_14], dx
            "\x89\x45\xAB"              # 11 mov [ebp+var_12], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 974h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\xAB\xAB"      # 7  mov [esp+0Ch+var_4], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            "\x66\x89\x4C\xAB\xAB"      # 5  mov word ptr [esp+13Ch+Src], cx
            "\x8B\x10"                  # 10 mov edx, [eax]
            "\x8D\x44\xAB\xAB"          # 12 lea eax, [esp+13Ch+Src]
            "\x50"                      # 16 push eax
            "\x51"                      # 17 push ecx
            "\x89\x54\xAB\xAB"          # 18 mov [esp+144h+Src+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 82Bh
            "\x8D\x44\xAB\xAB"          # 5  lea eax, [esp+13Ch+var_124]
            "\x66\x89\x4C\x24\xAB"      # 9  mov word ptr [esp+13Ch+Src], cx
            "\x50"                      # 14 push eax
            "\x8D\x8E\xAB\xAB\xAB\xAB"  # 15 lea ecx, [esi+0F8h]
            "\x89\x54\x24\xAB"          # 21 mov [esp+140h+var_124], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call sub_489990
            "\x8B\x08"                  # 30 mov ecx, [eax]
            "\x0F\xBF\x44\x24\xAB"      # 32 movsx eax, word ptr [esp+13Ch+Src]
            "\x8D\x54\x24\xAB"          # 37 lea edx, [esp+13Ch+Src]
            "\x52"                      # 41 push edx
            "\x50"                      # 42 push eax
            "\x89\x4C\x24\xAB"          # 43 mov [esp+144h+Src+2], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            "\x8B\xC8"                  # 52 mov ecx, eax
            "\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8C5h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x4C\x24\xAB"      # 7  mov word ptr [esp+144h+Src], cx
            "\x89\x54\x24\xAB"          # 12 mov [esp+144h+Src+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            "\x66\x89\x54\x24\xAB"      # 5  mov word ptr [esp+144h+Src+2], dx
            "\x66\x89\x7C\x24\xAB"      # 10 mov [esp+144h+var_128], di
            "\x66\x89\x44\x24\xAB"      # 15 mov [esp+144h+var_126], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0A9h
            "\x66\x89\x4C\x24\xAB"      # 5  mov [esp+44h+var_32], cx
            "\x66\x89\x54\x24\xAB"      # 10 mov [esp+44h+var_30], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8C3h
            "\x89\x84\x24\xAB\xAB\xAB\x00"  # 5  mov [esp+3A8h+var_1E], eax
            "\x89\x8C\x24\xAB\xAB\xAB\x00"  # 12 mov [esp+3A8h+var_1A], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8B8h
            "\x89\x94\x24\xAB\xAB\xAB\x00"  # 5  mov [esp+3A8h+var_1E], edx
            "\x89\x84\x24\xAB\xAB\xAB\x00"  # 12 mov [esp+3A8h+var_1A], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 28Ch
            "\x89\x7C\x24\xAB"          # 5  mov [esp+60h+var_2A], edi
            "\x66\x89\x54\x24\xAB"      # 9  mov [esp+60h+Src], dx
            "\x89\x44\x24\xAB"          # 14 mov [esp+60h+var_2E], eax
            "\xB9\xAB\xAB\xAB\xAB"      # 18 mov ecx, 9
            "\x8D\x74\x24\xAB"          # 23 lea esi, [esp+60h+var_54]
            "\x8D\x7C\x24\xAB"          # 27 lea edi, [esp+60h+var_26]
            "\xF3\xA5"                  # 31 rep movsd
            "\x8D\x4C\x24\xAB"          # 33 lea ecx, [esp+60h+Src]
            "\x51"                      # 37 push ecx
            "\x52"                      # 38 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            "\x8B\xC8"                  # 44 mov ecx, eax
            "\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8F7h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x4C\xAB\xAB"      # 7  mov word ptr [esp+8+Src], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E5h
            "\x66\x89\x44\xAB\xAB"      # 5  mov [esp+68h+Src], ax
            "\xB9\xAB\xAB\xAB\x00"      # 10 mov ecx, 9
            "\x8D\x74\x24\xAB"          # 15 lea esi, [esp+68h+var_58]
            "\x8D\x7C\x24\xAB"          # 19 lea edi, [esp+68h+var_2E]
            "\xF3\xA5"                  # 23 rep movsd
            "\x83\xC4\xAB"              # 25 add esp, 8
            "\x66\xA5"                  # 28 movs movsw
            "\x8D\x4C\x24\xAB"          # 30 lea ecx, [esp+60h+Src]
            "\x51"                      # 34 push ecx
            "\x50"                      # 35 push eax
            "\xA4"                      # 36 movs movsb
            "\xE8\xAB\xAB\xAB\xAB"      # 37 call CRagConnection_instanceR
            "\x8B\xC8"                  # 42 mov ecx, eax
            "\xE8"                      # 44 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 44,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 38,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8EBh
            "\x66\x89\x54\x24\xAB"      # 5  mov [esp+48h+Src], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call sub_452AB0
            "\x50"                      # 15 push eax
            "\x8D\x44\x24\xAB"          # 16 lea eax, [esp+4Ch+Dest]
            "\x50"                      # 20 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 21 call ds:sprintf
            "\x0F\xBF\x54\x24\xAB"      # 27 movsx edx, [esp+50h+Src]
            "\x83\xC4\xAB"              # 32 add esp, 8
            "\x8D\x4C\x24\xAB"          # 35 lea ecx, [esp+48h+Src]
            "\x51"                      # 39 push ecx
            "\x52"                      # 40 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            "\x8B\xC8"                  # 46 mov ecx, eax
            "\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 91Eh
            "\x89\x4C\x24\xAB"          # 5  mov [esp+0A4h+var_96], ecx
            "\x8D\x4E\xAB"              # 9  lea ecx, [esi+60h]
            "\x66\x89\x44\x24\xAB"      # 12 mov [esp+0A4h+Src], ax
            "\x89\x54\x24\xAB"          # 17 mov [esp+0A4h+var_92], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_447360
            "\x0F\xBF\x4C\x24\xAB"      # 26 movsx ecx, [esp+0A4h+Src]
            "\x66\x89\x44\x24\xAB"      # 31 mov [esp+0A4h+var_8E], ax
            "\x8D\x44\x24\xAB"          # 36 lea eax, [esp+0A4h+Src]
            "\x50"                      # 40 push eax
            "\x51"                      # 41 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call CRagConnection_instanceR
            "\x8B\xC8"                  # 47 mov ecx, eax
            "\xE8"                      # 49 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 49,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 43,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 83Bh
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\x66\x89\x54\x24\xAB"      # 7  mov word ptr [esp+8C0h+Src], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8F0h
            "\x8D\x4C\x24\xAB"          # 5  lea ecx, [esp+9Ch+Src]
            "\x66\x89\x54\xAB\xAB"      # 9  mov [esp+9Ch+Src], dx
            "\x8B\x47\xAB"              # 14 mov eax, [edi+0Ch]
            "\x51"                      # 17 push ecx
            "\x52"                      # 18 push edx
            "\x89\x44\x24\xAB"          # 19 mov [esp+0A4h+var_82], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8F9h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\xAB\xAB"      # 7  mov word ptr [esp+210h+var1], ax
            "\x89\x4C\x24\xAB"          # 12 mov [esp+210h+var_1F4+2], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8E7h
            "\x66\x89\x4C\x24\xAB"      # 5  mov word ptr [esp+3A4h+Src], cx
            "\x8B\x8E\xAB\xAB\xAB\xAB"  # 10 mov ecx, [esi+88h]
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_452AB0
            "\x50"                      # 21 push eax
            "\xFF\xD7"                  # 22 call edi
            "\xBA\xAB\xAB\xAB\xAB"      # 24 mov edx, 96h
            "\x83\xC4\xAB"              # 29 add esp, 4
            "\x66\x89\x44\x24\xAB"      # 32 mov word ptr [esp+3A4h+Src+2], ax
            "\x66\x3B\xC2"              # 37 cmp ax, dx
            "\x0F\x87\xAB\xAB\xAB\xAB"  # 40 ja loc_4FD40E
            "\xB9\xAB\xAB\xAB\xAB"      # 46 mov ecx, offset dword_9ADEB0
            "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB"  # 51 mov [esp+3A4h+var_38C], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call sub_5CDA80
            "\x0F\xBF\x54\x24\xAB"      # 64 movsx edx, word ptr [esp+3A4h+Src]
            "\x8D\x4C\x24\xAB"          # 69 lea ecx, [esp+3A4h+Src]
            "\x51"                      # 73 push ecx
            "\xB8\xAB\xAB\xAB\x00"      # 74 mov eax, 0Bh
            "\x52"                      # 79 push edx
            "\x66\x89\x44\x24\xAB"      # 80 mov [esp+3ACh+var_388], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 85 call CRagConnection_instanceR
            "\x8B\xC8"                  # 90 mov ecx, eax
            "\xE8"                      # 92 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 92,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 86,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            "\x89\x4C\x24\xAB"          # 5  mov [esp+1Eh], ecx
            "\x88\x54\x24\xAB"          # 9  mov ptr [esp+0ACh+var1+2], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            "\x8B\xC8"                  # 18 mov ecx, eax
            "\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 907h
            "\x66\x89\x14\xAB"          # 5  mov [esp+8+var_8], dx
            "\x8B\x54\x24\xAB"          # 9  mov edx, [esp+8+arg_4]
            "\x66\x89\x44\x24\xAB"      # 13 mov [esp+8+var_6], ax
            "\x8B\x44\x24\xAB"          # 18 mov eax, [esp+8+arg_0]
            "\x52"                      # 22 push edx
            "\x50"                      # 23 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 24 call sub_465130
            "\x83\xF8\xAB"              # 29 cmp eax, 3
            "\x0F\xBF\xAB\xAB"          # 32 movsx eax, [esp+8+var_8]
            "\x8D\x14\xAB"              # 36 lea edx, [esp+8+var_8]
            "\x52"                      # 39 push edx
            "\x0F\x95\xC1"              # 40 setnz cl
            "\x50"                      # 43 push eax
            "\x88\x4C\x24\xAB"          # 44 mov [esp+10h+var_4], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 48 call CRagConnection_instanceR
            "\x8B\xC8"                  # 53 mov ecx, eax
            "\xE8"                      # 55 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 55,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 49,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 94Dh
            "\x89\x54\x24\xAB"          # 5  mov [esp+28h+var_E], edx
            "\x89\x44\x24\xAB"          # 9  mov [esp+28h+var_A], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            "\x8B\xC8"                  # 18 mov ecx, eax
            "\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            "\x88\x54\x24\xAB"          # 5  mov [esp+0F8h+var_DA], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            "\x8B\xC8"                  # 14 mov ecx, eax
            "\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8D7h
            "\x6A\xAB"                  # 5  push 18h
            "\x50"                      # 7  push eax
            "\x66\x89\x4C\x24\xAB"      # 8  mov [esp+2Ch+var_20], cx
            "\x66\x89\x54\x24\xAB"      # 13 mov [esp+2Ch+var_1E], dx
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 18 call ds:strcpy_s
            "\x0F\xBF\x54\x24\xAB"      # 24 movsx edx, [esp+2Ch+var_20]
            "\x83\xC4\xAB"              # 29 add esp, 0Ch
            "\x8D\x0C\xAB"              # 32 lea ecx, [esp+20h+var_20]
            "\x51"                      # 35 push ecx
            "\x52"                      # 36 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 37 call CRagConnection_instanceR
            "\x8B\xC8"                  # 42 mov ecx, eax
            "\xE8"                      # 44 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 44,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 38,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90Ah
            "\x6A\xAB"                  # 5  push 18h
            "\x52"                      # 7  push edx
            "\x66\x89\x44\x24\xAB"      # 8  mov [esp+2Ch+var_20], ax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 13 call ds:strcpy_s
            "\x0F\xBF\x4C\x24\xAB"      # 19 movsx ecx, [esp+2Ch+var_20]
            "\x83\xC4\xAB"              # 24 add esp, 0Ch
            "\x8D\x04\xAB"              # 27 lea eax, [esp+20h+var_20]
            "\x50"                      # 30 push eax
            "\x51"                      # 31 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 32 call CRagConnection_instanceR
            "\x8B\xC8"                  # 37 mov ecx, eax
            "\xE8"                      # 39 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 39,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 33,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            "\x6A\xAB"                  # 5  push 18h
            "\x52"                      # 7  push edx
            "\x66\x89\x4C\x24\xAB"      # 8  mov [esp+2Ch+var_20], cx
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 13 call ds:strcpy_s
            "\x0F\xBF\x54\x24\xAB"      # 19 movsx edx, [esp+2Ch+var_20]
            "\x8A\x44\x24\xAB"          # 24 mov al, [esp+2Ch+arg_4]
            "\x83\xC4\xAB"              # 28 add esp, 0Ch
            "\x8D\x0C\xAB"              # 31 lea ecx, [esp+20h+var_20]
            "\x51"                      # 34 push ecx
            "\x52"                      # 35 push edx
            "\x88\x44\x24\xAB"          # 36 mov [esp+28h+var_1E], al
            "\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            "\x8B\xC8"                  # 45 mov ecx, eax
            "\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            "\x6A\xAB"                  # 5  push 18h
            "\x52"                      # 7  push edx
            "\x66\x89\x44\x24\xAB"      # 8  mov [esp+4Ch+Src], ax
            "\xFF\xD7"                  # 13 call edi
            "\x8D\x46\xAB"              # 15 lea eax, [esi+1Ch]
            "\x50"                      # 18 push eax
            "\x8D\x4C\x24\xAB"          # 19 lea ecx, [esp+50h+var_1D]
            "\x6A\xAB"                  # 23 push 18h
            "\x51"                      # 25 push ecx
            "\xFF\xD7"                  # 26 call edi
            "\x8B\x44\x24\xAB"          # 28 mov eax, [esp+58h+arg_0]
            "\x83\xC4\xAB"              # 32 add esp, 18h
            "\x88\x44\x24\xAB"          # 35 mov [esp+40h+var_36], al
            "\x83\xF8\xAB"              # 39 cmp eax, 2
            "\x75\x04"                  # 42 jnz short loc_5FF413
            "\xC6\x46\xAB\xAB"          # 44 mov byte ptr [esi+38h], 0
            "\x0F\xBF\x44\x24\xAB"      # 48 movsx eax, [esp+40h+Src]
            "\x8D\x54\x24\xAB"          # 53 lea edx, [esp+40h+Src]
            "\x52"                      # 57 push edx
            "\x50"                      # 58 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            "\x8B\xC8"                  # 64 mov ecx, eax
            "\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\x24\xAB"      # 7  mov [esp+3Ch+Src], ax
            "\x89\x4C\x24\xAB"          # 12 mov [esp+3Ch+var_1E], ecx
            "\xC6\x44\x24\xAB\xAB"      # 16 mov [esp+3Ch+var_1A], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0D0h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [esi+100h], 1
            "\x66\x89\x44\x24\xAB"      # 17 mov word ptr [esp+6D0h+Src], ax
            "\xC6\x44\x24\xAB\xAB"      # 22 mov byte ptr [esp+6D0h+Src+2], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            "\x8B\xC8"                  # 32 mov ecx, eax
            "\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 0D0h
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\xC7\x86\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [esi+100h], 1
            "\x66\x89\x54\x24\xAB"      # 17 mov word ptr [esp+6D0h+Src], dx
            "\xC6\x44\x24\xAB\xAB"      # 22 mov byte ptr [esp+6D0h+Src+2], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            "\x8B\xC8"                  # 32 mov ecx, eax
            "\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 87Fh
            "\x89\x87\xAB\xAB\xAB\xAB"  # 5  mov [edi+2E8h], eax
            "\x66\x89\x54\x24\xAB"      # 11 mov [esp+10h+Src], dx
            "\xFF\xD6"                  # 16 call esi
            "\x0F\xBF\x4C\x24\xAB"      # 18 movsx ecx, [esp+10h+Src]
            "\x89\x44\x24\xAB"          # 23 mov [esp+10h+var_6], eax
            "\x8D\x44\x24\xAB"          # 27 lea eax, [esp+10h+Src]
            "\x50"                      # 31 push eax
            "\x51"                      # 32 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            "\x8B\xC8"                  # 38 mov ecx, eax
            "\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 891h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\xAB\xAB"      # 7  mov [esp+28h+Src], ax
            "\x89\x5C\x24\xAB"          # 12 mov [esp+28h+var_6], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 149h
            "\x89\x54\x24\xAB"          # 5  mov [esp+14h+var_A], edx
            "\xC6\x44\x24\xAB\xAB"      # 9  mov [esp+14h+var_6], 2
            "\x66\x89\x44\xAB\xAB"      # 14 mov [esp+14h+var_5], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0ABh
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x4C\xAB\xAB"      # 7  mov ptr [esp+14D4h+var1], cx
            "\x66\x89\x54\xAB\xAB"      # 12 mov ptr [esp+14D4h+var1+2], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A9h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], ax
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], bx
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 23 mov [esp+14D4h+var2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 31 call CRagConnection_instanceR
            "\x8B\xC8"                  # 36 mov ecx, eax
            "\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 32,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 956h
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [ebp+354h], 1
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 17 mov [esp+14D4h+var1], dx
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 25 mov [esp+14D4h+var1+2], bx
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 33 mov [esp+14D4h+var2], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            "\x8B\xC8"                  # 46 mov ecx, eax
            "\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 439h
            "\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+350h], 1
            "\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, dword_9AB40C
            "\x51"                      # 20 push ecx
            "\x52"                      # 21 push edx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 22 mov [esp+14D4h+var1], dx
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 30 mov [esp+14D4h+var_1014], eax
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 37 mov [esp+14D4h+var1+2], bx
            "\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            "\x8B\xC8"                  # 50 mov ecx, eax
            "\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0E4h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], ax
            "\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0E8h
            "\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+108h], 1
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], bx
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 23 mov [esp+14D4h+var2], eax
            "\x89\x85\xAB\xAB\xAB\xAB"  # 30 mov [ebp+118h], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            "\x8B\xC8"                  # 41 mov ecx, eax
            "\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 897h
            "\x8B\xC3"                  # 5  mov eax, ebx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14CCh+var1], dx
            "\x8D\x50\xAB"              # 15 lea edx, [eax+1]
            "\xEB\x03"                  # 18 jmp short loc_742E30
            "\xAB\xAB\xAB"              # 20 align 10h
            "\x8A\x08"                  # 23 mov cl, [eax]
            "\x40"                      # 25 inc eax
            "\x84\xC9"                  # 26 test cl, cl
            "\x75\xF9"                  # 28 jnz short loc_742E30
            "\x2B\xC2"                  # 30 sub eax, edx
            "\x83\xF8\xAB"              # 32 cmp eax, 18h
            "\x0F\x83\xAB\xAB\xAB\xAB"  # 35 jnb loc_742D1E
            "\x6A\xAB"                  # 41 push 18h
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 43 lea eax, [esp+14D0h+var1+2]
            "\x53"                      # 50 push ebx
            "\x50"                      # 51 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 52 call ds:strncpy
            "\x0F\xBF\x94\x24\xAB\xAB\xAB\xAB"  # 58 movsx edx, [esp+14D8+var1]
            "\x83\xC4\xAB"              # 66 add esp, 0Ch
            "\x8D\x8C\x24\xAB\xAB\xAB\xAB"  # 69 lea ecx, [esp+14CCh+var1]
            "\x51"                      # 76 push ecx
            "\x52"                      # 77 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 78 call CRagConnection_instanceR
            "\x8B\xC8"                  # 83 mov ecx, eax
            "\xE8"                      # 85 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 85,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 79,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 103h
            "\x89\x94\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+14D4h+var1+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1E5h
            "\x50"                      # 5  push eax
            "\x51"                      # 6  push ecx
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], cx
            "\x89\x94\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1F9h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], ax
            "\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8B5h
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+14D4h+var1], dx
            "\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 15 mov [esp+14D4h+var1+2], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
            # many goto
        },
        {
            "instanceR": 23,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 869h
            "\x89\x47\xAB"              # 5  mov [edi+1Ch], eax
            "\x66\x89\x4C\x24\xAB"      # 8  mov [esp+68h+Src], cx
            "\xFF\xD6"                  # 13 call esi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, dword_9AAC44
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, dword ptr qword_9AB40C
            "\x89\x44\x24\xAB"          # 27 mov [esp+68h+var_46], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword ptr qword_9AB40C+4
            "\x89\x4C\x24\xAB"          # 36 mov [esp+68h+var_4A], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_9A93E8
            "\x89\x54\x24\xAB"          # 45 mov ptr [esp+68h+var1], edx
            "\x89\x44\x24\xAB"          # 49 mov ptr [esp+68h+var1+4], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_7D2640
            "\x88\x44\x24\xAB"          # 58 mov [esp+68h+var_42], al
            "\x0F\xBF\x44\x24\xAB"      # 62 movsx eax, [esp+68h+Src]
            "\x8D\x54\x24\xAB"          # 67 lea edx, [esp+68h+Src]
            "\x52"                      # 71 push edx
            "\x50"                      # 72 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            "\x8B\xC8"                  # 78 mov ecx, eax
            "\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1BFh
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\x24\xAB"      # 7  mov [esp+10h+Src], ax
            "\xC6\x44\x24\xAB\xAB"      # 12 mov [esp+10h+var_2], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1DDh
            "\x89\x54\x24\xAB"          # 5  mov [esp+0A4h+var_92], edx
            "\x8B\x96\xAB\xAB\xAB\xAB"  # 9  mov edx, [esi+188h]
            "\x66\x89\x44\x24\xAB"      # 15 mov [esp+0A4h+Src], ax
            "\x8B\x86\xAB\xAB\xAB\xAB"  # 20 mov eax, [esi+180h]
            "\x89\x4C\x24\xAB"          # 26 mov [esp+0A4h+var_8A], ecx
            "\x8B\x8E\xAB\xAB\xAB\xAB"  # 30 mov ecx, [esi+190h]
            "\x89\x54\x24\xAB"          # 36 mov [esp+0A4h+var_86], edx
            "\x89\x44\x24\xAB"          # 40 mov [esp+0A4h+var_8E], eax
            "\x8B\x86\xAB\xAB\xAB\xAB"  # 44 mov eax, [esi+18Ch]
            "\x8D\x54\x24\xAB"          # 50 lea edx, [esp+0A4h+var_7A]
            "\x89\x4C\x24\xAB"          # 54 mov [esp+0A4h+var_7E], ecx
            "\x52"                      # 58 push edx
            "\x8D\x4C\x24\xAB"          # 59 lea ecx, [esp+0A8h+var_5C]
            "\x89\x44\x24\xAB"          # 63 mov [esp+0A8h+var_82], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 67 call sub_6D1C80
            "\xE8\xAB\xAB\xAB\xAB"      # 72 call sub_788240
            "\x0F\xBF\x4C\x24\xAB"      # 77 movsx ecx, [esp+0A4h+Src]
            "\x88\x44\x24\xAB"          # 82 mov [esp+0A4h+var_6A], al
            "\x8D\x44\x24\xAB"          # 86 lea eax, [esp+0A4h+Src]
            "\x50"                      # 90 push eax
            "\x51"                      # 91 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 92 call CRagConnection_instanceR
            "\x8B\xC8"                  # 97 mov ecx, eax
            "\xE8"                      # 99 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 99,
            "packetId": 1,
            "retOffset": 0,
            "goto": (92, 4)
        },
        {
            "instanceR": 93,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 200h
            "\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+3F4h+var_272], ecx
            "\x89\x94\x24\xAB\xAB\xAB\xAB"  # 12 mov [esp+3F4h+var_26E], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 65h
            "\x66\x89\x54\xAB\xAB"      # 5  mov word ptr [esp+3ECh+cp], dx
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 10 mov edx, dword_9AAC44
            "\x66\x89\x44\x24\xAB"      # 16 mov ptr [esp+3ECh+var_396], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 21 mov eax, dword_9AAC48
            "\x89\x4C\x24\xAB"          # 26 mov dword ptr [esp+3ECh+var1], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 30 mov ecx, offset dword_9A93E8
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 35 mov addr1, 1
            "\x89\x54\x24\xAB"          # 45 mov ptr [esp+3ECh+var1+4], edx
            "\x89\x44\x24\xAB"          # 49 mov [esp+3ECh+var_39A], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_7D2640
            "\x0F\xBF\x54\x24\xAB"      # 58 movsx edx, word ptr [esp+3ECh+cp]
            "\x8D\x4C\x24\xAB"          # 63 lea ecx, [esp+3ECh+cp]
            "\x51"                      # 67 push ecx
            "\x52"                      # 68 push edx
            "\x88\x44\x24\xAB"          # 69 mov [esp+3F4h+var_396+2], al
            "\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            "\x8B\xC8"                  # 78 mov ecx, eax
            "\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 970h
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+3F4h+var_267], ax
            "\x88\x8C\x24\xAB\xAB\xAB\xAB"  # 13 mov [esp+3F4h+var_26E+4], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1FBh
            "\x56"                      # 5  push esi
            "\x52"                      # 6  push edx
            "\x89\x44\x24\xAB"          # 7  mov [esp+3F8h+var_38E], eax
            "\x66\x89\x4C\x24\xAB"      # 11 mov [esp+3F8h+Dst], cx
            "\xFF\xD5"                  # 16 call ebp
            "\x0F\xBF\x4C\x24\xAB"      # 18 movsx ecx, [esp+3F8h+Dst]
            "\x83\xC4\xAB"              # 23 add esp, 0Ch
            "\x8D\x44\x24\xAB"          # 26 lea eax, [esp+3ECh+Dst]
            "\x50"                      # 30 push eax
            "\xC6\x84\x24\xAB\xAB\xAB\xAB\xAB"  # 31 mov [esp+3F0h+var_359], 0
            "\x51"                      # 39 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            "\x8B\xC8"                  # 45 mov ecx, eax
            "\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 66h
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\x24\xAB"      # 7  mov word ptr [esp+3F4h+var_1], ax
            "\x88\x4C\x24\xAB"          # 12 mov ptr [esp+3F4h+var_1+2], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 869h
            "\x83\xC4\xAB"              # 5  add esp, 4
            "\x66\x89\x4C\x24\xAB"      # 8  mov word ptr [esp+3ECh+cp], cx
            "\xFF\xD7"                  # 13 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, dword_9AAC44
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, dword ptr qword_9AB40C
            "\x89\x44\x24\xAB"          # 27 mov ptr [esp+3ECh+var_396], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword ptr qword_9AB40C+4
            "\x89\x4C\x24\xAB"          # 36 mov [esp+3ECh+var_39A], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_9A93E8
            "\x89\x54\x24\xAB"          # 45 mov ptr [esp+3ECh+var_3A2], edx
            "\x89\x44\x24\xAB"          # 49 mov ptr [esp+3ECh+var_3A2+4], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_7D2640
            "\x88\x44\x24\xAB"          # 58 mov [esp+3ECh+var_396+4], al
            "\x0F\xBF\x44\xAB\xAB"      # 62 movsx eax, word ptr [esp+3ECh+cp]
            "\x8D\x54\xAB\xAB"          # 67 lea edx, [esp+3ECh+cp]
            "\x52"                      # 71 push edx
            "\x50"                      # 72 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            "\x8B\xC8"                  # 78 mov ecx, eax
            "\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 841h
            "\x88\x4C\x24\xAB"          # 5  mov byte ptr [esp+3F4h+var1+3], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            "\x8B\xC8"                  # 14 mov ecx, eax
            "\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 234h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x4C\x24\xAB"      # 7  mov [esp+20h+Src], cx
            "\x89\x74\x24\xAB"          # 12 mov [esp+20h+var_6], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2012-05-25
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 232h
            "\x89\x7C\x24\xAB"          # 5  mov [esp+40h+var_E], edi
            "\x88\x54\x24\xAB"          # 9  mov [esp+40h+var_A], dl
            "\x88\x44\x24\xAB"          # 13 mov [esp+40h+var_9], al
            "\x88\x5C\x24\xAB"          # 17 mov [esp+40h+var_8], bl
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2012-05-25
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 233h
            "\x51"                      # 5  push ecx
            "\x50"                      # 6  push eax
            "\x66\x89\x44\x24\xAB"      # 7  mov word ptr [esp+24h+Src], ax
            "\x89\x5C\x24\xAB"          # 12 mov dword ptr [esp+24h+Src+2], ebx
            "\x89\x6C\x24\xAB"          # 16 mov dword ptr [esp+24h+Src+6], ebp
            "\xC6\x44\x24\xAB\xAB"      # 20 mov [esp+24h+var_2], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            "\x8B\xC8"                  # 30 mov ecx, eax
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2012-05-25
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 21Dh
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\x66\x89\x54\x24\xAB"      # 7  mov [esp+34h+Src], dx
            "\x89\x44\x24\xAB"          # 12 mov [esp+34h+var_12], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2008-07-02
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2B6h
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_C], 2B6h
            "\x89\x55\xAB"              # 12 mov [ebp+var_8], edx
            "\x88\x45\xAB"              # 15 mov [ebp+var_4], al
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_110], cx
            "\x8B\x00"                  # 12 mov eax, [eax]
            "\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov [ebp+var_10E], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 20 lea eax, [ebp+var_110]
            "\x50"                      # 26 push eax
            "\x51"                      # 27 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 28 call CRagConnection_instanceR
            "\x8B\xC8"                  # 33 mov ecx, eax
            "\xE8"                      # 35 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 35,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 29,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 82Bh
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_110], ax
            "\x0F\xB6\x05\xAB\xAB\xAB\xAB"  # 12 movzx eax, byte_F3B5E6
            "\x89\x85\xAB\xAB\xAB\xAB"  # 19 mov [ebp+var_114], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 25 lea eax, [ebp+var_114]
            "\x50"                      # 31 push eax
            "\x8D\x8E\xAB\xAB\xAB\xAB"  # 32 lea ecx, [esi+0A4h]
            "\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_4A0400
            "\x8B\x00"                  # 43 mov eax, [eax]
            "\x89\x85\xAB\xAB\xAB\xAB"  # 45 mov [ebp+var_10E], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 51 lea eax, [ebp+var_110]
            "\x50"                      # 57 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 58 movsx eax, [ebp+var_110]
            "\x50"                      # 65 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 66 call CRagConnection_instanceR
            "\x8B\xC8"                  # 71 mov ecx, eax
            "\xE8"                      # 73 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 73,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 67,
        }
    ],
    # 2016-04-27
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], cx
            "\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 12 mov word ptr [ebp+var1+2], di
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CRagConnection_instanceR
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\xE8"                      # 26 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 26,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 20,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A52h
            "\x6A\xAB"                  # 5  push 10h
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_24], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_4DFE20
            "\x50"                      # 16 push eax
            "\x8D\x45\xAB"              # 17 lea eax, [ebp+var_22]
            "\x6A\xAB"                  # 20 push 10h
            "\x50"                      # 22 push eax
            "\x90"                      # 23 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 24 call near ptr 35AE380h
            "\x66\x8B\x86\xAB\xAB\xAB\xAB"  # 29 mov ax, [esi+0B0h]
            "\x66\x89\x45\xAB"          # 36 mov word ptr [ebp+var_14+2], ax
            "\x83\xC4\xAB"              # 40 add esp, 10h
            "\x8D\x45\xAB"              # 43 lea eax, [ebp+var_24]
            "\x50"                      # 46 push eax
            "\x0F\xBF\x45\xAB"          # 47 movsx eax, [ebp+var_24]
            "\x50"                      # 51 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            "\x8B\xC8"                  # 57 mov ecx, eax
            "\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A5Ch
            "\x6A\xAB"                  # 5  push 10h
            "\x66\x89\x45\xAB"          # 7  mov [ebp+var_18], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_4DFE20
            "\x50"                      # 16 push eax
            "\x8D\x45\xAB"              # 17 lea eax, [ebp+var_16]
            "\x6A\xAB"                  # 20 push 10h
            "\x50"                      # 22 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 35AE380h
            "\x90"                      # 28 no nop
            "\x83\xC4\xAB"              # 29 add esp, 10h
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+var_18]
            "\x50"                      # 35 push eax
            "\x0F\xBF\x45\xAB"          # 36 movsx eax, [ebp+var_18]
            "\x50"                      # 40 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 41 call CRagConnection_instanceR
            "\x8B\xC8"                  # 46 mov ecx, eax
            "\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 42,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A13h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_44], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call sub_4DFE20
            "\x50"                      # 14 push eax
            "\x8D\x45\xAB"              # 15 lea eax, [ebp+var_42]
            "\x6A\xAB"                  # 18 push 18h
            "\x50"                      # 20 push eax
            "\x90"                      # 21 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call near ptr 35B507Ch
            "\x83\xC4\xAB"              # 27 add esp, 0Ch
            "\x8D\x45\xAB"              # 30 lea eax, [ebp+var_44]
            "\x50"                      # 33 push eax
            "\x0F\xBF\x45\xAB"          # 34 movsx eax, [ebp+var_44]
            "\x50"                      # 38 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            "\x8B\xC8"                  # 44 mov ecx, eax
            "\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # 2016-04-27
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            "\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_1C+3], ecx
            "\x89\x75\xAB"              # 8  mov dword ptr [ebp+var_1C+7], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 0A08h
            "\x50"                      # 5  push eax
            "\x66\x89\xAB\xAB"          # 6  mov [ebp+var_38], cx
            "\x66\x0F\xD6\x45\xAB"      # 10 movq [ebp+var_36], xmm0
            "\x66\x0F\xD6\x45\xAB"      # 15 movq [ebp+var_2E], xmm0
            "\x66\x0F\xD6\x45\xAB"      # 20 movq [ebp+var_26], xmm0
            "\x0F\xBF\xC1"              # 25 movsx eax, cx
            "\x50"                      # 28 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            "\x8B\xC8"                  # 34 mov ecx, eax
            "\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_29C], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_F3E874
            "\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp-29Ah], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            "\x50"                      # 29 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_4B9C00
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            "\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            "\x72\xAB"                  # 46 jb short loc_52ED55
            "\x8B\x00"                  # 48 mov eax, [eax]
            "\x50"                      # 50 push eax
            "\x90"                      # 51 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 52 call near ptr 35C9001h
            "\x83\xC4\xAB"              # 57 add esp, 4
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov ptr [ebp+var_298+2], ax
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 0FFFFFFFFh
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 74 lea ecx, [ebp+var_290]
            "\xE8\xAB\xAB\xAB\xAB"      # 80 call sub_402620
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+var_29C]
            "\x50"                      # 91 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 92 movsx eax, [ebp+var_29C]
            "\x50"                      # 99 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            "\x8B\xC8"                  # 105 mov ecx, eax
            "\xE8"                      # 107 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2016-04-27
    [
        (
            "\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            "\xEB\x0A"                  # 5  jmp short loc_56F8E0
            "\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  align 10h
            "\xA1\xAB\xAB\xAB\xAB"      # 17 mov eax, dword_F3E874
            "\x8D\x4E\xAB"              # 22 lea ecx, [esi+8]
            "\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 25 mov word ptr [ebp+var1], di
            "\x89\x85\xAB\xAB\xAB\xAB"  # 32 mov dword ptr [ebp+var1+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_4A9120
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 43 mov word ptr [ebp+var1+6], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 50 lea eax, [ebp+var1]
            "\x50"                      # 56 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 57 movsx eax, word ptr [ebp+var1]
            "\x50"                      # 64 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            "\x8B\xC8"                  # 70 mov ecx, eax
            "\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2016-04-27
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 9E8h
            "\xC6\x45\xAB\xAB"          # 5  mov [ebp+var_E], 0
            "\x66\x0F\x13\x45\xAB"      # 9  movlpd qword ptr [ebp-0Dh], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A08h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_AC], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_88]
            "\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 18 cmovnb eax, [ebp+var_88]
            "\x50"                      # 25 push eax
            "\x0F\x57\xC0"              # 26 xorps xmm0, xmm0
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 29 lea eax, [ebp+var_AA]
            "\x50"                      # 35 push eax
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 36 movq [ebp+var_AA], xmm0
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 44 movq [ebp+var_A2], xmm0
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 52 movq [ebp+var_9A], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 60 call sub_A4339C
            "\x83\xC4\xAB"              # 65 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 68 lea eax, [ebp+var_AC]
            "\x50"                      # 74 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 75 movsx eax, [ebp+var_AC]
            "\x50"                      # 82 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 83 call CRagConnection_instanceR
            "\x8B\xC8"                  # 88 mov ecx, eax
            "\xE8"                      # 90 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 90,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 84,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8DDh
            "\x6A\xAB"                  # 5  push 18h
            "\x50"                      # 7  push eax
            "\x66\x89\x4D\xAB"          # 8  mov [ebp+var_20], cx
            "\x90"                      # 12 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call near ptr 35B507Ch
            "\x8A\x45\xAB"              # 18 mov al, [ebp+arg_4]
            "\x88\x45\xAB"              # 21 mov [ebp+var_1E], al
            "\x83\xC4\xAB"              # 24 add esp, 0Ch
            "\x8D\x45\xAB"              # 27 lea eax, [ebp+var_20]
            "\x50"                      # 30 push eax
            "\x0F\xBF\x45\xAB"          # 31 movsx eax, [ebp+var_20]
            "\x50"                      # 35 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call CRagConnection_instanceR
            "\x8B\xC8"                  # 41 mov ecx, eax
            "\xE8"                      # 43 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 43,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 37,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_38], ax
            "\x8B\x47\xAB"              # 9  mov eax, [edi+34h]
            "\x83\xC0\xAB"              # 12 add eax, 4
            "\x50"                      # 15 push eax
            "\x8D\x45\xAB"              # 16 lea eax, [ebp+var_35]
            "\x6A\xAB"                  # 19 push 18h
            "\x50"                      # 21 push eax
            "\x90"                      # 22 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 35B507Ch
            "\x8D\x47\xAB"              # 28 lea eax, [edi+1Ch]
            "\x50"                      # 31 push eax
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1D]
            "\x6A\xAB"                  # 35 push 18h
            "\x50"                      # 37 push eax
            "\x90"                      # 38 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 39 call near ptr 35B507Ch
            "\x8B\x45\xAB"              # 44 mov eax, [ebp+arg_0]
            "\x83\xC4\xAB"              # 47 add esp, 18h
            "\x88\x45\xAB"              # 50 mov [ebp+var_36], al
            "\x83\xAB\xAB"              # 53 cmp eax, 2
            "\x75\x04"                  # 56 jnz short loc_7ED871
            "\xC6\x47\xAB\xAB"          # 58 mov byte ptr [edi+38h], 0
            "\x8D\x45\xAB"              # 62 lea eax, [ebp+var_38]
            "\x50"                      # 65 push eax
            "\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+var_38]
            "\x50"                      # 70 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            "\x8B\xC8"                  # 76 mov ecx, eax
            "\xE8"                      # 78 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8D7h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            "\x66\x8B\x45\xAB"          # 9  mov ax, [ebp+arg_0]
            "\x6A\xAB"                  # 13 push 17h
            "\x51"                      # 15 push ecx
            "\x66\x89\x45\xAB"          # 16 mov [ebp+var_1E], ax
            "\x8D\x45\xAB"              # 20 lea eax, [ebp+var_1C]
            "\x6A\xAB"                  # 23 push 18h
            "\x50"                      # 25 push eax
            "\x90"                      # 26 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call near ptr 35B3F41h
            "\x83\xC4\xAB"              # 32 add esp, 10h
            "\x8D\x45\xAB"              # 35 lea eax, [ebp+var_20]
            "\x50"                      # 38 push eax
            "\x0F\xBF\x45\xAB"          # 39 movsx eax, [ebp+var_20]
            "\x50"                      # 43 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            "\x8B\xC8"                  # 49 mov ecx, eax
            "\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8DAh
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            "\x8B\x41\xAB"              # 9  mov eax, [ecx+34h]
            "\x83\xC0\xAB"              # 12 add eax, 4
            "\x50"                      # 15 push eax
            "\x8D\x45\xAB"              # 16 lea eax, [ebp+var_1E]
            "\x6A\xAB"                  # 19 push 18h
            "\x50"                      # 21 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call near ptr 35B507Ch
            "\x90"                      # 27 no nop
            "\x83\xC4\xAB"              # 28 add esp, 0Ch
            "\x8D\x45\xAB"              # 31 lea eax, [ebp+var_20]
            "\x50"                      # 34 push eax
            "\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+var_20]
            "\x50"                      # 39 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            "\x8B\xC8"                  # 45 mov ecx, eax
            "\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 90Ah
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            "\x8B\x41\xAB"              # 9  mov eax, [ecx+34h]
            "\x83\xC0\xAB"              # 12 add eax, 4
            "\x50"                      # 15 push eax
            "\x8D\x45\xAB"              # 16 lea eax, [ebp+var_1E]
            "\x6A\xAB"                  # 19 push 18h
            "\x50"                      # 21 push eax
            "\x90"                      # 22 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 35B507Ch
            "\x83\xC4\xAB"              # 28 add esp, 0Ch
            "\x8D\x45\xAB"              # 31 lea eax, [ebp+var_20]
            "\x50"                      # 34 push eax
            "\x0F\xBF\x45\xAB"          # 35 movsx eax, [ebp+var_20]
            "\x50"                      # 39 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            "\x8B\xC8"                  # 45 mov ecx, eax
            "\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2016-04-27
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 362h
            "\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [edi+2E4h], 1
            "\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var1+2], bx
            "\x66\x89\x8D\xAB\xAB\xAB\xAB"  # 22 mov [ebp+var_10C0], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            "\x8B\xC8"                  # 34 mov ecx, eax
            "\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2016-04-27
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0E8h
            "\xC7\x87\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [edi+104h], 1
            "\x66\x89\x9D\xAB\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var1+2], bx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 802h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1+4], ax
            "\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            "\xEB\x03"                  # 15 jmp short loc_954840
            "\xAB\xAB\xAB"              # 17 align 10h
            "\x8A\x01"                  # 20 mov al, [ecx]
            "\x41"                      # 22 inc ecx
            "\x84\xC0"                  # 23 test al, al
            "\x75\xF9"                  # 25 jnz short loc_954840
            "\x2B\xCA"                  # 27 sub ecx, edx
            "\x83\xF9\xAB"              # 29 cmp ecx, 18h
            "\x0F\x83\xAB\xAB\xAB\xAB"  # 32 jnb loc_95471E
            "\x6A\xAB"                  # 38 push 18h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebp+var_1118+6]
            "\x53"                      # 46 push ebx
            "\x50"                      # 47 push eax
            "\x90"                      # 48 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 49 call near ptr 35B9119h
            "\x83\xC4\xAB"              # 54 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_1118+4]
            "\x50"                      # 63 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 64 movsx eax, [ebp+var1+4]
            "\x50"                      # 71 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            "\x8B\xC8"                  # 77 mov ecx, eax
            "\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2016-04-27
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 9EFh
            "\x66\x0F\x13\x45\xAB"      # 5  movlpd [ebp+var_D], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            "\x8B\xC8"                  # 15 mov ecx, eax
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1DDh
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_34], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 9  mov eax, dword_EE39A0
            "\x66\x0F\xD6\x45\xAB"      # 14 movq [ebp+var_32+4], xmm0
            "\xF3\x0F\x7E\xAB\xAB\xAB\xAB\xAB"  # 19 movq xmm0, ptr [esi+180h]
            "\x89\x45\xAB"              # 27 mov dword ptr [ebp+var_32], eax
            "\x66\x0F\xD6\x45\xAB"      # 30 movq [ebp+var_26], xmm0
            "\xF3\x0F\x7E\xAB\xAB\xAB\xAB\xAB"  # 35 movq xmm0, ptr [esi+188h]
            "\x8D\x45\xAB"              # 43 lea eax, [ebp+var_16]
            "\x50"                      # 46 push eax
            "\x66\x0F\xD6\x45\xAB"      # 47 movq [ebp+var_1E], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 52 call sub_8C5960
            "\xE8\xAB\xAB\xAB\xAB"      # 57 call sub_9BCCF0
            "\x88\x45\xAB"              # 62 mov [ebp+var_6], al
            "\x8D\x45\xAB"              # 65 lea eax, [ebp+var_34]
            "\x50"                      # 68 push eax
            "\x0F\xBF\x45\xAB"          # 69 movsx eax, [ebp+var_34]
            "\x50"                      # 73 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 74 call CRagConnection_instanceR
            "\x8B\xC8"                  # 79 mov ecx, eax
            "\xE8"                      # 81 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 81,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 75,
        }
    ],
    # 2016-04-27
    [
        (
            "\xBE\xAB\xAB\x00\x00"      # 0  mov esi, 64h
            "\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_2B4], si
            "\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_2B2], eax
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 18 movq [ebp+var_29E], xmm0
            "\x83\xF9\xAB"              # 26 cmp ecx, 7
            "\x75\x49"                  # 29 jnz short loc_9C0212
            "\x6A\xAB"                  # 31 push 18h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 33 lea eax, [ebp+var_2F2+2]
            "\x50"                      # 39 push eax
            "\x8D\x87\xAB\xAB\xAB\xAB"  # 40 lea eax, [edi+138h]
            "\x50"                      # 46 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_784EA0
            "\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 52 movq xmm0, [ebp+var_2F2+2]
            "\x66\x8B\xB5\xAB\xAB\xAB\xAB"  # 60 mov si, [ebp+var_2B4]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 67 movq [ebp+var_296], xmm0
            "\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 75 movq xmm0, [ebp+var_2EA+2]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 83 movq [ebp+var_28E], xmm0
            "\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 91 movq xmm0, [ebp+var_2E2+2]
            "\x83\xC4\xAB"              # 99 add esp, 0Ch
            "\xEB\x28"                  # 102 jmp short loc_9C023A
            "\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 104 movq xmm0, [edi+138h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 112 movq [ebp+var1], xmm0
            "\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 120 movq xmm0, [edi+140h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 128 movq [ebp+var2], xmm0
            "\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 136 movq xmm0, [edi+148h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 144 movq [ebp+var_286], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 152 call sub_9BCCF0
            "\x88\x85\xAB\xAB\xAB\xAB"  # 157 mov [ebp+var_27E], al
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 163 lea eax, [ebp+var_2B4]
            "\x50"                      # 169 push eax
            "\x0F\xBF\xC6"              # 170 movsx eax, si
            "\x50"                      # 173 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 174 call CRagConnection_instanceR
            "\x8B\xC8"                  # 179 mov ecx, eax
            "\xE8"                      # 181 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 181,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 175,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1FBh
            "\x6A\xAB"                  # 5  push 32h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov [ebp+var_2B4], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_2AE]
            "\x56"                      # 20 push esi
            "\x50"                      # 21 push eax
            "\xFF\xD3"                  # 22 call ebx
            "\x83\xC4\xAB"              # 24 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 27 lea eax, [ebp+var_2B4]
            "\x50"                      # 33 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 34 movsx eax, [ebp+var_2B4]
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 41 mov [ebp+var_27D], 0
            "\x50"                      # 48 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            "\x8B\xC8"                  # 54 mov ecx, eax
            "\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2016-04-27
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 22Dh
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_274], ax
            "\xFF\xD3"                  # 12 call ebx
            "\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov dword ptr [ebp+var_268+2], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 20 mov eax, dword_F3E874
            "\x89\x85\xAB\xAB\xAB\xAB"  # 25 mov [ebp+var_272], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword_F3E878
            "\x89\x85\xAB\xAB\xAB\xAB"  # 36 mov [ebp+var_26E], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 42 mov eax, dword_F3E138
            "\xB9\xAB\xAB\xAB\xAB"      # 47 mov ecx, offset dword_F3D670
            "\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov [ebp-26Ah], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 58 call sub_A1BFC0
            "\x88\x85\xAB\xAB\xAB\xAB"  # 63 mov byte ptr [ebp+var_268+6], al
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 69 lea eax, [ebp+var_274]
            "\x50"                      # 75 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 76 movsx eax, [ebp+var_274]
            "\x50"                      # 83 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 84 call CRagConnection_instanceR
            "\x8B\xC8"                  # 89 mov ecx, eax
            "\xE8"                      # 91 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 91,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 85,
        }
    ],
    # 2009-01-20
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 28Dh
            "\x89\x15\xAB\xAB\xAB\xAB"  # 5  mov ds:dword_813538, edx
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2009-01-20
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 228h
            "\x89\x4C\x24\xAB"          # 5  mov [esp+424h+var_40E], ecx
            "\x89\x54\x24\xAB"          # 9  mov [esp+424h+var_40A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            "\x8B\xC8"                  # 18 mov ecx, eax
            "\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 2F1h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+7Ch+A], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0D3h
            "\x89\x4C\x24\xAB"          # 5  mov [esp+34h+var_20+2], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 9  mov ecx, offset off_813544
            "\x66\x89\x44\x24\xAB"      # 14 mov word ptr [esp+34h+var_20], ax
            "\xC6\x44\x24\xAB\xAB"      # 19 mov byte ptr [esp+34h+var_1C+2], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 24 call sub_54A790
            "\x0F\xBF\x4C\x24\xAB"      # 29 movsx ecx, ptr [esp+34h+var_20]
            "\x8B\xD0"                  # 34 mov edx, eax
            "\x33\x54\x24\xAB"          # 36 xor edx, [esp+34h+var_20+2]
            "\x04\x37"                  # 40 add al, 37h
            "\x30\x44\x24\xAB"          # 42 xor ptr [esp+34h+var_1C+2], al
            "\x8D\x44\x24\xAB"          # 46 lea eax, [esp+34h+var_20]
            "\x50"                      # 50 push eax
            "\x81\xF2\xAB\xAB\xAB\xAB"  # 51 xor edx, 75AAh
            "\x51"                      # 57 push ecx
            "\x89\x54\x24\xAB"          # 58 mov [esp+3Ch+var_20+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            "\x8B\xC8"                  # 67 mov ecx, eax
            "\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2009-01-20
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 89h
            "\x50"                      # 5  push eax
            "\x8B\xCE"                  # 6  mov ecx, esi
            "\x66\x89\x54\x24\xAB"      # 8  mov [esp+24h+var_10], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call sub_584020
            "\xFF\xD7"                  # 18 call edi
            "\x0F\xBF\x54\x24\xAB"      # 20 movsx edx, [esp+1Ch+var_10]
            "\x8D\x4C\x24\xAB"          # 25 lea ecx, [esp+1Ch+var_10]
            "\x51"                      # 29 push ecx
            "\x52"                      # 30 push edx
            "\x89\x44\x24\xAB"          # 31 mov [esp+24h+var_9], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 35 call CRagConnection_instanceR
            "\x8B\xC8"                  # 40 mov ecx, eax
            "\xE8"                      # 42 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 42,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 36,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8Ch
            "\x51"                      # 5  push ecx
            "\x8B\xCE"                  # 6  mov ecx, esi
            "\x66\x89\x44\x24\xAB"      # 8  mov word ptr [esp+34h+var_14], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call sub_584020
            "\x0F\xBF\x44\x24\xAB"      # 18 movsx eax, ptr [esp+2Ch+var_14]
            "\x8D\x54\x24\xAB"          # 23 lea edx, [esp+2Ch+var_14]
            "\x52"                      # 27 push edx
            "\x50"                      # 28 push eax
            "\x89\x5C\x24\xAB"          # 29 mov [esp+34h+var_A], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            "\x8B\xC8"                  # 38 mov ecx, eax
            "\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2009-01-20
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 116h
            "\x50"                      # 5  push eax
            "\x8B\xCD"                  # 6  mov ecx, ebp
            "\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 8  mov [ebp+30Ch], 1
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 18 mov [esp+1414h+var1], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_584020
            "\x6A\x07"                  # 31 push 7
            "\x8D\x8C\x24\xAB\xAB\xAB\xAB"  # 33 lea ecx, [esp+30Ch]
            "\x51"                      # 40 push ecx
            "\x8B\xCD"                  # 41 mov ecx, ebp
            "\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_584020
            "\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 48 movsx ecx, [esp+var2]
            "\x66\x8B\x54\x24\xAB"      # 56 mov dx, [esp+140Ch+var_13F8]
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 61 lea eax, [esp+140Ch+var1]
            "\x50"                      # 68 push eax
            "\x51"                      # 69 push ecx
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 70 mov [esp+1414h+var1+6], bx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 78 mov [esp+317h], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 86 call CRagConnection_instanceR
            "\x8B\xC8"                  # 91 mov ecx, eax
            "\xE8"                      # 93 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 93,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 87,
        }
    ],
    # 2009-01-20
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 41Fh
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset off_813544
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+var3], dx
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 18 mov [esp+var3+4], eax
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 25 mov [esp+var3+2], bx
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call sub_54A790
            "\x8B\x8C\x24\xAB\xAB\xAB\xAB"  # 38 mov ecx, [esp+var3+4]
            "\x33\xC8"                  # 45 xor ecx, eax
            "\x05\xAB\xAB\xAB\xAB"      # 47 add eax, 3B6Ch
            "\x66\x31\x84\x24\xAB\xAB\xAB\xAB"  # 52 xor [esp+var3+2], ax
            "\x0F\xBF\x84\x24\xAB\xAB\xAB\xAB"  # 60 movsx eax, [esp+var3]
            "\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 68 lea edx, [esp+var3]
            "\x52"                      # 75 push edx
            "\x81\xF1\xAB\xAB\xAB\xAB"  # 76 xor ecx, 3643h
            "\x50"                      # 82 push eax
            "\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 83 mov [esp+1414h+var1+4], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 90 call CRagConnection_instanceR
            "\x8B\xC8"                  # 95 mov ecx, eax
            "\xE8"                      # 97 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 97,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 91,
        }
    ],
    # 2009-01-20
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 409h
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 5  mov [esp+1414h+var1+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2009-01-20
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 1F9h
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+A+var1], dx
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 15 mov ptr [esp+A+var1+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 288h
            "\x6A\xAB"                  # 5  push 0
            "\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 7  lea edx, [esp+A+var_10F0]
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov [esp+A+var1], cx
            "\x52"                      # 22 push edx
            "\xB9\xAB\xAB\xAB\xAB"      # 23 mov ecx, offset dword_8106C0
            "\xE8\xAB\xAB\xAB\xAB"      # 28 call sub_69ADD0
            "\xC7\x84\x24\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 33 mov[esp+v],34h
            "\x83\xBC\x24\xAB\xAB\xAB\xAB\xAB"  # 44 cmp [esp+var6], 10h
            "\x66\x8B\x84\x24\xAB\xAB\xAB\xAB"  # 52 mov ax, ptr [esp+var5]
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 60 mov [esp+var4], ax
            "\x8B\x84\x24\xAB\xAB\xAB\xAB"  # 68 mov eax, [esp+140Ch+var_10C0]
            "\x73\x07"                  # 75 jnb short loc_5A3553
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 77 lea eax, [esp+140Ch+var_10C0]
            "\x50"                      # 84 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 85 call ds:dword_70D3B8
            "\x0F\xBF\x94\x24\xAB\xAB\xAB\xAB"  # 91 movsx edx, [esp+A+var1]
            "\x83\xC4\xAB"              # 99 add esp, 4
            "\x8D\x8C\x24\xAB\xAB\xAB\xAB"  # 102 lea ecx, [esp+var3]
            "\x51"                      # 109 push ecx
            "\x52"                      # 110 push edx
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 111 mov [esp+var3], ax
            "\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 119 mov [esp+1414h+var1+6], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 126 call CRagConnection_instanceR
            "\x8B\xC8"                  # 131 mov ecx, eax
            "\xE8"                      # 133 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 133,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 127,
        }
    ],
    # 2009-01-20
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2BAh
            "\x66\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 5  mov ptr [esp+var1], bx
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call CRagConnection_instanceR
            "\x8B\xC8"                  # 18 mov ecx, eax
            "\xE8"                      # 20 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 20,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 14,
        }
    ],
    # 2009-01-20
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 2D8h
            "\x50"                      # 5  push eax
            "\x52"                      # 6  push edx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+var1], dx
            "\xC7\x84\x24\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov [esp+v],0
            "\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 26 mov ptr [esp+var1+6], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            "\x8B\xC8"                  # 38 mov ecx, eax
            "\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 345h
            "\x89\x47\xAB"              # 5  mov [edi+1Ch], eax
            "\x66\x89\x4C\x24\xAB"      # 8  mov [esp+60h+var_54], cx
            "\xFF\xD6"                  # 13 call esi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, ds:dword_811734
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, ds:dword_811F4C
            "\x89\x44\x24\xAB"          # 27 mov [esp+60h+var_52], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, ds:dword_811F50
            "\x89\x4C\x24\xAB"          # 36 mov [esp+60h+var_4A], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_8106C0
            "\x89\x54\x24\xAB"          # 45 mov dword ptr [esp+var3], edx
            "\x89\x44\x24\xAB"          # 49 mov [esp+60h+var_4E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_6BD050
            "\x8B\x54\x24\xAB"          # 58 mov edx, dword ptr [esp+var2]
            "\x33\x54\x24\xAB"          # 62 xor edx, [esp+60h+var_52]
            "\x88\x44\x24\xAB"          # 66 mov byte ptr [esp+60h+var_46], al
            "\x8B\x44\x24\xAB"          # 70 mov eax, [esp+60h+var_4A]
            "\x33\x44\x24\xAB"          # 74 xor eax, [esp+60h+var_4E]
            "\x81\xF2\xAB\xAB\xAB\xAB"  # 78 xor edx, 2290h
            "\x89\x54\x24\xAB"          # 84 mov dword ptr [esp+var1], edx
            "\x0F\xBF\x54\x24\xAB"      # 88 movsx edx, [esp+60h+var_54]
            "\x8D\x4C\x24\xAB"          # 93 lea ecx, [esp+60h+var_54]
            "\x51"                      # 97 push ecx
            "\x35\xAB\xAB\xAB\xAB"      # 98 xor eax, 2290h
            "\x52"                      # 103 push edx
            "\x89\x44\x24\xAB"          # 104 mov [esp+68h+var_4E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 108 call CRagConnection_instanceR
            "\x8B\xC8"                  # 113 mov ecx, eax
            "\xE8"                      # 115 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 115,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 109,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 65h
            "\x66\x89\x44\x24\xAB"      # 5  mov ptr [esp+164h+var_14C], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 10 mov eax, ds:dword_811734
            "\x89\x4C\x24\xAB"          # 15 mov [esp+164h+var_142], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 19 mov ecx, offset dword_8106C0
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 24 mov ds:A, 1
            "\x89\x54\x24\xAB"          # 34 mov [esp+164h+var_14C+2], edx
            "\x89\x44\x24\xAB"          # 38 mov [esp+164h+var_146], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_6BD050
            "\x88\x44\x24\xAB"          # 47 mov ptr [esp+164h+var_13E+2], al
            "\x0F\xBF\x44\x24\xAB"      # 51 movsx eax, ptr [esp+164h+var_14C]
            "\x8D\x54\x24\xAB"          # 56 lea edx, [esp+164h+var_14C]
            "\x52"                      # 60 push edx
            "\x50"                      # 61 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            "\x8B\xC8"                  # 67 mov ecx, eax
            "\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2009-01-20
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x4C\x24\xAB"      # 2  mov [esp+55h], cx
            "\x66\x89\x54\x24\xAB"      # 7  mov word ptr [esp+16Ch+A+2], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2009-01-20
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 66h
            "\x51"                      # 5  push ecx
            "\x52"                      # 6  push edx
            "\x66\x89\x54\x24\xAB"      # 7  mov word ptr [esp+16Ch+var1], dx
            "\x88\x44\x24\xAB"          # 12 mov byte ptr [esp+16Ch+var2], al
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call CRagConnection_instanceR
            "\x8B\xC8"                  # 21 mov ecx, eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 17,
        }
    ],
    # 2009-01-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 345h
            "\x83\xC4\xAB"              # 5  add esp, 4
            "\x66\x89\x44\x24\xAB"      # 8  mov ptr [esp+164h+var_14C], ax
            "\xFF\xD7"                  # 13 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, ds:dword_811F4C
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, ds:dword_811F50
            "\x89\x44\x24\xAB"          # 27 mov [esp+164h+var_14C+2], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, ds:dword_811734
            "\x89\x4C\x24\xAB"          # 36 mov ptr [esp+164h+var_13E+1], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_8106C0
            "\x89\x54\x24\xAB"          # 45 mov [esp+164h+var_146], edx
            "\x89\x44\x24\xAB"          # 49 mov [esp+164h+var_142], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_6BD050
            "\x8B\x4C\x24\xAB"          # 58 mov ecx, ptr [esp+164h+var_13E+1]
            "\x33\x4C\x24\xAB"          # 62 xor ecx, [esp+164h+var_14C+2]
            "\x8B\x54\x24\xAB"          # 66 mov edx, [esp+164h+var_142]
            "\x33\x54\x24\xAB"          # 70 xor edx, [esp+164h+var_146]
            "\x81\xF1\xAB\xAB\xAB\xAB"  # 74 xor ecx, 2290h
            "\x88\x44\x24\xAB"          # 80 mov ptr [esp+164h+var_13E], al
            "\x89\x4C\x24\xAB"          # 84 mov ptr [esp+164h+var_13E+1], ecx
            "\x0F\xBF\x4C\x24\xAB"      # 88 movsx ecx, ptr [esp+164h+var_14C]
            "\x8D\x44\x24\xAB"          # 93 lea eax, [esp+164h+var_14C]
            "\x50"                      # 97 push eax
            "\x81\xF2\xAB\xAB\xAB\xAB"  # 98 xor edx, 2290h
            "\x51"                      # 104 push ecx
            "\x89\x54\x24\xAB"          # 105 mov [esp+16Ch+var_146], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 109 call CRagConnection_instanceR
            "\x8B\xC8"                  # 114 mov ecx, eax
            "\xE8"                      # 116 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 116,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 110,
        }
    ],
    # 2009-01-20
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 3F2h
            "\x89\x7C\x24\xAB"          # 5  mov [esp+38h+var_E], edi
            "\x88\x54\x24\xAB"          # 9  mov [esp+38h+var_A], dl
            "\x88\x44\x24\xAB"          # 13 mov [esp+38h+var_9], al
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call CRagConnection_instanceR
            "\x8B\xC8"                  # 22 mov ecx, eax
            "\xE8"                      # 24 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 24,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 18,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_24], 8Ch
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            "\x0F\xBF\x45\xAB"          # 11 movsx eax, [ebp+var_24]
            "\x8B\x4D\xAB"              # 15 mov ecx, [ebp+arg_0]
            "\x8D\x55\xAB"              # 18 lea edx, [ebp+var_24]
            "\x52"                      # 21 push edx
            "\x50"                      # 22 push eax
            "\x89\x4D\xAB"              # 23 mov [ebp+var_1A], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call CRagConnection_instanceR
            "\x8B\xC8"                  # 31 mov ecx, eax
            "\xE8"                      # 33 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 33,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 27,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_20], 0A2h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            "\x0F\xBF\x4D\xAB"          # 11 movsx ecx, [ebp+var_20]
            "\x8D\x45\xAB"              # 15 lea eax, [ebp+var_20]
            "\x89\x75\xAB"              # 18 mov [ebp+var_16], esi
            "\x50"                      # 21 push eax
            "\x51"                      # 22 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_28], 0A2h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            "\x0F\xBF\x55\xAB"          # 11 movsx edx, [ebp+var_28]
            "\x8D\x4D\xAB"              # 15 lea ecx, [ebp+var_28]
            "\x89\x5D\xAB"              # 18 mov [ebp+var_1E], ebx
            "\x51"                      # 21 push ecx
            "\x52"                      # 22 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_10], 16Ah
            "\x89\x45\xAB"              # 6  mov [ebp+var_E], eax
            "\xC6\x45\xAB\xAB"          # 9  mov [ebp+var_A], 1
            "\xE8\xAB\xAB\xAB\xAB"      # 13 call sub_538DB0
            "\x8A\x5D\xAB"              # 18 mov bl, [ebp+var_A]
            "\x8B\x75\xAB"              # 21 mov esi, [ebp+var_E]
            "\x8B\xC8"                  # 24 mov ecx, eax
            "\x04\xAB"                  # 26 add al, 6Ch
            "\x32\xD8"                  # 28 xor bl, al
            "\x33\xCE"                  # 30 xor ecx, esi
            "\x0F\xBF\x45\xAB"          # 32 movsx eax, [ebp+var_10]
            "\x8D\x55\xAB"              # 36 lea edx, [ebp+var_10]
            "\x81\xF1\xAB\xAB\xAB\xAB"  # 39 xor ecx, 0FEF4h
            "\x52"                      # 45 push edx
            "\x50"                      # 46 push eax
            "\x89\x4D\xAB"              # 47 mov [ebp+var_E], ecx
            "\x88\x5D\xAB"              # 50 mov [ebp+var_A], bl
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call CRagConnection_instanceR
            "\x8B\xC8"                  # 58 mov ecx, eax
            "\xE8"                      # 60 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 60,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 54,
        }
    ],
    # 2008-10-22
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 334h
            "\x66\x89\x55\xAB"          # 5  mov word ptr [ebp+arg_4+2], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call CRagConnection_instanceR
            "\x8B\xC8"                  # 14 mov ecx, eax
            "\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 10,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_2C], 116h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_581840
            "\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_28+4]
            "\x6A\xAB"                  # 14 push 7
            "\x51"                      # 16 push ecx
            "\x8B\xCB"                  # 17 mov ecx, ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call sub_581840
            "\x0F\xBF\x4D\xAB"          # 24 movsx ecx, [ebp+var_2C]
            "\x66\x8B\x55\xAB"          # 28 mov dx, word ptr [ebp+arg_4+4]
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+var_2C]
            "\x50"                      # 35 push eax
            "\x51"                      # 36 push ecx
            "\x66\x89\x75\xAB"          # 37 mov word ptr [ebp+var_28+2], si
            "\x66\x89\x55\xAB"          # 41 mov word ptr [ebp+var_28+0Bh], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 45 call CRagConnection_instanceR
            "\x8B\xC8"                  # 50 mov ecx, eax
            "\xE8"                      # 52 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 52,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 46,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18+4], 3F2h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_571D70
            "\xB9\xAB\xAB\xAB\xAB"      # 11 mov ecx, offset off_7CB2C4
            "\x89\x45\xAB"              # 16 mov dword ptr [ebp+var_18+8], eax
            "\x66\x89\x75\xAB"          # 19 mov word ptr [ebp+var_18+6], si
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_538DB0
            "\x8B\x4D\xAB"              # 28 mov ecx, dword ptr [ebp+var_18+8]
            "\x8B\xD0"                  # 31 mov edx, eax
            "\x05\xAB\xAB\xAB\xAB"      # 33 add eax, 11A4h
            "\x33\xD1"                  # 38 xor edx, ecx
            "\x66\x31\x45\xAB"          # 40 xor word ptr [ebp+var_18+6], ax
            "\x8D\x45\xAB"              # 44 lea eax, [ebp+var_18+4]
            "\x0F\xBF\x4D\xAB"          # 47 movsx ecx, word ptr [ebp+var_18+4]
            "\x81\xF2\xAB\xAB\xAB\xAB"  # 51 xor edx, 5F8Ch
            "\x50"                      # 57 push eax
            "\x51"                      # 58 push ecx
            "\x89\x55\xAB"              # 59 mov dword ptr [ebp+var_18+8], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            "\x8B\xC8"                  # 67 mov ecx, eax
            "\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2008-10-22
    [
        (
            "\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 286h
            "\x56"                      # 5  push esi
            "\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset byte_77F128
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call sub_59B620
            "\x81\x38\xAB\xAB\xAB\xAB"  # 16 cmp dword ptr [eax], 0E1h
            "\x75\xAB"                  # 22 jnz short loc_596A1A
            "\xB9\xAB\xAB\xAB\xAB"      # 24 mov ecx, offset off_77EB18
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call sub_59AB90
            "\x50"                      # 34 push eax
            "\x8D\x95\xAB\xAB\xAB\xAB"  # 35 lea edx, [ebp+var_6A8]
            "\x68\xAB\xAB\xAB\xAB"      # 41 push offset dword_70A93C
            "\x52"                      # 46 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_6BA4E8
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 52 lea eax, [ebp+var_6A8]
            "\x50"                      # 58 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call sub_6BB387
            "\x83\xC4\xAB"              # 64 add esp, 10h
            "\x8D\x4D\xAB"              # 67 lea ecx, [ebp+arg_4]
            "\x66\x89\x7D\xAB"          # 70 mov word ptr [ebp+arg_4], di
            "\x66\x89\x45\xAB"          # 74 mov word ptr [ebp+arg_4+2], ax
            "\x51"                      # 78 push ecx
            "\x57"                      # 79 push edi
            "\xE8\xAB\xAB\xAB\xAB"      # 80 call CRagConnection_instanceR
            "\x8B\xC8"                  # 85 mov ecx, eax
            "\xE8"                      # 87 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 87,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 81,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_18], 288h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_669AB0
            "\x66\x8B\x8D\xAB\xAB\xAB\xAB"  # 11 mov cx, word ptr [ebp+var_98]
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 18 mov [ebp+var_4], 41h
            "\x66\x89\x4D\xAB"          # 25 mov word ptr [ebp+var_18+4], cx
            "\x8D\x4D\xAB"              # 29 lea ecx, [ebp+var_7C]
            "\xE8\xAB\xAB\xAB\xAB"      # 32 call sub_4BF830
            "\x50"                      # 37 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_6BB387
            "\x0F\xBF\x4D\xAB"          # 43 movsx ecx, word ptr [ebp+var_18]
            "\x8B\x55\xAB"              # 47 mov edx, dword ptr [ebp+arg_4]
            "\x83\xC4\xAB"              # 50 add esp, 4
            "\x66\x89\x45\xAB"          # 53 mov word ptr [ebp+var_18+2], ax
            "\x8D\x45\xAB"              # 57 lea eax, [ebp+var_18]
            "\x50"                      # 60 push eax
            "\x51"                      # 61 push ecx
            "\x89\x55\xAB"              # 62 mov dword ptr [ebp+var_18+6], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            "\x8B\xC8"                  # 70 mov ecx, eax
            "\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # 2008-10-22
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 2D8h
            "\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var1], 0
            "\x89\xBD\xAB\xAB\xAB\xAB"  # 15 mov [ebp+var_DE], edi
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_48], 170h
            "\xFF\xD7"                  # 6  call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, ds:dword_7C9F20
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, ds:dword_7C9854
            "\x89\x45\xAB"              # 20 mov [ebp+var_46], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, ds:dword_7C9F1C
            "\x89\x4D\xAB"              # 28 mov [ebp+var_3E], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_7C8928
            "\x89\x45\xAB"              # 36 mov [ebp+var_42], eax
            "\x89\x55\xAB"              # 39 mov [ebp+var_3A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_685E90
            "\x8B\x7D\xAB"              # 47 mov edi, [ebp+var_46]
            "\x8B\x4D\xAB"              # 50 mov ecx, [ebp+var_3A]
            "\x8B\x55\xAB"              # 53 mov edx, [ebp+var_3E]
            "\x88\x45\xAB"              # 56 mov [ebp+var_36], al
            "\x8B\x45\xAB"              # 59 mov eax, [ebp+var_42]
            "\x33\xCA"                  # 62 xor ecx, edx
            "\x33\xC7"                  # 64 xor eax, edi
            "\x8D\x55\xAB"              # 66 lea edx, [ebp+var_48]
            "\x35\xAB\xAB\xAB\xAB"      # 69 xor eax, 3604h
            "\x81\xF1\xAB\xAB\xAB\xAB"  # 74 xor ecx, 3604h
            "\x89\x45\xAB"              # 80 mov [ebp+var_42], eax
            "\x52"                      # 83 push edx
            "\x0F\xBF\x45\xAB"          # 84 movsx eax, [ebp+var_48]
            "\x50"                      # 88 push eax
            "\x89\x4D\xAB"              # 89 mov [ebp+var_3E], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 92 call CRagConnection_instanceR
            "\x8B\xC8"                  # 97 mov ecx, eax
            "\xE8"                      # 99 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 99,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 93,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var_94], 64h
            "\xF3\xA5"                  # 9  rep movsd
            "\x8D\xB3\xAB\xAB\xAB\xAB"  # 11 lea esi, [ebx+120h]
            "\xB9\xAB\xAB\xAB\xAB"      # 17 mov ecx, 6
            "\x8D\x7D\xAB"              # 22 lea edi, [ebp+var_76]
            "\xF3\xA5"                  # 25 rep movsd
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call sub_5DE5D0
            "\x8D\x95\xAB\xAB\xAB\xAB"  # 32 lea edx, [ebp+var_94]
            "\x88\x45\xAB"              # 38 mov [ebp+var_5E], al
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 41 movsx eax, [ebp+var_94]
            "\x52"                      # 48 push edx
            "\x50"                      # 49 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 50 call CRagConnection_instanceR
            "\x8B\xC8"                  # 55 mov ecx, eax
            "\xE8"                      # 57 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 57,
            "packetId": (7, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 51,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_1C], 65h
            "\x66\x89\x7D\xAB"          # 6  mov word ptr [ebp+var_E], di
            "\x89\x45\xAB"              # 10 mov [ebp+var_1A], eax
            "\x89\x55\xAB"              # 13 mov [ebp+var_12], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_685E90
            "\x0F\xBF\x4D\xAB"          # 21 movsx ecx, [ebp+var_1C]
            "\x88\x45\xAB"              # 25 mov [ebp+var_E+2], al
            "\x8D\x45\xAB"              # 28 lea eax, [ebp+var_1C]
            "\x50"                      # 31 push eax
            "\x51"                      # 32 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            "\x8B\xC8"                  # 38 mov ecx, eax
            "\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2008-10-22
    [
        (
            "\x6A\xAB"                  # 0  push 66h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 2  mov word ptr [ebp+arg_0], 66h
            "\x88\x55\xAB"              # 8  mov byte ptr [ebp+arg_0+2], dl
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2008-10-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_1C], 170h
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 6  call ds:dword_6E0440
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 12 mov ecx, ds:dword_7C9854
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 18 mov edx, ds:dword_7C9F1C
            "\x89\x45\xAB"              # 24 mov [ebp+var_1A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, ds:dword_7C9F20
            "\x89\x4D\xAB"              # 32 mov dword ptr [ebp+var_E], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_7C8928
            "\x89\x55\xAB"              # 40 mov [ebp+var_16], edx
            "\x89\x45\xAB"              # 43 mov [ebp+var_12], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 46 call sub_685E90
            "\x8B\x55\xAB"              # 51 mov edx, [ebp+var_16]
            "\x8B\x7D\xAB"              # 54 mov edi, [ebp+var_1A]
            "\x33\xD7"                  # 57 xor edx, edi
            "\x88\x45\xAB"              # 59 mov [ebp+var_E+4], al
            "\x8B\x45\xAB"              # 62 mov eax, dword ptr [ebp+var_E]
            "\x81\xF2\xAB\xAB\xAB\xAB"  # 65 xor edx, 3604h
            "\x89\x55\xAB"              # 71 mov [ebp+var_16], edx
            "\x8B\x55\xAB"              # 74 mov edx, [ebp+var_12]
            "\x33\xC2"                  # 77 xor eax, edx
            "\x8D\x4D\xAB"              # 79 lea ecx, [ebp+var_1C]
            "\x0F\xBF\x55\xAB"          # 82 movsx edx, [ebp+var_1C]
            "\x35\xAB\xAB\xAB\xAB"      # 86 xor eax, 3604h
            "\x51"                      # 91 push ecx
            "\x52"                      # 92 push edx
            "\x89\x45\xAB"              # 93 mov [ebp+var_12], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 96 call CRagConnection_instanceR
            "\x8B\xC8"                  # 101 mov ecx, eax
            "\xE8"                      # 103 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 103,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 97,
        }
    ],
    # 2008-10-22
    [
        (
            "\x6A\xAB"                  # 0  push 7Dh
            "\x89\x75\xAB"              # 2  mov [ebp+var_6], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 5  call CRagConnection_instanceR
            "\x8B\xC8"                  # 10 mov ecx, eax
            "\xE8"                      # 12 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 12,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 6,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A13h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_2C], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 9  call sub_4AEC20
            "\x50"                      # 14 push eax
            "\x8D\x45\xAB"              # 15 lea eax, [ebp+var_2A]
            "\x6A\xAB"                  # 18 push 18h
            "\x50"                      # 20 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call near ptr 32E507Ch
            "\x90"                      # 26 no nop
            "\x83\xC4\xAB"              # 27 add esp, 0Ch
            "\x8D\x45\xAB"              # 30 lea eax, [ebp+var_2C]
            "\x50"                      # 33 push eax
            "\x0F\xBF\x45\xAB"          # 34 movsx eax, [ebp+var_2C]
            "\x50"                      # 38 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            "\x8B\xC8"                  # 44 mov ecx, eax
            "\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # 2015-07-22
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 9EEh
            "\x89\x4D\xAB"              # 5  mov dword ptr [ebp+var_10+3], ecx
            "\x89\x7D\xAB"              # 8  mov dword ptr [ebp+var_10+7], edi
            "\xE8\xAB\xAB\xAB\xAB"      # 11 call CRagConnection_instanceR
            "\x8B\xC8"                  # 16 mov ecx, eax
            "\xE8"                      # 18 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 18,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 12,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_29C], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword_E1513C
            "\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp-29Ah], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            "\x50"                      # 29 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_49BC80
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            "\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            "\x72\x02"                  # 46 jb short loc_4F09B2
            "\x8B\x00"                  # 48 mov eax, [eax]
            "\x50"                      # 50 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 51 call near ptr 32F9001h
            "\x90"                      # 56 no nop
            "\x83\xC4\xAB"              # 57 add esp, 4
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov word ptr [ebp+var1], ax
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 0FFFFFFFFh
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 74 lea ecx, [ebp+var_290]
            "\xE8\xAB\xAB\xAB\xAB"      # 80 call sub_402620
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+var_29C]
            "\x50"                      # 91 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 92 movsx eax, [ebp+var_29C]
            "\x50"                      # 99 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            "\x8B\xC8"                  # 105 mov ecx, eax
            "\xE8"                      # 107 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8E0h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_38], ax
            "\x8B\x47\xAB"              # 9  mov eax, [edi+34h]
            "\x83\xC0\xAB"              # 12 add eax, 4
            "\x50"                      # 15 push eax
            "\x8D\x45\xAB"              # 16 lea eax, [ebp+var_35]
            "\x6A\xAB"                  # 19 push 18h
            "\x50"                      # 21 push eax
            "\x90"                      # 22 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call near ptr 32E507Ch
            "\x8D\x47\xAB"              # 28 lea eax, [edi+1Ch]
            "\x50"                      # 31 push eax
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+var_1D]
            "\x6A\xAB"                  # 35 push 18h
            "\x50"                      # 37 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 38 call near ptr 32E507Ch
            "\x90"                      # 43 no nop
            "\x8B\x45\xAB"              # 44 mov eax, [ebp+arg_0]
            "\x83\xC4\xAB"              # 47 add esp, 18h
            "\x88\x45\xAB"              # 50 mov [ebp+var_36], al
            "\x83\xF8\xAB"              # 53 cmp eax, 2
            "\x75\x04"                  # 56 jnz short loc_715A01
            "\xC6\x47\xAB\xAB"          # 58 mov byte ptr [edi+38h], 0
            "\x8D\x45\xAB"              # 62 lea eax, [ebp+var_38]
            "\x50"                      # 65 push eax
            "\x0F\xBF\x45\xAB"          # 66 movsx eax, [ebp+var_38]
            "\x50"                      # 70 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 71 call CRagConnection_instanceR
            "\x8B\xC8"                  # 76 mov ecx, eax
            "\xE8"                      # 78 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 78,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 72,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8D7h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+var_20], ax
            "\x66\x8B\x45\xAB"          # 9  mov ax, [ebp+arg_0]
            "\x6A\xAB"                  # 13 push 17h
            "\x51"                      # 15 push ecx
            "\x66\x89\x45\xAB"          # 16 mov [ebp+var_1E], ax
            "\x8D\x45\xAB"              # 20 lea eax, [ebp+var_1C]
            "\x6A\xAB"                  # 23 push 18h
            "\x50"                      # 25 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call near ptr 32E3F41h
            "\x90"                      # 31 no nop
            "\x83\xC4\xAB"              # 32 add esp, 10h
            "\x8D\x45\xAB"              # 35 lea eax, [ebp+var_20]
            "\x50"                      # 38 push eax
            "\x0F\xBF\x45\xAB"          # 39 movsx eax, [ebp+var_20]
            "\x50"                      # 43 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 44 call CRagConnection_instanceR
            "\x8B\xC8"                  # 49 mov ecx, eax
            "\xE8"                      # 51 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 51,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 45,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 368h
            "\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+var_30+4], ax
            "\x8D\x51\xAB"              # 9  lea edx, [ecx+1]
            "\x8A\x01"                  # 12 mov al, [ecx]
            "\x41"                      # 14 inc ecx
            "\x84\xC0"                  # 15 test al, al
            "\x75\xF9"                  # 17 jnz short loc_8843F1
            "\x2B\xCA"                  # 19 sub ecx, edx
            "\x83\xF9\x18"              # 21 cmp ecx, 18h
            "\x0F\x83\xAB\xAB\xAB\xAB"  # 24 jnb loc_8842CE
            "\x6A\xAB"                  # 30 push 18h
            "\x8D\x45\xAB"              # 32 lea eax, [ebp+var_30+6]
            "\x53"                      # 35 push ebx
            "\x50"                      # 36 push eax
            "\x90"                      # 37 no nop
            "\xE8\xAB\xAB\xAB\xAB"      # 38 call near ptr 32E9119h
            "\x83\xC4\xAB"              # 43 add esp, 0Ch
            "\x8D\x45\xAB"              # 46 lea eax, [ebp+var_30+4]
            "\x50"                      # 49 push eax
            "\x0F\xBF\x45\xAB"          # 50 movsx eax, word ptr [ebp+var_30+4]
            "\x50"                      # 54 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 55 call CRagConnection_instanceR
            "\x8B\xC8"                  # 60 mov ecx, eax
            "\xE8"                      # 62 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 62,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 56,
        }
    ],
    # 2015-07-22
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 213h
            "\x66\x0F\xD6\x45\xAB"      # 5  movq [ebp+var_1C+2], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            "\x8B\xC8"                  # 15 mov ecx, eax
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2015-07-22
    [
        (
            "\xBE\xAB\xAB\x00\x00"      # 0  mov esi, 64h
            "\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_2D8], si
            "\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_2D6], eax
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 18 movq [ebp+var_2C2], xmm0
            "\x83\xF9\xAB"              # 26 cmp ecx, 7
            "\x75\x3D"                  # 29 jnz short loc_8EE368
            "\x6A\xAB"                  # 31 push 18h
            "\x8D\x45\xAB"              # 33 lea eax, [ebp+var_2C]
            "\x50"                      # 36 push eax
            "\x8D\x87\xAB\xAB\xAB\xAB"  # 37 lea eax, [edi+138h]
            "\x50"                      # 43 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 44 call sub_6ADE30
            "\xF3\x0F\x7E\x45\xAB"      # 49 movq xmm0, [ebp+var_2C]
            "\x66\x8B\xB5\xAB\xAB\xAB\xAB"  # 54 mov si, [ebp+var_2D8]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 61 movq [ebp+var_2BA], xmm0
            "\xF3\x0F\x7E\x45\xAB"      # 69 movq xmm0, qword ptr [ebp-24h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 74 movq [ebp+var_2B2], xmm0
            "\xF3\x0F\x7E\x45\xAB"      # 82 movq xmm0, [ebp+var_22+6]
            "\x83\xC4\xAB"              # 87 add esp, 0Ch
            "\xEB\x28"                  # 90 jmp short loc_8EE390
            "\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 92 movq xmm0, [edi+138h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 100 movq [ebp+var_2BA], xmm0
            "\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 108 movq xmm0, [edi+140h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 116 movq [ebp+var_2B2], xmm0
            "\xF3\x0F\x7E\x87\xAB\xAB\xAB\xAB"  # 124 movq xmm0, [edi+148h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 132 movq [ebp+var_2AA], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 140 call sub_8EAE80
            "\x88\x85\xAB\xAB\xAB\xAB"  # 145 mov [ebp+var_2A2], al
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 151 lea eax, [ebp+var_2D8]
            "\x50"                      # 157 push eax
            "\x0F\xBF\xC6"              # 158 movsx eax, si
            "\x50"                      # 161 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 162 call CRagConnection_instanceR
            "\x8B\xC8"                  # 167 mov ecx, eax
            "\xE8"                      # 169 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 169,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 163,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 65h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_298], ax
            "\x33\xC0"                  # 12 xor eax, eax
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 14 mov ptr [ebp+var_28C+2], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 21 mov eax, dword_E1513C
            "\x89\x85\xAB\xAB\xAB\xAB"  # 26 mov [ebp+var_296], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 32 mov eax, dword_E14A00
            "\x89\x85\xAB\xAB\xAB\xAB"  # 37 mov [ebp+var_292], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 43 mov eax, dword_E14A04
            "\xB9\xAB\xAB\xAB\xAB"      # 48 mov ecx, offset dword_E13F38
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 53 mov A, 1
            "\x89\x85\xAB\xAB\xAB\xAB"  # 63 mov [ebp-28Eh], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 69 call sub_958E90
            "\xB9\xAB\xAB\xAB\xAB"      # 74 mov ecx, offset dword_E13F38
            "\xE8\xAB\xAB\xAB\xAB"      # 79 call sub_94A210
            "\x88\x85\xAB\xAB\xAB\xAB"  # 84 mov byte ptr [ebp+var_28C+4], al
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 90 lea eax, [ebp+var_298]
            "\x50"                      # 96 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 97 movsx eax, [ebp+var_298]
            "\x50"                      # 104 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 105 call CRagConnection_instanceR
            "\x8B\xC8"                  # 110 mov ecx, eax
            "\xE8"                      # 112 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 112,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 106,
        }
    ],
    # 2015-07-22
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1FBh
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_2D8], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_2D2]
            "\x56"                      # 18 push esi
            "\x50"                      # 19 push eax
            "\xFF\xD3"                  # 20 call ebx
            "\x83\xC4\xAB"              # 22 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 25 lea eax, [ebp+var_2D8]
            "\x50"                      # 31 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 32 movsx eax, [ebp+var_2D8]
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 39 mov [ebp+var_2A1], 0
            "\x50"                      # 46 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            "\x8B\xC8"                  # 52 mov ecx, eax
            "\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_14], 89h
            "\xFF\xD3"                  # 6  call ebx
            "\x89\x45\xAB"              # 8  mov [ebp+var_D], eax
            "\x8D\x55\xAB"              # 11 lea edx, [ebp+var_14]
            "\x0F\xBF\x45\xAB"          # 14 movsx eax, [ebp+var_14]
            "\x52"                      # 18 push edx
            "\x50"                      # 19 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 89h
            "\xFF\xD7"                  # 6  call edi
            "\x0F\xBF\x4D\xAB"          # 8  movsx ecx, [ebp+var_18]
            "\x89\x45\xAB"              # 12 mov [ebp+var_14+3], eax
            "\x8D\x45\xAB"              # 15 lea eax, [ebp+var_18]
            "\x50"                      # 18 push eax
            "\x51"                      # 19 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_C], 89h
            "\xFF\xD6"                  # 6  call esi
            "\x89\x45\xAB"              # 8  mov [ebp+var_5], eax
            "\x8D\x55\xAB"              # 11 lea edx, [ebp+var_C]
            "\x0F\xBF\x45\xAB"          # 14 movsx eax, [ebp+var_C]
            "\x52"                      # 18 push edx
            "\x50"                      # 19 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2005-10-04
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0A2h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_1C], 0A2h
            "\x89\x75\xAB"              # 11 mov [ebp+var_16], esi
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2005-10-04
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0A2h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 5  mov [ebp+var_24], 0A2h
            "\x89\x5D\xAB"              # 11 mov [ebp+var_1E], ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call CRagConnection_instanceR
            "\x8B\xC8"                  # 19 mov ecx, eax
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 15,
        }
    ],
    # 2005-10-04
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0ABh
            "\x89\x83\xAB\xAB\xAB\xAB"  # 5  mov [ebx+0FCh], eax
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_4], 0ABh
            "\x66\x89\x4D\xAB"          # 17 mov word ptr [ebp+arg_4+2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call CRagConnection_instanceR
            "\x8B\xC8"                  # 26 mov ecx, eax
            "\xE8"                      # 28 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 28,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 22,
        }
    ],
    # 2005-10-04
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 116h
            "\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebx+2F4h], 1
            "\x66\xC7\x45\xAB\xAB\xAB"  # 15 mov word ptr [ebp+var_2C], 116h
            "\x66\x89\x45\xAB"          # 21 mov [ebp+var_1B], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 25 call CRagConnection_instanceR
            "\x8B\xC8"                  # 30 mov ecx, eax
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 26,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_30], 9Fh
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5337E0
            "\x89\x45\xAB"              # 11 mov [ebp-1Fh], eax
            "\x8D\x55\xAB"              # 14 lea edx, [ebp+var_30]
            "\x0F\xBF\x45\xAB"          # 17 movsx eax, word ptr [ebp+var_30]
            "\x52"                      # 21 push edx
            "\x50"                      # 22 push eax
            "\x66\x89\x75\xAB"          # 23 mov word ptr [ebp+var_2C+3], si
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call CRagConnection_instanceR
            "\x8B\xC8"                  # 32 mov ecx, eax
            "\xE8"                      # 34 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 34,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 28,
        }
    ],
    # 2005-10-04
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 103h
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_4], 37h
            "\x89\x55\xAB"              # 12 mov [ebp+var_38+2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x85\xAB\xAB\xAB\xAB\xAB\xAB"  # 0  mov [ebp+var1], 231h
            "\xF2\xAE"                  # 9  repne scasb
            "\xF7\xD1"                  # 11 not ecx
            "\x49"                      # 13 dec ecx
            "\x83\xF9\xAB"              # 14 cmp ecx, 18h
            "\x7D\x17"                  # 17 jge short loc_5576F1
            "\x41"                      # 19 inc ecx
            "\x8D\xBD\xAB\xAB\xAB\xAB"  # 20 lea edi, [ebp+var_23A]
            "\x8B\xD1"                  # 26 mov edx, ecx
            "\xC1\xE9\xAB"              # 28 shr ecx, 2
            "\xF3\xA5"                  # 31 rep movsd
            "\x8B\xCA"                  # 33 mov ecx, edx
            "\x83\xE1\xAB"              # 35 and ecx, 3
            "\xF3\xA4"                  # 38 rep movsb
            "\xEB\x14"                  # 40 jmp short loc_557705
            "\xB9\xAB\xAB\xAB\x00"      # 42 mov ecx, 6
            "\x8D\xBD\xAB\xAB\xAB\xAB"  # 47 lea edi, [ebp+var_23A]
            "\xF3\xA5"                  # 53 rep movsd
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 55 mov [ebp+var_223], 0
            "\x0F\xBF\x8D\xAB\xAB\xAB\xAB"  # 62 movsx ecx, [ebp+var_23C]
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 69 lea eax, [ebp+var_23C]
            "\x50"                      # 75 push eax
            "\x51"                      # 76 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 77 call CRagConnection_instanceR
            "\x8B\xC8"                  # 82 mov ecx, eax
            "\xE8"                      # 84 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 84,
            "packetId": (7, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 78,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_58], 9Bh
            "\xFF\xD7"                  # 6  call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, dword_7367E4
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, dword_736124
            "\x89\x45\xAB"              # 20 mov [ebp+var_3A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, dword_7367E0
            "\x89\x4D\xAB"              # 28 mov [ebp+var_43], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_735A18
            "\x89\x45\xAB"              # 36 mov [ebp+var_51], eax
            "\x89\x55\xAB"              # 39 mov [ebp+var_3E], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_612BF0
            "\x0F\xBF\x4D\xAB"          # 47 movsx ecx, [ebp+var_58]
            "\x88\x45\xAB"              # 51 mov [ebp+var_36], al
            "\x8D\x45\xAB"              # 54 lea eax, [ebp+var_58]
            "\x50"                      # 57 push eax
            "\x51"                      # 58 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call CRagConnection_instanceR
            "\x8B\xC8"                  # 64 mov ecx, eax
            "\xE8"                      # 66 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 66,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 60,
        }
    ],
    # 2005-10-04
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 233h
            "\x89\x7D\xAB"              # 5  mov [ebp+var_A], edi
            "\x89\x75\xAB"              # 8  mov [ebp+var_6], esi
            "\xC6\x45\xAB\xAB"          # 11 mov [ebp+var_2], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 15 call CRagConnection_instanceR
            "\x8B\xC8"                  # 20 mov ecx, eax
            "\xE8"                      # 22 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 22,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 16,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+cp], 65h
            "\x66\x89\x75\xAB"          # 6  mov [ebp+var_46], si
            "\x89\x55\xAB"              # 10 mov [ebp+var_52], edx
            "\x89\x45\xAB"              # 13 mov [ebp+var_4E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_612BF0
            "\x8D\x55\xAB"              # 21 lea edx, [ebp+cp]
            "\x88\x45\xAB"              # 24 mov byte ptr [ebp+var_44], al
            "\x0F\xBF\x45\xAB"          # 27 movsx eax, word ptr [ebp+cp]
            "\x52"                      # 31 push edx
            "\x50"                      # 32 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 33 call CRagConnection_instanceR
            "\x8B\xC8"                  # 38 mov ecx, eax
            "\xE8"                      # 40 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 40,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 34,
        }
    ],
    # 2005-10-04
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x4D\xAB"          # 2  mov word ptr [ebp+var_12+3], cx
            "\x66\x89\x55\xAB"          # 6  mov [ebp+var_D], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            "\x8B\xC8"                  # 15 mov ecx, eax
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2005-10-04
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_30], 1DDh
            "\xF3\xA5"                  # 6  rep movsd
            "\x8D\x55\xAB"              # 8  lea edx, [ebp+var_12]
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 11 lea ecx, [ebp+var_88]
            "\x52"                      # 17 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call sub_41A650
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_581DE0
            "\x0F\xBF\x4D\xAB"          # 28 movsx ecx, [ebp+var_30]
            "\x88\x45\xAB"              # 32 mov [ebp+var_2], al
            "\x8D\x45\xAB"              # 35 lea eax, [ebp+var_30]
            "\x50"                      # 38 push eax
            "\x51"                      # 39 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 40 call CRagConnection_instanceR
            "\x8B\xC8"                  # 45 mov ecx, eax
            "\xE8"                      # 47 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 47,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 41,
        }
    ],
    # 2011-09-29
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 827h
            "\x66\x89\x4C\x24\xAB"      # 5  mov word ptr [esp+13Ch+Src], cx
            "\x8B\x10"                  # 10 mov edx, [eax]
            "\x8D\x44\x24\xAB"          # 12 lea eax, [esp+13Ch+Src]
            "\x50"                      # 16 push eax
            "\x89\x54\x24\xAB"          # 17 mov [esp+140h+Src+2], edx
            "\x51"                      # 21 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2011-09-29
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 846h
            "\x51"                      # 5  push ecx
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 6  lea eax, [ebx-14Dh]
            "\x52"                      # 12 push edx
            "\x66\x89\x54\x24\xAB"      # 13 mov word ptr [esp+12C4h+p], dx
            "\x66\x89\x44\x24\xAB"      # 18 mov word ptr [esp+12C4h+p+2], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2011-09-29
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8B5h
            "\x52"                      # 5  push edx
            "\x51"                      # 6  push ecx
            "\x89\x9C\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+148Ch+var1], ebx
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov [esp+148Ch+var2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 22 call CRagConnection_instanceR
            "\x8B\xC8"                  # 27 mov ecx, eax
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 23,
        }
    ],
    # 2011-09-29
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 2  mov [esp+454h+var1+3], ax
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+454h+var2+1], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2011-09-29
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 8A2h
            "\x83\xC4\xAB"              # 5  add esp, 4
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 8  mov [esp+44Ch+cp], dx
            "\xFF\xD7"                  # 16 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 18 mov ecx, dword ptr qword_8DB2F4+4
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 24 mov edx, dword ptr qword_8DAAE0
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 30 mov [esp+0D2h], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 37 mov eax, dword ptr qword_8DB2F4
            "\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 42 mov [esp+44Ch+cp+6], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 49 mov ecx, offset dword_8D9390
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 54 mov [esp+44Ch+cp+2], eax
            "\x89\x94\x24\xAB\xAB\xAB\xAB"  # 61 mov [esp+44Ch+var2], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 68 call sub_7385E0
            "\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 73 movsx ecx, [esp+44Ch+cp]
            "\x88\x84\x24\xAB\xAB\xAB\xAB"  # 81 mov ptr [esp+44Ch+var1+2], al
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 88 lea eax, [esp+44Ch+cp]
            "\x50"                      # 95 push eax
            "\x51"                      # 96 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 97 call CRagConnection_instanceR
            "\x8B\xC8"                  # 102 mov ecx, eax
            "\xE8"                      # 104 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 104,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 98,
        }
    ],
    # 2011-09-29
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 8D4h
            "\x66\x89\x4C\x24\xAB"      # 5  mov ptr [esp+148h+var_12C+2], cx
            "\x66\x89\x7C\x24\xAB"      # 10 mov ptr [esp+148h+var_128], di
            "\x66\x89\x54\x24\xAB"      # 15 mov ptr [esp+148h+var_128+2], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 20 call CRagConnection_instanceR
            "\x8B\xC8"                  # 25 mov ecx, eax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 21,
        }
    ],
    # 2011-09-29
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 65h
            "\x66\x89\x54\x24\xAB"      # 5  mov word ptr [esp+3F0h+cp], dx
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 10 mov edx, dword_8D2818
            "\x66\x89\x44\x24\xAB"      # 16 mov ptr [esp+3F0h+var_372], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 21 mov eax, dword_8D281C
            "\x89\x4C\x24\xAB"          # 26 mov [esp+3F0h+var_37E], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 30 mov ecx, offset dword_8D10C8
            "\x89\x2D\xAB\xAB\xAB\xAB"  # 35 mov dword_8A4614, ebp
            "\x89\x54\x24\xAB"          # 41 mov ptr [esp+3F0h+var_37A], edx
            "\x89\x44\x24\xAB"          # 45 mov [esp+3F0h+var_376], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 49 call sub_73CA20
            "\x0F\xBF\x54\x24\xAB"      # 54 movsx edx, word ptr [esp+3F0h+cp]
            "\x8D\x4C\x24\xAB"          # 59 lea ecx, [esp+3F0h+cp]
            "\x51"                      # 63 push ecx
            "\x52"                      # 64 push edx
            "\x88\x84\x24\xAB\xAB\xAB\xAB"  # 65 mov [esp+3F8h+var_372+2], al
            "\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            "\x8B\xC8"                  # 77 mov ecx, eax
            "\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2011-09-29
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x44\x24\xAB"      # 2  mov [esp+3F8h+var_387], ax
            "\x66\x89\x4C\x24\xAB"      # 7  mov [esp+3F8h+var_385], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2011-09-29
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1FBh
            "\x56"                      # 5  push esi
            "\x52"                      # 6  push edx
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov [esp+3FCh+var_36A], eax
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov [esp+3FCh+Dst], cx
            "\xFF\xD5"                  # 22 call ebp
            "\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 24 movsx ecx, [esp+3FCh+Dst]
            "\x83\xC4\xAB"              # 32 add esp, 0Ch
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 35 lea eax, [esp+3F0h+Dst]
            "\x50"                      # 42 push eax
            "\xC6\x84\x24\xAB\xAB\xAB\xAB\xAB"  # 43 mov [esp+3F4h+var_335], 0
            "\x51"                      # 51 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            "\x8B\xC8"                  # 57 mov ecx, eax
            "\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # 2011-09-29
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 8A2h
            "\x83\xC4\xAB"              # 5  add esp, 4
            "\x66\x89\x4C\x24\xAB"      # 8  mov word ptr [esp+3F0h+cp], cx
            "\xFF\xD7"                  # 13 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, dword_8D2818
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, dword_8D302C
            "\x89\x44\x24\xAB"          # 27 mov ptr [esp+3F0h+var_372], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, dword_8D3030
            "\x89\x4C\x24\xAB"          # 36 mov [esp+3F0h+var_376], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset dword_8D10C8
            "\x89\x54\x24\xAB"          # 45 mov [esp+3F0h+var_37E], edx
            "\x89\x44\x24\xAB"          # 49 mov ptr [esp+3F0h+var_37A], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_73CA20
            "\x88\x84\x24\xAB\xAB\xAB\xAB"  # 58 mov [esp+3F0h+var_372+4], al
            "\x0F\xBF\x44\x24\xAB"      # 65 movsx eax, word ptr [esp+3F0h+cp]
            "\x8D\x54\x24\xAB"          # 70 lea edx, [esp+3F0h+cp]
            "\x52"                      # 74 push edx
            "\x50"                      # 75 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 76 call CRagConnection_instanceR
            "\x8B\xC8"                  # 81 mov ecx, eax
            "\xE8"                      # 83 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 83,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 77,
        }
    ],
    # 2005-12-19
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_20], 0A2h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            "\x0F\xBF\x45\xAB"          # 11 movsx eax, [ebp+var_20]
            "\x8D\x55\xAB"              # 15 lea edx, [ebp+var_20]
            "\x89\x5D\xAB"              # 18 mov [ebp+var_18], ebx
            "\x52"                      # 21 push edx
            "\x50"                      # 22 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # 2005-12-19
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_28], 116h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            "\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_24+3]
            "\x51"                      # 14 push ecx
            "\x8B\xCB"                  # 15 mov ecx, ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call sub_5447E0
            "\x0F\xBF\x4D\xAB"          # 22 movsx ecx, word ptr [ebp+var_28]
            "\x66\x8B\x55\xAB"          # 26 mov dx, word ptr [ebp+arg_8]
            "\x8D\x45\xAB"              # 30 lea eax, [ebp+var_28]
            "\x50"                      # 33 push eax
            "\x51"                      # 34 push ecx
            "\x66\x89\x75\xAB"          # 35 mov word ptr [ebp+var_24+1], si
            "\x66\x89\x55\xAB"          # 39 mov word ptr [ebp+var_1C], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 43 call CRagConnection_instanceR
            "\x8B\xC8"                  # 48 mov ecx, eax
            "\xE8"                      # 50 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 50,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 44,
        }
    ],
    # 2005-12-19
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_2C], 9Fh
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            "\x8D\x45\xAB"              # 11 lea eax, [ebp+var_24]
            "\x8B\xCB"                  # 14 mov ecx, ebx
            "\x50"                      # 16 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call sub_5447E0
            "\xB9\xAB\xAB\xAB\xAB"      # 22 mov ecx, offset dword_73A978
            "\xE8\xAB\xAB\xAB\xAB"      # 27 call sub_536310
            "\x0F\xBF\x55\xAB"          # 32 movsx edx, word ptr [ebp+var_2C]
            "\x8D\x4D\xAB"              # 36 lea ecx, [ebp+var_2C]
            "\x89\x45\xAB"              # 39 mov [ebp+var_20+1], eax
            "\x51"                      # 42 push ecx
            "\x52"                      # 43 push edx
            "\x66\x89\x75\xAB"          # 44 mov word ptr [ebp+var_28+2], si
            "\xE8\xAB\xAB\xAB\xAB"      # 48 call CRagConnection_instanceR
            "\x8B\xC8"                  # 53 mov ecx, eax
            "\xE8"                      # 55 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 55,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 49,
        }
    ],
    # 2005-12-19
    [
        (
            "\x66\xC7\x45\xA8\xAB\xAB"  # 0  mov [ebp+var_58], 9Bh
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_5447E0
            "\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_48]
            "\x51"                      # 14 push ecx
            "\x8B\xCB"                  # 15 mov ecx, ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 17 call sub_5447E0
            "\x8D\x55\xAB"              # 22 lea edx, [ebp+var_42]
            "\x8B\xCB"                  # 25 mov ecx, ebx
            "\x52"                      # 27 push edx
            "\xE8\xAB\xAB\xAB\xAB"      # 28 call sub_5447E0
            "\xFF\xD6"                  # 33 call esi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 35 mov ecx, dword_73B744
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 41 mov edx, dword_73B084
            "\x89\x45\xAB"              # 47 mov [ebp+var_3C], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 50 mov eax, dword_73B740
            "\x89\x4D\xAB"              # 55 mov [ebp+var_46], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 58 mov ecx, offset dword_73A978
            "\x89\x45\xAB"              # 63 mov [ebp+var_4C], eax
            "\x89\x55\xAB"              # 66 mov [ebp+var_40], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 69 call sub_6168E0
            "\x0F\xBF\x4D\xAB"          # 74 movsx ecx, [ebp+var_58]
            "\x88\x45\xAB"              # 78 mov [ebp+var_38], al
            "\x8D\x45\xAB"              # 81 lea eax, [ebp+var_58]
            "\x50"                      # 84 push eax
            "\x51"                      # 85 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 86 call CRagConnection_instanceR
            "\x8B\xC8"                  # 91 mov ecx, eax
            "\xE8"                      # 93 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 93,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 87,
        }
    ],
    # 2005-12-19
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+cp], 65h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 6  mov [ebp+var_A], 0
            "\x89\x55\xAB"              # 12 mov [ebp+var_16], edx
            "\x89\x45\xAB"              # 15 mov [ebp+var_12], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call sub_6168E0
            "\x8D\x55\xAB"              # 23 lea edx, [ebp+cp]
            "\x88\x45\xAB"              # 26 mov byte ptr [ebp+var_8], al
            "\x0F\xBF\x45\xAB"          # 29 movsx eax, word ptr [ebp+cp]
            "\x52"                      # 33 push edx
            "\x50"                      # 34 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 35 call CRagConnection_instanceR
            "\x8B\xC8"                  # 40 mov ecx, eax
            "\xE8"                      # 42 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 42,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "instanceR": 36,
        }
    ],
    # 2014-01-08
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 970h
            "\x66\x89\x45\xAB"          # 5  mov [ebp-13h], ax
            "\x88\x4D\xAB"              # 9  mov [ebp-16h], cl
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # 2011-01-04
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 2  mov [esp+4A0h+var1+3], cx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+4A0h+var2+1], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2010-10-19
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 2  mov [esp+378h+var1], dx
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 10 mov [esp+378h+var2], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 18 call CRagConnection_instanceR
            "\x8B\xC8"                  # 23 mov ecx, eax
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 19,
        }
    ],
    # 2018-06-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+buf], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, dword ptr account_id
            "\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp-29Ah], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            "\x50"                      # 29 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_511740
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            "\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            "\x72\x02"                  # 46 jb short loc_5E0A58
            "\x8B\x00"                  # 48 mov eax, [eax]
            "\x50"                      # 50 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 51 call ds:atoi
            "\x83\xC4\x04"              # 57 add esp, 4
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov ptr [ebp+var_298+2], ax
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 67 mov [ebp+var_4], 0FFFFFFFFh
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 74 lea ecx, [ebp+var_290]
            "\xE8\xAB\xAB\xAB\xAB"      # 80 call sub_41F0C0
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 85 lea eax, [ebp+buf]
            "\x50"                      # 91 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 92 movsx eax, [ebp+buf]
            "\x50"                      # 99 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 100 call CRagConnection_instanceR
            "\x8B\xC8"                  # 105 mov ecx, eax
            "\xE8"                      # 107 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 107,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 101,
        }
    ],
    # 2018-06-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A49h
            "\x66\x89\x45\xAB"          # 5  mov word ptr [ebp+buf], ax
            "\x8D\x45\xAB"              # 9  lea eax, [ebp+Src]
            "\x0F\x43\x45\xAB"          # 12 cmovnb eax, [ebp+Src]
            "\x50"                      # 16 push eax
            "\x8D\x45\xAB"              # 17 lea eax, [ebp+Dst]
            "\x6A\xAB"                  # 20 push 10h
            "\x50"                      # 22 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call ds:strcpy_s
            "\x66\x8B\x45\xAB"          # 29 mov ax, [ebp+arg_18]
            "\x66\x89\x45\xAB"          # 33 mov [ebp+var_12], ax
            "\x83\xC4\x0C"              # 37 add esp, 0Ch
            "\x8D\x45\xAB"              # 40 lea eax, [ebp+buf]
            "\x50"                      # 43 push eax
            "\x0F\xBF\x45\xAB"          # 44 movsx eax, word ptr [ebp+buf]
            "\x50"                      # 48 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            "\x8B\xC8"                  # 54 mov ecx, eax
            "\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2018-06-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2C4h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], ax
            "\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            "\x8D\x49\xAB"              # 15 lea ecx, [ecx+0]
            "\x8A\x01"                  # 18 mov al, [ecx]
            "\x41"                      # 20 inc ecx
            "\x84\xC0"                  # 21 test al, al
            "\x75\xF9"                  # 23 jnz short loc_958080
            "\x2B\xCA"                  # 25 sub ecx, edx
            "\x83\xF9\x18"              # 27 cmp ecx, 18h
            "\x0F\x83\xAB\xAB\xAB\xAB"  # 30 jnb loc_957F5E
            "\x6A\x18"                  # 36 push 18h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 38 lea eax, [ebp+var_BF16]
            "\x53"                      # 44 push ebx
            "\x50"                      # 45 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 46 call ds:strncpy
            "\x83\xC4\x0C"              # 52 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 55 lea eax, [ebp+var1]
            "\x50"                      # 61 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 62 movsx eax, word ptr [ebp+var1]
            "\x50"                      # 69 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 70 call CRagConnection_instanceR
            "\x8B\xC8"                  # 75 mov ecx, eax
            "\xE8"                      # 77 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 77,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 71,
        }
    ],
    # 2018-06-20
    [
        (
            "\xBE\xAB\xAB\x00\x00"      # 0  mov esi, 64h
            "\x66\x89\xB5\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_33C], si
            "\x89\x85\xAB\xAB\xAB\xAB"  # 12 mov [ebp+var_33A], eax
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 18 movq [ebp+var_326], xmm0
            "\x83\xF9\x07"              # 26 cmp ecx, 7
            "\x75\x49"                  # 29 jnz short loc_9CF3FF
            "\x6A\x18"                  # 31 push 18h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 33 lea eax, [ebp+var_2DC]
            "\x50"                      # 39 push eax
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebx+138h]
            "\x50"                      # 46 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_796F90
            "\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 52 movq xmm0, [ebp+var_2DC]
            "\x66\x8B\xB5\xAB\xAB\xAB\xAB"  # 60 mov si, word ptr [ebp+var_33C]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 67 movq [ebp+var_31E], xmm0
            "\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 75 movq xmm0, [ebp+var_2D4]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 83 movq [ebp+var_316], xmm0
            "\xF3\x0F\x7E\x85\xAB\xAB\xAB\xAB"  # 91 movq xmm0, [ebp+var_2CC]
            "\x83\xC4\x0C"              # 99 add esp, 0Ch
            "\xEB\x28"                  # 102 jmp short loc_9CF427
            "\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 104 movq xmm0, [ebx+138h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 112 movq [ebp+var_31E], xmm0
            "\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 120 movq xmm0, [ebx+140h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 128 movq [ebp+var_316], xmm0
            "\xF3\x0F\x7E\x83\xAB\xAB\xAB\xAB"  # 136 movq xmm0, [ebx+148h]
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 144 movq [ebp+var_30E], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 152 call sub_9CB920
            "\x88\x85\xAB\xAB\xAB\xAB"  # 157 mov [ebp+var_306], al
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 163 lea eax, [ebp+var_33C]
            "\x50"                      # 169 push eax
            "\x0F\xBF\xC6"              # 170 movsx eax, si
            "\x50"                      # 173 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 174 call CRagConnection_instanceR
            "\x8B\xC8"                  # 179 mov ecx, eax
            "\xE8"                      # 181 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 181,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 175,
        }
    ],
    # 2018-06-20
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 1FBh
            "\x6A\xAB"                  # 5  push 32h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 7  mov word ptr [ebp+var1], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_336]
            "\x56"                      # 20 push esi
            "\x50"                      # 21 push eax
            "\xFF\xD7"                  # 22 call edi
            "\x83\xC4\x0C"              # 24 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 27 lea eax, [ebp+var1]
            "\x50"                      # 33 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 34 movsx eax, word ptr [ebp+var1]
            "\xC6\x85\xAB\xAB\xAB\xAB\xAB"  # 41 mov [ebp+var_305], 0
            "\x50"                      # 48 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 49 call CRagConnection_instanceR
            "\x8B\xC8"                  # 54 mov ecx, eax
            "\xE8"                      # 56 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 56,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 50,
        }
    ],
    # 2018-07-04
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0A49h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+p.packet_id], ax
            "\x8D\x45\xAB"              # 9  lea eax, [ebp+a1]
            "\x0F\x43\x45\xAB"          # 12 cmovnb eax, [ebp+a1]
            "\x50"                      # 16 push eax
            "\x8D\x45\xAB"              # 17 lea eax, [ebp+p.map_name]
            "\x6A\xAB"                  # 20 push 10h
            "\x50"                      # 22 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 23 call strcpy_s
            "\x8B\x45\xAB"              # 29 mov eax, [ebp+item_id]
            "\x89\x45\xAB"              # 32 mov [ebp+p.item_id], eax
            "\x83\xC4\x0C"              # 35 add esp, 0Ch
            "\x8D\x45\xAB"              # 38 lea eax, [ebp+p]
            "\x50"                      # 41 push eax
            "\x0F\xBF\x45\xAB"          # 42 movsx eax, [ebp+p.packet_id]
            "\x50"                      # 46 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call CRagConnection_instanceR
            "\x8B\xC8"                  # 52 mov ecx, eax
            "\xE8"                      # 54 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 54,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 48,
        }
    ],
    # 2018-07-04
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 83Ch
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 5  mov A, 0FFFFFFFFh
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 15 mov B, 0FFFFFFFFh
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 25 mov C, 0FFFFFFFFh
            "\x66\x89\x45\xAB"          # 35 mov word ptr [ebp+var_20], ax
            "\x8B\x46\xAB"              # 39 mov eax, [esi+4]
            "\x89\x45\xAB"              # 42 mov [ebp+var_20+2], eax
            "\x8B\x06"                  # 45 mov eax, [esi]
            "\x8D\x4E\xAB"              # 47 lea ecx, [esi+60h]
            "\x89\x45\xAB"              # 50 mov [ebp+var_1A], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call sub_508EE0
            "\x89\x45\xAB"              # 58 mov [ebp+var_16], eax
            "\x8D\x45\xAB"              # 61 lea eax, [ebp+var_20]
            "\x50"                      # 64 push eax
            "\x0F\xBF\x45\xAB"          # 65 movsx eax, word ptr [ebp+var_20]
            "\x50"                      # 69 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 70 call CRagConnection_instanceR
            "\x8B\xC8"                  # 75 mov ecx, eax
            "\xE8"                      # 77 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 77,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 71,
        }
    ],
    # 2018-07-04
    [
        (
            "\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            "\x8D\x49\xAB"              # 5  lea ecx, [ecx+0]
            "\x56"                      # 8  push esi
            "\x6A\x08"                  # 9  push 8
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 11 lea eax, [ebp+var_95C]
            "\x50"                      # 17 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 18 mov ecx, offset g_session
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CSession_sub_AC5C40
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 28 mov [ebp+var_4], 9
            "\xA1\xAB\xAB\xAB\xAB"      # 35 mov eax, g_session.m_account_id
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 40 lea ecx, [ebp+var_95C]
            "\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 46 mov [ebp+var_81C], di
            "\x89\x85\xAB\xAB\xAB\xAB"  # 53 mov dword ptr [ebp+var_81C+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 59 call sub_508EE0
            "\x89\x85\xAB\xAB\xAB\xAB"  # 64 mov [ebp+var_816], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 70 lea eax, [ebp+var_81C]
            "\x50"                      # 76 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 77 movsx eax, [ebp+var_81C]
            "\x50"                      # 84 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 85 call CRagConnection_instanceR
            "\x8B\xC8"                  # 90 mov ecx, eax
            "\xE8"                      # 92 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 92,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 86,
        }
    ],
    # 2018-07-04
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 9B0h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var_23C], ax
            "\xA1\xAB\xAB\xAB\xAB"      # 12 mov eax, g_session.m_account_id
            "\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp+var_23C+2], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 23 lea eax, [ebp+var_290]
            "\x50"                      # 29 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 30 call sub_5113C0
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 35 mov [ebp+var_4], 1
            "\x83\x78\xAB\xAB"          # 42 cmp dword ptr [eax+14h], 10h
            "\x72\x02"                  # 46 jb short loc_5E0D48
            "\x8B\xAB"                  # 48 mov eax, [eax]
            "\x50"                      # 50 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 51 call atoi
            "\x83\xC4\x04"              # 57 add esp, 4
            "\x89\x85\xAB\xAB\xAB\xAB"  # 60 mov [ebp-236h], eax
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 66 mov [ebp+var_4], 0FFFFFFFFh
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 73 lea ecx, [ebp+var_290]
            "\xE8\xAB\xAB\xAB\xAB"      # 79 call sub_41F0C0
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 84 lea eax, [ebp+var_23C]
            "\x50"                      # 90 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 91 movsx eax, word ptr [ebp+var1]
            "\x50"                      # 98 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 99 call CRagConnection_instanceR
            "\x8B\xC8"                  # 104 mov ecx, eax
            "\xE8"                      # 106 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 106,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 100,
        }
    ],
    # 2018-07-04
    [
        (
            "\xBF\xAB\xAB\x00\x00"      # 0  mov edi, 9C3h
            "\xEB\x08"                  # 5  jmp short loc_623FA0
            "\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  align 10h
            "\xA1\xAB\xAB\xAB\xAB"      # 15 mov eax, g_session.m_account_id
            "\x8D\x4E\xAB"              # 20 lea ecx, [esi+8]
            "\x66\x89\xBD\xAB\xAB\xAB\xAB"  # 23 mov word ptr [ebp+A], di
            "\x89\x85\xAB\xAB\xAB\xAB"  # 30 mov [ebp+A+2], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call sub_508EE0
            "\x89\x85\xAB\xAB\xAB\xAB"  # 41 mov [ebp+A+6], eax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 47 lea eax, [ebp+A]
            "\x50"                      # 53 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 54 movsx eax, word ptr [ebp+A]
            "\x50"                      # 61 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 62 call CRagConnection_instanceR
            "\x8B\xC8"                  # 67 mov ecx, eax
            "\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 63,
        }
    ],
    # 2018-05-10 iro
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 9EEh
            "\x8B\x48\xAB"              # 5  mov ecx, [eax+10h]
            "\x8B\x40\xAB"              # 8  mov eax, [eax+14h]
            "\x89\x45\xAB"              # 11 mov dword ptr [ebp+Src+7], eax
            "\x8D\x45\xAB"              # 14 lea eax, [ebp+Src]
            "\x50"                      # 17 push eax
            "\x52"                      # 18 push edx
            "\x66\x89\x55\xAB"          # 19 mov word ptr [ebp+Src], dx
            "\x88\x5D\xAB"              # 23 mov byte ptr [ebp+Src+2], bl
            "\x89\x4D\xAB"              # 26 mov dword ptr [ebp+Src+3], ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 29 call CRagConnection_instanceR
            "\x8B\xC8"                  # 34 mov ecx, eax
            "\xE8"                      # 36 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 36,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 30,
        }
    ],
    # 2018-05-10 iro
    [
        (
            "\x6A\xAB"                  # 0  push 7Dh
            "\x66\x0F\xD6\x85\xAB\xAB\xAB\xAB"  # 2  movq [ebp+var_19A], xmm0
            "\xE8\xAB\xAB\xAB\xAB"      # 10 call CRagConnection_instanceR
            "\x8B\xC8"                  # 15 mov ecx, eax
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 11,
        }
    ],
    # 2018-05-10 iro
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 437h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+var1], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 12 lea eax, [ebp+var_10A0+6]
            "\x50"                      # 18 push eax
            "\x6A\x00"                  # 19 push 0
            "\x51"                      # 21 push ecx
            "\x53"                      # 22 push ebx
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call sub_87A4B0
            "\x83\xC4\x10"              # 28 add esp, 10h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 31 lea eax, [ebp+var_10A0+4]
            "\x50"                      # 37 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 38 movsx eax, word ptr [ebp+var1]
            "\x50"                      # 45 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 46 call CRagConnection_instanceR
            "\x8B\xC8"                  # 51 mov ecx, eax
            "\xE8"                      # 53 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 53,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 47,
        }
    ],
    # 2018-05-10 iro
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 288h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov word ptr [ebp+A+4], ax
            "\x6A\x00"                  # 12 push 0
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 14 lea eax, [ebp+var_EC]
            "\x50"                      # 20 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 21 mov ecx, offset g_session
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call CSession_sub_938CA0
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 31 mov [ebp+var_4], 30h
            "\x66\x8B\x85\xAB\xAB\xAB\xAB"  # 38 mov ax, word ptr [ebp+var_DC]
            "\x83\xBD\xAB\xAB\xAB\xAB\xAB"  # 45 cmp [ebp+var_AC], 10h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 52 mov [ebp+var_1098], ax
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 59 lea eax, [ebp+Str]
            "\x0F\x43\x85\xAB\xAB\xAB\xAB"  # 65 cmovnb eax, [ebp+Str]
            "\x50"                      # 72 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 73 call ds:atoi
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 79 mov word ptr [ebp+A+6], ax
            "\x83\xC4\x04"              # 86 add esp, 4
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 89 lea eax, [ebp+A+4]
            "\x50"                      # 95 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 96 movsx eax, word ptr [ebp+A+4]
            "\x50"                      # 103 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 104 call CRagConnection_instanceR
            "\x8B\xC8"                  # 109 mov ecx, eax
            "\xE8"                      # 111 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 111,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 105,
        }
    ],
    # 2018-05-10 iro
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 8A3h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\x83\xC8\xAB"              # 9  or eax, 0FFFFFFFFh
            "\x38\x0D\xAB\xAB\xAB\xAB"  # 12 cmp byte ptr g_session+3EF0h, cl
            "\x0F\x45\xF0"              # 18 cmovnz esi, eax
            "\x8D\x45\xAB"              # 21 lea eax, [ebp+var_30]
            "\x50"                      # 24 push eax
            "\x56"                      # 25 push esi
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_80FC80
            "\x8D\x45\xAB"              # 31 lea eax, [ebp+var_24+4]
            "\x50"                      # 34 push eax
            "\xFF\x75\xAB"              # 35 push [ebp+arg_C]
            "\xE8\xAB\xAB\xAB\xAB"      # 38 call sub_80FC80
            "\x66\x8B\x45\xAB"          # 43 mov ax, word ptr [ebp+arg_8]
            "\x66\x89\x45\xAB"          # 47 mov [ebp+var_32], ax
            "\x83\xC4\x10"              # 51 add esp, 10h
            "\x8D\x45\xAB"              # 54 lea eax, [ebp+Src]
            "\x50"                      # 57 push eax
            "\x0F\xBF\x45\xAB"          # 58 movsx eax, [ebp+Src]
            "\x50"                      # 62 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 63 call CRagConnection_instanceR
            "\x8B\xC8"                  # 68 mov ecx, eax
            "\xE8"                      # 70 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 70,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 64,
        }
    ],
    # aro
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0ABh
            "\x52"                      # 5  push edx
            "\x50"                      # 6  push eax
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov ptr [esp+var1], ax
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 15 mov ptr [esp+var2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 23 call CRagConnection_instanceR
            "\x8B\xC8"                  # 28 mov ecx, eax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 24,
        }
    ],
    # aro
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 85h
            "\x53"                      # 5  push ebx
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 6  mov word [esp+var1], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 14 call sub_6C7060
            "\x0F\xBF\x84\x24\xAB\xAB\xAB\xAB"  # 19 movsx eax, word [esp+var1]
            "\x83\xC4\x10"              # 27 add esp, 10h
            "\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 30 lea edx, [esp+var2]
            "\x52"                      # 37 push edx
            "\x50"                      # 38 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 39 call CRagConnection_instanceR
            "\x8B\xC8"                  # 44 mov ecx, eax
            "\xE8"                      # 46 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 46,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 40,
        }
    ],
    # aro
    [
        (
            "\xBA\xAB\xAB\x00\x00"      # 0  mov edx, 288h
            "\x50"                      # 5  push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 6  mov ecx, offset g_session
            "\x66\x89\x94\x24\xAB\xAB\xAB\xAB"  # 11 mov word [esp+var3], dx
            "\xE8\xAB\xAB\xAB\xAB"      # 19 call CSession_sub_7345B0
            "\xC7\x84\x24\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 24 mov [esp+v4],
            "\x83\xBC\x24\xAB\xAB\xAB\xAB\xAB"  # 35 cmp dword [esp+var5], 10h
            "\x66\x8B\x8C\x24\xAB\xAB\xAB\xAB"  # 43 mov cx, word [esp+var2]
            "\x8B\x84\x24\xAB\xAB\xAB\xAB"  # 51 mov eax, [esp+var7]
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 58 mov word [esp+var6], cx
            "\x73\x07"                  # 66 jnb short loc_6CFC48
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 68 lea eax, [esp+1274h+Str]
            "\x50"                      # 75 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 76 call ds:atoi
            "\x66\x89\x84\x24\xAB\xAB\xAB\xAB"  # 82 mov word [esp+var1+2], ax
            "\x0F\xBF\x84\x24\xAB\xAB\xAB\xAB"  # 90 movsx eax, word [esp+var1]
            "\x83\xC4\x04"              # 98 add esp, 4
            "\x8D\x94\x24\xAB\xAB\xAB\xAB"  # 101 lea edx, [esp+1274h+var_104C]
            "\x52"                      # 108 push edx
            "\x50"                      # 109 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 110 call CRagConnection_instanceR
            "\x8B\xC8"                  # 115 mov ecx, eax
            "\xE8"                      # 117 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 117,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 111,
        }
    ],
    # aro
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 23Bh
            "\x66\x89\x44\x24\xAB"      # 5  mov word ptr [esp+48h+Src], ax
            "\x74\x03"                  # 10 jz short loc_71105E
            "\x83\xCF\xAB"              # 12 or edi, 0FFFFFFFFh
            "\x8D\x4C\x24\xAB"          # 15 lea ecx, [esp+48h+var_30]
            "\x51"                      # 19 push ecx
            "\x57"                      # 20 push edi
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_65FA60
            "\x8B\x44\x24\xAB"          # 26 mov eax, [esp+50h+arg_C]
            "\x8D\x54\x24\xAB"          # 30 lea edx, [esp+50h+var_20]
            "\x52"                      # 34 push edx
            "\x50"                      # 35 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 36 call sub_65FA60
            "\x0F\xBF\x44\x24\xAB"      # 41 movsx eax, word ptr [esp+58h+Src]
            "\x66\x8B\x4C\x24\xAB"      # 46 mov cx, word ptr [esp+58h+arg_8]
            "\x83\xC4\x10"              # 51 add esp, 10h
            "\x8D\x54\x24\xAB"          # 54 lea edx, [esp+48h+Src]
            "\x52"                      # 58 push edx
            "\x50"                      # 59 push eax
            "\x66\x89\x4C\x24\xAB"      # 60 mov word ptr [esp+50h+Src+2], cx
            "\xE8\xAB\xAB\xAB\xAB"      # 65 call CRagConnection_instanceR
            "\x8B\xC8"                  # 70 mov ecx, eax
            "\xE8"                      # 72 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 72,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 66,
        }
    ],
    # aro
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 65h
            "\x66\x89\x4C\x24\xAB"      # 5  mov [esp+48h], cx
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 10 mov ecx, g_session.m_auth_code1
            "\x33\xD2"                  # 16 xor edx, edx
            "\x66\x89\x54\x24\xAB"      # 18 mov word ptr [esp+16Ch+var1], dx
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 23 mov edx, dword ptr g_session+1854h
            "\x89\x4C\x24\xAB"          # 29 mov [esp+16Ch+var_11E], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 33 mov ecx, offset g_session
            "\xC7\x05\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 38 mov A, 1
            "\x89\x44\x24\xAB"          # 48 mov [esp+4Ah], eax
            "\x89\x54\x24\xAB"          # 52 mov [esp+16Ch+var_11A], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 56 call CSession_GetSex
            "\x0F\xBF\x4C\x24\xAB"      # 61 movsx ecx, word ptr [esp+48h]
            "\x88\x44\x24\xAB"          # 66 mov [esp+16Ch+var_116+2], al
            "\x8D\x44\x24\xAB"          # 70 lea eax, [esp+48h]
            "\x50"                      # 74 push eax
            "\x51"                      # 75 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 76 call CRagConnection_instanceR
            "\x8B\xC8"                  # 81 mov ecx, eax
            "\xE8"                      # 83 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 83,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 77,
        }
    ],
    # aro
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\x66\x89\x54\x24\xAB"      # 2  mov [esp+174h+p.hair_color], dx
            "\x66\x89\x44\x24\xAB"      # 7  mov [esp+174h+p.hair_style], ax
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
    # aro
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 1FBh
            "\x57"                      # 5  push edi
            "\x52"                      # 6  push edx
            "\x89\x84\x24\xAB\xAB\xAB\xAB"  # 7  mov dword ptr [esp+D], eax
            "\x66\x89\x8C\x24\xAB\xAB\xAB\xAB"  # 14 mov word ptr [esp+C], cx
            "\xFF\xD3"                  # 22 call ebx
            "\x0F\xBF\x8C\x24\xAB\xAB\xAB\xAB"  # 24 movsx ecx, word [esp+B]
            "\x83\xC4\x0C"              # 32 add esp, 0Ch
            "\x8D\x84\x24\xAB\xAB\xAB\xAB"  # 35 lea eax, [esp+16Ch+pncb]
            "\x50"                      # 42 push eax
            "\xC6\x84\x24\xAB\xAB\xAB\xAB\xAB"  # 43 mov [esp+A], 0
            "\x51"                      # 51 push ecx
            "\xE8\xAB\xAB\xAB\xAB"      # 52 call CRagConnection_instanceR
            "\x8B\xC8"                  # 57 mov ecx, eax
            "\xE8"                      # 59 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 59,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 53,
        }
    ],
    # aro
    [
        (
            "\xB9\xAB\xAB\x00\x00"      # 0  mov ecx, 72h
            "\x83\xC4\x04"              # 5  add esp, 4
            "\x66\x89\x4C\x24\xAB"      # 8  mov [esp+48h], cx
            "\xFF\xD5"                  # 13 call ebp
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 15 mov ecx, g_session.m_auth_code1
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 21 mov edx, g_session.m_account_id
            "\x89\x44\x24\xAB"          # 27 mov dword ptr [esp+16Ch+var1], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 31 mov eax, g_session.m_char_id
            "\x89\x4C\x24\xAB"          # 36 mov [esp+16Ch+var_11A], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 40 mov ecx, offset g_session
            "\x89\x54\x24\xAB"          # 45 mov [esp+4Ah], edx
            "\x89\x44\x24\xAB"          # 49 mov [esp+16Ch+var_11E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 53 call CSession_GetSex
            "\x88\x44\x24\xAB"          # 58 mov [esp+16Ch+var1+4], al
            "\x0F\xBF\x44\x24\xAB"      # 62 movsx eax, word ptr [esp+48h]
            "\x8D\x54\x24\xAB"          # 67 lea edx, [esp+48h]
            "\x52"                      # 71 push edx
            "\x50"                      # 72 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 73 call CRagConnection_instanceR
            "\x8B\xC8"                  # 78 mov ecx, eax
            "\xE8"                      # 80 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 80,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 74,
        }
    ],
    # 2017-10-18 zero
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 2C4h
            "\x66\x89\x85\xAB\xAB\xAB\xAB"  # 5  mov [ebp+var_BF18], ax
            "\x8D\x51\xAB"              # 12 lea edx, [ecx+1]
            "\xEB\x03"                  # 15 jmp short loc_9E7B80
            "\xAB\xAB\xAB"              # 17 align 10h
            "\x8A\x01"                  # 20 mov al, [ecx]
            "\x41"                      # 22 inc ecx
            "\x84\xC0"                  # 23 test al, al
            "\x75\xF9"                  # 25 jnz short loc_9E7B80
            "\x2B\xCA"                  # 27 sub ecx, edx
            "\x83\xF9\xAB"              # 29 cmp ecx, 18h
            "\x0F\x83\xAB\xAB\xAB\xAB"  # 32 jnb loc_9E7A5E
            "\x6A\xAB"                  # 38 push 18h
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 40 lea eax, [ebp+var_BF16]
            "\x53"                      # 46 push ebx
            "\x50"                      # 47 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 48 call ds:strncpy
            "\x83\xC4\x0C"              # 54 add esp, 0Ch
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 57 lea eax, [ebp+var_BF18]
            "\x50"                      # 63 push eax
            "\x0F\xBF\x85\xAB\xAB\xAB\xAB"  # 64 movsx eax, [ebp+var_BF18]
            "\x50"                      # 71 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 72 call CRagConnection_instanceR
            "\x8B\xC8"                  # 77 mov ecx, eax
            "\xE8"                      # 79 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 79,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 73,
        }
    ],
    # 2017-10-18 zero
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 436h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\xFF\xD7"                  # 9  call edi
            "\x89\x45\xAB"              # 11 mov [ebp+var_2E], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 14 mov eax, cchWideChar
            "\x89\x45\xAB"              # 19 mov [ebp+var_3A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 22 mov eax, dword_106A698
            "\x89\x45\xAB"              # 27 mov [ebp+var_36], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 30 mov eax, dword_1069F58
            "\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_1069218
            "\x89\x45\xAB"              # 40 mov [ebp+var_32], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 43 call sub_ABDAC0
            "\x88\x45\xAB"              # 48 mov [ebp+var_2A], al
            "\x8D\x45\xAB"              # 51 lea eax, [ebp+Src]
            "\x50"                      # 54 push eax
            "\x0F\xBF\x45\xAB"          # 55 movsx eax, [ebp+Src]
            "\x50"                      # 59 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 60 call CRagConnection_instanceR
            "\x8B\xC8"                  # 65 mov ecx, eax
            "\xE8"                      # 67 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 67,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 61,
        }
    ],
    # 2017-10-18 zero
    [
        (
            "\xB8\xAB\xAB\x00\x00"      # 0  mov eax, 0AD0h
            "\x66\x89\x45\xAB"          # 5  mov [ebp+Src], ax
            "\x6A\xAB"                  # 9  push 9
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 11 lea eax, [ebx+1B8h]
            "\x50"                      # 17 push eax
            "\x8D\x45\xAB"              # 18 lea eax, [ebp+Dst]
            "\x6A\xAB"                  # 21 push 9
            "\x50"                      # 23 push eax
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 24 call ds:memcpy_s
            "\x83\xC4\x10"              # 30 add esp, 10h
            "\x8D\x45\xAB"              # 33 lea eax, [ebp+Src]
            "\x50"                      # 36 push eax
            "\x0F\xBF\x45\xAB"          # 37 movsx eax, [ebp+Src]
            "\x50"                      # 41 push eax
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call CRagConnection_instanceR
            "\x8B\xC8"                  # 47 mov ecx, eax
            "\xE8"                      # 49 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 49,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 43,
        }
    ],
    # 2017-10-18 zero
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 233h
            "\x89\x7D\xAB"              # 5  mov dword ptr [ebp+Src+6], edi
            "\xC6\x45\xAB\xAB"          # 8  mov [ebp+var_6], 0
            "\xE8\xAB\xAB\xAB\xAB"      # 12 call CRagConnection_instanceR
            "\x8B\xC8"                  # 17 mov ecx, eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "instanceR": 13,
        }
    ],
]
