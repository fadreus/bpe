#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2016 - 2018
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x86\xAB\xAB\xAB\xAB"  # 2  lea eax, [esi+CSession.m_var]
            "\x50"                      # 8  push eax
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aIseffecton
            "\xFF\xB5\xAB\xAB\xAB\xAB"  # 18 push [ebp+phkResult]
            "\xFF\xD3"                  # 24 call ebx
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (14, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x96\xAB\xAB\xAB\xAB"  # 2  lea edx, [esi+4E2Ch]
            "\x52"                      # 8  push edx
            "\x6A\x04"                  # 9  push 4
            "\x50"                      # 11 push eax
            "\x8B\x85\xAB\xAB\xAB\xAB"  # 12 mov eax, [ebp+phkResult]
            "\x68\xAB\xAB\xAB\xAB"      # 18 push offset aIseffecton
            "\x50"                      # 23 push eax
            "\xFF\xD3"                  # 24 call ebx
        ),
        {
            "fixedOffset": 19,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (19, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x85\xAB\xAB\xAB\xAB"  # 0  mov eax, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x96\xAB\xAB\xAB\xAB"  # 8  lea edx, [esi+0C8Ch]
            "\x52"                      # 14 push edx
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aM_monstersnapo
            "\x50"                      # 24 push eax
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4)
        },
        {
            "strOffset": (20, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x95\xAB\xAB\xAB\xAB"  # 0  mov edx, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x8E\xAB\xAB\xAB\xAB"  # 8  lea ecx, [esi+0C90h]
            "\x51"                      # 14 push ecx
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aM_monstersna_0
            "\x52"                      # 24 push edx
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4)
        },
        {
            "strOffset": (20, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x8D\xAB\xAB\xAB\xAB"  # 0  mov ecx, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x86\xAB\xAB\xAB\xAB"  # 8  lea eax, [esi+0C84h]
            "\x50"                      # 14 push eax
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aM_isitemsnap
            "\x51"                      # 24 push ecx
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4)
        },
        {
            "strOffset": (20, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x86\xAB\xAB\xAB\xAB"  # 2  lea eax, [esi+4E5Ch]
            "\x50"                      # 8  push eax
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x8B\x8D\xAB\xAB\xAB\xAB"  # 13 mov ecx, [ebp+phkResult]
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aM_bautoopendet
            "\x51"                      # 24 push ecx
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (20, False)
        }
    ],
    # 2013-01-03
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 2  lea ecx, [ebp+3678h]
            "\x51"                      # 8  push ecx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aIseffecton
            "\x52"                      # 18 push edx
            "\x89\x44\x24\xAB"          # 19 mov dword ptr [esp+33Ch+Data], eax
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (14, False)
        }
    ],
    # 2013-01-03
    [
        (
            "\x8B\x4C\x24\xAB"          # 0  mov ecx, [esp+324h+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x85\xAB\xAB\xAB\xAB"  # 6  lea eax, [ebp+0C70h]
            "\x50"                      # 12 push eax
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_monstersnapo
            "\x51"                      # 22 push ecx
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4)
        },
        {
            "strOffset": (18, False)
        }
    ],
    # 2013-01-03
    [
        (
            "\x8B\x44\x24\xAB"          # 0  mov eax, [esp+324h+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x95\xAB\xAB\xAB\xAB"  # 6  lea edx, [ebp+0C74h]
            "\x52"                      # 12 push edx
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_monstersna_0
            "\x50"                      # 22 push eax
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4)
        },
        {
            "strOffset": (18, False)
        }
    ],
    # 2013-01-03
    [
        (
            "\x8B\x54\x24\xAB"          # 0  mov edx, [esp+324h+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 6  lea ecx, [ebp+0C68h]
            "\x51"                      # 12 push ecx
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_isitemsnap
            "\x52"                      # 22 push edx
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4)
        },
        {
            "strOffset": (18, False)
        }
    ],
    # 2013-01-15
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x86\xAB\xAB\xAB\xAB"  # 2  lea eax, [esi+3694h]
            "\x50"                      # 8  push eax
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aIseffecton
            "\x51"                      # 18 push ecx
            "\x89\x95\xAB\xAB\xAB\xAB"  # 19 mov dword ptr [ebp+Data], edx
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (14, False)
        }
    ],
    # 2010-01-05
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x8E\xAB\xAB\xAB\xAB"  # 2  lea ecx, [esi+2B10h]
            "\x89\x45\xAB"              # 8  mov dword ptr [ebp+Data], eax
            "\x51"                      # 11 push ecx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aIseffecton
            "\x52"                      # 21 push edx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x4D\xAB"              # 0  mov ecx, [ebp+phkResult]
            "\x8D\x86\xAB\xAB\xAB\xAB"  # 3  lea eax, [esi+1120h]
            "\x6A\x04"                  # 9  push 4
            "\x50"                      # 11 push eax
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_monstersnapo
            "\x51"                      # 21 push ecx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x45\xAB"              # 0  mov eax, [ebp+phkResult]
            "\x8D\x96\xAB\xAB\xAB\xAB"  # 3  lea edx, [esi+1124h]
            "\x6A\x04"                  # 9  push 4
            "\x52"                      # 11 push edx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_monstersna_0
            "\x50"                      # 21 push eax
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x55\xAB"              # 0  mov edx, [ebp+phkResult]
            "\x8D\x8E\xAB\xAB\xAB\xAB"  # 3  lea ecx, [esi+1118h]
            "\x6A\x04"                  # 9  push 4
            "\x51"                      # 11 push ecx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_isitemsnap
            "\x52"                      # 21 push edx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2009-01-02
    [
        (
            "\x8B\x55\xAB"              # 0  mov edx, [ebp+phkResult]
            "\x81\xC6\xAB\xAB\xAB\xAB"  # 3  add esi, 2454h
            "\x6A\x04"                  # 9  push 4
            "\x56"                      # 11 push esi
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_isnoctrl
            "\x52"                      # 21 push edx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2009-05-20
    [
        (
            "\x8B\x4C\x24\xAB"          # 0  mov ecx, [esp+32Ch+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x81\xC5\xAB\xAB\xAB\xAB"  # 6  add ebp, 2BD8h
            "\x55"                      # 12 push ebp
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_isnoctrl
            "\x51"                      # 22 push ecx
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4)
        },
        {
            "strOffset": (18, False)
        }
    ],
    # 2007-01-02
    [
        (
            "\x8D\x86\xAB\xAB\xAB\xAB"  # 0  lea eax, [esi+1BB4h]
            "\x6A\x04"                  # 6  push 4
            "\x50"                      # 8  push eax
            "\x6A\x04"                  # 9  push 4
            "\x8B\x4D\xAB"              # 11 mov ecx, [ebp+phkResult]
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_bautoopendet
            "\x51"                      # 21 push ecx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (2, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2007-01-02
    [
        (
            "\x8B\x45\xAB"              # 0  mov eax, [ebp+phkResult]
            "\x81\xC6\xAB\xAB\xAB\xAB"  # 3  add esi, 1D00h
            "\x6A\x04"                  # 9  push 4
            "\x56"                      # 11 push esi
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aOnhouserai
            "\x50"                      # 21 push eax
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2006-04-25
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x8F\xAB\xAB\xAB\xAB"  # 2  lea ecx, [edi+1B10h]
            "\x89\x45\xAB"              # 8  mov dword ptr [ebp+Data], eax
            "\x51"                      # 11 push ecx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aIseffecton
            "\x52"                      # 21 push edx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (4, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2006-04-25
    [
        (
            "\x8B\x4D\xAB"              # 0  mov ecx, [ebp+phkResult]
            "\x8D\x87\xAB\xAB\xAB\xAB"  # 3  lea eax, [edi+7DCh]
            "\x6A\x04"                  # 9  push 4
            "\x50"                      # 11 push eax
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_monstersnapo
            "\x51"                      # 21 push ecx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2006-04-25
    [
        (
            "\x8B\x45\xAB"              # 0  mov eax, [ebp+phkResult]
            "\x8D\x97\xAB\xAB\xAB\xAB"  # 3  lea edx, [edi+7E0h]
            "\x6A\x04"                  # 9  push 4
            "\x52"                      # 11 push edx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_monstersna_0
            "\x50"                      # 21 push eax
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2006-04-25
    [
        (
            "\x8B\x55\xAB"              # 0  mov edx, [ebp+phkResult]
            "\x8D\x8F\xAB\xAB\xAB\xAB"  # 3  lea ecx, [edi+7D4h]
            "\x6A\x04"                  # 9  push 4
            "\x51"                      # 11 push ecx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_isitemsnap
            "\x52"                      # 21 push edx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2006-04-25
    [
        (
            "\x8D\x87\xAB\xAB\xAB\xAB"  # 0  lea eax, [edi+1B34h]
            "\x6A\x04"                  # 6  push 4
            "\x50"                      # 8  push eax
            "\x6A\x04"                  # 9  push 4
            "\x8B\x4D\xAB"              # 11 mov ecx, [ebp+phkResult]
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_bautoopendet
            "\x51"                      # 21 push ecx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (2, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2006-04-25
    [
        (
            "\x8B\x45\xAB"              # 0  mov eax, [ebp+phkResult]
            "\x81\xC7\xAB\xAB\xAB\xAB"  # 3  add edi, 1C78h
            "\x6A\x04"                  # 9  push 4
            "\x57"                      # 11 push edi
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aOnhouserai
            "\x50"                      # 21 push eax
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2018-06-05
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x46\xAB"              # 2  lea eax, [esi+60h]
            "\x50"                      # 5  push eax
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_bshowskillde
            "\xFF\xB5\xAB\xAB\xAB\xAB"  # 15 push [ebp+phkResult]
            "\xFF\xD3"                  # 21 call ebx
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1)
        },
        {
            "strOffset": (11, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x95\xAB\xAB\xAB\xAB"  # 0  mov edx, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x81\xC6\xAB\xAB\xAB\xAB"  # 8  add esi, 55CCh
            "\x56"                      # 14 push esi
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aM_blockmouse
            "\x52"                      # 24 push edx
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4)
        },
        {
            "strOffset": (20, False)
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x85\xAB\xAB\xAB\xAB"  # 0  mov eax, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x81\xC6\xAB\xAB\xAB\xAB"  # 8  add esi, 4DF0h
            "\x56"                      # 14 push esi
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aFog_0
            "\x50"                      # 24 push eax
            "\xFF\xD3"                  # 25 call ebx
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4)
        },
        {
            "strOffset": (20, False)
        }
    ],
    # 2013-01-03
    [
        (
            "\x8B\x54\x24\xAB"          # 0  mov edx, [esp+324h+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x81\xC5\xAB\xAB\xAB\xAB"  # 6  add ebp, 3E20h
            "\x55"                      # 12 push ebp
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_blockmouse
            "\x52"                      # 22 push edx
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4)
        },
        {
            "strOffset": (18, False)
        }
    ],
    # 2010-01-05
    [
        (
            "\x81\xC6\xAB\xAB\xAB\xAB"  # 0  add esi, 2DFCh
            "\x6A\x04"                  # 6  push 4
            "\x56"                      # 8  push esi
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x8B\x4D\xAB"              # 13 mov ecx, [ebp+phkResult]
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_blockmouse
            "\x51"                      # 21 push ecx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (2, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2009-01-07
    [
        (
            "\x8B\x4D\xAB"              # 0  mov ecx, [ebp+phkResult]
            "\x81\xC6\xAB\xAB\xAB\xAB"  # 3  add esi, 9FDh
            "\x6A\x04"                  # 9  push 4
            "\x56"                      # 11 push esi
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aM_bshowskillde
            "\x51"                      # 21 push ecx
            "\xFF\xD3"                  # 22 call ebx
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4)
        },
        {
            "strOffset": (17, False)
        }
    ],
    # 2013-05-23 iro
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x45\xAB"              # 2  lea eax, [ebp+74h]
            "\x50"                      # 5  push eax
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_bshowskillde
            "\x51"                      # 15 push ecx
            "\xFF\xD3"                  # 16 call ebx
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2013-05-23 iro
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x4D\xAB"              # 2  lea ecx, [ebp+70h]
            "\x51"                      # 5  push ecx
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_issimpleskil
            "\x52"                      # 15 push edx
            "\xFF\xD3"                  # 16 call ebx
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2013-08-30 iro
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x81\xC5\xAB\xAB\x00\x00"  # 2  add ebp, 3C14h
            "\x55"                      # 8  push ebp
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x8B\x44\x24\xAB"          # 13 mov eax, [esp+334h+phkResult]
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aM_blockmouse
            "\x50"                      # 22 push eax
            "\xFF\xD3"                  # 23 call ebx
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (4, 4),
        },
        {
            "strOffset": (18, False),
        }
    ],
]
