#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2016 - 2018
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x68\xAB\xAB\xAB\xAB"      # 2  push (offset g_session+5244h)
            "\x6A\x04"                  # 7  push 4
            "\x6A\x00"                  # 9  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 11 push offset aMemorialdunwnd
            "\xFF\xB5\xAB\xAB\xAB\xAB"  # 16 push [ebp+phkResult]
            "\xFF\xD6"                  # 22 call esi
        ),
        {
            "fixedOffset": 12,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (12, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x68\xAB\xAB\xAB\xAB"      # 2  push offset Data
            "\x6A\x04"                  # 7  push 4
            "\x6A\x00"                  # 9  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 11 push offset aMemorialdunwnd
            "\x50"                      # 16 push eax
            "\xFF\xD6"                  # 17 call esi
        ),
        {
            "fixedOffset": 12,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (12, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x68\xAB\xAB\xAB\xAB"      # 2  push offset dword_CDD52C
            "\x6A\x04"                  # 7  push 4
            "\x6A\x00"                  # 9  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 11 push offset aMemorialdunw_0
            "\x51"                      # 16 push ecx
            "\xFF\xD6"                  # 17 call esi
        ),
        {
            "fixedOffset": 12,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (12, False),
        }
    ],
    # 2014-01-08
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x68\xAB\xAB\xAB\xAB"      # 2  push offset dword_BD5A9C
            "\x6A\x04"                  # 7  push 4
            "\x6A\x00"                  # 9  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 11 push offset aMemorialdunw_0
            "\x52"                      # 16 push edx
            "\xFF\xD6"                  # 17 call esi
        ),
        {
            "fixedOffset": 12,
            "retOffset": 0,
            "memberOffset": (3, 4),
        },
        {
            "strOffset": (12, False),
        }
    ],
]
