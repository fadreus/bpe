#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2016 - 2018
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x43\xAB"              # 2  lea eax, [ebx+4Ch]
            "\x50"                      # 5  push eax
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_minimapzoomw
            "\xFF\xB5\xAB\xAB\xAB\xAB"  # 15 push [ebp+phkResult]
            "\xFF\xD6"                  # 21 call esi
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2016 - 2018
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 2  lea eax, [ebx+3BAh]
            "\x50"                      # 8  push eax
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aOnbtsubchat
            "\xFF\xB5\xAB\xAB\xAB\xAB"  # 18 push [ebp+phkResult]
            "\xFF\xD6"                  # 24 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (4, 4),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2016 - 2018
    [
        (
            "\x8D\x43\xAB"              # 0  lea eax, [ebx+54h]
            "\x6A\x04"                  # 3  push 4
            "\x50"                      # 5  push eax
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aItemstorewndnu
            "\xFF\xB5\xAB\xAB\xAB\xAB"  # 15 push [ebp+phkResult]
            "\xFF\xD6"                  # 21 call esi
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (2, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8D\x43\xAB"              # 0  lea eax, [ebx+60h]
            "\x50"                      # 3  push eax
            "\x6A\x04"                  # 4  push 4
            "\x57"                      # 6  push edi
            "\x68\xAB\xAB\xAB\xAB"      # 7  push offset aM_minimapzoomw
            "\x51"                      # 12 push ecx
            "\xFF\xD6"                  # 13 call esi
        ),
        {
            "fixedOffset": 8,
            "retOffset": 0,
            "memberOffset": (2, 1),
        },
        {
            "strOffset": (8, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x95\xAB\xAB\xAB\xAB"  # 0  mov edx, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x8B\xAB\xAB\xAB\xAB"  # 8  lea ecx, [ebx+3DEh]
            "\x51"                      # 14 push ecx
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aOnbtsubchat
            "\x52"                      # 24 push edx
            "\xFF\xD6"                  # 25 call esi
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4),
        },
        {
            "strOffset": (20, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x95\xAB\xAB\xAB\xAB"  # 0  mov edx, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x4B\xAB"              # 8  lea ecx, [ebx+6Ch]
            "\x51"                      # 11 push ecx
            "\x6A\x04"                  # 12 push 4
            "\x57"                      # 14 push edi
            "\x68\xAB\xAB\xAB\xAB"      # 15 push offset aM_isdrawcompas
            "\x52"                      # 20 push edx
            "\xFF\xD6"                  # 21 call esi
        ),
        {
            "fixedOffset": 16,
            "retOffset": 0,
            "memberOffset": (10, 1),
        },
        {
            "strOffset": (16, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x85\xAB\xAB\xAB\xAB"  # 0  mov eax, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x53\xAB"              # 8  lea edx, [ebx+44h]
            "\x52"                      # 11 push edx
            "\x6A\x04"                  # 12 push 4
            "\x57"                      # 14 push edi
            "\x68\xAB\xAB\xAB\xAB"      # 15 push offset aM_chatwndy
            "\x50"                      # 20 push eax
            "\xFF\xD6"                  # 21 call esi
        ),
        {
            "fixedOffset": 16,
            "retOffset": 0,
            "memberOffset": (10, 1),
        },
        {
            "strOffset": (16, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x85\xAB\xAB\xAB\xAB"  # 0  mov eax, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x93\xAB\xAB\xAB\xAB"  # 8  lea edx, [ebx+3DDh]
            "\x52"                      # 14 push edx
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aOnstsubchat
            "\x50"                      # 24 push eax
            "\xFF\xD6"                  # 25 call esi
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4),
        },
        {
            "strOffset": (20, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x8D\xAB\xAB\xAB\xAB"  # 0  mov ecx, [ebp+phkResult]
            "\x6A\x04"                  # 6  push 4
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 8  lea eax, [ebx+3DCh]
            "\x50"                      # 14 push eax
            "\x6A\x04"                  # 15 push 4
            "\x6A\x00"                  # 17 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 19 push offset aChatwndstickon
            "\x51"                      # 24 push ecx
            "\xFF\xD6"                  # 25 call esi
        ),
        {
            "fixedOffset": 20,
            "retOffset": 0,
            "memberOffset": (10, 4),
        },
        {
            "strOffset": (20, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x4C\x24\xAB"          # 0  mov ecx, [esp+31Ch+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 6  lea eax, [ebx+3CEh]
            "\x50"                      # 12 push eax
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aOnbtsubchat
            "\x51"                      # 22 push ecx
            "\xFF\xD6"                  # 23 call esi
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4),
        },
        {
            "strOffset": (18, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x54\x24\xAB"          # 0  mov edx, [esp+31Ch+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x4B\xAB"              # 6  lea ecx, [ebx+64h]
            "\x51"                      # 9  push ecx
            "\x6A\x04"                  # 10 push 4
            "\x57"                      # 12 push edi
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aM_isdrawcompas
            "\x52"                      # 18 push edx
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (8, 1),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x44\x24\xAB"          # 0  mov eax, [esp+31Ch+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x53\xAB"              # 6  lea edx, [ebx+3Ch]
            "\x52"                      # 9  push edx
            "\x6A\x04"                  # 10 push 4
            "\x57"                      # 12 push edi
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aM_chatwndy
            "\x50"                      # 18 push eax
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (8, 1),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2015-01-07
    [
        (
            "\x8B\x54\x24\xAB"          # 0  mov edx, [esp+31Ch+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x8B\xAB\xAB\xAB\xAB"  # 6  lea ecx, [ebx+3CDh]
            "\x51"                      # 12 push ecx
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aOnstsubchat
            "\x52"                      # 22 push edx
            "\xFF\xD6"                  # 23 call es
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4),
        },
        {
            "strOffset": (18, False),
        }
    ],
    # 2013-01-03
    [
        (
            "\x8B\x44\x24\xAB"          # 0  mov eax, [esp+31Ch+phkResult]
            "\x6A\x04"                  # 4  push 4
            "\x8D\x93\xAB\xAB\xAB\xAB"  # 6  lea edx, [ebx+3CCh]
            "\x52"                      # 12 push edx
            "\x6A\x04"                  # 13 push 4
            "\x6A\x00"                  # 15 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 17 push offset aChatwndstickon
            "\x50"                      # 22 push eax
            "\xFF\xD6"                  # 23 call esi
        ),
        {
            "fixedOffset": 18,
            "retOffset": 0,
            "memberOffset": (8, 4),
        },
        {
            "strOffset": (18, False),
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x55\xAB"              # 0  mov edx, [ebp+phkResult]
            "\x8D\x8B\xAB\xAB\xAB\xAB"  # 3  lea ecx, [ebx+3AEh]
            "\x6A\x04"                  # 9  push 4
            "\x51"                      # 11 push ecx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aOnbtsubchat
            "\x52"                      # 21 push edx
            "\xFF\xD6"                  # 22 call esi
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4),
        },
        {
            "strOffset": (17, False),
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x55\xAB"              # 0  mov edx, [ebp+phkResult]
            "\x8D\x4B\xAB"              # 3  lea ecx, [ebx+68h]
            "\x6A\x04"                  # 6  push 4
            "\x51"                      # 8  push ecx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aM_isdrawcompas
            "\x52"                      # 18 push edx
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (5, 1),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x45\xAB"              # 0  mov eax, [ebp+phkResult]
            "\x8D\x53\xAB"              # 3  lea edx, [ebx+44h]
            "\x6A\x04"                  # 6  push 4
            "\x52"                      # 8  push edx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aM_chatwndy
            "\x50"                      # 18 push eax
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (5, 1),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x45\xAB"              # 0  mov eax, [ebp+phkResult]
            "\x8D\x93\xAB\xAB\xAB\xAB"  # 3  lea edx, [ebx+3ADh]
            "\x6A\x04"                  # 9  push 4
            "\x52"                      # 11 push edx
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aOnstsubchat
            "\x50"                      # 21 push eax
            "\xFF\xD6"                  # 22 call esi
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4),
        },
        {
            "strOffset": (17, False),
        }
    ],
    # 2010-01-05
    [
        (
            "\x8B\x4D\xAB"              # 0  mov ecx, [ebp+phkResult]
            "\x8D\x83\xAB\xAB\xAB\xAB"  # 3  lea eax, [ebx+3ACh]
            "\x6A\x04"                  # 9  push 4
            "\x50"                      # 11 push eax
            "\x6A\x04"                  # 12 push 4
            "\x6A\x00"                  # 14 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 16 push offset aChatwndstickon
            "\x51"                      # 21 push ecx
            "\xFF\xD6"                  # 22 call esi
        ),
        {
            "fixedOffset": 17,
            "retOffset": 0,
            "memberOffset": (5, 4),
        },
        {
            "strOffset": (17, False),
        }
    ],
    # 2010-01-05
    [
        (
            "\x8D\x43\xAB"              # 0  lea eax, [ebx+60h]
            "\x6A\x04"                  # 3  push 4
            "\x50"                      # 5  push eax
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_minimapzoomw
            "\x51"                      # 15 push ecx
            "\xFF\xD6"                  # 16 call esi
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (2, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2009-01-07
    [
        (
            "\x81\xC3\xAB\xAB\xAB\xAB"  # 0  add ebx, 23Eh
            "\x6A\x04"                  # 6  push 4
            "\x53"                      # 8  push ebx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aOnbtsubchat
            "\x52"                      # 18 push edx
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (2, 4),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2009-01-13
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x81\xC3\xAB\xAB\xAB\xAB"  # 2  add ebx, 23Ah
            "\x53"                      # 8  push ebx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aOnbtsubchat
            "\x52"                      # 18 push edx
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (4, 4),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2009-01-13
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x4B\xAB"              # 2  lea ecx, [ebx+28h]
            "\x51"                      # 5  push ecx
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_isdrawcompas
            "\x52"                      # 15 push edx
            "\xFF\xD6"                  # 16 call esi
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2009-01-13
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x43\xAB"              # 2  lea eax, [ebx+24h]
            "\x50"                      # 5  push eax
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_minimapargb
            "\x51"                      # 15 push ecx
            "\xFF\xD6"                  # 16 call esi
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2009-01-13
    [
        (
            "\x6A\x04"                  # 0  push 4
            "\x8D\x53\xAB"              # 2  lea edx, [ebx+8]
            "\x52"                      # 5  push edx
            "\x6A\x04"                  # 6  push 4
            "\x6A\x00"                  # 8  push 0
            "\x68\xAB\xAB\xAB\xAB"      # 10 push offset aM_chatwndy
            "\x50"                      # 15 push eax
            "\xFF\xD6"                  # 16 call esi
        ),
        {
            "fixedOffset": 11,
            "retOffset": 0,
            "memberOffset": (4, 1),
        },
        {
            "strOffset": (11, False),
        }
    ],
    # 2009-04-29
    [
        (
            "\x81\xC3\xAB\xAB\xAB\xAB"  # 0  add ebx, 25Eh
            "\x6A\x04"                  # 6  push 4
            "\x53"                      # 8  push ebx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aOnbtsubchat
            "\x51"                      # 18 push ecx
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (2, 4),
        },
        {
            "strOffset": (14, False),
        }
    ],
    # 2007-07-18
    [
        (
            "\x81\xC3\xAB\xAB\xAB\xAB"  # 0  add ebx, 22Eh
            "\x6A\x04"                  # 6  push 4
            "\x53"                      # 8  push ebx
            "\x6A\x04"                  # 9  push 4
            "\x6A\x00"                  # 11 push 0
            "\x68\xAB\xAB\xAB\xAB"      # 13 push offset aOnbtsubchat
            "\x50"                      # 18 push eax
            "\xFF\xD6"                  # 19 call esi
        ),
        {
            "fixedOffset": 14,
            "retOffset": 0,
            "memberOffset": (2, 4),
        },
        {
            "strOffset": (14, False),
        }
    ],
]
