#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


blocks = [
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_10], 7Eh
            "\xFF\xAB"                  # 6  call ebx
            "\x89\x45\xAB"              # 8  mov [ebp+var_E], eax
            "\x8D\x55\xAB"              # 11 lea edx, [ebp+var_10]
            "\x0F\xBF\x45\xAB"          # 14 movsx eax, [ebp+var_10]
            "\x52"                      # 18 push edx
            "\x50"                      # 19 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_instanceR
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (21, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\xAB"                      # 5  push edx
            "\x6A\xAB"                  # 6  push 7Dh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 8  mov [ebp+var_2], 7Dh
            "\xE8"                      # 14 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 14,
            "packetId": (7, 1),
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_10], 7Eh
            "\xFF\xD6"                  # 6  call esi
            "\x0F\xBF\x4D\xF0"          # 8  movsx ecx, [ebp+var_10]
            "\x89\x45\xF2"              # 12 mov [ebp+var_E], eax
            "\x8D\x45\xF0"              # 15 lea eax, [ebp+var_10]
            "\x50"                      # 18 push eax
            "\x51"                      # 19 push ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_instanceR
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (21, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 94h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_1C], 94h
            "\x89\x45\xAB"              # 16 mov [ebp+var_1A], eax
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 94h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_1C], 94h
            "\xE8"                      # 16 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 16,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 193h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_18], 193h
            "\x89\x75\xAB"              # 16 mov [ebp+var_16], esi
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 193h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_18], 193h
            "\x89\x5D\xAB"              # 16 mov [ebp+var_16], ebx
            "\xE8"                      # 19 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 19,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 90h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_10], 90h
            "\x89\x45\xAB"              # 16 mov [ebp+var_E], eax
            "\xC6\x45\xAB\xAB"          # 19 mov [ebp+var_A], 1
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\x52"                      # 5  push edx
            "\x68\xAB\xAB\x00\x00"      # 6  push 0D0h
            "\xC7\x83\xAB\xAB\x00\x00\xAB\xAB\x00\x00"  # 11 mov [ebx+0E4h], 1
            "\x66\xC7\x45\xAB\xAB\xAB"  # 21 mov word ptr [ebp+arg_4], 0D0h
            "\xC6\x45\xAB\xAB"          # 27 mov byte ptr [ebp+arg_4+2], 1
            "\xE8"                      # 31 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 31,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0CFh
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\xE8"                      # 10 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 10,
            "packetId": 1,
            "retOffset": 0,
            "goto": (5, 4),  # offset 5, jmp size addr 4
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\x50"                      # 5  push eax
            "\x68\xAB\xAB\x00\x00"      # 6  push 0ABh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_4], 0ABh
            "\x66\x89\x55\xAB"          # 17 mov word ptr [ebp+arg_4+2], dx
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\x50"                      # 5  push eax
            "\x68\xAB\xAB\x00\x00"      # 6  push 0A9h
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+var_14], 0A9h
            "\x66\x89\x55\xAB"          # 17 mov word ptr [ebp+var_14+2], dx
            "\x66\x89\x75\xAB"          # 21 mov word ptr [ebp+var_10], si
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 0A2h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\xC7\x83\xAB\xAB\x00\x00\xAB\xAB\x00\x00"  # 10 mov [ebx+2C4h], 1
            "\x66\xC7\x45\xAB\xAB\xAB"  # 20 mov word ptr [ebp+var_14], 0A2h
            "\x66\x89\x45\xAB"          # 26 mov word ptr [ebp+var_10], ax
            "\xE8"                      # 30 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 30,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_14], 0A7h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call sub_51AF40
            "\x89\x45\xAB"              # 11 mov [ebp+var_10], eax
            "\x8D\x55\xAB"              # 14 lea edx, [ebp+var_14]
            "\x0F\xBF\x45\xAB"          # 17 movsx eax, word ptr [ebp+var_14]
            "\x52"                      # 21 push edx
            "\x50"                      # 22 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 23 mov ecx, offset g_instanceR
            "\x66\x89\x75\xAB"          # 28 mov word ptr [ebp+var_14+2], si
            "\xE8"                      # 32 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 32,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (24, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 103h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\xC7\x45\xAB\xAB\xAB\x00\x00"  # 10 mov [ebp+var_4], 29h
            "\x89\x85\xAB\xAB\xAB\xAB"  # 17 mov [ebp+var_84+2], eax
            "\xE8"                      # 23 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 23,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_48], 72h
            "\xFF\xD7"                  # 6  call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 8  mov ecx, dword_690A94
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 14 mov edx, dword_690A98
            "\x89\x45\xAB"              # 20 mov [ebp+var_3A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 23 mov eax, dword_690378
            "\x89\x4D\xAB"              # 28 mov [ebp+var_46], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 31 mov ecx, offset dword_68FDB8
            "\x89\x55\xAB"              # 36 mov [ebp+var_42], edx
            "\x89\x45\xAB"              # 39 mov [ebp+var_3E], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 42 call sub_5A6F00
            "\x0F\xBF\xAB\xAB"          # 47 movsx edx, [ebp+var_48]
            "\x8D\x4D\xAB"              # 51 lea ecx, [ebp+var_48]
            "\x88\x45\xAB"              # 54 mov [ebp+var_36], al
            "\x51"                      # 57 push ecx
            "\x52"                      # 58 push edx
            "\xB9\xAB\xAB\xAB\xAB"      # 59 mov ecx, offset g_instanceR
            "\xE8"                      # 64 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 64,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (60, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_18], 65h
            "\x66\x89\x75\xAB"          # 6  mov word ptr [ebp+var_E+4], si
            "\x89\x55\xAB"              # 10 mov [ebp+var_16], edx
            "\x89\x45\xAB"              # 13 mov [ebp+var_12], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_5A6F00
            "\x88\x45\xAB"              # 21 mov [ebp+var_8], al
            "\x8D\x55\xAB"              # 24 lea edx, [ebp+var_18]
            "\x0F\xBF\x45\xAB"          # 27 movsx eax, [ebp+var_18]
            "\x52"                      # 31 push edx
            "\x50"                      # 32 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 33 mov ecx, offset g_instanceR
            "\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": (4, 2),
            "retOffset": 0,
            "goto": (33, 4)
        },
        {
            "g_instanceR": (34, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x6A\xAB"                  # 0  push 67h
            "\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_instanceR
            "\x66\x89\x55\xAB"          # 7  mov [ebp-9], dx
            "\xE8"                      # 11 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 11,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_instanceR": (3, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_34], 68h
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call _strncpy
            "\xA1\xAB\xAB\xAB\xAB"      # 11 mov eax, g_serviceType
            "\x83\xC4\xAB"              # 16 add esp, 0Ch
            "\x85\xC0"                  # 19 test eax, eax
            "\xC6\x45\xAB\xAB"          # 21 mov [ebp+var_7], 0
            "\x75\xAB"                  # 25 jnz short loc_537169
            "\x8A\x83\xAB\xAB\xAB\x00"  # 27 mov al, [ebx+0A7h]
            "\x84\xC0"                  # 33 test al, al
            "\x74\xAB"                  # 35 jz short loc_537169
            "\xC6\x45\xAB\xAB"          # 37 mov [ebp+var_2E], 0
            "\x56"                      # 41 push esi
            "\x68\xAB\xAB\xAB\xAB"      # 42 push offset aS
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call nullsub_4
            "\x0F\xBF\xAB\xAB"          # 52 movsx ecx, [ebp+var_34]
            "\x83\xC4\xAB"              # 56 add esp, 8
            "\x8D\x45\xAB"              # 59 lea eax, [ebp+var_34]
            "\x50"                      # 62 push eax
            "\x51"                      # 63 push ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 64 mov ecx, offset g_instanceR
            "\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (65, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+cp], 72h
            "\xFF\x15\xAB\xAB\xAB\xAB"  # 6  call ds:timeGetTime
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 12 mov ecx, dword_690378
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 18 mov edx, dword_690A94
            "\x89\x45\xAB"              # 24 mov dword ptr [ebp+var_E], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 27 mov eax, dword_690A98
            "\x89\x4D\xAB"              # 32 mov [ebp+var_12], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 35 mov ecx, offset dword_68FDB8
            "\x89\x55\xAB"              # 40 mov [ebp-1Ah], edx
            "\x89\x45\xAB"              # 43 mov [ebp+var_16], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 46 call sub_5A6F00
            "\x88\x45\xAB"              # 51 mov [ebp+var_E+4], al
            "\x8D\x55\xAB"              # 54 lea edx, [ebp+cp]
            "\x0F\xBF\x45\xAB"          # 57 movsx eax, [ebp+cp]
            "\x52"                      # 61 push edx
            "\x50"                      # 62 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 63 mov ecx, offset g_instanceR
            "\xE8"                      # 68 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 68,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (64, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 1BFh
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\xC6\x45\xAB\xAB"          # 10 mov [ebp+var_2], 1
            "\xE8"                      # 14 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 14,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2003-10-28
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\x50"                      # 5  push eax
            "\x68\xAB\xAB\x00\x00"      # 6  push 1BFh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov [ebp+var_4], 1BFh
            "\xC6\x45\xAB\xAB"          # 17 mov [ebp+var_2], 1
            "\xE8"                      # 21 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 21,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2004-04-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_14], 7Eh
            "\xFF\xD7"                  # 6  call edi
            "\x0F\xBF\x4D\xAB"          # 8  movsx ecx, [ebp+var_14]
            "\x89\x45\xAB"              # 12 mov [ebp+var_12], eax
            "\x8D\x45\xAB"              # 15 lea eax, [ebp+var_14]
            "\x50"                      # 18 push eax
            "\x51"                      # 19 push ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 20 mov ecx, offset g_instanceR
            "\xE8"                      # 25 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 25,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (21, False),
        }
    ],
    # 2004-04-22
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 149h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\x66\xC7\x45\xAB\xAB\xAB"  # 10 mov [ebp+var_C], 149h
            "\x89\x45\xAB"              # 16 mov [ebp+var_A], eax
            "\xC6\x45\xAB\xAB"          # 19 mov [ebp+var_6], 2
            "\x66\xC7\x45\xAB\xAB\xAB"  # 23 mov [ebp+var_5], 0Ah
            "\xE8"                      # 29 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 29,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2004-04-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_48], 72h
            "\xA3\xAB\xAB\xAB\xAB"      # 6  mov dword_63D184, eax
            "\xFF\xD7"                  # 11 call edi
            "\x8B\x0D\xAB\xAB\xAB\xAB"  # 13 mov ecx, dword_67EE28
            "\x8B\x15\xAB\xAB\xAB\xAB"  # 19 mov edx, dword_67E718
            "\x89\x45\xAB"              # 25 mov [ebp+var_3A], eax
            "\xA1\xAB\xAB\xAB\xAB"      # 28 mov eax, dword_67EE24
            "\x89\x4D\xAB"              # 33 mov [ebp+var_42], ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 36 mov ecx, offset dword_67E1B8
            "\x89\x45\xAB"              # 41 mov [ebp+var_46], eax
            "\x89\x55\xAB"              # 44 mov [ebp+var_3E], edx
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call sub_5BDCD0
            "\x0F\xBF\x4D\xAB"          # 52 movsx ecx, [ebp+var_48]
            "\x88\x45\xAB"              # 56 mov [ebp+var_36], al
            "\x8D\x45\xAB"              # 59 lea eax, [ebp+var_48]
            "\x50"                      # 62 push eax
            "\x51"                      # 63 push ecx
            "\xB9\xAB\xAB\xAB\xAB"      # 64 mov ecx, offset g_instanceR
            "\xE8"                      # 69 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 69,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (65, False),
        }
    ],
    # 2004-04-22
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\x50"                      # 5  push eax
            "\x68\xAB\xAB\x00\x00"      # 6  push 1DBh
            "\x66\xC7\x45\xAB\xAB\xAB"  # 11 mov word ptr [ebp+arg_0+2], 1DBh
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2004-04-22
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_6C], 1FBh
            "\xE8\xAB\xAB\xAB\xAB"      # 6  call _strncpy
            "\xA1\xAB\xAB\xAB\xAB"      # 11 mov eax, g_serviceType
            "\x83\xC4\xAB"              # 16 add esp, 18h
            "\x85\xC0"                  # 19 test eax, eax
            "\xC6\x45\xAB\xAB"          # 21 mov [ebp+var_35], 0
            "\x75\x0E"                  # 25 jnz short loc_53D606
            "\x8A\x83\xAB\xAB\xAB\xAB"  # 27 mov al, [ebx+0A7h]
            "\x84\xC0"                  # 33 test al, al
            "\x74\x04"                  # 35 jz short loc_53D606
            "\xC6\x45\xAB\xAB"          # 37 mov [ebp+var_2E], 0
            "\x57"                      # 41 push edi
            "\x68\xAB\xAB\xAB\xAB"      # 42 push offset aS
            "\xE8\xAB\xAB\xAB\xAB"      # 47 call nullsub_4
            "\xA1\xAB\xAB\xAB\xAB"      # 52 mov eax, g_serviceType
            "\x83\xC4\xAB"              # 57 add esp, 8
            "\x83\xF8\xAB"              # 60 cmp eax, 0Ah
            "\x75\x0B"                  # 63 jnz short loc_53D629
            "\x0F\xBF\x45\xAB"          # 65 movsx eax, [ebp+var_6C]
            "\x8D\x55\xAB"              # 69 lea edx, [ebp+var_6C]
            "\x52"                      # 72 push edx
            "\x50"                      # 73 push eax
            "\xEB\x09"                  # 74 jmp short loc_53D632
            "\x0F\xBF\x55\xAB"          # 76 movsx edx, [ebp+var_34]
            "\x8D\x4D\xAB"              # 80 lea ecx, [ebp+var_34]
            "\x51"                      # 83 push ecx
            "\x52"                      # 84 push edx
            "\xB9\xAB\xAB\xAB\xAB"      # 85 mov ecx, offset g_instanceR
            "\xE8"                      # 90 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 90,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (86, False),
        }
    ],
    # 2004-08-09
    [
        (
            "\xB9\xAB\xAB\xAB\xAB"      # 0  mov ecx, offset g_instanceR
            "\x50"                      # 5  push eax
            "\x68\xAB\xAB\x00\x00"      # 6  push 0BBh
            "\xE8"                      # 11 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 11,
            "packetId": 7,
            "retOffset": 6,
        },
        {
            "g_instanceR": (1, False),
        }
    ],
    # 2004-08-09
    [
        (
            "\x6A\xAB"                  # 0  push 72h
            "\xB9\xAB\xAB\xAB\xAB"      # 2  mov ecx, offset g_instanceR
            "\xC7\x83\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB"  # 7  mov [ebx+2DCh], 1
            "\x66\xC7\x45\xAB\xAB\xAB"  # 17 mov word ptr [ebp+var_18], 72h
            "\x66\x89\x45\xAB"          # 23 mov word ptr [ebp+var_10], ax
            "\xE8"                      # 27 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 27,
            "packetId": (1, 1),
            "retOffset": 0,
        },
        {
            "g_instanceR": (3, False),
        }
    ],
    # 2004-08-09
    [
        (
            "\x68\xAB\xAB\x00\x00"      # 0  push 103h
            "\xB9\xAB\xAB\xAB\xAB"      # 5  mov ecx, offset g_instanceR
            "\xC7\x45\xAB\xAB\xAB\xAB\xAB"  # 10 mov [ebp+var_4], 2Fh
            "\xE8"                      # 17 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 17,
            "packetId": 1,
            "retOffset": 0,
        },
        {
            "g_instanceR": (6, False),
        }
    ],
    # 2004-08-09
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov word ptr [ebp+var_1C], 65h
            "\x66\x89\x75\xAB"          # 6  mov word ptr [ebp+var_10+2], si
            "\x89\x55\xAB"              # 10 mov [ebp+var_1C+2], edx
            "\x89\x45\xAB"              # 13 mov [ebp+var_16], eax
            "\xE8\xAB\xAB\xAB\xAB"      # 16 call sub_5C6420
            "\x8D\x55\xAB"              # 21 lea edx, [ebp+var_1C]
            "\x88\x45\xAB"              # 24 mov [ebp+var_C], al
            "\x0F\xBF\x45\xAB"          # 27 movsx eax, word ptr [ebp+var_1C]
            "\x52"                      # 31 push edx
            "\x50"                      # 32 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 33 mov ecx, offset g_instanceR
            "\xE8"                      # 38 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 38,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (34, False),
        }
    ],
    # 2004-08-09
    [
        (
            "\x66\xC7\x45\xAB\xAB\xAB"  # 0  mov [ebp+var_30], 1DDh
            "\x89\x45\xAB"              # 6  mov [ebp+var_2E], eax
            "\xF3\xA5"                  # 9  rep movsd
            "\x8D\x4D\xAB"              # 11 lea ecx, [ebp+var_12]
            "\x51"                      # 14 push ecx
            "\x8D\x8D\xAB\xAB\xAB\xAB"  # 15 lea ecx, [ebp+var_88]
            "\xE8\xAB\xAB\xAB\xAB"      # 21 call sub_41E600
            "\xE8\xAB\xAB\xAB\xAB"      # 26 call sub_546020
            "\x88\x45\xAB"              # 31 mov [ebp+var_2], al
            "\x8D\x55\xAB"              # 34 lea edx, [ebp+var_30]
            "\x0F\xBF\x45\xAB"          # 37 movsx eax, [ebp+var_30]
            "\x52"                      # 41 push edx
            "\x50"                      # 42 push eax
            "\xB9\xAB\xAB\xAB\xAB"      # 43 mov ecx, offset g_instanceR
            "\xE8"                      # 48 call CRagConnection_GetPacketSize
        ),
        {
            "fixedOffset": 48,
            "packetId": (4, 2),
            "retOffset": 0,
        },
        {
            "g_instanceR": (44, False),
        }
    ],
]
