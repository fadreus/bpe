#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def showError(self, ret, block):
    if ret is not False:
        self.log("Error: found duplicate matched block")
        self.log(str(ret[0]), False)
        self.log(str(block), False)
        exit(1)


def matchBlock(self, blocks, offset, relativeVars, mainVar):
    ret = False
    rawVaDiff = self.exe.codeSection.rawVaDiff
    for block in blocks:
        code = block[0]
        blockOffsets = block[1]
        blockRelOffsets = block[2]
        fixedOffset = blockOffsets["fixedOffset"]
        offset1 = offset - fixedOffset
        if self.exe.matchWildcardOffset(code,
                                        "\xAB",
                                        offset1) is True:
            found = True
            for var in relativeVars:
                # skip variable check what not present in block
                if var not in blockRelOffsets:
                    continue
                relOffset = blockRelOffsets[var]
                relative = True
                if type(relOffset) is tuple:
                    relative = relOffset[1]
                    relOffset = relOffset[0]
                relVal = relativeVars[var]
                if relative is True:
                    if self.getAddr(offset1,
                                    relOffset,
                                    relOffset + 4) != relVal:
                        found = False
                        break
                else:
                    if self.exe.readUInt(offset1 + relOffset) != relVal:
                        found = False
                        break
            if found is False:
                continue
            retOffset = blockOffsets["retOffset"]
            if retOffset == -1:
                showError(self, ret, block)
                ret = (block, -1, 0)
                continue
            if mainVar == "":
                showError(self, ret, block)
                ret = (block, offset1 + retOffset, 0)
                continue
            mainOffset = blockOffsets[mainVar]
            mainVal = 0
            if type(mainOffset) is tuple:
                sz = mainOffset[1]
                mainOffset = mainOffset[0]
                if sz is False:
                    mainVal = self.getAddr(offset1,
                                           mainOffset,
                                           mainOffset + 4) - rawVaDiff
                elif sz == 1:
                    mainVal = self.exe.readUByte(offset1 + mainOffset)
                elif sz == 2:
                    mainVal = self.exe.readUWord(offset1 + mainOffset)
                elif sz == 4:
                    mainVal = self.exe.readUInt(offset1 + mainOffset)
                else:
                    self.log("Error: unknown main var type size: {0}".
                             format(sz))
                    exit(1)
            else:
                mainVal = self.exe.readUInt(offset1 + mainOffset)
            showError(self, ret, block)
            ret = (block, offset1 + retOffset, mainVal)
            continue
    return ret


def searchBlocks(self,
                 name,
                 offsets,
                 blocks,
                 relativeVars,
                 mainVar,
                 showStat=True):
    found = []
    errors = []
    for offset in offsets:
        # print hex(self.exe.rawToVa(offset))
        val = matchBlock(self, blocks, offset, relativeVars, mainVar)
        if val is False:
            errors.append(offset)
        else:
            if val[1] == -1:
                pass  # ignored block
            else:
                found.append(val)
    if showStat is True and (len(errors) > 0 or len(found) > 0):
        errMsg = ""
        foundMsg = ""
        if len(errors) > 0:
            errMsg = " errors {0}".format(len(errors))
        if len(found) > 0:
            foundMsg = " found {0}".format(len(found))
        self.log("{0}:{1}{2}.".format(name, foundMsg, errMsg))
    return (found, errors)


def blocksToLabels(self, found, labelName):
    # id = set(addr,)
    packetToAddr = dict()
    # addr = label
    labels = dict()
    for f in found:
        addr = self.exe.rawToVa(f[1])
        packetId = hex(f[2])[2:]
        addrs = packetToAddr.get(packetId, set())
        addrs.add(addr)
        packetToAddr[packetId] = addrs
    for packetId in packetToAddr:
        addrs = sorted(packetToAddr[packetId])
        if len(addrs) == 1:
            labels[addrs[0]] = labelName.format(packetId)
        else:
            idx = 1
            for addr in addrs:
                labels[addr] = labelName.format("{0}_{1}".
                                                format(packetId, idx))
                idx = idx + 1
    for addr in labels:
        label = labels[addr]
        self.addVaLabel(label, addr, False)
    return labels


def showBlocksInfo(self, offsets, found, errors):
    self.log("total addresses: {0}".format(len(offsets)))
    self.log("found blocks: {0}".format(len(found)))
    for f in found:
        self.log(" {0}: {1}".format(hex(self.exe.rawToVa(f[1])), hex(f[2])))
    self.log("errors blocks: {0}".format(len(errors)))
    for offset in errors:
        self.log(" {0}".format(hex(self.exe.rawToVa(offset))))
