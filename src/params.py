#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.blocks.blocks import searchBlocks
from src.strings import searchStrings


def checkInArr(self, arr, vals):
    for val in vals:
        if val not in arr:
            self.log("Error: not found string {0}".format(val))
            exit(1)


def searchParams2(self, vals, blocks, getRet, varName, reportErrors=True,
                  maxFound=1):
    strings = searchStrings(self, vals, 0)
    arr = dict()
    varData = 0
    codes = self.exe.codes
    toHex = self.exe.toHex
    readUInt = self.exe.readUInt
    cache = self.exe.cache.codes
    for string in strings:
        offset = strings[string]
        data = toHex(offset, 4)
        if data in cache.arr:
            offsets = cache.arr[data]
        else:
            offsets = codes(data, -1)
            cache.arr[data] = offsets
        relVars = {"strOffset": offset}
        found, errors = searchBlocks(self,
                                     "param {0}".format(string),
                                     offsets,
                                     blocks,
                                     relVars,
                                     "memberOffset",
                                     False)
        szFound = len(found)
        szErrors = len(errors)
        if szFound > 0 and (szFound == szErrors or maxFound != 1) and \
           szFound <= maxFound:
            if getRet is True:
                retOffset = found[0][1]
                varData1 = readUInt(retOffset)
                if varData != 0 and varData != varData1:
                    self.log("Error: found different {0}".format(varName))
                    exit(1)
                varData = varData1
            if szFound == 1:
                arr[string] = found[0][2]
            else:
                data = []
                for f in range(0, szFound):
                    data.append(found[f][2])
                arr[string] = data
        elif reportErrors is False and szFound == 0 and szErrors > 1:
            pass
        elif szFound + szErrors < 2:
            pass
        else:
            self.log("Error: found wrong number of {0}: "
                     "found: {1}, errors: {2}".
                     format(string, szFound, szErrors))
            exit(1)
    return arr, varData
