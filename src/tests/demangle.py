#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from src.vtbl import demangleClassName


class demangle(unittest.TestCase):
    def test1(self):
        name, flags = demangleClassName(".?AVerror_category@std@@")
        self.assertEqual("std::error_category", name)
        self.assertEqual((True,), flags)


    def test2(self):
        name, flags = demangleClassName(".?AUBOF@YExcel@@")
        self.assertEqual("YExcel::BOF", name)
        self.assertEqual((False,), flags)


    def test3(self):
        className = (".?AV?$NRecycleObjectPool@"
                     "VCAccountLinkedUserDataLoadAsyncWork@@@@")
        name, flags = demangleClassName(className)
        self.assertEqual(("NRecycleObjectPool<CAccountLinkedUser"
                         "DataLoadAsyncWork>"),
                         name)
        self.assertEqual((True,), flags)


    def test4(self):
        name, flags = demangleClassName(".?AVThreadSafeBuffer@?A0xdfa215f3@@")
        self.assertEqual("anonymous::ThreadSafeBuffer", name)
        self.assertEqual((True,), flags)
