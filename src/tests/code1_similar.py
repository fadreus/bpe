#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from src.tests.common import loadClient


class code1_similar(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def test1(self):
        code = ("\x00\x04")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 8)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code, 0, 8)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 8)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code, 3, 8)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 7)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 3, 7)
        self.assertTrue(offset is False)


    def test2(self):
        sz = 9
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("\x4D\x5A\x90\x00\x03\x00\x00\x00\x04",
                         self.extractor.exe.exe)
        code = ("\x00\x04")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 6)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code, 6)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 7)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code, 7)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 8)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 8)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 7, 8)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.code(code, 7, 8)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 7)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 7, 7)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 7, 7)
        self.assertTrue(offset is False)


    def test3(self):
        sz = 3
        self.extractor.exe.exe = self.extractor.exe.exe[6:6 + sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("\x00\x00\x04", self.extractor.exe.exe)
        code = ("\x00\x04")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.code(code, 1, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 2, 2)
        self.assertTrue(offset is False)


    def test4(self):
        sz = 2
        self.extractor.exe.exe = self.extractor.exe.exe[6:6 + sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("\x00\x00", self.extractor.exe.exe)
        code = ("\x00\x04")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 2, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 2, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 2, 3)
        self.assertTrue(offset is False)
