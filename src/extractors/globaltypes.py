#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchAndSetNameType(self, code, varName, typeName, rawVaDiff):
    offsets = self.exe.codes(code, -1)
    if offsets is False:
        self.log("Error: {0} usage".format(varName))
        exit(1)
    setFuncCallPrefix = self.setFuncCallPrefix
    for offset in offsets:
        setFuncCallPrefix(offset - rawVaDiff, typeName)


def setStructType(self, varName, varAddr, typeName, rawVaDiff):
    if varAddr == 0:
        return
    self.makeVaStruct(varAddr, typeName)
    code = "\xB9" + self.exe.toHex(varAddr, 4)
    searchAndSetNameType(self, code, varName, typeName, rawVaDiff)


def setPointerType(self, varName, varAddr, typeName, rawVaDiff):
    if varAddr == 0:
        return
    self.setVarType(varAddr, typeName + "*")
    code = "\x8B\x0D" + self.exe.toHex(varAddr, 4)
    searchAndSetNameType(self, code, varName, typeName, rawVaDiff)


def setGlobalTypes(self):
    rawVaDiff = self.exe.codeSection.rawVaDiff
    setStructType(self, "g_windowMgr", self.gWindowMgr, "UIWindowMgr",
                  rawVaDiff)
    setStructType(self, "g_modeMgr", self.g_modeMgr, "CModeMgr", rawVaDiff)
    setStructType(self, "g_skinMgr", self.gSkinMgr, "CSkinMgr", rawVaDiff)
    setStructType(self, "g_session", self.session, "CSession", rawVaDiff)
    setStructType(self, "g_instanceR", self.g_instanceR, "CRagConnection",
                  rawVaDiff)
    # not defined type
    # m_resMgr
    # g_zoneServerAddr
    # setStructType(self, "setStructType", self.mResMgr, "CResMgr", rawVaDiff)
    setPointerType(self,
                   "g_CCheatDefenderMgr",
                   self.gCheatDefenderMgr,
                   "CCheatDefenderMgr",
                   rawVaDiff)
    setPointerType(self,
                   "g_soundMgr",
                   self.soundMgr,
                   "CSoundMgr",
                   rawVaDiff)
