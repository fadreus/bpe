#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchShuffle23(self, errorExit):
    if self.encPacketKeys == 0 and self.comboAddr != 0:
        self.log("Error: search shuffle23 disabled due "
                 "missing enc_packet_keys")
        exit(1)
    if self.session == 0:
        if self.packetVersion < "20090000":  # 2007
            sessionHex = "\xAB\xAB\xAB\xAB"
        else:
            self.log("Error: search shuffle23 disabled due missing g_session")
            exit(1)
    else:
        sessionHex = self.exe.toHex(self.session, 4)

    if self.comboAddr == 0 and self.packetVersion > "20110000":
        self.log("Error: search shuffle23 disabled due missing combo function")
        if self.clientType != "iro":
            exit(1)
    if self.instanceR == 0 and self.g_instanceR == 0:
        self.log("Error: search shuffle23 disabled due "
                 "missing CRagConnection::instanceR")
        exit(1)
        if self.packetVersion > "20050000":
            exit(1)
        return
    if self.sendPacket == 0:
        self.log("Error: search shuffle23 disabled due "
                 "missing CRagConnection::SendPacket")
        exit(1)

    offset = False
    czEnter = 0
    if self.comboAddr != 0:
        # search shuffle23 (CZ_ENTER) in CLoginMode_virt28
        # 0  mov ecx, enc_packet_keys
        # 6  push 1
        # 8  call comboFunction
        # 13 call CRagConnection_instanceR
        # 18 mov esi, eax
        # 20 call ebx
        # 22 add eax, 3E8h
        # 27 mov [esi+1Ch], eax
        # 30 mov eax, 436h
        # 35 mov [ebp+packet.packet_id], ax
        # 39 call ebx
        # 41 mov [ebp+packet.client_time], eax
        # 44 mov eax, g_session.m_account_id
        # 49 mov [ebp+packet.account_id], eax
        # 52 mov eax, g_session.m_char_id
        # 57 mov [ebp+packet.char_id], eax
        # 60 mov eax, g_session.m_auth_code1
        # 65 mov ecx, offset g_session
        # 70 mov [ebp+packet.session_key1], eax
        # 73 call CSession_GetSex
        # 78 mov [ebp+packet.sex], al
        # 81 lea eax, [ebp+packet]
        # 84 push eax
        # 85 movsx eax, [ebp+packet.packet_id]
        # 89 push eax
        # 90 call CRagConnection_instanceR
        # 95 mov ecx, eax
        # 97 call CRagConnection_GetPacketSize
        # 102 push eax
        # 103 call CRagConnection_instanceR
        # 108 mov ecx, eax
        # 110 call CRagConnection_SendPacket
        # 0  push 1
        # 2  call CRagConnection_instanceR
        # 7  mov ecx, eax
        # 9  call CConnection_SetBlock
        code = (
            "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
            "\x6A\x01"                        # 6
            "\xE8\xAB\xAB\xAB\xAB"            # 8
            "\xE8\xAB\xAB\xAB\xAB"            # 13
            "\x8B\xF0"                        # 18
            "\xFF\xAB"                        # 20
            "\x05\xE8\x03\x00\x00"            # 22
            "\x89\x46\xAB"                    # 27
            "\xB8\xAB\xAB\x00\x00"            # 30
            "\x66\x89\x45\xAB"                # 35
            "\xFF\xAB"                        # 39
            "\x89\x45\xAB"                    # 41
            "\xA1\xAB\xAB\xAB\xAB"            # 44
            "\x89\x45\xAB"                    # 49
            "\xA1\xAB\xAB\xAB\xAB"            # 52
            "\x89\x45\xAB"                    # 57
            "\xA1\xAB\xAB\xAB\xAB"            # 60
            "\xB9" + sessionHex +             # 65
            "\x89\x45\xAB"                    # 70
            "\xE8\xAB\xAB\xAB\xAB"            # 73
            "\x88\x45\xAB"                    # 78
            "\x8D\x45\xAB"                    # 81
            "\x50"                            # 84
            "\x0F\xBF\x45\xAB"                # 85
            "\x50"                            # 89
            "\xE8\xAB\xAB\xAB\xAB"            # 90
            "\x8B\xC8"                        # 95
            "\xE8\xAB\xAB\xAB\xAB"            # 97
            "\x50"                            # 102
            "\xE8\xAB\xAB\xAB\xAB"            # 103
            "\x8B\xC8"                        # 108
            "\xE8\xAB\xAB\xAB\xAB"            # 110
            "\x6A\x01"                        # 115
            "\xE8\xAB\xAB\xAB\xAB"            # 117
            "\x8B\xC8"                        # 122
            "\xE8\xAB\xAB\xAB\xAB"            # 124
        )
        setBlockOffset = 125
        sessionOffset = 66
        comboOffset = 9
        instanceOffsets = (14, 91, 104, 118)
        enc_packet_keysOffset = 0
        CSession_GetSexOffset = 74
        encryptionInitKeysOffset = 0
        encryptionInitKeys0Offset = 0
        getPacketSizeOffset = 98
        sendPacketOffset = 111
        traceOffset = 0
        setPaddingValueOffsets = 0
        dwTimeOffset = (29, 1)
        packetIdNumOffset = (31, 4)
        clientTimeOffset2 = (43, 1)
        accountIdOffset1 = (45, 4)
        accountIdOffset2 = (51, 1)
        charIdOffset1 = (53, 4)
        charIdOffset2 = (59, 1)
        authCodeOffset1 = (61, 4)
        authCodeOffset2 = (72, 1)
        packetIdOffsets2 = ((38, 1), (83, 1), (88, 1))
        sexOffset2 = (80, 1)
        key1Offset = 0
        key2Offset = 0
        key3Offset = 0
        searchEncryptionInitKeys = False
        offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 1
            # 8  call comboFunction
            # 13 call CRagConnection_instanceR
            # 18 mov ebx, eax
            # 20 call edi
            # 22 add eax, 3E8h
            # 27 mov edx, 947h
            # 32 mov [ebx+1Ch], eax
            # 35 mov [ebp+var_54.packet_id], dx
            # 39 call edi
            # 41 mov ecx, dword_CD9534
            # 47 mov edx, dword_CD8D5C
            # 53 mov [ebp+var_54.client_time], eax
            # 56 mov eax, accountId
            # 61 mov [ebp+var_54.char_id], ecx
            # 64 mov ecx, offset g_session
            # 69 mov [ebp+var_54.account_id], eax
            # 72 mov [ebp+var_54.session_key1], edx
            # 75 call sub_8F4A10
            # 80 movsx ecx, [ebp+var_54.packet_id]
            # 84 mov [ebp+var_54.sex], al
            # 87 lea eax, [ebp+var_54]
            # 90 push eax
            # 91 push ecx
            # 92 call CRagConnection_instanceR
            # 97 mov ecx, eax
            # 99 call CRagConnection_GetPacketSize
            # 104 push eax
            # 105 call CRagConnection_instanceR
            # 110 mov ecx, eax
            # 112 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x6A\x01"                        # 6
                "\xE8\xAB\xAB\xAB\xAB"            # 8
                "\xE8\xAB\xAB\xAB\xAB"            # 13
                "\x8B\xD8"                        # 18
                "\xFF\xAB"                        # 20
                "\x05\xE8\x03\x00\x00"            # 22
                "\xBA\xAB\xAB\x00\x00"            # 27
                "\x89\x43\xAB"                    # 32
                "\x66\x89\x55\xAB"                # 35
                "\xFF\xAB"                        # 39
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 41
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 47
                "\x89\x45\xAB"                    # 53
                "\xA1\xAB\xAB\xAB\xAB"            # 56
                "\x89\x4D\xAB"                    # 61
                "\xB9" + sessionHex +             # 64
                "\x89\x45\xAB"                    # 69
                "\x89\x55\xAB"                    # 72
                "\xE8\xAB\xAB\xAB\xAB"            # 75
                "\x0F\xBF\x4D\xAB"                # 80
                "\x88\x45\xAB"                    # 84
                "\x8D\x45\xAB"                    # 87
                "\x50"                            # 90
                "\x51"                            # 91
                "\xE8\xAB\xAB\xAB\xAB"            # 92
                "\x8B\xC8"                        # 97
                "\xE8\xAB\xAB\xAB\xAB"            # 99
                "\x50"                            # 104
                "\xE8\xAB\xAB\xAB\xAB"            # 105
                "\x8B\xC8"                        # 110
                "\xE8\xAB\xAB\xAB\xAB"            # 112
                "\x6A\x01"                        # 117
                "\xE8\xAB\xAB\xAB\xAB"            # 119
                "\x8B\xC8"                        # 124
                "\xE8\xAB\xAB\xAB\xAB"            # 126
            )
            setBlockOffset = 127
            sessionOffset = 65
            comboOffset = 9
            instanceOffsets = (14, 93, 106, 120)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 76
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 100
            sendPacketOffset = 113
            traceOffset = 0
            setPaddingValueOffsets = 0
            dwTimeOffset = (34, 1)
            packetIdNumOffset = (28, 4)
            clientTimeOffset2 = (55, 1)
            accountIdOffset1 = (57, 4)
            accountIdOffset2 = (71, 1)
            charIdOffset1 = (43, 4)
            charIdOffset2 = (63, 1)
            authCodeOffset1 = (49, 4)
            authCodeOffset2 = (74, 1)
            packetIdOffsets2 = ((38, 1), (83, 1), (89, 1))
            sexOffset2 = (86, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 799A799Ah
            # 11 push 199A799Ah
            # 16 push 1E1A559Ah
            # 21 call encryptionInitKeys
            # 26 call CRagConnection_instanceR
            # 31 mov edi, eax
            # 33 call esi
            # 35 add eax, 3E8h
            # 40 mov ecx, 940h
            # 45 mov [edi+1Ch], eax
            # 48 mov [esp+6Ch+var_54.packet_id], cx
            # 53 call esi
            # 55 mov ecx, g_session.m_auth_code1
            # 61 mov edx, g_session.m_account_id
            # 67 mov [esp+6Ch+var_54.client_time], eax
            # 71 mov eax, g_session.m_char_id
            # 76 mov [esp+6Ch+var_54.session_key1], ecx
            # 80 mov ecx, offset g_session
            # 85 mov [esp+6Ch+var_54.account_id], edx
            # 89 mov [esp+6Ch+var_54.char_id], eax
            # 93 call CSession_GetSex
            # 98 mov [esp+6Ch+var_54.sex], al
            # 102 movsx eax, [esp+6Ch+var_54.packet_id]
            # 107 lea edx, [esp+6Ch+var_54]
            # 111 push edx
            # 112 push eax
            # 113 call CRagConnection_instanceR
            # 118 mov ecx, eax
            # 120 call CRagConnection_GetPacketSize
            # 125 push eax
            # 126 call CRagConnection_instanceR
            # 131 mov ecx, eax
            # 133 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x68\xAB\xAB\xAB\xAB"            # 6
                "\x68\xAB\xAB\xAB\xAB"            # 11
                "\x68\xAB\xAB\xAB\xAB"            # 16
                "\xE8\xAB\xAB\xAB\xAB"            # 21
                "\xE8\xAB\xAB\xAB\xAB"            # 26
                "\x8B\xF8"                        # 31
                "\xFF\xAB"                        # 33
                "\x05\xE8\x03\x00\x00"            # 35
                "\xB9\xAB\xAB\x00\x00"            # 40
                "\x89\x47\xAB"                    # 45
                "\x66\x89\x4C\x24\xAB"            # 48
                "\xFF\xAB"                        # 53
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 55
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 61
                "\x89\x44\x24\xAB"                # 67
                "\xA1\xAB\xAB\xAB\xAB"            # 71
                "\x89\x4C\x24\xAB"                # 76
                "\xB9" + sessionHex +             # 80
                "\x89\x54\x24\xAB"                # 85
                "\x89\x44\x24\xAB"                # 89
                "\xE8\xAB\xAB\xAB\xAB"            # 93
                "\x88\x44\x24\xAB"                # 98
                "\x0F\xBF\x44\x24\xAB"            # 102
                "\x8D\x54\x24\xAB"                # 107
                "\x52"                            # 111
                "\x50"                            # 112
                "\xE8\xAB\xAB\xAB\xAB"            # 113
                "\x8B\xC8"                        # 118
                "\xE8\xAB\xAB\xAB\xAB"            # 120
                "\x50"                            # 125
                "\xE8\xAB\xAB\xAB\xAB"            # 126
                "\x8B\xC8"                        # 131
                "\xE8\xAB\xAB\xAB\xAB"            # 133
                "\x6A\x01"                        # 138
                "\xE8\xAB\xAB\xAB\xAB"            # 140
                "\x8B\xC8"                        # 145
                "\xE8\xAB\xAB\xAB\xAB"            # 147
            )
            setBlockOffset = 148
            sessionOffset = 81
            comboOffset = 0
            instanceOffsets = (27, 114, 127, 141)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 94
            encryptionInitKeysOffset = 22
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 121
            sendPacketOffset = 134
            traceOffset = 0
            setPaddingValueOffsets = 0
            dwTimeOffset = (47, 1)
            packetIdNumOffset = (41, 4)
            clientTimeOffset2 = (70, 1)
            accountIdOffset1 = (63, 4)
            accountIdOffset2 = (88, 1)
            charIdOffset1 = (72, 4)
            charIdOffset2 = (92, 1)
            authCodeOffset1 = (57, 4)
            authCodeOffset2 = (79, 1)
            packetIdOffsets2 = ((52, 1), (106, 1), (110, 1))
            sexOffset2 = (101, 1)
            key1Offset = 17
            key2Offset = 12
            key3Offset = 7
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            offset, section = self.exe.string("PACKET_CZ_ENTER")
            if offset is False:
                czEnter = 0
            else:
                czEnter = section.rawToVa(offset)
            # 0  mov ecx, enc_packet_keys
            # 6  push 45A01BD4h
            # 11 push 6AA48AFh
            # 16 push 273C25CDh
            # 21 call CEncryption_InitKeys
            # 26 push offset aPacket_cz_ente
            # 31 call Trace
            # 36 mov edx, 92Dh
            # 41 add esp, 4
            # 44 mov [ebp+packet.packet_id], dx
            # 51 call edi
            # 53 mov ecx, g_session.m_char_id
            # 59 mov edx, g_session.m_auth_code1
            # 65 mov [ebp+packet.client_time], eax
            # 71 mov eax, g_session.m_account_id
            # 76 mov [ebp+packet.char_id], ecx
            # 82 mov ecx, offset g_session
            # 87 mov [ebp+packet.account_id], eax
            # 93 mov [ebp+packet.session_key1], edx
            # 99 call CSession_GetSex
            # 104 movsx ecx, [ebp+packet.packet_id]
            # 111 mov [ebp+packet.sex], al
            # 117 lea eax, [ebp+packet]
            # 123 push eax
            # 124 push ecx
            # 125 call CRagConnection_instanceR
            # 130 mov ecx, eax
            # 132 call CRagConnection_GetPacketSize
            # 137 push eax
            # 138 call CRagConnection_instanceR
            # 143 mov ecx, eax
            # 145 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x68\xAB\xAB\xAB\xAB"            # 6
                "\x68\xAB\xAB\xAB\xAB"            # 11
                "\x68\xAB\xAB\xAB\xAB"            # 16
                "\xE8\xAB\xAB\xAB\xAB"            # 21
                "\x68" + self.exe.toHex(czEnter, 4) +  # 26
                "\xE8\xAB\xAB\xAB\xAB"            # 31
                "\xBA\xAB\xAB\x00\x00"            # 36
                "\x83\xC4\x04"                    # 41
                "\x66\x89\x95\xAB\xAB\xAB\xAB"    # 44
                "\xFF\xAB"                        # 51
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 53
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 59
                "\x89\x85\xAB\xAB\xAB\xAB"        # 65
                "\xA1\xAB\xAB\xAB\xAB"            # 71
                "\x89\x8D\xAB\xAB\xAB\xAB"        # 76
                "\xB9" + sessionHex +             # 82
                "\x89\x85\xAB\xAB\xAB\xAB"        # 87
                "\x89\x95\xAB\xAB\xAB\xAB"        # 93
                "\xE8\xAB\xAB\xAB\xAB"            # 99
                "\x0F\xBF\x8D\xAB\xAB\xAB\xAB"    # 104
                "\x88\x85\xAB\xAB\xAB\xAB"        # 111
                "\x8D\x85\xAB\xAB\xAB\xAB"        # 117
                "\x50"                            # 123
                "\x51"                            # 124
                "\xE8\xAB\xAB\xAB\xAB"            # 125
                "\x8B\xC8"                        # 130
                "\xE8\xAB\xAB\xAB\xAB"            # 132
                "\x50"                            # 137
                "\xE8\xAB\xAB\xAB\xAB"            # 138
                "\x8B\xC8"                        # 143
                "\xE8\xAB\xAB\xAB\xAB"            # 145
                "\x6A\x01"                        # 150
                "\xE8\xAB\xAB\xAB\xAB"            # 152
                "\x8B\xC8"                        # 157
                "\xE8\xAB\xAB\xAB\xAB"            # 159
            )
            setBlockOffset = 160
            sessionOffset = 83
            comboOffset = 0
            instanceOffsets = (126, 139, 153)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 100
            encryptionInitKeysOffset = 22
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 133
            sendPacketOffset = 146
            traceOffset = 32
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (37, 4)
            clientTimeOffset2 = (67, 4)
            accountIdOffset1 = (72, 4)
            accountIdOffset2 = (89, 4)
            charIdOffset1 = (55, 4)
            charIdOffset2 = (78, 4)
            authCodeOffset1 = (61, 4)
            authCodeOffset2 = (95, 4)
            packetIdOffsets2 = ((47, 4), (107, 4), (119, 4))
            sexOffset2 = (113, 4)
            key1Offset = 17
            key2Offset = 12
            key3Offset = 7
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  call RegCloseKey
            # 6  push offset aPacket_cz_ente
            # 11 call Trace
            # 16 mov eax, 366h
            # 21 add esp, 4
            # 24 mov [ebp+var_26.packet.packet_id], ax
            # 28 call edi
            # 30 mov ecx, g_session.m_account_id
            # 36 mov edx, g_session.m_char_id
            # 42 mov [ebp+var_26.packet.client_time], eax
            # 45 mov eax, g_session.m_auth_code1
            # 50 mov [ebp+var_26.packet.account_id], ecx
            # 53 mov ecx, offset g_session
            # 58 mov [ebp+var_26.packet.char_id], edx
            # 61 mov [ebp+var_26.packet.session_key1], eax
            # 64 call CSession_GetSex
            # 69 movsx edx, [ebp+var_26.packet.packet_id]
            # 73 lea ecx, [ebp+var_26.packet]
            # 76 push ecx
            # 77 push edx
            # 78 mov [ebp+var_26.packet.sex], al
            # 81 call CRagConnection_instanceR
            # 86 mov ecx, eax
            # 88 call CRagConnection_GetPacketSize
            # 93 push eax
            # 94 call CRagConnection_instanceR
            # 99 mov ecx, eax
            # 101 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 0
                "\x68" + self.exe.toHex(czEnter, 4) +  # 6
                "\xE8\xAB\xAB\xAB\xAB"            # 11
                "\xB8\xAB\xAB\x00\x00"            # 16
                "\x83\xC4\x04"                    # 21
                "\x66\x89\x45\xAB"                # 24
                "\xFF\xAB"                        # 28
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 30
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 36
                "\x89\x45\xAB"                    # 42
                "\xA1\xAB\xAB\xAB\xAB"            # 45
                "\x89\x4D\xAB"                    # 50
                "\xB9" + sessionHex +             # 53
                "\x89\x55\xAB"                    # 58
                "\x89\x45\xAB"                    # 61
                "\xE8\xAB\xAB\xAB\xAB"            # 64
                "\x0F\xBF\x55\xAB"                # 69
                "\x8D\x4D\xAB"                    # 73
                "\x51"                            # 76
                "\x52"                            # 77
                "\x88\x45\xAB"                    # 78
                "\xE8\xAB\xAB\xAB\xAB"            # 81
                "\x8B\xC8"                        # 86
                "\xE8\xAB\xAB\xAB\xAB"            # 88
                "\x50"                            # 93
                "\xE8\xAB\xAB\xAB\xAB"            # 94
                "\x8B\xC8"                        # 99
                "\xE8\xAB\xAB\xAB\xAB"            # 101
                "\x6A\x01"                        # 106
                "\xE8\xAB\xAB\xAB\xAB"            # 108
                "\x8B\xC8"                        # 113
                "\xE8\xAB\xAB\xAB\xAB"            # 115
            )
            setBlockOffset = 116
            sessionOffset = 54
            comboOffset = 0
            instanceOffsets = (82, 95, 109)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 65
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 89
            sendPacketOffset = 102
            traceOffset = 12
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (17, 4)
            clientTimeOffset2 = (44, 1)
            accountIdOffset1 = (32, 4)
            accountIdOffset2 = (52, 1)
            charIdOffset1 = (38, 4)
            charIdOffset2 = (60, 1)
            authCodeOffset1 = (46, 4)
            authCodeOffset2 = (63, 1)
            packetIdOffsets2 = ((27, 1), (72, 1), (75, 1))
            sexOffset2 = (80, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = True
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push ebp
            # 7  push ebp
            # 8  push ebp
            # 9  call CEncryption_InitKeys
            # 14 push offset aPacket_cz_ente
            # 19 call Trace
            # 24 mov ecx, 436h
            # 29 add esp, 4
            # 32 mov [esp+5374h+packet.packet_id], cx
            # 37 call edi
            # 39 mov ecx, g_session.m_auth_code1
            # 45 mov edx, g_session.m_account_id
            # 51 mov [esp+5374h+packet.client_time], eax
            # 55 mov eax, g_session.m_char_id
            # 60 mov [esp+5374h+packet.session_key1], ecx
            # 64 mov ecx, offset g_session
            # 69 mov [esp+5374h+packet.account_id], edx
            # 73 mov [esp+5374h+packet.char_id], eax
            # 77 call CSession_GetSex
            # 82 mov [esp+5374h+packet.sex], al
            # 86 movsx eax, [esp+5374h+packet.packet_id]
            # 91 lea edx, [esp+5374h+packet]
            # 95 push edx
            # 96 push eax
            # 97 call CRagConnection_instanceR
            # 102 mov ecx, eax
            # 104 call CRagConnection_GetPacketSize
            # 109 push eax
            # 110 call CRagConnection_instanceR
            # 115 mov ecx, eax
            # 117 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x55"                            # 6
                "\x55"                            # 7
                "\x55"                            # 8
                "\xE8\xAB\xAB\xAB\xAB"            # 9
                "\x68" + self.exe.toHex(czEnter, 4) +  # 14
                "\xE8\xAB\xAB\xAB\xAB"            # 19
                "\xB9\xAB\xAB\x00\x00"            # 24
                "\x83\xC4\x04"                    # 29
                "\x66\x89\x4C\x24\xAB"            # 32
                "\xFF\xAB"                        # 37
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 39
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 45
                "\x89\x44\x24\xAB"                # 51
                "\xA1\xAB\xAB\xAB\xAB"            # 55
                "\x89\x4C\x24\xAB"                # 60
                "\xB9" + sessionHex +             # 64
                "\x89\x54\x24\xAB"                # 69
                "\x89\x44\x24\xAB"                # 73
                "\xE8\xAB\xAB\xAB\xAB"            # 77
                "\x88\x44\x24\xAB"                # 82
                "\x0F\xBF\x44\x24\xAB"            # 86
                "\x8D\x54\x24\xAB"                # 91
                "\x52"                            # 95
                "\x50"                            # 96
                "\xE8\xAB\xAB\xAB\xAB"            # 97
                "\x8B\xC8"                        # 102
                "\xE8\xAB\xAB\xAB\xAB"            # 104
                "\x50"                            # 109
                "\xE8\xAB\xAB\xAB\xAB"            # 110
                "\x8B\xC8"                        # 115
                "\xE8\xAB\xAB\xAB\xAB"            # 117
                "\x6A\x01"                        # 122
                "\xE8\xAB\xAB\xAB\xAB"            # 124
                "\x8B\xC8"                        # 129
                "\xE8\xAB\xAB\xAB\xAB"            # 131
            )
            setBlockOffset = 132
            sessionOffset = 65
            comboOffset = 0
            instanceOffsets = (98, 111, 125)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 78
            encryptionInitKeysOffset = 10
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 105
            sendPacketOffset = 118
            traceOffset = 20
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (25, 4)
            clientTimeOffset2 = (54, 1)
            accountIdOffset1 = (47, 4)
            accountIdOffset2 = (72, 1)
            charIdOffset1 = (56, 4)
            charIdOffset2 = (76, 1)
            authCodeOffset1 = (41, 4)
            authCodeOffset2 = (63, 1)
            packetIdOffsets2 = ((36, 1), (90, 1), (94, 1))
            sexOffset2 = (85, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  push 0
            # 10 push 0
            # 12 call CEncryption_InitKeys
            # 17 push offset aPacket_cz_ente
            # 22 call Trace
            # 27 mov ecx, 436h
            # 32 add esp, 4
            # 35 mov [esp+3D0h+packet.packet_id], cx
            # 40 call edi
            # 42 mov ecx, g_session.m_auth_code1
            # 48 mov edx, g_session.m_account_id
            # 54 mov [esp+3D0h+packet.client_time], eax
            # 58 mov eax, g_session.m_char_id
            # 63 mov [esp+3D0h+packet.session_key1], ecx
            # 67 mov ecx, offset g_session
            # 72 mov [esp+3D0h+packet.account_id], edx
            # 76 mov [esp+3D0h+packet.char_id], eax
            # 80 call CSession_GetSex
            # 85 mov [esp+3D0h+packet.sex], al
            # 89 movsx eax, [esp+3D0h+packet.packet_id]
            # 94 lea edx, [esp+3D0h+packet]
            # 98 push edx
            # 99 push eax
            # 100 call CRagConnection_instanceR
            # 105 mov ecx, eax
            # 107 call CRagConnection_GetPacketSize
            # 112 push eax
            # 113 call CRagConnection_instanceR
            # 118 mov ecx, eax
            # 120 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x6A\x00"                        # 6
                "\x6A\x00"                        # 8
                "\x6A\x00"                        # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 12
                "\x68" + self.exe.toHex(czEnter, 4) +  # 17
                "\xE8\xAB\xAB\xAB\xAB"            # 22
                "\xB9\xAB\xAB\x00\x00"            # 27
                "\x83\xC4\x04"                    # 32
                "\x66\x89\x4C\x24\xAB"            # 35
                "\xFF\xAB"                        # 40
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 48
                "\x89\x44\x24\xAB"                # 54
                "\xA1\xAB\xAB\xAB\xAB"            # 58
                "\x89\x4C\x24\xAB"                # 63
                "\xB9" + sessionHex +             # 67
                "\x89\x54\x24\xAB"                # 72
                "\x89\x44\x24\xAB"                # 76
                "\xE8\xAB\xAB\xAB\xAB"            # 80
                "\x88\x44\x24\xAB"                # 85
                "\x0F\xBF\x44\x24\xAB"            # 89
                "\x8D\x54\x24\xAB"                # 94
                "\x52"                            # 98
                "\x50"                            # 99
                "\xE8\xAB\xAB\xAB\xAB"            # 100
                "\x8B\xC8"                        # 105
                "\xE8\xAB\xAB\xAB\xAB"            # 107
                "\x50"                            # 112
                "\xE8\xAB\xAB\xAB\xAB"            # 113
                "\x8B\xC8"                        # 118
                "\xE8\xAB\xAB\xAB\xAB"            # 120
                "\x6A\x01"                        # 125
                "\xE8\xAB\xAB\xAB\xAB"            # 127
                "\x8B\xC8"                        # 132
                "\xE8\xAB\xAB\xAB\xAB"            # 134
            )
            setBlockOffset = 135
            sessionOffset = 68
            comboOffset = 0
            instanceOffsets = (101, 114, 128)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 81
            encryptionInitKeysOffset = 13
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 108
            sendPacketOffset = 121
            traceOffset = 23
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (28, 4)
            clientTimeOffset2 = (57, 1)
            accountIdOffset1 = (50, 4)
            accountIdOffset2 = (75, 1)
            charIdOffset1 = (59, 4)
            charIdOffset2 = (79, 1)
            authCodeOffset1 = (44, 4)
            authCodeOffset2 = (66, 1)
            packetIdOffsets2 = ((39, 1), (93, 1), (97, 1))
            sexOffset2 = (88, 1)
            # here keys one byte size, ignoring because normal keys is 4 bytes
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  push 0
            # 10 push 0
            # 12 call CEncryption_InitKeys
            # 17 push offset aPacket_cz_ente
            # 22 call Trace
            # 27 mov ecx, 436h
            # 32 add esp, 4
            # 35 mov [esp+3D0h+packet.packet_id], cx
            # 40 call edi
            # 42 mov ecx, g_session.m_auth_code1
            # 48 mov edx, g_session.m_account_id
            # 54 mov [esp+3D0h+packet.client_time], eax
            # 58 mov eax, g_session.m_char_id
            # 63 mov [esp+3D0h+packet.session_key1], ecx
            # 67 mov ecx, offset g_session
            # 72 mov [esp+3D0h+packet.account_id], edx
            # 76 mov [esp+3D0h+packet.char_id], eax
            # 80 call CSession_GetSex
            # 85 mov [esp+3D0h+packet.sex], al
            # 89 movsx eax, [esp+3D0h+packet.packet_id]
            # 94 lea edx, [esp+3D0h+packet]
            # 98 push edx
            # 99 push eax
            # 100 call CRagConnection_instanceR
            # 105 mov ecx, eax
            # 107 call CRagConnection_GetPacketSize
            # 112 push eax
            # 113 call CRagConnection_instanceR
            # 118 mov ecx, eax
            # 120 call CRagConnection_SendPacket
            # 0  push ebp
            # 1  call CRagConnection_instanceR
            # 6  mov ecx, eax
            # 8  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x6A\x00"                        # 6
                "\x6A\x00"                        # 8
                "\x6A\x00"                        # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 12
                "\x68" + self.exe.toHex(czEnter, 4) +  # 17
                "\xE8\xAB\xAB\xAB\xAB"            # 22
                "\xB9\xAB\xAB\x00\x00"            # 27
                "\x83\xC4\x04"                    # 32
                "\x66\x89\x4C\x24\xAB"            # 35
                "\xFF\xAB"                        # 40
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 48
                "\x89\x44\x24\xAB"                # 54
                "\xA1\xAB\xAB\xAB\xAB"            # 58
                "\x89\x4C\x24\xAB"                # 63
                "\xB9" + sessionHex +             # 67
                "\x89\x54\x24\xAB"                # 72
                "\x89\x44\x24\xAB"                # 76
                "\xE8\xAB\xAB\xAB\xAB"            # 80
                "\x88\x44\x24\xAB"                # 85
                "\x0F\xBF\x44\x24\xAB"            # 89
                "\x8D\x54\x24\xAB"                # 94
                "\x52"                            # 98
                "\x50"                            # 99
                "\xE8\xAB\xAB\xAB\xAB"            # 100
                "\x8B\xC8"                        # 105
                "\xE8\xAB\xAB\xAB\xAB"            # 107
                "\x50"                            # 112
                "\xE8\xAB\xAB\xAB\xAB"            # 113
                "\x8B\xC8"                        # 118
                "\xE8\xAB\xAB\xAB\xAB"            # 120
                "\x55"                            # 125
                "\xE8\xAB\xAB\xAB\xAB"            # 126
                "\x8B\xC8"                        # 131
                "\xE8\xAB\xAB\xAB\xAB"            # 133
            )
            setBlockOffset = 134
            sessionOffset = 68
            comboOffset = 0
            instanceOffsets = (101, 114, 127)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 81
            encryptionInitKeysOffset = 13
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 108
            sendPacketOffset = 121
            traceOffset = 23
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (28, 4)
            clientTimeOffset2 = (57, 1)
            accountIdOffset1 = (50, 4)
            accountIdOffset2 = (75, 1)
            charIdOffset1 = (59, 4)
            charIdOffset2 = (79, 1)
            authCodeOffset1 = (44, 4)
            authCodeOffset2 = (66, 1)
            packetIdOffsets2 = ((39, 1), (93, 1), (97, 1))
            sexOffset2 = (88, 1)
            # here keys one byte size, ignoring because normal keys is 4 bytes
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  push 0
            # 10 push 0
            # 12 call CEncryption_InitKeys
            # 17 push offset aPacket_cz_ente
            # 22 call Trace
            # 27 mov ecx, 436h
            # 32 add esp, 4
            # 35 mov [esp+3F4h+Dst.packet.packet_id], cx
            # 40 call edi
            # 42 mov ecx, g_session.m_auth_code1
            # 48 mov edx, g_session.m_account_id
            # 54 mov [esp+3F4h+Dst.packet.client_time], eax
            # 58 mov eax, g_session.m_char_id
            # 63 mov [esp+3F4h+Dst.packet.session_key1], ecx
            # 67 mov ecx, offset g_session
            # 72 mov [esp+3F4h+Dst.packet.account_id], edx
            # 76 mov [esp+3F4h+Dst.packet.char_id], eax
            # 80 call CSession_GetSex
            # 85 mov [esp+3F4h+Dst.packet.sex], al
            # 92 movsx eax, [esp+3F4h+Dst.packet.packet_id]
            # 97 lea edx, [esp+3F4h+Dst.packet]
            # 101 push edx
            # 102 push eax
            # 103 call CRagConnection_instanceR
            # 108 mov ecx, eax
            # 110 call CRagConnection_GetPacketSize
            # 115 push eax
            # 116 call CRagConnection_instanceR
            # 121 mov ecx, eax
            # 123 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x6A\x00"                        # 6
                "\x6A\x00"                        # 8
                "\x6A\x00"                        # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 12
                "\x68" + self.exe.toHex(czEnter, 4) +  # 17
                "\xE8\xAB\xAB\xAB\xAB"            # 22
                "\xB9\xAB\xAB\x00\x00"            # 27
                "\x83\xC4\x04"                    # 32
                "\x66\x89\x4C\x24\xAB"            # 35
                "\xFF\xAB"                        # 40
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 48
                "\x89\x44\x24\xAB"                # 54
                "\xA1\xAB\xAB\xAB\xAB"            # 58
                "\x89\x4C\x24\xAB"                # 63
                "\xB9" + sessionHex +             # 67
                "\x89\x54\x24\xAB"                # 72
                "\x89\x44\x24\xAB"                # 76
                "\xE8\xAB\xAB\xAB\xAB"            # 80
                "\x88\x84\x24\xAB\xAB\xAB\xAB"    # 85
                "\x0F\xBF\x44\x24\xAB"            # 92
                "\x8D\x54\x24\xAB"                # 97
                "\x52"                            # 101
                "\x50"                            # 102
                "\xE8\xAB\xAB\xAB\xAB"            # 103
                "\x8B\xC8"                        # 108
                "\xE8\xAB\xAB\xAB\xAB"            # 110
                "\x50"                            # 115
                "\xE8\xAB\xAB\xAB\xAB"            # 116
                "\x8B\xC8"                        # 121
                "\xE8\xAB\xAB\xAB\xAB"            # 123
                "\x6A\x01"                        # 128
                "\xE8\xAB\xAB\xAB\xAB"            # 130
                "\x8B\xC8"                        # 135
                "\xE8\xAB\xAB\xAB\xAB"            # 137
            )
            setBlockOffset = 138
            sessionOffset = 68
            comboOffset = 0
            instanceOffsets = (104, 117, 131)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 81
            encryptionInitKeysOffset = 13
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 111
            sendPacketOffset = 124
            traceOffset = 23
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (28, 4)
            clientTimeOffset2 = (57, 1)
            accountIdOffset1 = (50, 4)
            accountIdOffset2 = (75, 1)
            charIdOffset1 = (59, 4)
            charIdOffset2 = (79, 1)
            authCodeOffset1 = (44, 4)
            authCodeOffset2 = (66, 1)
            packetIdOffsets2 = ((39, 1), (96, 1), (100, 1))
            sexOffset2 = (88, 4)
            # here keys one byte size, ignoring because normal keys is 4 bytes
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  push 0
            # 10 push 0
            # 12 call CEncryption_InitKeys
            # 17 push offset aPacket_cz_ente
            # 22 call Trace
            # 27 mov ecx, 436h
            # 32 add esp, 4
            # 35 mov [esp+3F4h+Dst.packet.packet_id], cx
            # 40 call edi
            # 42 mov ecx, g_session.m_auth_code1
            # 48 mov edx, g_session.m_account_id
            # 54 mov [esp+3F4h+Dst.packet.client_time], eax
            # 58 mov eax, g_session.m_char_id
            # 63 mov [esp+3F4h+Dst.packet.session_key1], ecx
            # 67 mov ecx, offset g_session
            # 72 mov [esp+3F4h+Dst.packet.account_id], edx
            # 76 mov [esp+3F4h+Dst.packet.char_id], eax
            # 80 call CSession_GetSex
            # 85 mov [esp+3F4h+Dst.packet.sex], al
            # 92 movsx eax, [esp+3F4h+Dst.packet.packet_id]
            # 97 lea edx, [esp+3F4h+Dst.packet]
            # 101 push edx
            # 102 push eax
            # 103 call CRagConnection_instanceR
            # 108 mov ecx, eax
            # 110 call CRagConnection_GetPacketSize
            # 115 push eax
            # 116 call CRagConnection_instanceR
            # 121 mov ecx, eax
            # 123 call CRagConnection_SendPacket
            # 0  push ebp
            # 1  call CRagConnection_instanceR
            # 6  mov ecx, eax
            # 8  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x6A\x00"                        # 6
                "\x6A\x00"                        # 8
                "\x6A\x00"                        # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 12
                "\x68" + self.exe.toHex(czEnter, 4) +  # 17
                "\xE8\xAB\xAB\xAB\xAB"            # 22
                "\xB9\xAB\xAB\x00\x00"            # 27
                "\x83\xC4\x04"                    # 32
                "\x66\x89\x4C\x24\xAB"            # 35
                "\xFF\xAB"                        # 40
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 48
                "\x89\x44\x24\xAB"                # 54
                "\xA1\xAB\xAB\xAB\xAB"            # 58
                "\x89\x4C\x24\xAB"                # 63
                "\xB9" + sessionHex +             # 67
                "\x89\x54\x24\xAB"                # 72
                "\x89\x44\x24\xAB"                # 76
                "\xE8\xAB\xAB\xAB\xAB"            # 80
                "\x88\x84\x24\xAB\xAB\xAB\xAB"    # 85
                "\x0F\xBF\x44\x24\xAB"            # 92
                "\x8D\x54\x24\xAB"                # 97
                "\x52"                            # 101
                "\x50"                            # 102
                "\xE8\xAB\xAB\xAB\xAB"            # 103
                "\x8B\xC8"                        # 108
                "\xE8\xAB\xAB\xAB\xAB"            # 110
                "\x50"                            # 115
                "\xE8\xAB\xAB\xAB\xAB"            # 116
                "\x8B\xC8"                        # 121
                "\xE8\xAB\xAB\xAB\xAB"            # 123
                "\x55"                            # 128
                "\xE8\xAB\xAB\xAB\xAB"            # 129
                "\x8B\xC8"                        # 134
                "\xE8\xAB\xAB\xAB\xAB"            # 136
            )
            setBlockOffset = 137
            sessionOffset = 68
            comboOffset = 0
            instanceOffsets = (104, 117, 130)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 81
            encryptionInitKeysOffset = 13
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 111
            sendPacketOffset = 124
            traceOffset = 23
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (28, 4)
            clientTimeOffset2 = (57, 1)
            accountIdOffset1 = (50, 4)
            accountIdOffset2 = (75, 1)
            charIdOffset1 = (59, 4)
            charIdOffset2 = (79, 1)
            authCodeOffset1 = (44, 4)
            authCodeOffset2 = (66, 1)
            packetIdOffsets2 = ((39, 1), (96, 1), (100, 1))
            sexOffset2 = (88, 4)
            # here keys one byte size, ignoring because normal keys is 4 bytes
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, enc_packet_keys
            # 6  push 0
            # 8  push 0
            # 10 push 0
            # 12 call CEncryption_InitKeys
            # 17 push offset aPacket_cz_ente
            # 22 call Trace
            # 27 mov ecx, 436h
            # 32 add esp, 4
            # 35 mov [esp+3F4h+Dst.packet.packet_id], cx
            # 40 call edi
            # 42 mov ecx, g_session.m_auth_code1
            # 48 mov edx, g_session.m_account_id
            # 54 mov [esp+3F4h+Dst.packet.client_time], eax
            # 58 mov eax, g_session.m_char_id
            # 63 mov [esp+3F4h+Dst.packet.session_key1], ecx
            # 67 mov ecx, offset g_session
            # 72 mov [esp+3F4h+Dst.packet.account_id], edx
            # 76 mov [esp+3F4h+Dst.packet.char_id], eax
            # 80 call CSession_GetSex
            # 85 mov [esp+3F4h+Dst.packet.sex], al
            # 92 movsx eax, [esp+3F4h+Dst.packet.packet_id]
            # 97 lea edx, [esp+3F4h+Dst.packet]
            # 101 push edx
            # 102 push eax
            # 103 call CRagConnection_instanceR
            # 108 mov ecx, eax
            # 110 call CRagConnection_GetPacketSize
            # 115 push eax
            # 116 call CRagConnection_instanceR
            # 121 mov ecx, eax
            # 123 call CRagConnection_SendPacket
            # 0  push ebp
            # 1  call CRagConnection_instanceR
            # 6  mov ecx, eax
            # 8  call CConnection_SetBlock
            code = (
                "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
                "\x6A\x00"                        # 6
                "\x6A\x00"                        # 8
                "\x6A\x00"                        # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 12
                "\x68" + self.exe.toHex(czEnter, 4) +  # 17
                "\xE8\xAB\xAB\xAB\xAB"            # 22
                "\xB9\xAB\xAB\x00\x00"            # 27
                "\x83\xC4\x04"                    # 32
                "\x66\x89\x4C\x24\xAB"            # 35
                "\xFF\xAB"                        # 40
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 42
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 48
                "\x89\x44\x24\xAB"                # 54
                "\xA1\xAB\xAB\xAB\xAB"            # 58
                "\x89\x4C\x24\xAB"                # 63
                "\xB9" + sessionHex +             # 67
                "\x89\x54\x24\xAB"                # 72
                "\x89\x44\x24\xAB"                # 76
                "\xE8\xAB\xAB\xAB\xAB"            # 80
                "\x88\x84\x24\xAB\xAB\xAB\xAB"    # 85
                "\x0F\xBF\x44\x24\xAB"            # 92
                "\x8D\x54\x24\xAB"                # 97
                "\x52"                            # 101
                "\x50"                            # 102
                "\xE8\xAB\xAB\xAB\xAB"            # 103
                "\x8B\xC8"                        # 108
                "\xE8\xAB\xAB\xAB\xAB"            # 110
                "\x50"                            # 115
                "\xE8\xAB\xAB\xAB\xAB"            # 116
                "\x8B\xC8"                        # 121
                "\xE8\xAB\xAB\xAB\xAB"            # 123
                "\x55"                            # 128
                "\xE8\x17\x89\xD3\xFF"            # 129
                "\x8B\xC8"                        # 134
                "\xE8\x40\x85\xD3\xFF"            # 136
            )
            setBlockOffset = 137
            sessionOffset = 68
            comboOffset = 0
            instanceOffsets = (104, 117, 130)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 81
            encryptionInitKeysOffset = 13
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 111
            sendPacketOffset = 124
            traceOffset = 23
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (28, 4)
            clientTimeOffset2 = (57, 1)
            accountIdOffset1 = (50, 4)
            accountIdOffset2 = (75, 1)
            charIdOffset1 = (59, 4)
            charIdOffset2 = (79, 1)
            authCodeOffset1 = (44, 4)
            authCodeOffset2 = (66, 1)
            packetIdOffsets2 = ((39, 1), (96, 1), (100, 1))
            sexOffset2 = (88, 4)
            # here keys one byte size, ignoring because normal keys is 4 bytes
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False and self.instanceR != 0 and self.comboAddr == 0:
        # no more combo and keys
        # search in recv_packet_92
        # 0  call CRagConnection_instanceR
        # 5  mov edi, eax
        # 7  call esi
        # 9  add eax, 3E8h
        # 14 mov [edi+1Ch], eax
        # 17 mov [ebp+packet.packet_id], 436h
        # 23 call esi
        # 25 mov ecx, g_session_m_char_id
        # 31 mov edx, g_session_m_auth_key1
        # 37 mov [ebp+packet.client_time], eax
        # 40 mov eax, g_session_m_account_id
        # 45 mov [ebp+packet.char_id], ecx
        # 48 mov ecx, offset g_session
        # 53 mov [ebp+packet.account_id], eax
        # 56 mov [ebp+packet.session_key1], edx
        # 59 call CSession_GetSex
        # 64 movsx ecx, [ebp+packet.packet_id]
        # 68 mov [ebp+packet.sex], al
        # 71 lea eax, [ebp+packet]
        # 74 push eax
        # 75 push ecx
        # 76 call CRagConnection_instanceR
        # 81 mov ecx, eax
        # 83 call CRagConnection_GetPacketSize
        # 88 push eax
        # 89 call CRagConnection_instanceR
        # 94 mov ecx, eax
        # 96 call CRagConnection_SendPacket
        # 0  push 1
        # 2  call CRagConnection_instanceR
        # 7  mov ecx, eax
        # 9  call CConnection_SetBlock
        code = (
            "\xE8\xAB\xAB\xAB\xAB"            # 0
            "\x8B\xF8"                        # 5
            "\xFF\xAB"                        # 7
            "\x05\xE8\x03\x00\x00"            # 9
            "\x89\x47\xAB"                    # 14
            "\x66\xC7\x45\xAB\xAB\xAB"        # 17
            "\xFF\xAB"                        # 23
            "\x8B\x0D\xAB\xAB\xAB\xAB"        # 25
            "\x8B\x15\xAB\xAB\xAB\xAB"        # 31
            "\x89\x45\xAB"                    # 37
            "\xA1\xAB\xAB\xAB\xAB"            # 40
            "\x89\x4D\xAB"                    # 45
            "\xB9" + sessionHex +             # 48
            "\x89\x45\xAB"                    # 53
            "\x89\x55\xAB"                    # 56
            "\xE8\xAB\xAB\xAB\xAB"            # 59
            "\x0F\xBF\x4D\xAB"                # 64
            "\x88\x45\xAB"                    # 68
            "\x8D\x45\xAB"                    # 71
            "\x50"                            # 74
            "\x51"                            # 75
            "\xE8\xAB\xAB\xAB\xAB"            # 76
            "\x8B\xC8"                        # 81
            "\xE8\xAB\xAB\xAB\xAB"            # 83
            "\x50"                            # 88
            "\xE8\xAB\xAB\xAB\xAB"            # 89
            "\x8B\xC8"                        # 94
            "\xE8\xAB\xAB\xAB\xAB"            # 96
            "\x6A\x01"                        # 101
            "\xE8\xAB\xAB\xAB\xAB"            # 103
            "\x8B\xC8"                        # 108
            "\xE8\xAB\xAB\xAB\xAB"            # 110
        )
        setBlockOffset = 111
        sessionOffset = 49
        comboOffset = 0
        instanceOffsets = (1, 77, 90, 104)
        enc_packet_keysOffset = 0
        CSession_GetSexOffset = 60
        encryptionInitKeysOffset = 0
        encryptionInitKeys0Offset = 0
        getPacketSizeOffset = 84
        sendPacketOffset = 97
        traceOffset = 0
        setPaddingValueOffsets = 0
        dwTimeOffset = (16, 1)
        packetIdNumOffset = (21, 2)
        clientTimeOffset2 = (39, 1)
        accountIdOffset1 = (41, 4)
        accountIdOffset2 = (55, 1)
        charIdOffset1 = (27, 4)
        charIdOffset2 = (47, 1)
        authCodeOffset1 = (33, 4)
        authCodeOffset2 = (58, 1)
        packetIdOffsets2 = ((20, 1), (67, 1), (73, 1))
        sexOffset2 = (70, 1)
        key1Offset = 0
        key2Offset = 0
        key3Offset = 0
        searchEncryptionInitKeys = False
        offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  call CRagConnection_instanceR
            # 5  mov edi, eax
            # 7  call esi
            # 9  add eax, 3E8h
            # 14 mov ecx, 436h
            # 19 mov [edi+1Ch], eax
            # 22 mov [esp+68h+Src.packet_id], cx
            # 27 call esi
            # 29 mov ecx, g_session_m_auth_key1
            # 35 mov edx, g_session_m_account_id
            # 41 mov [esp+68h+Src.client_time], eax
            # 45 mov eax, g_session_m_char_id
            # 50 mov [esp+68h+Src.session_key1], ecx
            # 54 mov ecx, offset g_session
            # 59 mov [esp+68h+Src.account_id], edx
            # 63 mov [esp+68h+Src.char_id], eax
            # 67 call CSession_GetSex
            # 72 mov [esp+68h+Src.sex], al
            # 76 movsx eax, [esp+68h+Src.packet_id]
            # 81 lea edx, [esp+68h+Src]
            # 85 push edx
            # 86 push eax
            # 87 call CRagConnection_instanceR
            # 92 mov ecx, eax
            # 94 call CRagConnection_GetPacketSize
            # 99 push eax
            # 100 call CRagConnection_instanceR
            # 105 mov ecx, eax
            # 107 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\xE8\xAB\xAB\xAB\xAB"            # 0
                "\x8B\xF8"                        # 5
                "\xFF\xAB"                        # 7
                "\x05\xE8\x03\x00\x00"            # 9
                "\xB9\xAB\xAB\x00\x00"            # 14
                "\x89\x47\xAB"                    # 19
                "\x66\x89\x4C\x24\xAB"            # 22
                "\xFF\xD6"                        # 27
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 29
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 35
                "\x89\x44\x24\xAB"                # 41
                "\xA1\xAB\xAB\xAB\xAB"            # 45
                "\x89\x4C\x24\xAB"                # 50
                "\xB9" + sessionHex +             # 54
                "\x89\x54\x24\xAB"                # 59
                "\x89\x44\x24\xAB"                # 63
                "\xE8\xAB\xAB\xAB\xAB"            # 67
                "\x88\x44\x24\xAB"                # 72
                "\x0F\xBF\x44\x24\xAB"            # 76
                "\x8D\x54\x24\xAB"                # 81
                "\x52"                            # 85
                "\x50"                            # 86
                "\xE8\xAB\xAB\xAB\xAB"            # 87
                "\x8B\xC8"                        # 92
                "\xE8\xAB\xAB\xAB\xAB"            # 94
                "\x50"                            # 99
                "\xE8\xAB\xAB\xAB\xAB"            # 100
                "\x8B\xC8"                        # 105
                "\xE8\xAB\xAB\xAB\xAB"            # 107
                "\x6A\x01"                        # 112
                "\xE8\xAB\xAB\xAB\xAB"            # 114
                "\x8B\xC8"                        # 119
                "\xE8\xAB\xAB\xAB\xAB"            # 121
            )
            setBlockOffset = 122
            sessionOffset = 55
            comboOffset = 0
            instanceOffsets = (1, 88, 101, 115)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 68
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 95
            sendPacketOffset = 108
            traceOffset = 0
            setPaddingValueOffsets = 0
            dwTimeOffset = (21, 1)
            packetIdNumOffset = (15, 4)
            clientTimeOffset2 = (44, 1)
            accountIdOffset1 = (37, 4)
            accountIdOffset2 = (62, 1)
            charIdOffset1 = (46, 4)
            charIdOffset2 = (66, 1)
            authCodeOffset1 = (31, 4)
            authCodeOffset2 = (53, 1)
            packetIdOffsets2 = ((26, 1), (80, 1), (84, 1))
            sexOffset2 = (75, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  call CRagConnection_instanceR
            # 5  mov esi, eax
            # 7  call edi
            # 9  add eax, 3E8h
            # 14 mov [esi+1Ch], eax
            # 17 mov [ebp+packet.packet_id], 436h
            # 23 call edi
            # 25 mov ecx, g_session_m_char_id
            # 31 mov edx, g_session_m_auth_key1
            # 37 mov [ebp+packet.client_time], eax
            # 40 mov eax, g_session_m_account_id
            # 45 mov [ebp+packet.char_id], ecx
            # 48 mov ecx, offset g_session
            # 53 mov [ebp+packet.account_id], eax
            # 56 mov [ebp+packet.session_key1], edx
            # 59 call CSession_GetSex
            # 64 movsx ecx, [ebp+packet.packet_id]
            # 68 mov [ebp+packet.sex], al
            # 71 lea eax, [ebp+packet]
            # 74 push eax
            # 75 push ecx
            # 76 call CRagConnection_instanceR
            # 81 mov ecx, eax
            # 83 call CRagConnection_GetPacketSize
            # 88 push eax
            # 89 call CRagConnection_instanceR
            # 94 mov ecx, eax
            # 96 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\xE8\xAB\xAB\xAB\xAB"            # 0
                "\x8B\xF0"                        # 5
                "\xFF\xAB"                        # 7
                "\x05\xE8\x03\x00\x00"            # 9
                "\x89\x46\xAB"                    # 14
                "\x66\xC7\x45\xAB\xAB\xAB"        # 17
                "\xFF\xAB"                        # 23
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 25
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 31
                "\x89\x45\xAB"                    # 37
                "\xA1\xAB\xAB\xAB\xAB"            # 40
                "\x89\x4D\xAB"                    # 45
                "\xB9" + sessionHex +             # 48
                "\x89\x45\xAB"                    # 53
                "\x89\x55\xAB"                    # 56
                "\xE8\xAB\xAB\xAB\xAB"            # 59
                "\x0F\xBF\x4D\xAB"                # 64
                "\x88\x45\xAB"                    # 68
                "\x8D\x45\xAB"                    # 71
                "\x50"                            # 74
                "\x51"                            # 75
                "\xE8\xAB\xAB\xAB\xAB"            # 76
                "\x8B\xC8"                        # 81
                "\xE8\xAB\xAB\xAB\xAB"            # 83
                "\x50"                            # 88
                "\xE8\xAB\xAB\xAB\xAB"            # 89
                "\x8B\xC8"                        # 94
                "\xE8\xAB\xAB\xAB\xAB"            # 96
                "\x6A\x01"                        # 101
                "\xE8\xAB\xAB\xAB\xAB"            # 103
                "\x8B\xC8"                        # 108
                "\xE8\xAB\xAB\xAB\xAB"            # 110
            )
            setBlockOffset = 111
            sessionOffset = 49
            comboOffset = 0
            instanceOffsets = (1, 77, 90, 104)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 60
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 84
            sendPacketOffset = 97
            traceOffset = 0
            setPaddingValueOffsets = 0
            dwTimeOffset = (16, 1)
            packetIdNumOffset = (21, 2)
            clientTimeOffset2 = (39, 1)
            accountIdOffset1 = (41, 4)
            accountIdOffset2 = (55, 1)
            charIdOffset1 = (27, 4)
            charIdOffset2 = (47, 1)
            authCodeOffset1 = (33, 4)
            authCodeOffset2 = (58, 1)
            packetIdOffsets2 = ((20, 1), (67, 1), (73, 1))
            sexOffset2 = (70, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False and self.instanceR != 0 \
       and (self.packetVersion < "20100000" or self.comboAddr == 0):
        if czEnter == 0:
            offset, section = self.exe.string("PACKET_CZ_ENTER")
            if offset is False:
                czEnter = 0
            else:
                czEnter = section.rawToVa(offset)
        # packet struct unknown, skipping fields because this
        # 2009-01-07
        # some fields encrypted in this way
        # p.account_id = client_time ^ g_session.account_id ^ 0xC03B;
        # p.char_id = g_session.m_char_id ^ g_session.m_auth_key1 ^ 0xC03B;
        #
        # 0  mov ecx, offset enc_packet_keys
        # 5  call CEncryption_InitKeys0
        # 10 push offset aPacket_cz_ente
        # 15 call Trace
        # 20 add esp, 4
        # 23 mov [ebp+packet.packet_id], 428h
        # 29 call ds:timeGetTime
        # 35 mov ecx, ds:g_session_m_auth_key1
        # 41 mov edx, ds:g_session_account_id
        # 47 mov [ebp+packet.client_time], eax
        # 50 mov eax, ds:g_session_m_char_id
        # 55 mov [ebp+packet.session_key1], ecx
        # 58 mov ecx, offset g_session
        # 63 mov [ebp+packet.account_id], edx
        # 66 mov [ebp+packet.char_id], eax
        # 69 call CSession_GetSex
        # 74 mov edx, [ebp+packet.account_id]
        # 77 mov edi, [ebp+packet.client_time]
        # 80 xor edx, edi
        # 82 mov [ebp+packet.sex], al
        # 85 mov eax, [ebp+packet.session_key1]
        # 88 xor edx, 0C03Bh
        # 94 mov [ebp+packet.account_id], edx
        # 97 mov edx, [ebp+packet.char_id]
        # 100 xor eax, edx
        # 102 lea ecx, [ebp+packet]
        # 105 movsx edx, [ebp+packet.packet_id]
        # 109 xor eax, 0C03Bh
        # 114 push ecx
        # 115 push edx
        # 116 mov [ebp+packet.char_id], eax
        # 119 call CRagConnection_instanceR
        # 124 mov ecx, eax
        # 126 call CRagConnection_GetPacketSize
        # 131 push eax
        # 132 call CRagConnection_instanceR
        # 137 mov ecx, eax
        # 139 call CRagConnection_SendPacket
        # 0  push 1
        # 2  call CRagConnection_instanceR
        # 7  mov ecx, eax
        # 9  call CConnection_SetBlock
        code = (
            "\xB9\xAB\xAB\xAB\xAB"            # 0
            "\xE8\xAB\xAB\xAB\xAB"            # 5
            "\x68" + self.exe.toHex(czEnter, 4) +  # 10
            "\xE8\xAB\xAB\xAB\xAB"            # 15
            "\x83\xC4\x04"                    # 20
            "\x66\xC7\x45\xAB\xAB\xAB"        # 23
            "\xFF\x15\xAB\xAB\xAB\xAB"        # 29
            "\x8B\x0D\xAB\xAB\xAB\xAB"        # 35
            "\x8B\x15\xAB\xAB\xAB\xAB"        # 41
            "\x89\x45\xAB"                    # 47
            "\xA1\xAB\xAB\xAB\xAB"            # 50
            "\x89\x4D\xAB"                    # 55
            "\xB9" + sessionHex +             # 58
            "\x89\x55\xAB"                    # 63
            "\x89\x45\xAB"                    # 66
            "\xE8\xAB\xAB\xAB\xAB"            # 69
            "\x8B\x55\xAB"                    # 74
            "\x8B\x7D\xAB"                    # 77
            "\x33\xD7"                        # 80
            "\x88\x45\xAB"                    # 82
            "\x8B\x45\xAB"                    # 85
            "\x81\xF2\xAB\xAB\x00\x00"        # 88 encrypt key
            "\x89\x55\xAB"                    # 94
            "\x8B\x55\xAB"                    # 97
            "\x33\xC2"                        # 100
            "\x8D\x4D\xAB"                    # 102
            "\x0F\xBF\x55\xAB"                # 105
            "\x35\xAB\xAB\x00\x00"            # 109 encrypt key
            "\x51"                            # 114
            "\x52"                            # 115
            "\x89\x45\xAB"                    # 116
            "\xE8\xAB\xAB\xAB\xAB"            # 119
            "\x8B\xC8"                        # 124
            "\xE8\xAB\xAB\xAB\xAB"            # 126
            "\x50"                            # 131
            "\xE8\xAB\xAB\xAB\xAB"            # 132
            "\x8B\xC8"                        # 137
            "\xE8\xAB\xAB\xAB\xAB"            # 139
            "\x6A\x01"                        # 144
            "\xE8\xAB\xAB\xAB\xAB"            # 146
            "\x8B\xC8"                        # 151
            "\xE8\xAB\xAB\xAB\xAB"            # 153
        )
        setBlockOffset = 154
        sessionOffset = 59
        comboOffset = 0
        instanceOffsets = (120, 133, 147)
        enc_packet_keysOffset = 1
        CSession_GetSexOffset = 70
        encryptionInitKeysOffset = 0
        encryptionInitKeys0Offset = 6
        getPacketSizeOffset = 127
        sendPacketOffset = 140
        traceOffset = 16
        setPaddingValueOffsets = 0
        dwTimeOffset = 0
        packetIdNumOffset = (27, 2)
        clientTimeOffset2 = (79, 1)
        accountIdOffset1 = (43, 4)
        accountIdOffset2 = (96, 1)
        charIdOffset1 = (51, 4)
        charIdOffset2 = (118, 1)
        authCodeOffset1 = (37, 4)
        authCodeOffset2 = (57, 1)
        packetIdOffsets2 = ((26, 1), (104, 1), (108, 1))
        sexOffset2 = (84, 1)
        key1Offset = 0
        key2Offset = 0
        key3Offset = 0
        searchEncryptionInitKeys = False
        offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 2009-01-20
            # 0  mov ecx, offset enc_packet_keys
            # 5  call CEncryption_InitKeys0
            # 10 push offset aPacket_cz_ente
            # 15 call Trace
            # 20 mov eax, 345h
            # 25 add esp, 4
            # 28 mov [esp+164h+p.packet_id], ax
            # 33 call edi
            # 35 mov ecx, ds:g_session_account_id
            # 41 mov edx, ds:g_session_m_char_id
            # 47 mov [esp+164h+p.client_time], eax
            # 51 mov eax, ds:g_session_m_auth_key1
            # 56 mov [esp+164h+p.account_id], ecx
            # 60 mov ecx, offset g_session
            # 65 mov [esp+164h+p.char_id], edx
            # 69 mov [esp+164h+p.session_key1], eax
            # 73 call CSession_GetSex
            # 78 mov ecx, [esp+164h+p.account_id]
            # 82 xor ecx, [esp+164h+p.client_time]
            # 86 mov edx, [esp+164h+p.session_key1]
            # 90 xor edx, [esp+164h+p.char_id]
            # 94 xor ecx, 2290h
            # 100 mov [esp+164h+p.sex], al
            # 104 mov [esp+164h+p.account_id], ecx
            # 108 movsx ecx, [esp+164h+p.packet_id]
            # 113 lea eax, [esp+164h+p]
            # 117 push eax
            # 118 xor edx, 2290h
            # 124 push ecx
            # 125 mov [esp+16Ch+p.char_id], edx
            # 129 call CRagConnection_instanceR
            # 134 mov ecx, eax
            # 136 call CRagConnection_GetPacketSize
            # 141 push eax
            # 142 call CRagConnection_instanceR
            # 147 mov ecx, eax
            # 149 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\xB9\xAB\xAB\xAB\xAB"            # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x68" + self.exe.toHex(czEnter, 4) +  # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\xB8\xAB\xAB\x00\x00"            # 20
                "\x83\xC4\x04"                    # 25
                "\x66\x89\x44\x24\xAB"            # 28
                "\xFF\xAB"                        # 33
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 35
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 41
                "\x89\x44\x24\xAB"                # 47
                "\xA1\xAB\xAB\xAB\xAB"            # 51
                "\x89\x4C\x24\xAB"                # 56
                "\xB9" + sessionHex +             # 60
                "\x89\x54\x24\xAB"                # 65
                "\x89\x44\x24\xAB"                # 69
                "\xE8\xAB\xAB\xAB\xAB"            # 73
                "\x8B\x4C\x24\xAB"                # 78
                "\x33\x4C\x24\xAB"                # 82
                "\x8B\x54\x24\xAB"                # 86
                "\x33\x54\x24\xAB"                # 90
                "\x81\xF1\xAB\xAB\x00\x00"        # 94  encryption
                "\x88\x44\x24\xAB"                # 100
                "\x89\x4C\x24\xAB"                # 104
                "\x0F\xBF\x4C\x24\xAB"            # 108
                "\x8D\x44\x24\xAB"                # 113
                "\x50"                            # 117
                "\x81\xF2\xAB\xAB\x00\x00"        # 118 encryption
                "\x51"                            # 124
                "\x89\x54\x24\xAB"                # 125
                "\xE8\xAB\xAB\xAB\xAB"            # 129
                "\x8B\xC8"                        # 134
                "\xE8\xAB\xAB\xAB\xAB"            # 136
                "\x50"                            # 141
                "\xE8\xAB\xAB\xAB\xAB"            # 142
                "\x8B\xC8"                        # 147
                "\xE8\xAB\xAB\xAB\xAB"            # 149
                "\x6A\x01"                        # 154
                "\xE8\xAB\xAB\xAB\xAB"            # 156
                "\x8B\xC8"                        # 161
                "\xE8\xAB\xAB\xAB\xAB"            # 163
            )
            setBlockOffset = 164
            sessionOffset = 61
            comboOffset = 0
            instanceOffsets = (130, 143, 157)
            enc_packet_keysOffset = 1
            CSession_GetSexOffset = 74
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 6
            getPacketSizeOffset = 137
            sendPacketOffset = 150
            traceOffset = 16
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (21, 4)
            clientTimeOffset2 = (85, 1)
            accountIdOffset1 = (37, 4)
            accountIdOffset2 = (59, 1)
            charIdOffset1 = (43, 4)
            charIdOffset2 = (68, 1)
            authCodeOffset1 = (52, 4)
            authCodeOffset2 = (72, 1)
            packetIdOffsets2 = ((32, 1), (112, 1), (116, 1))
            sexOffset2 = (103, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 2009-04-06
            # 0  mov ecx, offset enc_packet_keys
            # 5  call CEncryption_InitKeys0
            # 10 push offset aPacket_cz_ente
            # 15 call Trace
            # 20 add esp, 4
            # 23 mov [ebp+p.packet_id], 436h
            # 29 call ds:timeGetTime
            # 35 mov ecx, g_session_m_auth_key1
            # 41 mov edx, g_session_account_id
            # 47 mov [ebp+p.client_time], eax
            # 50 mov eax, g_session_m_char_id
            # 55 mov [ebp+p.session_key1], ecx
            # 58 mov ecx, offset g_session
            # 63 mov [ebp+p.account_id], edx
            # 66 mov [ebp+p.char_id], eax
            # 69 call CSession_GetSex
            # 74 mov ecx, [ebp+p.char_id]
            # 77 mov edx, [ebp+p.client_time]
            # 80 mov esi, [ebp+p.account_id]
            # 83 mov [ebp+p.sex], al
            # 86 mov eax, [ebp+p.session_key1]
            # 89 xor esi, edx
            # 91 movsx edx, [ebp+p.packet_id]
            # 95 xor ecx, eax
            # 97 mov [ebp+p.account_id], esi
            # 100 mov [ebp+p.char_id], ecx
            # 103 lea ecx, [ebp+p]
            # 106 push ecx
            # 107 push edx
            # 108 call CRagConnection_instanceR
            # 113 mov ecx, eax
            # 115 call CRagConnection_GetPacketSize
            # 120 push eax
            # 121 call CRagConnection_instanceR
            # 126 mov ecx, eax
            # 128 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\xB9\xAB\xAB\xAB\xAB"            # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x68" + self.exe.toHex(czEnter, 4) +  # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\x83\xC4\x04"                    # 20
                "\x66\xC7\x45\xAB\xAB\xAB"        # 23
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 29
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 35
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 41
                "\x89\x45\xAB"                    # 47
                "\xA1\xAB\xAB\xAB\xAB"            # 50
                "\x89\x4D\xAB"                    # 55
                "\xB9" + sessionHex +             # 58
                "\x89\x55\xAB"                    # 63
                "\x89\x45\xAB"                    # 66
                "\xE8\xAB\xAB\xAB\xAB"            # 69
                "\x8B\x4D\xAB"                    # 74
                "\x8B\x55\xAB"                    # 77
                "\x8B\x75\xAB"                    # 80
                "\x88\x45\xAB"                    # 83
                "\x8B\x45\xAB"                    # 86
                "\x33\xF2"                        # 89
                "\x0F\xBF\x55\xAB"                # 91
                "\x33\xC8"                        # 95
                "\x89\x75\xAB"                    # 97
                "\x89\x4D\xAB"                    # 100
                "\x8D\x4D\xAB"                    # 103
                "\x51"                            # 106
                "\x52"                            # 107
                "\xE8\xAB\xAB\xAB\xAB"            # 108
                "\x8B\xC8"                        # 113
                "\xE8\xAB\xAB\xAB\xAB"            # 115
                "\x50"                            # 120
                "\xE8\xAB\xAB\xAB\xAB"            # 121
                "\x8B\xC8"                        # 126
                "\xE8\xAB\xAB\xAB\xAB"            # 128
                "\x6A\x01"                        # 133
                "\xE8\xAB\xAB\xAB\xAB"            # 135
                "\x8B\xC8"                        # 140
                "\xE8\xAB\xAB\xAB\xAB"            # 142
            )
            setBlockOffset = 143
            sessionOffset = 59
            comboOffset = 0
            instanceOffsets = (109, 122, 136)
            enc_packet_keysOffset = 1
            CSession_GetSexOffset = 70
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 6
            getPacketSizeOffset = 116
            sendPacketOffset = 129
            traceOffset = 16
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (27, 2)
            clientTimeOffset2 = (49, 1)
            accountIdOffset1 = (43, 4)
            accountIdOffset2 = (65, 1)
            charIdOffset1 = (51, 4)
            charIdOffset2 = (68, 1)
            authCodeOffset1 = (37, 4)
            authCodeOffset2 = (57, 1)
            packetIdOffsets2 = ((26, 1), (94, 1), (105, 1))
            sexOffset2 = (85, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, offset enc_packet_keys
            # 5  call CEncryption_InitKeys0
            # 10 push offset aPacket_cz_ente
            # 15 call Trace
            # 20 add esp, 4
            # 23 lea edx, [ebp+p.pad]
            # 26 mov ecx, esi
            # 28 mov [ebp+p.packet_id], 9Bh
            # 34 push 5
            # 36 push edx
            # 37 call CLoginMode_SetPaddingValue
            # 42 lea eax, [ebp+p.pad2]
            # 45 push 4
            # 47 push eax
            # 48 mov ecx, esi
            # 50 call CLoginMode_SetPaddingValue
            # 55 lea ecx, [ebp+p.pad3]
            # 58 push 6
            # 60 push ecx
            # 61 mov ecx, esi
            # 63 call CLoginMode_SetPaddingValue
            # 68 call ds:timeGetTime
            # 74 mov ecx, g_session_m_auth_key1
            # 80 mov edx, g_session_account_id
            # 86 mov [ebp+p.client_time], eax
            # 89 mov eax, g_session_m_char_id
            # 94 mov [ebp+p.session_key1], ecx
            # 97 mov ecx, offset g_session
            # 102 mov [ebp+p.account_id], edx
            # 105 mov [ebp+p.char_id], eax
            # 108 call CSession_GetSex
            # 113 mov [ebp+p.sex], al
            # 116 lea edx, [ebp+p]
            # 119 movsx eax, [ebp+p.packet_id]
            # 123 push edx
            # 124 push eax
            # 125 call CRagConnection_instanceR
            # 130 mov ecx, eax
            # 132 call CRagConnection_GetPacketSize
            # 137 push eax
            # 138 call CRagConnection_instanceR
            # 143 mov ecx, eax
            # 145 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\xB9\xAB\xAB\xAB\xAB"            # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x68" + self.exe.toHex(czEnter, 4) +  # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\x83\xC4\x04"                    # 20
                "\x8D\x55\xAB"                    # 23
                "\x8B\xAB"                        # 26
                "\x66\xC7\x45\xAB\xAB\xAB"        # 28
                "\x6A\xAB"                        # 34
                "\x52"                            # 36
                "\xE8\xAB\xAB\xAB\xAB"            # 37
                "\x8D\x45\xAB"                    # 42
                "\x6A\xAB"                        # 45
                "\x50"                            # 47
                "\x8B\xCE"                        # 48
                "\xE8\xAB\xAB\xAB\xAB"            # 50
                "\x8D\x4D\xAB"                    # 55
                "\x6A\xAB"                        # 58
                "\x51"                            # 60
                "\x8B\xCE"                        # 61
                "\xE8\xAB\xAB\xAB\xAB"            # 63
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 68
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 74
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 80
                "\x89\x45\xAB"                    # 86
                "\xA1\xAB\xAB\xAB\xAB"            # 89
                "\x89\x4D\xAB"                    # 94
                "\xB9" + sessionHex +             # 97
                "\x89\x55\xAB"                    # 102
                "\x89\x45\xAB"                    # 105
                "\xE8\xAB\xAB\xAB\xAB"            # 108
                "\x88\x45\xAB"                    # 113
                "\x8D\x55\xAB"                    # 116
                "\x0F\xBF\x45\xAB"                # 119
                "\x52"                            # 123
                "\x50"                            # 124
                "\xE8\xAB\xAB\xAB\xAB"            # 125
                "\x8B\xC8"                        # 130
                "\xE8\xAB\xAB\xAB\xAB"            # 132
                "\x50"                            # 137
                "\xE8\xAB\xAB\xAB\xAB"            # 138
                "\x8B\xC8"                        # 143
                "\xE8\xAB\xAB\xAB\xAB"            # 145
                "\x6A\x01"                        # 150
                "\xE8\xAB\xAB\xAB\xAB"            # 152
                "\x8B\xC8"                        # 157
                "\xE8\xAB\xAB\xAB\xAB"            # 159
            )
            setBlockOffset = 160
            sessionOffset = 98
            comboOffset = 0
            instanceOffsets = (126, 139, 153)
            enc_packet_keysOffset = 1
            CSession_GetSexOffset = 109
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 6
            getPacketSizeOffset = 133
            sendPacketOffset = 146
            traceOffset = 16
            setPaddingValueOffsets = (38, 51, 64)
            dwTimeOffset = 0
            packetIdNumOffset = (32, 2)
            clientTimeOffset2 = (88, 1)
            accountIdOffset1 = (82, 4)
            accountIdOffset2 = (104, 1)
            charIdOffset1 = (90, 4)
            charIdOffset2 = (107, 1)
            authCodeOffset1 = (76, 4)
            authCodeOffset2 = (96, 1)
            packetIdOffsets2 = ((31, 1), (118, 1), (122, 1))
            sexOffset2 = (115, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  mov ecx, offset enc_packet_keys
            # 5  call CEncryption_InitKeys0
            # 10 push offset aPacket_cz_ente
            # 15 call Trace
            # 20 add esp, 4
            # 23 lea edx, [ebp+p.pad]
            # 26 mov ecx, esi
            # 28 mov [ebp+p.packet_id], 9Bh
            # 34 push 5
            # 36 push edx
            # 37 call CLoginMode_SetPaddingValue
            # 42 lea eax, [ebp+p.pad2]
            # 45 push 4
            # 47 push eax
            # 48 mov ecx, esi
            # 50 call CLoginMode_SetPaddingValue
            # 55 lea ecx, [ebp+p.pad3]
            # 58 push 6
            # 60 push ecx
            # 61 mov ecx, esi
            # 63 call CLoginMode_SetPaddingValue
            # 68 call ds:timeGetTime
            # 74 mov ecx, g_session_m_auth_key1
            # 80 mov edx, g_session_account_id
            # 86 mov [ebp+p.client_time], eax
            # 89 mov eax, g_session_m_char_id
            # 94 mov [ebp+p.session_key1], ecx
            # 97 mov ecx, offset g_session
            # 102 mov [ebp+p.account_id], edx
            # 105 mov [ebp+p.char_id], eax
            # 108 call CSession_GetSex
            # 113 mov [ebp+p.sex], al
            # 116 lea edx, [ebp+p]
            # 119 movsx eax, [ebp+p.packet_id]
            # 123 push edx
            # 124 push eax
            # 125 call CRagConnection_instanceR
            # 130 mov ecx, eax
            # 132 call CRagConnection_GetPacketSize
            # 137 push eax
            # 138 call CRagConnection_instanceR
            # 143 mov ecx, eax
            # 145 call CRagConnection_SendPacket
            code = (
                "\xB9\xAB\xAB\xAB\xAB"            # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x68" + self.exe.toHex(czEnter, 4) +  # 10
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\x83\xC4\x04"                    # 20
                "\x8D\x55\xAB"                    # 23
                "\x8B\xAB"                        # 26
                "\x66\xC7\x45\xAB\xAB\xAB"        # 28
                "\x6A\xAB"                        # 34
                "\x52"                            # 36
                "\xE8\xAB\xAB\xAB\xAB"            # 37
                "\x8D\x45\xAB"                    # 42
                "\x6A\xAB"                        # 45
                "\x50"                            # 47
                "\x8B\xCE"                        # 48
                "\xE8\xAB\xAB\xAB\xAB"            # 50
                "\x8D\x4D\xAB"                    # 55
                "\x6A\xAB"                        # 58
                "\x51"                            # 60
                "\x8B\xCE"                        # 61
                "\xE8\xAB\xAB\xAB\xAB"            # 63
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 68
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 74
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 80
                "\x89\x45\xAB"                    # 86
                "\xA1\xAB\xAB\xAB\xAB"            # 89
                "\x89\x4D\xAB"                    # 94
                "\xB9" + sessionHex +             # 97
                "\x89\x55\xAB"                    # 102
                "\x89\x45\xAB"                    # 105
                "\xE8\xAB\xAB\xAB\xAB"            # 108
                "\x88\x45\xAB"                    # 113
                "\x8D\x55\xAB"                    # 116
                "\x0F\xBF\x45\xAB"                # 119
                "\x52"                            # 123
                "\x50"                            # 124
                "\xE8\xAB\xAB\xAB\xAB"            # 125
                "\x8B\xC8"                        # 130
                "\xE8\xAB\xAB\xAB\xAB"            # 132
                "\x50"                            # 137
                "\xE8\xAB\xAB\xAB\xAB"            # 138
                "\x8B\xC8"                        # 143
                "\xE8\xAB\xAB\xAB\xAB"            # 145
            )
            setBlockOffset = 0
            sessionOffset = 98
            comboOffset = 0
            instanceOffsets = (126, 139)
            enc_packet_keysOffset = 1
            CSession_GetSexOffset = 109
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 6
            getPacketSizeOffset = 133
            sendPacketOffset = 146
            traceOffset = 16
            setPaddingValueOffsets = (38, 51, 64)
            dwTimeOffset = 0
            packetIdNumOffset = (32, 2)
            clientTimeOffset2 = (88, 1)
            accountIdOffset1 = (82, 4)
            accountIdOffset2 = (104, 1)
            charIdOffset1 = (90, 4)
            charIdOffset2 = (107, 1)
            authCodeOffset1 = (76, 4)
            authCodeOffset2 = (96, 1)
            packetIdOffsets2 = ((31, 1), (118, 1), (122, 1))
            sexOffset2 = (115, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  push offset aPacket_cz_ente
            # 5  call Trace
            # 10 add esp, 4
            # 13 lea edx, [ebp+p.pad1]
            # 16 mov ecx, esi
            # 18 mov [ebp+p.packet_id], 9Bh
            # 24 push 2
            # 26 push edx
            # 27 call CLoginMode_SetPaddingValue
            # 32 lea eax, [ebp+p.pad2]
            # 35 push 1
            # 37 push eax
            # 38 mov ecx, esi
            # 40 call CLoginMode_SetPaddingValue
            # 45 lea ecx, [ebp+p.pad3]
            # 48 push 4
            # 50 push ecx
            # 51 mov ecx, esi
            # 53 call CLoginMode_SetPaddingValue
            # 58 call ds:timeGetTime
            # 64 mov ecx, dword ptr g_session_m_auth_key1
            # 70 mov edx, g_session_m_account_id
            # 76 mov [ebp+p.client_time], eax
            # 79 mov eax, g_session_m_char_id
            # 84 mov [ebp+p.session_key1], ecx
            # 87 mov ecx, offset g_session
            # 92 mov [ebp+p.account_id], edx
            # 95 mov [ebp+p.char_id], eax
            # 98 call CSession_GetSex
            # 103 mov [ebp+p.sex], al
            # 106 lea edx, [ebp+p]
            # 109 movsx eax, [ebp+p.packet_id]
            # 113 push edx
            # 114 push eax
            # 115 call CRagConnection_instanceR
            # 120 mov ecx, eax
            # 122 call CRagConnection_GetPacketSize
            # 127 push eax
            # 128 call CRagConnection_instanceR
            # 133 mov ecx, eax
            # 135 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x83\xC4\x04"                    # 10
                "\x8D\x55\xAB"                    # 13
                "\x8B\xAB"                        # 16
                "\x66\xC7\x45\xAB\xAB\xAB"        # 18
                "\x6A\xAB"                        # 24
                "\x52"                            # 26
                "\xE8\xAB\xAB\xAB\xAB"            # 27
                "\x8D\x45\xAB"                    # 32
                "\x6A\xAB"                        # 35
                "\x50"                            # 37
                "\x8B\xAB"                        # 38
                "\xE8\xAB\xAB\xAB\xAB"            # 40
                "\x8D\x4D\xAB"                    # 45
                "\x6A\xAB"                        # 48
                "\x51"                            # 50
                "\x8B\xAB"                        # 51
                "\xE8\xAB\xAB\xAB\xAB"            # 53
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 58
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 64
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 70
                "\x89\x45\xAB"                    # 76
                "\xA1\xAB\xAB\xAB\xAB"            # 79
                "\x89\x4D\xAB"                    # 84
                "\xB9" + sessionHex +             # 87
                "\x89\x55\xAB"                    # 92
                "\x89\x45\xAB"                    # 95
                "\xE8\xAB\xAB\xAB\xAB"            # 98
                "\x88\x45\xAB"                    # 103
                "\x8D\x55\xAB"                    # 106
                "\x0F\xBF\x45\xAB"                # 109
                "\x52"                            # 113
                "\x50"                            # 114
                "\xE8\xAB\xAB\xAB\xAB"            # 115
                "\x8B\xC8"                        # 120
                "\xE8\xAB\xAB\xAB\xAB"            # 122
                "\x50"                            # 127
                "\xE8\xAB\xAB\xAB\xAB"            # 128
                "\x8B\xC8"                        # 133
                "\xE8\xAB\xAB\xAB\xAB"            # 135
                "\x6A\x01"                        # 140
                "\xE8\xAB\xAB\xAB\xAB"            # 142
                "\x8B\xC8"                        # 147
                "\xE8\xAB\xAB\xAB\xAB"            # 149
            )
            setBlockOffset = 150
            sessionOffset = 88
            comboOffset = 0
            instanceOffsets = (116, 129, 143)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 99
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 123
            sendPacketOffset = 136
            traceOffset = 6
            setPaddingValueOffsets = (28, 41, 54)
            dwTimeOffset = 0
            packetIdNumOffset = (22, 2)
            clientTimeOffset2 = (78, 1)
            accountIdOffset1 = (72, 4)
            accountIdOffset2 = (94, 1)
            charIdOffset1 = (80, 4)
            charIdOffset2 = (97, 1)
            authCodeOffset1 = (66, 4)
            authCodeOffset2 = (86, 1)
            packetIdOffsets2 = ((21, 1), (108, 1), (112, 1))
            sexOffset2 = (105, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 2008-06-17
            # 0  push offset aPacket_cz_ente
            # 5  call Trace
            # 10 add esp, 4
            # 13 lea edx, [ebp+p.pad1]
            # 16 mov ecx, esi
            # 18 mov dword ptr [ebp+p.packet_id], 9Bh
            # 25 push 2
            # 27 push edx
            # 28 call CLoginMode_SetPaddingValue
            # 33 lea eax, [ebp+p.pad2]
            # 36 push 1
            # 38 push eax
            # 39 mov ecx, esi
            # 41 call CLoginMode_SetPaddingValue
            # 46 lea ecx, [ebp+p.pad3]
            # 49 push 4
            # 51 push ecx
            # 52 mov ecx, esi
            # 54 call CLoginMode_SetPaddingValue
            # 59 call ds:timeGetTime
            # 65 mov ecx, g_session_m_auth_key1
            # 71 mov edx, g_session_m_account_id
            # 77 mov [ebp+p.client_time], eax
            # 80 mov eax, g_session_m_char_id
            # 85 mov [ebp+p.session_key1], ecx
            # 88 mov ecx, offset g_session
            # 93 mov [ebp+p.account_id], edx
            # 96 mov [ebp+p.char_id], eax
            # 99 call CSession_GetSex
            # 104 mov [ebp+p.sex], al
            # 107 mov eax, dword ptr [ebp+p.packet_id]
            # 110 lea edx, [ebp+p]
            # 113 push edx
            # 114 push eax
            # 115 call CRagConnection_instanceR
            # 120 mov ecx, eax
            # 122 call CRagConnection_GetPacketSize
            # 127 push eax
            # 128 call CRagConnection_instanceR
            # 133 mov ecx, eax
            # 135 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x83\xC4\x04"                    # 10
                "\x8D\x55\xAB"                    # 13
                "\x8B\xAB"                        # 16
                "\xC7\x45\xAB\xAB\xAB\x00\x00"    # 18
                "\x6A\xAB"                        # 25
                "\x52"                            # 27
                "\xE8\xAB\xAB\xAB\xAB"            # 28
                "\x8D\x45\xAB"                    # 33
                "\x6A\xAB"                        # 36
                "\x50"                            # 38
                "\x8B\xAB"                        # 39
                "\xE8\xAB\xAB\xAB\xAB"            # 41
                "\x8D\x4D\xAB"                    # 46
                "\x6A\xAB"                        # 49
                "\x51"                            # 51
                "\x8B\xAB"                        # 52
                "\xE8\xAB\xAB\xAB\xAB"            # 54
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 59
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 65
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 71
                "\x89\x45\xAB"                    # 77
                "\xA1\xAB\xAB\xAB\xAB"            # 80
                "\x89\x4D\xAB"                    # 85
                "\xB9" + sessionHex +             # 88
                "\x89\x55\xAB"                    # 93
                "\x89\x45\xAB"                    # 96
                "\xE8\xAB\xAB\xAB\xAB"            # 99
                "\x88\x45\xAB"                    # 104
                "\x8B\x45\xAB"                    # 107
                "\x8D\x55\xAB"                    # 110
                "\x52"                            # 113
                "\x50"                            # 114
                "\xE8\xAB\xAB\xAB\xAB"            # 115
                "\x8B\xC8"                        # 120
                "\xE8\xAB\xAB\xAB\xAB"            # 122
                "\x50"                            # 127
                "\xE8\xAB\xAB\xAB\xAB"            # 128
                "\x8B\xC8"                        # 133
                "\xE8\xAB\xAB\xAB\xAB"            # 135
                "\x6A\x01"                        # 140
                "\xE8\xAB\xAB\xAB\xAB"            # 142
                "\x8B\xC8"                        # 147
                "\xE8\xAB\xAB\xAB\xAB"            # 149
            )
            setBlockOffset = 150
            sessionOffset = 89
            comboOffset = 0
            instanceOffsets = (116, 129, 143)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 100
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 123
            sendPacketOffset = 136
            traceOffset = 6
            setPaddingValueOffsets = (29, 42, 55)
            dwTimeOffset = 0
            packetIdNumOffset = (21, 4)
            clientTimeOffset2 = (79, 1)
            accountIdOffset1 = (73, 4)
            accountIdOffset2 = (95, 1)
            charIdOffset1 = (81, 4)
            charIdOffset2 = (98, 1)
            authCodeOffset1 = (67, 4)
            authCodeOffset2 = (87, 1)
            packetIdOffsets2 = ((20, 1), (109, 1), (112, 1))
            sexOffset2 = (106, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 2008-11-19
            # 0  push offset aPacket_cz_ente
            # 5  call Trace
            # 10 add esp, 4
            # 13 mov dword ptr [ebp+var_20.packet_id], 436h
            # 20 call ds:timeGetTime
            # 26 mov ecx, dword ptr g_session_m_auth_key1
            # 32 mov edx, g_session_m_account_id
            # 38 mov [ebp+var_20.client_time], eax
            # 41 mov eax, g_session_m_char_id
            # 46 mov [ebp+var_20.session_key1], ecx
            # 49 mov ecx, offset g_session
            # 54 mov [ebp+var_20.account_id], edx
            # 57 mov [ebp+var_20.char_id], eax
            # 60 call CSession_GetSex
            # 65 mov [ebp+var_20.sex], al
            # 68 mov eax, dword ptr [ebp+var_20.packet_id]
            # 71 lea edx, [ebp+var_20]
            # 74 push edx
            # 75 push eax
            # 76 call CRagConnection_instanceR
            # 81 mov ecx, eax
            # 83 call CRagConnection_GetPacketSize
            # 88 push eax
            # 89 call CRagConnection_instanceR
            # 94 mov ecx, eax
            # 96 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x83\xC4\x04"                    # 10
                "\xC7\x45\xAB\xAB\xAB\x00\x00"    # 13
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 20
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 26
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 32
                "\x89\x45\xAB"                    # 38
                "\xA1\xAB\xAB\xAB\xAB"            # 41
                "\x89\x4D\xAB"                    # 46
                "\xB9" + sessionHex +             # 49
                "\x89\x55\xAB"                    # 54
                "\x89\x45\xAB"                    # 57
                "\xE8\xAB\xAB\xAB\xAB"            # 60
                "\x88\x45\xAB"                    # 65
                "\x8B\x45\xAB"                    # 68
                "\x8D\x55\xAB"                    # 71
                "\x52"                            # 74
                "\x50"                            # 75
                "\xE8\xAB\xAB\xAB\xAB"            # 76
                "\x8B\xC8"                        # 81
                "\xE8\xAB\xAB\xAB\xAB"            # 83
                "\x50"                            # 88
                "\xE8\xAB\xAB\xAB\xAB"            # 89
                "\x8B\xC8"                        # 94
                "\xE8\xAB\xAB\xAB\xAB"            # 96
                "\x6A\x01"                        # 101
                "\xE8\xAB\xAB\xAB\xAB"            # 103
                "\x8B\xC8"                        # 108
                "\xE8\xAB\xAB\xAB\xAB"            # 110
            )
            setBlockOffset = 111
            sessionOffset = 50
            comboOffset = 0
            instanceOffsets = (77, 90, 104)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 61
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 84
            sendPacketOffset = 97
            traceOffset = 6
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (16, 4)
            clientTimeOffset2 = (40, 1)
            accountIdOffset1 = (34, 4)
            accountIdOffset2 = (56, 1)
            charIdOffset1 = (42, 4)
            charIdOffset2 = (59, 1)
            authCodeOffset1 = (28, 4)
            authCodeOffset2 = (48, 1)
            packetIdOffsets2 = ((15, 1), (70, 1), (73, 1))
            sexOffset2 = (67, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 2007-01-02
            # 0  push offset aPacket_cz_ente
            # 5  mov dword_73D518, 1
            # 15 call Trace
            # 20 add esp, 4
            # 23 lea edx, [ebp+var_24.pad1]
            # 26 mov ecx, esi
            # 28 mov [ebp+var_24.packet_id], 9Bh
            # 34 push 2
            # 36 push edx
            # 37 call CLoginMode_SetPaddingValue
            # 42 lea eax, [ebp+var_24.pad2]
            # 45 push 1
            # 47 push eax
            # 48 mov ecx, esi
            # 50 call CLoginMode_SetPaddingValue
            # 55 lea ecx, [ebp+var_24.pad3]
            # 58 push 4
            # 60 push ecx
            # 61 mov ecx, esi
            # 63 call CLoginMode_SetPaddingValue
            # 68 call ds:timeGetTime
            # 74 mov ecx, dword ptr g_session_m_auth_key1
            # 80 mov edx, g_session_m_account_id
            # 86 mov [ebp+var_24.client_time], eax
            # 89 mov eax, g_session_m_char_id
            # 94 mov [ebp+var_24.session_key1], ecx
            # 97 mov ecx, offset g_session
            # 102 mov [ebp+var_24.account_id], edx
            # 105 mov [ebp+var_24.char_id], eax
            # 108 call CSession_GetSex
            # 113 mov [ebp+var_24.sex], al
            # 116 lea edx, [ebp+var_24]
            # 119 movsx eax, [ebp+var_24.packet_id]
            # 123 push edx
            # 124 push eax
            # 125 call CRagConnection_instanceR
            # 130 mov ecx, eax
            # 132 call CRagConnection_GetPacketSize
            # 137 push eax
            # 138 call CRagConnection_instanceR
            # 143 mov ecx, eax
            # 145 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xC7\x05\xAB\xAB\xAB\xAB\x01\x00\x00\x00"  # 5
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\x83\xC4\x04"                    # 20
                "\x8D\x55\xAB"                    # 23
                "\x8B\xAB"                        # 26
                "\x66\xC7\x45\xAB\xAB\xAB"        # 28
                "\x6A\xAB"                        # 34
                "\x52"                            # 36
                "\xE8\xAB\xAB\xAB\xAB"            # 37
                "\x8D\x45\xAB"                    # 42
                "\x6A\xAB"                        # 45
                "\x50"                            # 47
                "\x8B\xAB"                        # 48
                "\xE8\xAB\xAB\xAB\xAB"            # 50
                "\x8D\x4D\xAB"                    # 55
                "\x6A\xAB"                        # 58
                "\x51"                            # 60
                "\x8B\xAB"                        # 61
                "\xE8\xAB\xAB\xAB\xAB"            # 63
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 68
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 74
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 80
                "\x89\x45\xAB"                    # 86
                "\xA1\xAB\xAB\xAB\xAB"            # 89
                "\x89\x4D\xAB"                    # 94
                "\xB9" + sessionHex +             # 97
                "\x89\x55\xAB"                    # 102
                "\x89\x45\xAB"                    # 105
                "\xE8\xAB\xAB\xAB\xAB"            # 108
                "\x88\x45\xAB"                    # 113
                "\x8D\x55\xAB"                    # 116
                "\x0F\xBF\x45\xAB"                # 119
                "\x52"                            # 123
                "\x50"                            # 124
                "\xE8\xAB\xAB\xAB\xAB"            # 125
                "\x8B\xC8"                        # 130
                "\xE8\xAB\xAB\xAB\xAB"            # 132
                "\x50"                            # 137
                "\xE8\xAB\xAB\xAB\xAB"            # 138
                "\x8B\xC8"                        # 143
                "\xE8\xAB\xAB\xAB\xAB"            # 145
                "\x6A\x01"                        # 150
                "\xE8\xAB\xAB\xAB\xAB"            # 152
                "\x8B\xC8"                        # 157
                "\xE8\xAB\xAB\xAB\xAB"            # 159
            )
            setBlockOffset = 160
            sessionOffset = 98
            comboOffset = 0
            instanceOffsets = (126, 139, 153)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 109
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 133
            sendPacketOffset = 146
            traceOffset = 16
            setPaddingValueOffsets = (38, 51, 64)
            dwTimeOffset = 0
            packetIdNumOffset = (32, 2)
            clientTimeOffset2 = (88, 1)
            accountIdOffset1 = (82, 4)
            accountIdOffset2 = (104, 1)
            charIdOffset1 = (90, 4)
            charIdOffset2 = (107, 1)
            authCodeOffset1 = (76, 4)
            authCodeOffset2 = (96, 1)
            packetIdOffsets2 = ((31, 1), (118, 1), (122, 1))
            sexOffset2 = (115, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  push offset aPacket_cz_ente
            # 5  call Trace
            # 10 add esp, 4
            # 13 push 2
            # 15 lea ecx, [esp+13Ch+var_C4.pad1]
            # 19 mov eax, 9Bh
            # 24 push ecx
            # 25 mov ecx, ebp
            # 27 mov [esp+140h+var_C4.packet_id], ax
            # 32 call CLoginMode_SetPaddingValue
            # 37 push 1
            # 39 lea edx, [esp+13Ch+var_C4.pad2]
            # 46 push edx
            # 47 mov ecx, ebp
            # 49 call CLoginMode_SetPaddingValue
            # 54 push 4
            # 56 lea eax, [esp+13Ch+var_C4.pad3]
            # 63 push eax
            # 64 mov ecx, ebp
            # 66 call CLoginMode_SetPaddingValue
            # 71 call edi
            # 73 mov ecx, g_session_m_account_id
            # 79 mov edx, dword ptr g_session_m_char_id
            # 85 mov [esp+138h+var_C4.client_time], eax
            # 92 mov eax, dword ptr g_session_m_auth_key1
            # 97 mov [esp+138h+var_C4.account_id], ecx
            # 101 mov ecx, offset g_session
            # 106 mov [esp+138h+var_C4.char_id], edx
            # 110 mov [esp+138h+var_C4.session_key1], eax
            # 117 call CSession_GetSex
            # 122 movsx edx, [esp+138h+var_C4.packet_id]
            # 127 lea ecx, [esp+138h+var_C4]
            # 131 push ecx
            # 132 push edx
            # 133 mov [esp+140h+var_C4.sex], al
            # 140 call CRagConnection_instanceR
            # 145 mov ecx, eax
            # 147 call CRagConnection_GetPacketSize
            # 152 push eax
            # 153 call CRagConnection_instanceR
            # 158 mov ecx, eax
            # 160 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xE8\xAB\xAB\xAB\xAB"            # 5
                "\x83\xC4\x04"                    # 10
                "\x6A\xAB"                        # 13
                "\x8D\x4C\x24\xAB"                # 15
                "\xB8\xAB\xAB\x00\x00"            # 19
                "\x51"                            # 24
                "\x8B\xCD"                        # 25
                "\x66\x89\x44\x24\xAB"            # 27
                "\xE8\xAB\xAB\xAB\xAB"            # 32
                "\x6A\xAB"                        # 37
                "\x8D\x94\x24\xAB\xAB\xAB\xAB"    # 39
                "\x52"                            # 46
                "\x8B\xCD"                        # 47
                "\xE8\xAB\xAB\xAB\xAB"            # 49
                "\x6A\xAB"                        # 54
                "\x8D\x84\x24\xAB\xAB\xAB\xAB"    # 56
                "\x50"                            # 63
                "\x8B\xCD"                        # 64
                "\xE8\xAB\xAB\xAB\xAB"            # 66
                "\xFF\xAB"                        # 71
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 73
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 79
                "\x89\x84\x24\xAB\xAB\xAB\xAB"    # 85
                "\xA1\xAB\xAB\xAB\xAB"            # 92
                "\x89\x4C\x24\xAB"                # 97
                "\xB9" + sessionHex +             # 101
                "\x89\x54\x24\xAB"                # 106
                "\x89\x84\x24\xAB\xAB\xAB\xAB"    # 110
                "\xE8\xAB\xAB\xAB\xAB"            # 117
                "\x0F\xBF\x54\x24\xAB"            # 122
                "\x8D\x4C\x24\xAB"                # 127
                "\x51"                            # 131
                "\x52"                            # 132
                "\x88\x84\x24\xAB\xAB\xAB\xAB"    # 133
                "\xE8\xAB\xAB\xAB\xAB"            # 140
                "\x8B\xC8"                        # 145
                "\xE8\xAB\xAB\xAB\xAB"            # 147
                "\x50"                            # 152
                "\xE8\xAB\xAB\xAB\xAB"            # 153
                "\x8B\xC8"                        # 158
                "\xE8\xAB\xAB\xAB\xAB"            # 160
                "\x6A\x01"                        # 165
                "\xE8\xAB\xAB\xAB\xAB"            # 167
                "\x8B\xC8"                        # 172
                "\xE8\xAB\xAB\xAB\xAB"            # 174
            )
            setBlockOffset = 175
            sessionOffset = 102
            comboOffset = 0
            instanceOffsets = (141, 154, 168)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 118
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 148
            sendPacketOffset = 161
            traceOffset = 6
            setPaddingValueOffsets = (33, 50, 67)
            dwTimeOffset = 0
            packetIdNumOffset = (20, 4)
            clientTimeOffset2 = (88, 4)
            accountIdOffset1 = (75, 4)
            accountIdOffset2 = (100, 1)
            charIdOffset1 = (81, 4)
            charIdOffset2 = (109, 1)
            authCodeOffset1 = (93, 4)
            authCodeOffset2 = (113, 4)
            packetIdOffsets2 = ((126, 1), (130, 1))
            sexOffset2 = (136, 4, -8)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 2004-12-13
            # 0  push offset aPacket_cz_ente
            # 5  mov dword_69E5D8, 1
            # 15 call Trace
            # 20 add esp, 4
            # 23 mov [ebp+var_2C.packet_id], 0F5h
            # 29 call ds:timeGetTime
            # 35 mov ecx, g_session_m_auth_key1
            # 41 mov edx, g_session_m_account_id
            # 47 mov [ebp+var_2C.client_time], eax
            # 50 mov eax, g_session_m_char_id
            # 55 mov [ebp+var_2C.session_key1], ecx
            # 58 mov ecx, offset g_session
            # 63 mov [ebp+var_2C.account_id], edx
            # 66 mov [ebp+var_2C.char_id], eax
            # 69 call CSession_GetSex
            # 74 mov [ebp+var_2C.sex], al
            # 77 lea edx, [ebp+var_2C]
            # 80 movsx eax, [ebp+var_2C.packet_id]
            # 84 push edx
            # 85 push eax
            # 86 call CRagConnection_instanceR
            # 91 mov ecx, eax
            # 93 call CRagConnection_GetPacketSize
            # 98 push eax
            # 99 call CRagConnection_instanceR
            # 104 mov ecx, eax
            # 106 call CRagConnection_SendPacket
            # 111 push 1
            # 113 call CRagConnection_instanceR
            # 118 mov ecx, eax
            # 120 call CConnection_SetBlock
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xC7\x05\xAB\xAB\xAB\xAB\x01\x00\x00\x00"  # 5
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\x83\xC4\x04"                    # 20
                "\x66\xC7\x45\xAB\xAB\xAB"        # 23
                "\xFF\x15\xAB\xAB\xAB\xAB"        # 29
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 35
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 41
                "\x89\x45\xAB"                    # 47
                "\xA1\xAB\xAB\xAB\xAB"            # 50
                "\x89\x4D\xAB"                    # 55
                "\xB9" + sessionHex +             # 58
                "\x89\x55\xAB"                    # 63
                "\x89\x45\xAB"                    # 66
                "\xE8\xAB\xAB\xAB\xAB"            # 69
                "\x88\x45\xAB"                    # 74
                "\x8D\x55\xAB"                    # 77
                "\x0F\xBF\x45\xAB"                # 80
                "\x52"                            # 84
                "\x50"                            # 85
                "\xE8\xAB\xAB\xAB\xAB"            # 86
                "\x8B\xC8"                        # 91
                "\xE8\xAB\xAB\xAB\xAB"            # 93
                "\x50"                            # 98
                "\xE8\xAB\xAB\xAB\xAB"            # 99
                "\x8B\xC8"                        # 104
                "\xE8\xAB\xAB\xAB\xAB"            # 106
                "\x6A\x01"                        # 111
                "\xE8\xAB\xAB\xAB\xAB"            # 113
                "\x8B\xC8"                        # 118
                "\xE8\xAB\xAB\xAB\xAB"            # 120
            )
            setBlockOffset = 121
            sessionOffset = 59
            comboOffset = 0
            instanceOffsets = (87, 100, 114)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 70
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 94
            sendPacketOffset = 107
            traceOffset = 16
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            packetIdNumOffset = (27, 2)
            clientTimeOffset2 = (49, 1)
            accountIdOffset1 = (43, 4)
            accountIdOffset2 = (65, 1)
            charIdOffset1 = (51, 4)
            charIdOffset2 = (68, 1)
            authCodeOffset1 = (37, 4)
            authCodeOffset2 = (57, 1)
            packetIdOffsets2 = ((26, 1), (79, 1), (83, 1))
            sexOffset2 = (76, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False and self.packetVersion < "20070000":
        if czEnter == 0:
            offset, section = self.exe.string("PACKET_CZ_ENTER")
            if offset is False:
                czEnter = 0
            else:
                czEnter = section.rawToVa(offset)
        # two parts search
        # 2006-01-02
        # 0  push offset aPacket_cz_ente
        # 5  mov dword_7150B8, 1
        # 15 call Trace
        # 20 mov esi, ds:timeGetTime
        # 26 add esp, 4
        # 29 mov [ebp+p.packet_id], 9Bh
        # 35 call esi
        # 37 mov edi, eax
        # 39 shr edi, 4
        # 42 call esi
        code = (
            "\x68" + self.exe.toHex(czEnter, 4) +  # 0
            "\xC7\x05\xAB\xAB\xAB\xAB\x01\x00\x00\x00"  # 5
            "\xE8\xAB\xAB\xAB\xAB"            # 15
            "\x8B\x35\xAB\xAB\xAB\xAB"        # 20
            "\x83\xC4\x04"                    # 26
            "\x66\xC7\x45\xAB\xAB\xAB"        # 29
            "\xFF\xD6"                        # 35
            "\x8B\xF8"                        # 37
            "\xC1\xEF\x04"                    # 39
            "\xFF\xD6"                        # 42
        )
        packetIdNumOffset = (33, 2)
        traceOffset = 16
        offset = self.exe.codeWildcard(code, "\xAB")
        if offset is False:
            # 0  push offset aPacket_cz_ente
            # 5  mov dword_68AC40, 1
            # 15 call Trace
            # 20 mov esi, ds:timeGetTime
            # 26 add esp, 4
            # 29 lea edx, [ebp+p.field_2]
            # 32 mov [ebp+p.packet_id], 7Eh
            # 38 push 10h
            # 40 push edx
            # 41 call esi
            code = (
                "\x68" + self.exe.toHex(czEnter, 4) +  # 0
                "\xC7\x05\xAB\xAB\xAB\xAB\x01\x00\x00\x00"  # 5
                "\xE8\xAB\xAB\xAB\xAB"            # 15
                "\x8B\x35\xAB\xAB\xAB\xAB"        # 20
                "\x83\xC4\x04"                    # 26
                "\x8D\x55\xAB"                    # 29
                "\x66\xC7\x45\xAB\xAB\xAB"        # 32
                "\x6A\x10"                        # 38
                "\x52"                            # 40
                "\xFF\xD6"                        # 41
            )
            packetIdNumOffset = (36, 2)
            traceOffset = 16
            offset = self.exe.codeWildcard(code, "\xAB")
        if offset is not False and self.instanceR != 0:
            offset1 = offset
            offset = False
            # 0  jl short loc_5817B5
            # 2  mov byte ptr [ebp+p.session_key1+2], 0
            # 6  call esi
            # 8  mov ecx, g_session_m_auth_key1
            # 14 mov edx, g_session_m_account_id
            # 20 mov [ebp+p.client_time], eax
            # 23 mov eax, g_session_m_char_id
            # 28 mov [ebp+p.session_key1], ecx
            # 31 mov ecx, offset g_session
            # 36 mov [ebp+p.account_id], edx
            # 39 mov [ebp+p.char_id], eax
            # 42 call CSession_GetSex
            # 47 mov [ebp+p.sex], al
            # 50 lea edx, [ebp+p]
            # 53 movsx eax, [ebp+p.packet_id]
            # 57 push edx
            # 58 push eax
            # 59 call CRagConnection_instanceR
            # 64 mov ecx, eax
            # 66 call CRagConnection_GetPacketSize
            # 71 push eax
            # 72 call CRagConnection_instanceR
            # 77 mov ecx, eax
            # 79 call CRagConnection_SendPacket
            # 0  push 1
            # 2  call CRagConnection_instanceR
            # 7  mov ecx, eax
            # 9  call CConnection_SetBlock
            code = (
                "\x7C\xAB"                        # 0
                "\xC6\x45\xAB\xAB"                # 2
                "\xFF\xAB"                        # 6
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 8
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 14
                "\x89\x45\xAB"                    # 20
                "\xA1\xAB\xAB\xAB\xAB"            # 23
                "\x89\x4D\xAB"                    # 28
                "\xB9" + sessionHex +             # 31
                "\x89\x55\xAB"                    # 36
                "\x89\x45\xAB"                    # 39
                "\xE8\xAB\xAB\xAB\xAB"            # 42
                "\x88\x45\xAB"                    # 47
                "\x8D\x55\xAB"                    # 50
                "\x0F\xBF\x45\xAB"                # 53
                "\x52"                            # 57
                "\x50"                            # 58
                "\xE8\xAB\xAB\xAB\xAB"            # 59
                "\x8B\xC8"                        # 64
                "\xE8\xAB\xAB\xAB\xAB"            # 66
                "\x50"                            # 71
                "\xE8\xAB\xAB\xAB\xAB"            # 72
                "\x8B\xC8"                        # 77
                "\xE8\xAB\xAB\xAB\xAB"            # 79
                "\x6A\x01"                        # 84
                "\xE8\xAB\xAB\xAB\xAB"            # 86
                "\x8B\xC8"                        # 91
                "\xE8\xAB\xAB\xAB\xAB"            # 93
            )
            setBlockOffset = 94
            sessionOffset = 32
            comboOffset = 0
            instanceOffsets = (60, 73, 87)
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 43
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 67
            sendPacketOffset = 80
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            clientTimeOffset2 = (22, 1)
            accountIdOffset1 = (16, 4)
            accountIdOffset2 = (38, 1)
            charIdOffset1 = (24, 4)
            charIdOffset2 = (41, 1)
            authCodeOffset1 = (10, 4)
            authCodeOffset2 = (30, 1)
            packetIdOffsets2 = ((52, 1), (56, 1))
            sexOffset2 = (49, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           offset1,
                                           offset1 + 0x150)
        if offset is not False and self.g_instanceR != 0:
            offset1 = offset
            offset = False
            # 2004-08-16
            # 0  jb short loc_544377
            # 2  mov byte ptr [ebp+p.session_key1], 0
            # 6  call esi
            # 8  mov ecx, g_session_m_auth_key1
            # 14 mov edx, g_session_m_account_id
            # 20 mov [ebp+p.client_time], eax
            # 23 mov eax, g_session_m_char_id
            # 28 mov [ebp+p.session_key1], ecx
            # 31 mov ecx, offset g_session
            # 36 mov [ebp+p.account_id], edx
            # 39 mov [ebp+p.char_id], eax
            # 42 call CSession_GetSex
            # 47 mov [ebp+p.sex], al
            # 50 lea edx, [ebp+p]
            # 53 movsx eax, [ebp+p.packet_id]
            # 57 push edx
            # 58 push eax
            # 59 mov ecx, offset g_instanceR
            # 64 call CRagConnection_GetPacketSize
            # 69 push eax
            # 70 mov ecx, offset g_instanceR
            # 75 call CRagConnection_SendPacket
            # 0  push 1
            # 2  mov ecx, offset g_instanceR
            # 7  call CConnection_SetBlock
            code = (
                "\x72\xAB"                        # 0
                "\xC6\x45\xAB\x00"                # 2
                "\xFF\xAB"                        # 6
                "\x8B\x0D\xAB\xAB\xAB\xAB"        # 8
                "\x8B\x15\xAB\xAB\xAB\xAB"        # 14
                "\x89\x45\xAB"                    # 20
                "\xA1\xAB\xAB\xAB\xAB"            # 23
                "\x89\x4D\xAB"                    # 28
                "\xB9" + sessionHex +             # 31
                "\x89\x55\xAB"                    # 36
                "\x89\x45\xAB"                    # 39
                "\xE8\xAB\xAB\xAB\xAB"            # 42
                "\x88\x45\xAB"                    # 47
                "\x8D\x55\xAB"                    # 50
                "\x0F\xBF\x45\xAB"                # 53
                "\x52"                            # 57
                "\x50"                            # 58
                "\xB9" + self.exe.toHex(self.g_instanceR, 4) +  # 59
                "\xE8\xAB\xAB\xAB\xAB"            # 64
                "\x50"                            # 69
                "\xB9" + self.exe.toHex(self.g_instanceR, 4) +  # 70
                "\xE8\xAB\xAB\xAB\xAB"            # 75
                "\x6A\x01"                        # 80
                "\xB9" + self.exe.toHex(self.g_instanceR, 4) +  # 82
                "\xE8\xAB\xAB\xAB\xAB"            # 87
            )
            setBlockOffset = 88
            sessionOffset = 32
            comboOffset = 0
            instanceOffsets = 0
            enc_packet_keysOffset = 0
            CSession_GetSexOffset = 43
            encryptionInitKeysOffset = 0
            encryptionInitKeys0Offset = 0
            getPacketSizeOffset = 65
            sendPacketOffset = 76
            setPaddingValueOffsets = 0
            dwTimeOffset = 0
            clientTimeOffset2 = (22, 1)
            accountIdOffset1 = (16, 4)
            accountIdOffset2 = (38, 1)
            charIdOffset1 = (24, 4)
            charIdOffset2 = (41, 1)
            authCodeOffset1 = (10, 4)
            authCodeOffset2 = (30, 1)
            packetIdOffsets2 = ((52, 1), (56, 1))
            sexOffset2 = (49, 1)
            key1Offset = 0
            key2Offset = 0
            key3Offset = 0
            searchEncryptionInitKeys = False
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           offset1,
                                           offset1 + 0x150)

        if offset is not False:
            traceOffset = traceOffset - offset + offset1
            packetIdNumOffset = (packetIdNumOffset[0] - offset + offset1,
                                 packetIdNumOffset[1])

    if offset is False and self.g_instanceR != 0:
        offset, section = self.exe.string("PACKET_CZ_ENTER")
        if offset is False:
            czEnter = 0
        else:
            czEnter = section.rawToVa(offset)
        # search where used g_instanceR variable
        # 2004-03-22
        # 0  push offset aPacket_cz_ente
        # 5  mov dword_66E850, 1
        # 15 call Trace
        # 20 add esp, 4
        # 23 mov [ebp+cp.packet_id], 72h
        # 29 call ds:timeGetTime
        # 35 mov ecx, g_session_m_auth_key1
        # 41 mov edx, g_session_m_account_id
        # 47 mov [ebp+cp.client_time], eax
        # 50 mov eax, g_session_m_char_id
        # 55 mov [ebp+cp.session_key1], ecx
        # 58 mov ecx, offset g_session
        # 63 mov [ebp+cp.account_id], edx
        # 66 mov [ebp+cp.char_id], eax
        # 69 call CSession_GetSex
        # 74 mov [ebp+cp.sex], al
        # 77 lea edx, [ebp+cp]
        # 80 movsx eax, [ebp+cp.packet_id]
        # 84 push edx
        # 85 push eax
        # 86 mov ecx, offset g_instanceR
        # 91 call CRagConnection_GetPacketSize
        # 96 push eax
        # 97 mov ecx, offset g_instanceR
        # 102 call CRagConnection_SendPacket
        # 0  push 1
        # 2  mov ecx, offset g_instanceR
        # 7  call CConnection_SetBlock
        code = (
            "\x68" + self.exe.toHex(czEnter, 4) +  # 0
            "\xC7\x05\xAB\xAB\xAB\xAB\x01\x00\x00\x00"  # 5
            "\xE8\xAB\xAB\xAB\xAB"            # 15
            "\x83\xC4\x04"                    # 20
            "\x66\xC7\x45\xAB\xAB\xAB"        # 23
            "\xFF\x15\xAB\xAB\xAB\xAB"        # 29
            "\x8B\x0D\xAB\xAB\xAB\xAB"        # 35
            "\x8B\x15\xAB\xAB\xAB\xAB"        # 41
            "\x89\x45\xAB"                    # 47
            "\xA1\xAB\xAB\xAB\xAB"            # 50
            "\x89\x4D\xAB"                    # 55
            "\xB9" + sessionHex +             # 58
            "\x89\x55\xAB"                    # 63
            "\x89\x45\xAB"                    # 66
            "\xE8\xAB\xAB\xAB\xAB"            # 69
            "\x88\x45\xAB"                    # 74
            "\x8D\x55\xAB"                    # 77
            "\x0F\xBF\x45\xAB"                # 80
            "\x52"                            # 84
            "\x50"                            # 85
            "\xB9" + self.exe.toHex(self.g_instanceR, 4) +  # 86
            "\xE8\xAB\xAB\xAB\xAB"            # 91
            "\x50"                            # 96
            "\xB9" + self.exe.toHex(self.g_instanceR, 4) +  # 97
            "\xE8\xAB\xAB\xAB\xAB"            # 102
            "\x6A\x01"                        # 107
            "\xB9" + self.exe.toHex(self.g_instanceR, 4) +  # 109
            "\xE8\xAB\xAB\xAB\xAB"            # 114
        )
        setBlockOffset = 115
        sessionOffset = 59
        comboOffset = 0
        instanceOffsets = 0
        enc_packet_keysOffset = 0
        CSession_GetSexOffset = 70
        encryptionInitKeysOffset = 0
        encryptionInitKeys0Offset = 0
        getPacketSizeOffset = 92
        sendPacketOffset = 103
        traceOffset = 16
        setPaddingValueOffsets = 0
        dwTimeOffset = 0
        packetIdNumOffset = (27, 2)
        clientTimeOffset2 = (49, 1)
        accountIdOffset1 = (43, 4)
        accountIdOffset2 = (65, 1)
        charIdOffset1 = (51, 4)
        charIdOffset2 = (68, 1)
        authCodeOffset1 = (37, 4)
        authCodeOffset2 = (57, 1)
        packetIdOffsets2 = ((26, 1), (79, 1), (83, 1))
        sexOffset2 = (76, 1)
        key1Offset = 0
        key2Offset = 0
        key3Offset = 0
        searchEncryptionInitKeys = False
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        self.log("failed in search shuffle23 packet")
        if errorExit is True:
            exit(1)
        return
    if instanceOffsets != 0:
        for instanceOffset in instanceOffsets:
            instance = self.getAddr(offset, instanceOffset, instanceOffset + 4)
            if instance != self.instanceR:
                self.log("Error: found different CRagConnection::instanceR")
                exit(1)
    if comboOffset != 0:
        comboAddr = self.getAddr(offset, comboOffset, comboOffset + 4)
        if comboAddr != self.comboAddr:
            self.log("Error: found wrong combo function")
            exit(1)
    getPacketSizeFunction = self.getAddr(offset,
                                         getPacketSizeOffset,
                                         getPacketSizeOffset + 4)
    if getPacketSizeFunction != self.getPacketSizeFunction:
        self.log("Error: found wrong CRagConnection::GetPacketSize")
        exit(1)
    sendPacket = self.getAddr(offset, sendPacketOffset, sendPacketOffset + 4)
    if sendPacket != self.sendPacket:
        self.log("Error: found wrong CRagConnection::SendPacket")
        exit(1)
    packet = 0
    for packetIdOffset in packetIdOffsets2:
        packet0 = self.getVarAddr(offset, packetIdOffset)
        if packet == 0:
            packet = packet0
        else:
            if packet != packet0:
                self.log("Error: found different packet var offsets")
                exit(1)
    self.CSession_getSex = self.getAddr(offset,
                                        CSession_GetSexOffset,
                                        CSession_GetSexOffset + 4)
    self.addRawFunc("CSession::GetSex", self.CSession_getSex)
    if setBlockOffset != 0:
        setBlock = self.getAddr(offset, setBlockOffset, setBlockOffset + 4)
        if self.setBlock != 0:
            if self.setBlock != setBlock:
                self.log("Error: found different CConnection::SetBlock")
                exit(1)
        else:
            self.setBlock = setBlock
            self.addRawFunc("CConnection::SetBlock", self.setBlock)
    if traceOffset != 0:
        self.trace = self.getAddr(offset, traceOffset, traceOffset + 4)
        self.addRawFunc("Trace", self.trace)
    if packetIdNumOffset[1] == 4:
        self.shuffle23 = self.exe.readUInt(offset + packetIdNumOffset[0])
    elif packetIdNumOffset[1] == 2:
        self.shuffle23 = self.exe.readUWord(offset + packetIdNumOffset[0])
    else:
        self.log("Error: wrong packet id size")
        exit(1)
    self.log("Shuffle packet 23 (CZ_ENTER): {0}".format(
        hex(self.shuffle23)))
    if dwTimeOffset != 0:
        if dwTimeOffset[1] == 1:
            dwTime = self.exe.readUByte(offset + dwTimeOffset[0])
        else:
            self.log("Error: wrong size for dwTime")
            exit(1)
        self.addStruct("CRagConnection")
        self.addStructMember("m_dwTime", dwTime, 4, True)
    if enc_packet_keysOffset != 0:
        # probably need fix combofunction search and read enc_packet_keys
        self.encPacketKeys = self.exe.readUInt(offset + enc_packet_keysOffset)
        self.addVaVar("enc_packet_keys", self.encPacketKeys)
    if setPaddingValueOffsets != 0:
        setPaddingValue = 0
        for setPaddingValueOffset in setPaddingValueOffsets:
            pad = self.getAddr(offset,
                               setPaddingValueOffset,
                               setPaddingValueOffset + 4)
            if setPaddingValue == 0:
                setPaddingValue = pad
            elif setPaddingValue != pad:
                self.log("Error: found different CLoginMode::SetPaddingValue")
                exit(1)
        self.CLoginMode_SetPaddingValue = setPaddingValue
        self.addRawFunc("CLoginMode::SetPaddingValue",
                        self.CLoginMode_SetPaddingValue)
    # search g_session
    if sessionHex == "\xAB\xAB\xAB\xAB":
        if sessionOffset == 0:
            self.log("Error: search g_session in shuffle23 failed.")
            exit(1)
        self.session = self.exe.readUInt(offset + sessionOffset)
        self.addVaVar("g_session", self.session)
    accountId1 = self.exe.readUInt(offset + accountIdOffset1[0]) - self.session
    charId1 = self.exe.readUInt(offset + charIdOffset1[0]) - self.session
    authCode1 = self.exe.readUInt(offset + authCodeOffset1[0]) - self.session
    self.addStruct("CSession")
    self.addStructMember("m_account_id", accountId1, 4, True)
    self.addStructMember("m_char_id", charId1, 4, True)
    self.addStructMember("m_auth_code1", authCode1, 4, True)
    clientTime2 = self.getVarAddr(offset, clientTimeOffset2) - packet
    accountId2 = self.getVarAddr(offset, accountIdOffset2) - packet
    charId2 = self.getVarAddr(offset, charIdOffset2) - packet
    authCode2 = self.getVarAddr(offset, authCodeOffset2) - packet
    sex2 = self.getVarAddr(offset, sexOffset2) - packet
    if len(sexOffset2) > 2:
        sex2 = sex2 + sexOffset2[2]
    # additional search for CEncryption::InitKeys
    if searchEncryptionInitKeys is True:
        # 0  mov ecx, enc_packet_keys
        # 6  push 62805311h
        # 11 push 7D386860h
        # 16 push 69FB1C38h
        # 21 call CEncryption_InitKeys
        code = (
            "\x8B\x0D" + self.exe.toHex(self.encPacketKeys, 4) +  # 0
            "\x68\xAB\xAB\xAB\xAB"            # 6
            "\x68\xAB\xAB\xAB\xAB"            # 11
            "\x68\xAB\xAB\xAB\xAB"            # 16
            "\xE8\xAB\xAB\xAB\xAB"            # 21
        )
        key1Offset = 17
        key2Offset = 12
        key3Offset = 7
        encryptionInitKeysOffset = 22
        encryptionInitKeys0Offset = 0
        offset2 = self.exe.codeWildcard(code, "\xAB", offset - 0x100, offset)
        if offset2 is False:
            self.log("failed in search CEncryption::InitKeys packet")
            if errorExit is True:
                exit(1)
            return
        offset = offset2
    if encryptionInitKeysOffset != 0:
        self.encryptionInitKeys = self.getAddr(offset,
                                               encryptionInitKeysOffset,
                                               encryptionInitKeysOffset + 4)
        self.addRawFunc("CEncryption::InitKeys", self.encryptionInitKeys)
    if encryptionInitKeys0Offset != 0:
        self.encryptionInitKeys0 = self.getAddr(offset,
                                                encryptionInitKeys0Offset,
                                                encryptionInitKeys0Offset + 4)
        self.addRawFunc("CEncryption::InitKeys0", self.encryptionInitKeys0)
    if key1Offset != 0:
        self.key1 = self.exe.readUInt(offset + key1Offset)
        self.key2 = self.exe.readUInt(offset + key2Offset)
        self.key3 = self.exe.readUInt(offset + key3Offset)
        self.log("Encryption key1: {0}".format(hex(self.key1)))
        self.log("Encryption key2: {0}".format(hex(self.key2)))
        self.log("Encryption key3: {0}".format(hex(self.key3)))

    debug = False
    self.addStruct("struct_packet_CZ_ENTER")
    self.addStructMember("packet_id", 0, 2, debug)
    self.addStructMember("account_id", accountId2, 4, debug)
    self.addStructMember("char_id", charId2, 4, debug)
    self.addStructMember("session_key1", authCode2, 4, debug)
    self.addStructMember("client_time", clientTime2, 4, debug)
    self.addStructMember("sex", sex2, 1, debug)
