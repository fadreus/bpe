#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.calls import searchCallsTo4


def searchMsgStringTable(self):
    offset, section = self.exe.string("msgStringTable.txt")
    if offset is False:
        self.log("failed in search msgStringTable.txt")
        exit(1)
        return
    msgStringTableV = section.rawToVa(offset)
    # 0  push 0
    # 2  push offset "msgStringTable.txt"
    # 7  push offset s_msgStringTable
    # 12 mov ecx, s_obj_0
    # 17 call CommonObject::TokenFileToPCharList
    # 22 xor esi, esi
    # 24 mov edi, offset msgStringTable
    code = (
        "\x6A\x00" +
        "\x68" + self.exe.toHex(msgStringTableV, 4) +
        "\x68\xAB\xAB\xAB\xAB" +
        "\xB9\xAB\xAB\xAB\xAB" +
        "\xE8\xAB\xAB\xAB\xFF" +
        "\x33\xAB" +
        "\xBF")
    s_msgStringTableOffset = 8
    s_obj_0Offset = 13
    funcOffset = 18
    msgStringTableOffset = 25
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 0  push 0
        # 2  push offset "msgStringTable.txt"
        # 7  push offset s_msgStringTable
        # 12 mov ecx, s_obj_0
        # 17 call CommonObject::TokenFileToPCharList
        code = (
            "\x6A\x00" +
            "\x68" + self.exe.toHex(msgStringTableV, 4) +
            "\x68\xAB\xAB\xAB\xAB" +
            "\xB9\xAB\xAB\xAB\xAB" +
            "\xE8\xAB\xAB\xAB\xFF")
        s_msgStringTableOffset = 8
        s_obj_0Offset = 13
        funcOffset = 18
        msgStringTableOffset = 0
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2004-02-03
        # 0  xor esi, esi
        # 2  lea ecx, [ebp+s_obj_0]
        # 8  push    esi
        # 9  push offset "msgStringTable.txt"
        # 14 mov [ebp+var_4], esi
        # 17 call func
        code = (
            "\x33\xF6" +
            "\x8D\xAB\xAB\xAB\xAB\xFF" +
            "\xAB" +
            "\x68" + self.exe.toHex(msgStringTableV, 4) +
            "\x89\xAB\xAB" +
            "\xE8\xAB\xAB\xAB\xAB")
        s_msgStringTableOffset = 0
        s_obj_0Offset = 0
        funcOffset = 0
        msgStringTableOffset = 0
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        self.log("failed in msgStringTable.txt usage")
        exit(1)
        return
    if s_obj_0Offset > 0:
        self.s_obj_0 = self.exe.readUInt(offset + s_obj_0Offset)
        self.addVaVar("s_obj_0", self.s_obj_0)
    if funcOffset != 0:
        self.CommonObject_TokenFileToPCharList = self.getAddr(offset,
                                                              funcOffset,
                                                              funcOffset + 4)
        self.addRawFunc("CommonObject::TokenFileToPCharList",
                        self.CommonObject_TokenFileToPCharList)
    self.s_msgStringTable = self.exe.readUInt(
        offset + s_msgStringTableOffset)
    self.addVaVar("s_msgStringTable", self.s_msgStringTable)
    if msgStringTableOffset == 0:
        if self.gServiceType == 0:
            self.log("failed search msgStringTable "
                     "because no g_serviceType")
            exit(1)
            return
        # 0  cmp g_serviceType, 0
        # 7  jnz short addr1
        # 9  push esi
        # 10 xor esi, esi
        # 12 xor edx, edx
        # 14 mov edi, edi
        # 16 mov ecx, msgStringTable[edx]
        code = (
            "\x83\xAB" + self.exe.toHex(self.gServiceType, 4) + "\x00" +
            "\x75\xAB" +
            "\x56" +
            "\x33\xAB" +
            "\x33\xAB" +
            "\x8B\xAB" +
            "\x8B\xAB\xAB\xAB\xAB\x00")
        msgStringTableOffset = 18
        offset2 = self.exe.codeWildcard(code, "\xAB", offset - 0x40, offset)
        if offset2 is False:
            # 0  cmp g_serviceType, 0
            # 7  push esi
            # 8  jnz short addr1
            # 10 xor esi, esi
            # 12 xor edx, edx
            # 14 mov edi, edi
            # 16 mov ecx, msgStringTable[edx]
            code = (
                "\x83\xAB" + self.exe.toHex(self.gServiceType, 4) + "\x00" +
                "\x56" +
                "\x75\xAB" +
                "\x33\xAB" +
                "\x33\xAB" +
                "\x8B\xAB" +
                "\x8B\xAB\xAB\xAB\xAB\x00")
            msgStringTableOffset = 18
            offset2 = self.exe.codeWildcard(code,
                                            "\xAB",
                                            offset - 0x40,
                                            offset)
        if offset2 is False:
            # 2008-06-20
            # 0  cmp g_serviceType, 0
            # 7  push esi
            # 8  jnz short addr1
            # 10 xor esi, esi
            # 12 lea esp, [esp+0]
            # 16 mov eax, addr1
            # 21 sub eax, addr2
            # 27 sar eax, 2
            # 30 cmp esi, eax
            # 32 jb short addr3
            # 34 call __invalid_parameter_noinfo
            # 39 mov ecx, msgStringTable[esi*8]
            code = (
                "\x83\xAB" + self.exe.toHex(self.gServiceType, 4) + "\x00" +
                "\x56" +
                "\x75\xAB" +
                "\x33\xAB" +
                "\x8D\xAB\xAB\x00" +
                "\xA1\xAB\xAB\xAB\x00" +
                "\x2B\xAB\xAB\xAB\xAB\x00" +
                "\xC1\xAB\x02" +
                "\x3B\xAB" +
                "\x72\xAB" +
                "\xE8\xAB\xAB\xAB\x00" +
                "\x8B\xAB\xAB\xAB\xAB\xAB\x00")
            msgStringTableOffset = 42
            offset2 = self.exe.codeWildcard(code,
                                            "\xAB",
                                            offset - 0x60,
                                            offset)
        if offset2 is False and self.packetVersion <= "20040203":
            # 0 push ebp
            # 1 mov ebp, esp
            # 3 push 0FFFFFFFFh
            # 5 push offset A
            code = (
                "\x55" +
                "\x8B\xEC" +
                "\x6A\xFF" +
                "\x68\xAB\xAB\xAB\x00")
            offset2 = self.exe.codeWildcard(code,
                                            "\xAB",
                                            offset - 0x90,
                                            offset)
            if offset2 is False:
                self.log("failed search msgStringTable for old clients.")
                exit(1)
                return
            offsets = searchCallsTo4(self, "\xE8", self.exe.rawToVa(offset2))
            if offsets is False:
                self.log("failed search msgStringTable for old clients 2.")
                exit(1)
            if len(offsets) != 1:
                self.log("found wrong number of cross references "
                         "for msgStringTable")
                exit(1)
            offset = offsets[0]
            # 0  mov eax, g_serviceType
            # 5  push esi
            # 6  test eax, eax
            # 8  jnz addr1
            # 10 xor ecx, ecx
            # 12 mov eax, offset msgStringTable
            code = (
                "\xA1" + self.exe.toHex(self.gServiceType, 4) +
                "\x56" +
                "\x85\xAB" +
                "\x75\xAB" +
                "\x33\xAB" +
                "\xB8\xAB\xAB\xAB\x00")
            msgStringTableOffset = 13
            offset2 = self.exe.codeWildcard(code,
                                            "\xAB",
                                            offset - 0x40,
                                            offset)
        if offset2 is False:
            self.log("failed search msgStringTable.")
            exit(1)
            return
        self.initMsgStrings = offset2
        self.addRawFunc("InitMsgStrings", self.initMsgStrings)
        offset = offset2
    self.msgStringTable = self.exe.readUInt(offset + msgStringTableOffset)
    self.addVaVar("msgStringTable", self.msgStringTable)


def extractMsgStrings(self):
    exe = self.exe
    sections = exe.sections
    readUInt2 = exe.readUInt2
    readStr = exe.readStr
    if exe.themida is False:
        if ".data" not in sections:
            exe.log("Error: .data section not found")
            exit(1)
    else:
        if "sect_0" not in sections:
            exe.log("Error: themida code section not found")
            exit(1)
    if exe.themida is False:
        dataSection = sections[".data"]
    else:
        dataSection = sections["sect_0"]

    pointer = dataSection.vaToRaw(self.msgStringTable) - 4
    msgId = 0
    self.msgStrings = dict()
    vaToRawUnknown = exe.vaToRawUnknown
    while True:
        msgId2, strAddr = readUInt2(pointer)
        if msgId != msgId2:
            break
        strAddr = vaToRawUnknown(strAddr)
        if strAddr is False:
            data = "\x00\x00\x00 virtual data"
        else:
            data = readStr(strAddr)
            if data is False:
                exe.log("Error: search msgstring message failed: {0}".
                        format(hex(msgId)))
                exit(1)
        self.msgStrings[msgId] = data
        msgId = msgId + 1
        pointer = pointer + 8
    if len(self.msgStrings) == 0:
        exe.log("Error: msgstringtable corrupted or wrong")
        exit(1)
