#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchServerType(self, errorExit):
    offset, section = self.exe.string("sakray")
    if offset is False:
        self.log("string 'sakray' not found.")
        if errorExit is True:
            exit(1)
        return
    sakrayVa = section.rawToVa(offset)
    # push offset 'sakray'
    code = "\x68" + self.exe.toHex(sakrayVa, 4)
    offset = self.exe.code(code)
    if offset is False:
        self.log("reference to 'sakray' missing.")
        if errorExit is True:
            exit(1)
        return
    # mov g_serverType, 1
    code = "\xC7\x05\xAB\xAB\xAB\xAB\x01\x00\x00\x00"
    offset = self.exe.codeWildcard(code, "\xAB", offset, offset + 0x50)
    if offset is False:
        self.log("g_serverType search failed")
        if errorExit is True:
            exit(1)
        return
    self.gServerType = self.exe.readUInt(offset + 2)
    self.addVaVar("g_serverType", self.gServerType)
