#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchWindowMgrDeleteWindow(self, errorExit):
    # search in recv_packet_8D9

    windowMgrHex = self.exe.toHex(self.gWindowMgr, 4)

    # 0  push 0D1h
    # 5  mov ecx, offset g_windowMgr
    # 10 mov [ebp+var_44], edi
    # 13 mov byte ptr [ebx+38h], 1
    # 17 call UIWindowMgr_QueryWindow
    # 22 mov [ebp+window], eax
    # 25 test eax, eax
    # 27 jnz short loc_892F45
    # 29 push 0D1h
    # 34 mov ecx, offset g_windowMgr
    # 39 call UIWindowMgr_DeleteWindow
    # 44 test al, al
    # 46 jnz short loc_892F45
    # 48 push 0D1h
    # 53 mov ecx, offset g_windowMgr
    # 58 call UIWindowMgr_MakeWindow
    # 63 mov [ebp+window], eax
    code = (
        "\x68\xD1\x00\x00\x00"            # 0
        "\xB9" + windowMgrHex +           # 5
        "\x89\x7D\xAB"                    # 10
        "\xC6\x43\xAB\xAB"                # 13
        "\xE8\xAB\xAB\xAB\xAB"            # 17
        "\x89\x45\xAB"                    # 22
        "\x85\xC0"                        # 25
        "\x75\x25"                        # 27
        "\x68\xD1\x00\x00\x00"            # 29
        "\xB9" + windowMgrHex +           # 34
        "\xE8\xAB\xAB\xAB\xAB"            # 39
        "\x84\xC0"                        # 44
        "\x75\x12"                        # 46
        "\x68\xD1\x00\x00\x00"            # 48
        "\xB9" + windowMgrHex +           # 53
        "\xE8\xAB\xAB\xAB\xAB"            # 58
        "\x89\x45"                        # 63
    )
    queryWindowOffset = 18
    deleteWindowOffset = 40
    makeWindowOffset = 59
    offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        # 0  push 0D1h
        # 5  mov ecx, offset g_windowMgr
        # 10 mov [ebp+var_8], eax
        # 13 mov byte ptr [eax+38h], 1
        # 17 call UIWindowMgr_QueryWindow
        # 22 mov edi, eax
        # 24 test edi, edi
        # 26 jnz short loc_811B45
        # 28 push 0D1h
        # 33 mov ecx, offset g_windowMgr
        # 38 call UIWindowMgr_DeleteWindow
        # 43 test al, al
        # 45 jnz loc_811C03
        # 51 push 0D1h
        # 56 mov ecx, offset g_windowMgr
        # 61 call UIWindowMgr_MakeWindow
        # 66 mov edi, eax
        # 68 test edi, edi
        # 70 jz loc_811C03
        code = (
            "\x68\xD1\x00\x00\x00"            # 0
            "\xB9" + windowMgrHex +           # 5
            "\x89\x45\xAB"                    # 10
            "\xC6\x40\xAB\xAB"                # 13
            "\xE8\xAB\xAB\xAB\xAB"            # 17
            "\x8B\xF8"                        # 22
            "\x85\xFF"                        # 24
            "\x75\x30"                        # 26
            "\x68\xD1\x00\x00\x00"            # 28
            "\xB9" + windowMgrHex +           # 33
            "\xE8\xAB\xAB\xAB\xAB"            # 38
            "\x84\xC0"                        # 43
            "\x0F\x85\xAB\xAB\xAB\xAB"        # 45
            "\x68\xD1\x00\x00\x00"            # 51
            "\xB9" + windowMgrHex +           # 56
            "\xE8\xAB\xAB\xAB\xAB"            # 61
            "\x8B\xF8"                        # 66
            "\x85\xFF"                        # 68
            "\x0F\x84"                        # 70
        )
        queryWindowOffset = 18
        deleteWindowOffset = 39
        makeWindowOffset = 62
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        # 0  push 0D1h
        # 5  mov ecx, offset g_windowMgr
        # 10 mov [ebp+var_50], esi
        # 13 mov [ebp+var_4C], eax
        # 16 mov byte ptr [esi+38h], 1
        # 20 call UIWindowMgr_QueryWindow
        # 25 xor ebx, ebx
        # 27 mov [ebp+var_54], eax
        # 30 cmp eax, ebx
        # 32 jnz short loc_6DC527
        # 34 push 0D1h
        # 39 mov ecx, offset g_windowMgr
        # 44 call UIWindowMgr_DeleteWindow
        # 49 test al, al
        # 51 jnz short loc_6DC527
        # 53 push 0D1h
        # 58 mov ecx, offset g_windowMgr
        # 63 call UIWindowMgr_MakeWindow
        # 68 mov [ebp+var_54], eax
        code = (
            "\x68\xD1\x00\x00\x00"            # 0
            "\xB9" + windowMgrHex +           # 5
            "\x89\x75\xAB"                    # 10
            "\x89\x45\xAB"                    # 13
            "\xC6\x46\xAB\xAB"                # 16
            "\xE8\xAB\xAB\xAB\xAB"            # 20
            "\x33\xDB"                        # 25
            "\x89\x45\xAB"                    # 27
            "\x3B\xC3"                        # 30
            "\x75\x25"                        # 32
            "\x68\xD1\x00\x00\x00"            # 34
            "\xB9" + windowMgrHex +           # 39
            "\xE8\xAB\xAB\xAB\xAB"            # 44
            "\x84\xC0"                        # 49
            "\x75\x12"                        # 51
            "\x68\xD1\x00\x00\x00"            # 53
            "\xB9" + windowMgrHex +           # 58
            "\xE8\xAB\xAB\xAB\xAB"            # 63
            "\x89\x45"                        # 68
        )
        queryWindowOffset = 21
        deleteWindowOffset = 45
        makeWindowOffset = 64
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        # 0  push 0D6h
        # 5  mov ecx, offset g_windowMgr
        # 10 mov [esp+70h+var_54], edi
        # 14 mov byte ptr [ebx+38h], 1
        # 18 call UIWindowMgr_QueryWindow
        # 23 mov [esp+14h], eax
        # 27 test eax, eax
        # 29 jnz short loc_663A3D
        # 31 push 0D6h
        # 36 mov ecx, offset g_windowMgr
        # 41 call UIWindowMgr_DeleteWindow
        # 46 test al, al
        # 48 jnz short loc_663A3D
        # 50 push 0D6h
        # 55 mov ecx, offset g_windowMgr
        # 60 call UIWindowMgr_MakeWindow
        # 65 mov [esp+14h], eax
        code = (
            "\x68\xD6\x00\x00\x00"            # 0
            "\xB9" + windowMgrHex +           # 5
            "\x89\x7C\x24\xAB"                # 10
            "\xC6\x43\xAB\xAB"                # 14
            "\xE8\xAB\xAB\xAB\xAB"            # 18
            "\x89\x44\x24\xAB"                # 23
            "\x85\xC0"                        # 27
            "\x75\x26"                        # 29
            "\x68\xD6\x00\x00\x00"            # 31
            "\xB9" + windowMgrHex +           # 36
            "\xE8\xAB\xAB\xAB\xAB"            # 41
            "\x84\xC0"                        # 46
            "\x75\x13"                        # 48
            "\x68\xD6\x00\x00\x00"            # 50
            "\xB9" + windowMgrHex +           # 55
            "\xE8\xAB\xAB\xAB\xAB"            # 60
            "\x89\x44\x24"                    # 65
        )
        queryWindowOffset = 19
        deleteWindowOffset = 42
        makeWindowOffset = 61
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        # 0  push 0D6h
        # 5  mov ecx, offset g_windowMgr
        # 10 mov [ebp+var_50], esi
        # 13 mov [ebp+var_4C], eax
        # 16 mov byte ptr [esi+38h], 1
        # 20 call UIWindowMgr_QueryWindow
        # 25 xor ebx, ebx
        # 27 mov [ebp+var_54], eax
        # 30 cmp eax, ebx
        # 32 jnz short loc_691917
        # 34 push 0D6h
        # 39 mov ecx, offset g_windowMgr
        # 44 call UIWindowMgr_DeleteWindow
        # 49 test al, al
        # 51 jnz short loc_691917
        # 53 push 0D6h
        # 58 mov ecx, offset g_windowMgr
        # 63 call UIWindowMgr_MakeWindow
        # 68 mov [ebp+var_54], eax
        code = (
            "\x68\xD6\x00\x00\x00"            # 0
            "\xB9" + windowMgrHex +           # 5
            "\x89\x75\xAB"                    # 10
            "\x89\x45\xAB"                    # 13
            "\xC6\x46\xAB\xAB"                # 16
            "\xE8\xAB\xAB\xAB\xAB"            # 20
            "\x33\xDB"                        # 25
            "\x89\x45\xAB"                    # 27
            "\x3B\xC3"                        # 30
            "\x75\x25"                        # 32
            "\x68\xD6\x00\x00\x00"            # 34
            "\xB9" + windowMgrHex +           # 39
            "\xE8\xAB\xAB\xAB\xAB"            # 44
            "\x84\xC0"                        # 49
            "\x75\x12"                        # 51
            "\x68\xD6\x00\x00\x00"            # 53
            "\xB9" + windowMgrHex +           # 58
            "\xE8\xAB\xAB\xAB\xAB"            # 63
            "\x89\x45"                        # 68
        )
        queryWindowOffset = 21
        deleteWindowOffset = 45
        makeWindowOffset = 64
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        # 0  push 0D1h
        # 5  mov ecx, offset g_windowMgr
        # 10 mov [esp+70h+var_54], edi
        # 14 mov byte ptr [ebx+38h], 1
        # 18 call UIWindowMgr_QueryWindow
        # 23 mov [esp+6Ch+var_58], eax
        # 27 test eax, eax
        # 29 jnz short loc_66399D
        # 31 push 0D1h
        # 36 mov ecx, offset g_windowMgr
        # 41 call UIWindowMgr_DeleteWindow
        # 46 test al, al
        # 48 jnz short loc_66399D
        # 50 push 0D1h
        # 55 mov ecx, offset g_windowMgr
        # 60 call UIWindowMgr_MakeWindow
        # 65 mov [esp+6Ch+var_58], eax
        code = (
            "\x68\xD1\x00\x00\x00"            # 0
            "\xB9" + windowMgrHex +           # 5
            "\x89\x7C\x24\xAB"                # 10
            "\xC6\x43\xAB\xAB"                # 14
            "\xE8\xAB\xAB\xAB\xAB"            # 18
            "\x89\x44\x24\xAB"                # 23
            "\x85\xC0"                        # 27
            "\x75\x26"                        # 29
            "\x68\xD1\x00\x00\x00"            # 31
            "\xB9" + windowMgrHex +           # 36
            "\xE8\xAB\xAB\xAB\xAB"            # 41
            "\x84\xC0"                        # 46
            "\x75\x13"                        # 48
            "\x68\xD1\x00\x00\x00"            # 50
            "\xB9" + windowMgrHex +           # 55
            "\xE8\xAB\xAB\xAB\xAB"            # 60
            "\x89\x44\x24"                    # 65
        )
        queryWindowOffset = 19
        deleteWindowOffset = 42
        makeWindowOffset = 61
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        # 0  push 0D6h
        # 5  mov ecx, offset g_windowMgr
        # 10 mov [esp+70h+var_58], ebx
        # 14 mov [esp+70h+var_54], ebp
        # 18 mov byte ptr [ebx+38h], 1
        # 22 call UIWindowMgr_QueryWindow
        # 27 mov [esp+6Ch+var_50], eax
        # 31 test eax, eax
        # 33 jnz short loc_5D31B1
        # 35 push 0D6h
        # 40 mov ecx, offset g_windowMgr
        # 45 call UIWindowMgr_DeleteWindow
        # 50 test al, al
        # 52 jnz short loc_5D31B1
        # 54 push 0D6h
        # 59 mov ecx, offset g_windowMgr
        # 64 call UIWindowMgr_MakeWindow
        # 69 mov [esp+6Ch+var_50], eax
        code = (
            "\x68\xD6\x00\x00\x00"            # 0
            "\xB9" + windowMgrHex +           # 5
            "\x89\x5C\x24\xAB"                # 10
            "\x89\x6C\x24\xAB"                # 14
            "\xC6\x43\xAB\xAB"                # 18
            "\xE8\xAB\xAB\xAB\xAB"            # 22
            "\x89\x44\x24\xAB"                # 27
            "\x85\xC0"                        # 31
            "\x75\x26"                        # 33
            "\x68\xD6\x00\x00\x00"            # 35
            "\xB9" + windowMgrHex +           # 40
            "\xE8\xAB\xAB\xAB\xAB"            # 45
            "\x84\xC0"                        # 50
            "\x75\x13"                        # 52
            "\x68\xD6\x00\x00\x00"            # 54
            "\xB9" + windowMgrHex +           # 59
            "\xE8\xAB\xAB\xAB\xAB"            # 64
            "\x89\x44\x24"                    # 69
        )
        queryWindowOffset = 23
        deleteWindowOffset = 46
        makeWindowOffset = 65
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        self.log("failed in search UIWindowMgr::DeleteWindow.")
        if errorExit is True and self.packetVersion >= "20120000":
            exit(1)
        return

    makeWindow = self.getAddr(offset,
                              makeWindowOffset,
                              makeWindowOffset + 4)
    if self.UIWindowMgrMakeWindow != makeWindow:
        self.log("Error: found wrong UIWindowMgr::MakeWindow")
        exit(1)
    self.UIWindowMgrQueryWindow = self.getAddr(offset,
                                               queryWindowOffset,
                                               queryWindowOffset + 4)
    self.addRawFunc("UIWindowMgr::QueryWindow",
                    self.UIWindowMgrQueryWindow)

    self.UIWindowMgrDeleteWindow = self.getAddr(offset,
                                                deleteWindowOffset,
                                                deleteWindowOffset + 4)
    self.UIWindowMgrDeleteWindowVa = self.exe.rawToVa(
        self.UIWindowMgrDeleteWindow)
    self.addRawFunc("UIWindowMgr::DeleteWindow",
                    self.UIWindowMgrDeleteWindow)
