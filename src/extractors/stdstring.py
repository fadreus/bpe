#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchStdString(self):
    # search in recv_packet_73
    if self.packet73Tmp == 0:
        self.log("Error: packet73 not found")
        exit(1)

    offset, section = self.exe.string("server:%s\n")
    if offset is False:
        self.log("failed in search 'server:%s\n'")
        exit(1)
    serverStr = section.rawToVa(offset)

    offset, section = self.exe.string("ip:%s\n")
    if offset is False:
        self.log("failed in search 'ip:%s\n'")
        exit(1)
    ipStr = section.rawToVa(offset)

    serverStrHex = self.exe.toHex(serverStr, 4)
    ipStrHex = self.exe.toHex(ipStr, 4)

    # 0  mov esi, fprintf
    # 6  push offset endl
    # 11 push edi
    # 12 call esi ; fprintf
    # 14 cmp g_session_zone_server_name.m_allocated_len, 10h
    # 21 mov eax, offset g_session_zone_server_name
    # 26 cmovnb eax, g_session_zone_server_name.m_cstr
    # 33 push eax
    # 34 push offset aServerS
    # 39 push edi
    # 40 call esi ; fprintf
    # 42 push offset g_zoneServerAddr
    # 47 push offset aIpS
    # 52 push edi
    # 53 call esi ; fprintf
    code = (
        "\x8B\x35\xAB\xAB\xAB\xAB"        # 0
        "\x68\xAB\xAB\xAB\xAB"            # 6
        "\x57"                            # 11
        "\xFF\xD6"                        # 12
        "\x83\x3D\xAB\xAB\xAB\xAB\xAB"    # 14
        "\xB8\xAB\xAB\xAB\xAB"            # 21
        "\x0F\x43\x05\xAB\xAB\xAB\xAB"    # 26
        "\x50"                            # 33
        "\x68" + serverStrHex +           # 34
        "\x57"                            # 39
        "\xFF\xD6"                        # 40
        "\x68\xAB\xAB\xAB\xAB"            # 42
        "\x68" + ipStrHex +               # 47
        "\x57"                            # 52
        "\xFF\xD6"                        # 53
    )
    fprintfOffset = (2, True)
    allocLenOffset = (16, 4)
    minLenOffset = (20, 1)
    nameOffset = (22, 4)
    cStrOffset = (29, 4)
    g_zoneServerAddrOffset = 43
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.packet73Tmp + 10,
                                   self.packet73Tmp + 0x100)

    if offset is False:
        # 2015-01-07
        # 0  mov edi, fprintf
        # 6  push offset _word_9CBA34
        # 11 push esi
        # 12 call edi
        # 14 mov eax, g_session.m_zone_server_name.m_cstr
        # 19 add esp, 0Ch
        # 22 cmp g_session.m_zone_server_name.m_allocated_len, 10h
        # 29 jnb short loc_85E5A0
        # 31 mov eax, offset g_session.m_zone_server_name
        # 36 push eax
        # 37 push offset aServerS
        # 42 push esi
        # 43 call edi
        # 45 push offset g_zoneServerAddr
        # 50 push offset aIpS
        # 55 push esi
        # 56 call edi
        code = (
            "\x8B\x3D\xAB\xAB\xAB\xAB"        # 0
            "\x68\xAB\xAB\xAB\xAB"            # 6
            "\x56"                            # 11
            "\xFF\xD7"                        # 12
            "\xA1\xAB\xAB\xAB\xAB"            # 14
            "\x83\xC4\x0C"                    # 19
            "\x83\x3D\xAB\xAB\xAB\xAB\xAB"    # 22
            "\x73\xAB"                        # 29
            "\xB8\xAB\xAB\xAB\xAB"            # 31
            "\x50"                            # 36
            "\x68" + serverStrHex +           # 37
            "\x56"                            # 42
            "\xFF\xD7"                        # 43
            "\x68\xAB\xAB\xAB\xAB"            # 45
            "\x68" + ipStrHex +               # 50
            "\x56"                            # 55
            "\xFF\xD7"                        # 56
        )
        fprintfOffset = (2, True)
        allocLenOffset = (24, 4)
        minLenOffset = (28, 1)
        nameOffset = (32, 4)
        cStrOffset = (15, 4)
        g_zoneServerAddrOffset = 46
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.packet73Tmp + 10,
                                       self.packet73Tmp + 0x100)

    if offset is False:
        self.log("failed in search std::string")
        if self.packetVersion < "20110000" or \
           (self.clientType == "iro" and self.packetVersion < "20120000"):
            return
        exit(1)

    if fprintfOffset[1] is True:
        self.fprintf = self.exe.readUInt(offset + fprintfOffset[0])
        self.addVaFunc("fprintf", self.fprintf)
    else:
        self.fprintf = self.getAddr(offset,
                                    fprintfOffset[0],
                                    fprintfOffset[0] + 4)
        self.addRawFunc("fprintf", self.fprintf)
    self.g_zoneServerAddr = self.exe.readUInt(offset + g_zoneServerAddrOffset)
    self.addVaVar("g_zoneServerAddr", self.g_zoneServerAddr)

    name = self.getVarAddr(offset, nameOffset)
    allocLen = self.getVarAddr(offset, allocLenOffset) - name
    cStr = self.getVarAddr(offset, cStrOffset) - name
    minLen = self.getVarAddr(offset, minLenOffset)
    if cStr != 0:
        self.log("Error: wrong c_str offset found: {0}".format(cStr))
        exit(1)
    if allocLen != minLen + 4:
        self.log("Error: wrong m_allocated_len in std::string found: {0}, {1}".
                 format(allocLen, minLen))
        exit(1)
    self.stdstringSize = allocLen + 4

    debug = True
    self.addStruct("struct_std_string")
    self.addStructMember("m_cstr", 0, 4, debug)
    self.addStructMember("m_len", minLen, 4, debug)
    self.addStructMember("m_allocated_len", allocLen, 4, debug)
    self.setStructMemberType(0, "char*")
    self.setStructComment("Size {0}".format(hex(self.stdstringSize)))

    self.addStruct("CSession")
    offset = name - self.session
    self.addStructMember("m_zone_server_name",
                         offset,
                         self.stdstringSize,
                         True)
    self.setStructMemberType(offset, "struct_std_string")
