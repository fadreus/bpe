#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchLua(self, errorExit):
    offset, _ = self.exe.string("Lua 5.1")
    if offset is False:
        self.log("Error: Lua 5.1 not found")
        if errorExit is True and self.packetVersion >= "20120110":
            exit(1)
        return
    # 1. search size of lua struct
    # 0  push ebp
    # 1  mov ebp, esp
    # 3  push ebx
    # 4  mov ebx, [ebp+ud]
    # 7  push esi
    # 8  push edi
    # 9  mov edi, [ebp+arg_4]
    # 12 push 178h
    # 17 push 0
    # 19 push 0
    # 21 push edi
    # 22 call ebx
    # 24 mov esi, eax
    # 26 add esp, 10h
    code = (
        "\x55"                            # 0
        "\x8B\xEC"                        # 1
        "\x53"                            # 3
        "\x8B\x5D\x08"                    # 4
        "\x56"                            # 7
        "\x57"                            # 8
        "\x8B\x7D\x0C"                    # 9
        "\x68\x78\x01\x00\x00"            # 12
        "\x6A\x00"                        # 17
        "\x6A\x00"                        # 19
        "\x57"                            # 21
        "\xFF\xD3"                        # 22
        "\x8B\xF0"                        # 24
        "\x83\xC4\x10"                    # 26
    )
    sizeOffset = 13
    offset = self.exe.code(code)
    if offset is False:
        # 0  push ebp
        # 1  mov ebp, [esp+8+arg_0]
        # 5  push esi
        # 6  push edi
        # 7  mov edi, [esp+10h+arg_4]
        # 11 push 178h
        # 16 xor ebx, ebx
        # 18 push ebx
        # 19 push ebx
        # 20 push edi
        # 21 call ebp
        # 23 mov esi, eax
        # 25 add esp, 10h
        code = (
            "\x55"                            # 0
            "\x8B\x6C\x24\x0C"                # 1
            "\x56"                            # 5
            "\x57"                            # 6
            "\x8B\x7C\x24\x18"                # 7
            "\x68\x78\x01\x00\x00"            # 11
            "\x33\xDB"                        # 16
            "\x53"                            # 18
            "\x53"                            # 19
            "\x57"                            # 20
            "\xFF\xD5"                        # 21
            "\x8B\xF0"                        # 23
            "\x83\xC4\x10"                    # 25
        )
        sizeOffset = 12
        offset = self.exe.code(code)
    if offset is False:
        # 0  push ebp
        # 1  mov ebp, esp
        # 3  push ebx
        # 4  push esi
        # 5  push edi
        # 6  mov edi, [ebp+arg_4]
        # 9  push 178h
        # 14 xor ebx, ebx
        # 16 push ebx
        # 17 push ebx
        # 18 push edi
        # 19 call [ebp+arg_0]
        # 22 mov esi, eax
        # 24 add esp, 10h
        code = (
            "\x55"                            # 0
            "\x8B\xEC"                        # 1
            "\x53"                            # 3
            "\x56"                            # 4
            "\x57"                            # 5
            "\x8B\x7D\x0C"                    # 6
            "\x68\x78\x01\x00\x00"            # 9
            "\x33\xDB"                        # 14
            "\x53"                            # 16
            "\x53"                            # 17
            "\x57"                            # 18
            "\xFF\x55\x08"                    # 19
            "\x8B\xF0"                        # 22
            "\x83\xC4\x10"                    # 24
        )
        sizeOffset = 10
        offset = self.exe.code(code)
    if offset is False:
        self.log("Error: lua_newstate not found")
        exit(1)
    # need for quick convert raw to va
    codeSection = self.exe.codeSection
    self.lua_StateSize = self.exe.readUInt(offset + sizeOffset)
    self.addStruct("lua_State")
    self.setStructComment("Size {0}".format(hex(self.lua_StateSize)))
    self.addStructMember("padding_end", self.lua_StateSize - 1, 1)
    self.lua_newstate = offset - codeSection.rawVaDiff
    self.addVaFuncType("lua_newstate",
                       self.lua_newstate,
                       "lua_State *__cdecl lua_newstate(void *f, void *ud)")

    # 2. search first block in luaopen_package
    offset, section = self.exe.string("_LOADLIB")
    if offset is False:
        self.log("Error: string '_LOADLIB' not found")
        exit(1)
    loadLibStr = section.rawToVa(offset)
    offset, section = self.exe.string("package")
    if offset is False:
        self.log("Error: string 'package' not found")
        exit(1)
    packageStr = section.rawToVa(offset)
    # 0  push offset a_loadlib
    # 5  push esi
    # 6  call luaL_newmetatable
    # 11 push 0
    # 13 push offset lua_gctm
    # 18 push esi
    # 19 call lua_pushcclosure
    # 24 push offset a__gc
    # 29 push 0FFFFFFFEh
    # 31 push esi
    # 32 call lua_setfield
    # 37 push offset pk_funcs
    # 42 push offset aPackage
    # 47 push esi
    # 48 call luaL_register
    # 53 push 0FFFFFFFFh
    # 55 push esi
    # 56 call lua_pushvalue
    # 61 push 0FFFFD8EFh
    # 66 push esi
    # 67 call lua_replace
    # 72 push 0
    # 74 push 4
    # 76 push esi
    # 77 call lua_createtable
    # 82 mov eax, lua_loaders
    code = (
        "\x68" + self.exe.toHex(loadLibStr, 4) +  # 0
        "\x56"                            # 5
        "\xE8\xAB\xAB\xAB\xAB"            # 6
        "\x6A\x00"                        # 11
        "\x68\xAB\xAB\xAB\xAB"            # 13
        "\x56"                            # 18
        "\xE8\xAB\xAB\xAB\xAB"            # 19
        "\x68\xAB\xAB\xAB\xAB"            # 24
        "\x6A\xFE"                        # 29
        "\x56"                            # 31
        "\xE8\xAB\xAB\xAB\xAB"            # 32
        "\x68\xAB\xAB\xAB\xAB"            # 37
        "\x68" + self.exe.toHex(packageStr, 4) +  # 42
        "\x56"                            # 47
        "\xE8\xAB\xAB\xAB\xAB"            # 48
        "\x6A\xFF"                        # 53
        "\x56"                            # 55
        "\xE8\xAB\xAB\xAB\xAB"            # 56
        "\x68\xAB\xAB\xAB\xAB"            # 61
        "\x56"                            # 66
        "\xE8\xAB\xAB\xAB\xAB"            # 67
        "\x6A\x00"                        # 72
        "\x6A\x04"                        # 74
        "\x56"                            # 76
        "\xE8\xAB\xAB\xAB\xAB"            # 77
        "\xA1\xAB\xAB\xAB\xAB"            # 82
    )
    luaL_newmetatableOffset = 7
    lua_gctmOffset = 14
    lua_pushcclosureOffset = 20
    lua_setfieldOffset = 33
    pk_funcsOffset = 38
    luaL_registerOffset = 49
    lua_pushvalueOffset = 57
    lua_replaceOffset = 68
    lua_createtableOffset = 78
    lua_loadersOffset = 83
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        self.log("Error: first block in luaopen_package not found")
        exit(1)
    luaopen_packageOffset = offset
    self.luaL_newmetatable = self.getAddr(offset,
                                          luaL_newmetatableOffset,
                                          luaL_newmetatableOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_gctm = self.exe.readUInt(offset + lua_gctmOffset)
    self.lua_pushcclosure = self.getAddr(offset,
                                         lua_pushcclosureOffset,
                                         lua_pushcclosureOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_setfield = self.getAddr(offset,
                                     lua_setfieldOffset,
                                     lua_setfieldOffset + 4) - \
        codeSection.rawVaDiff
    pk_funcs = self.exe.readUInt(offset + pk_funcsOffset)
    self.luaL_register = self.getAddr(offset,
                                      luaL_registerOffset,
                                      luaL_registerOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_pushvalue = self.getAddr(offset,
                                      lua_pushvalueOffset,
                                      lua_pushvalueOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_replace = self.getAddr(offset,
                                    lua_replaceOffset,
                                    lua_replaceOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_createtable = self.getAddr(offset,
                                        lua_createtableOffset,
                                        lua_createtableOffset + 4) - \
        codeSection.rawVaDiff
    loaders = self.exe.readUInt(offset + lua_loadersOffset)
    self.addVaFuncType("luaL_newmetatable",
                       self.luaL_newmetatable,
                       "int __cdecl luaL_newmetatable "
                       "(lua_State *L, const char *tname)")
    self.addVaFuncType("lua_gctm",
                       self.lua_gctm,
                       "int __cdecl lua_gctm(lua_State *L)")
    self.addVaFuncType("lua_pushcclosure",
                       self.lua_pushcclosure,
                       "void  __cdecl lua_pushcclosure"
                       "(lua_State *L, void *fn, int n)")
    self.addVaFuncType("lua_setfield",
                       self.lua_setfield,
                       "void __cdecl lua_setfield"
                       "(lua_State *L, int idx, const char *k)")
    self.addVaFuncType("luaL_register",
                       self.luaL_register,
                       "void __cdecl luaL_register"
                       "(lua_State *L, const char *libname, const void *l)")
    self.addVaFuncType("lua_pushvalue",
                       self.lua_pushvalue,
                       "void __cdecl lua_pushvalue(lua_State *L, int idx)")
    self.addVaFuncType("lua_replace",
                       self.lua_replace,
                       "void __cdecl lua_replace(lua_State *L, int idx)")
    self.addVaFuncType("lua_createtable",
                       self.lua_createtable,
                       "void __cdecl lua_createtable"
                       "(lua_State *L, int narr, int nrec)")
    self.luaTables["pk_funcs"] = pk_funcs
    self.luaTables["loaders"] = loaders

    # 3. search second block in luaopen_package
    offset, section = self.exe.string("config")
    if offset is False:
        self.log("Error: string 'config' not found")
        exit(1)
    configStr = section.rawToVa(offset)
    offset, section = self.exe.string("_LOADED")
    if offset is False:
        self.log("Error: string '_LOADED' not found")
        exit(1)
    loadedStr = section.rawToVa(offset)

    # 0  call lua_setprogdir
    # 5  push offset aCpath
    # 10 push 0FFFFFFFEh
    # 12 push esi
    # 13 call lua_setfield
    # 18 push 9
    # 20 push offset a?_0     ; "\\\n
    # 25 push esi
    # 26 call lua_pushlstring
    # 31 push offset aConfig
    # 36 push 0FFFFFFFEh
    # 38 push esi
    # 39 call lua_setfield
    # 44 push 2
    # 46 push offset a_loaded
    # 51 push 0FFFFD8F0h
    # 56 push esi
    # 57 call luaL_findtable
    # 62 push offset aLoaded
    # 67 push 0FFFFFFFEh
    # 69 push esi
    # 70 call lua_setfield
    # 75 add esp, 40h
    # 78 push 0
    # 80 push 0
    # 82 push esi
    # 83 call lua_createtable
    # 88 push offset aPreload
    # 93 push 0FFFFFFFEh
    # 95 push esi
    # 96 call lua_setfield
    # 101 push 0FFFFD8EEh
    # 106 push esi
    # 107 call lua_pushvalue
    # 112 push offset ll_funcs
    # 117 push 0
    # 119 push esi
    # 120 call luaL_register
    # 125 push 0FFFFFFFEh
    # 127 push esi
    # 128 call lua_settop
    # 133 add esp, 34h
    code = (
        "\xE8\xAB\xAB\xAB\xAB"            # 0
        "\x68\xAB\xAB\xAB\xAB"            # 5
        "\x6A\xFE"                        # 10
        "\x56"                            # 12
        "\xE8\xAB\xAB\xAB\xAB"            # 13
        "\x6A\x09"                        # 18
        "\x68\xAB\xAB\xAB\xAB"            # 20
        "\x56"                            # 25
        "\xE8\xAB\xAB\xAB\xAB"            # 26
        "\x68" + self.exe.toHex(configStr, 4) +  # 31
        "\x6A\xFE"                        # 36
        "\x56"                            # 38
        "\xE8\xAB\xAB\xAB\xAB"            # 39
        "\x6A\x02"                        # 44
        "\x68" + self.exe.toHex(loadedStr, 4) +  # 46
        "\x68\xAB\xAB\xFF\xFF"            # 51
        "\x56"                            # 56
        "\xE8\xAB\xAB\xAB\xAB"            # 57
        "\x68\xAB\xAB\xAB\xAB"            # 62
        "\x6A\xFE"                        # 67
        "\x56"                            # 69
        "\xE8\xAB\xAB\xAB\xAB"            # 70
        "\x83\xC4\xAB"                    # 75
        "\x6A\x00"                        # 78
        "\x6A\x00"                        # 80
        "\x56"                            # 82
        "\xE8\xAB\xAB\xAB\xAB"            # 83
        "\x68\xAB\xAB\xAB\xAB"            # 88
        "\x6A\xFE"                        # 93
        "\x56"                            # 95
        "\xE8\xAB\xAB\xAB\xAB"            # 96
        "\x68\xAB\xAB\xFF\xFF"            # 101
        "\x56"                            # 106
        "\xE8\xAB\xAB\xAB\xAB"            # 107
        "\x68\xAB\xAB\xAB\xAB"            # 112
        "\x6A\x00"                        # 117
        "\x56"                            # 119
        "\xE8\xAB\xAB\xAB\xAB"            # 120
        "\x6A\xFE"                        # 125
        "\x56"                            # 127
        "\xE8\xAB\xAB\xAB\xAB"            # 128
        "\x83\xC4\x34"                    # 133
    )
    lua_setprogdirOffset = 1
    lua_pushlstringOffset = 27
    luaL_findtableOffset = 58
    lua_createtableOffset = 84
    lua_pushvalueOffset = 108
    ll_funcsOffset = 113
    luaL_registerOffset = 121
    lua_settopOffset = 129
    lua_setfieldOffsets = (14, 40, 71, 97)
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   luaopen_packageOffset,
                                   luaopen_packageOffset + 0x200)
    if offset is False:
        self.log("Error: second block in luaopen_package not found")
        exit(1)
    for lua_setfieldOffset in lua_setfieldOffsets:
        lua_setfield = self.getAddr(offset,
                                    lua_setfieldOffset,
                                    lua_setfieldOffset + 4) - \
            codeSection.rawVaDiff
        if self.lua_setfield != lua_setfield:
            self.log("Error: found different lua_setfield")
            exit(1)
    self.lua_setprogdir = self.getAddr(offset,
                                       lua_setprogdirOffset,
                                       lua_setprogdirOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_pushlstring = self.getAddr(offset,
                                        lua_pushlstringOffset,
                                        lua_pushlstringOffset + 4) - \
        codeSection.rawVaDiff
    self.luaL_findtable = self.getAddr(offset,
                                       luaL_findtableOffset,
                                       luaL_findtableOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_createtable = self.getAddr(offset,
                                        lua_createtableOffset,
                                        lua_createtableOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_pushvalue = self.getAddr(offset,
                                      lua_pushvalueOffset,
                                      lua_pushvalueOffset + 4) - \
        codeSection.rawVaDiff
    luaL_register = self.getAddr(offset,
                                 luaL_registerOffset,
                                 luaL_registerOffset + 4) - \
        codeSection.rawVaDiff
    if luaL_register != self.luaL_register:
        self.log("Error: found different luaL_register")
        exit(1)
    self.lua_settop = self.getAddr(offset,
                                   lua_settopOffset,
                                   lua_settopOffset + 4) - \
        codeSection.rawVaDiff
    ll_funcs = self.exe.readUInt(offset + ll_funcsOffset)
    self.luaTables["ll_funcs"] = ll_funcs
    self.addVaFuncType("lua_setprogdir",
                       self.lua_setprogdir,
                       "void __cdecl lua_setprogdir(lua_State *L)")
    self.addVaFuncType("lua_pushlstring",
                       self.lua_pushlstring,
                       "void __cdecl lua_pushlstring"
                       "(lua_State *L, const char *s, size_t l)")
    self.addVaFuncType("luaL_findtable",
                       self.luaL_findtable,
                       "const char *__cdecl luaL_findtable(lua_State *L, "
                       "int idx, const char *fname, int szhint)")
    self.addVaFuncType("lua_createtable",
                       self.lua_createtable,
                       "void __cdecl lua_createtable"
                       "(lua_State *L, int narr, int nrec)")
    self.addVaFuncType("lua_pushvalue",
                       self.lua_pushvalue,
                       "void __cdecl lua_pushvalue(lua_State *L, int idx)")
    self.addVaFuncType("lua_settop",
                       self.lua_settop,
                       "void __cdecl lua_settop(lua_State *L, int idx)")

    # 4. search first part in ll_require
    # 0  push 0
    # 2  push 1
    # 4  push esi
    # 5  call luaL_checklstring
    # 10 push 1
    # 12 push esi
    # 13 mov edi, eax
    # 15 call lua_settop
    # 20 push offset fname
    # 25 push 0FFFFD8F0h
    # 30 push esi
    # 31 call lua_getfield
    # 36 push edi
    # 37 push 2
    # 39 push esi
    # 40 call lua_getfield
    # 45 push 0FFFFFFFFh
    # 47 push esi
    # 48 call lua_toboolean
    # 53 add esp, 34h
    # 56 test eax, eax
    # 58 jz short loc_40FA12
    # 60 push 0FFFFFFFFh
    # 62 push esi
    # 63 call lua_touserdata
    # 68 add esp, 8
    # 71 cmp eax, offset sentinel
    # 76 jnz loc_40FB4B
    # 82 push edi
    # 83 push offset aLoopOrPrevious
    # 88 push esi
    # 89 call luaL_error
    # 94 add esp, 0Ch
    # 97 mov eax, 1
    # 102 pop edi
    # 103 pop esi
    # 104 pop ebp
    # 105 ret retn
    # 106 push offset aLoaders
    # 111 push 0FFFFD8EFh
    # 116 push esi
    # 117 call lua_getfield
    # 122 push 0FFFFFFFFh
    # 124 push esi
    # 125 call lua_type
    # 130 add esp, 14h
    code = (
        "\x6A\x00"                        # 0
        "\x6A\x01"                        # 2
        "\x56"                            # 4
        "\xE8\xAB\xAB\xAB\xAB"            # 5
        "\x6A\x01"                        # 10
        "\x56"                            # 12
        "\x8B\xF8"                        # 13
        "\xE8\xAB\xAB\xAB\xAB"            # 15
        "\x68" + self.exe.toHex(loadedStr, 4) +  # 20
        "\x68\xAB\xAB\xFF\xFF"            # 25
        "\x56"                            # 30
        "\xE8\xAB\xAB\xAB\xAB"            # 31
        "\x57"                            # 36
        "\x6A\x02"                        # 37
        "\x56"                            # 39
        "\xE8\xAB\xAB\xAB\xAB"            # 40
        "\x6A\xFF"                        # 45
        "\x56"                            # 47
        "\xE8\xAB\xAB\xAB\xAB"            # 48
        "\x83\xC4\x34"                    # 53
        "\x85\xC0"                        # 56
        "\x74\x2E"                        # 58
        "\x6A\xFF"                        # 60
        "\x56"                            # 62
        "\xE8\xAB\xAB\xAB\xAB"            # 63
        "\x83\xC4\x08"                    # 68
        "\x3D\xAB\xAB\xAB\xAB"            # 71
        "\x0F\x85\xAB\xAB\xAB\xAB"        # 76
        "\x57"                            # 82
        "\x68\xAB\xAB\xAB\xAB"            # 83
        "\x56"                            # 88
        "\xE8\xAB\xAB\xAB\xAB"            # 89
        "\x83\xC4\x0C"                    # 94
        "\xAB\xAB\xAB\x00\x00"            # 97
        "\xAB"                            # 102
        "\x5E"                            # 103
        "\x5D"                            # 104
        "\xC3"                            # 105
        "\x68\xAB\xAB\xAB\xAB"            # 106
        "\x68\xAB\xAB\xAB\xAB"            # 111
        "\x56"                            # 116
        "\xE8\xAB\xAB\xAB\xAB"            # 117
        "\x6A\xFF"                        # 122
        "\x56"                            # 124
        "\xE8\xAB\xAB\xAB\xAB"            # 125
        "\x83\xC4\x14"                    # 130
    )
    luaL_checklstringOffset = 6
    lua_settopOffset = 16
    lua_tobooleanOffset = 49
    lua_touserdataOffset = 64
    luaL_errorOffset = 90
    lua_typeOffset = 126
    lua_getfieldOffsets = (32, 41, 118)
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 0  push 0
        # 2  push 1
        # 4  push esi
        # 5  call sub_68AD70
        # 10 push 1
        # 12 push esi
        # 13 mov edi, eax
        # 15 call sub_688CF0
        # 20 push offset a_loaded
        # 25 push 0FFFFD8F0h
        # 30 push esi
        # 31 call lua_getfield
        # 36 push edi
        # 37 push 2
        # 39 push esi
        # 40 call lua_getfield
        # 45 push 0FFFFFFFFh
        # 47 push esi
        # 48 call sub_689140
        # 53 add esp, 34h
        # 56 test eax, eax
        # 58 jz short loc_69670F
        # 60 push 0FFFFFFFFh
        # 62 push esi
        # 63 call lua_touserdata
        # 68 add esp, 8
        # 71 cmp eax, offset sentinel
        # 76 jnz loc_696848
        # 82 push edi
        # 83 push offset aLoopOrPrevious
        # 88 push esi
        # 89 call luaL_error
        # 94 add esp, 0Ch
        # 97 pop edi
        # 98 mov eax, 1
        # 103 pop esi
        # 104 ret retn
        # 105 push offset aLoaders
        # 110 push 0FFFFD8EFh
        # 115 push esi
        # 116 call lua_getfield
        # 121 push 0FFFFFFFFh
        # 123 push esi
        # 124 call lua_type
        # 129 add esp, 14h
        code = (
            "\x6A\x00"                        # 0
            "\x6A\x01"                        # 2
            "\x56"                            # 4
            "\xE8\xAB\xAB\xAB\xAB"            # 5
            "\x6A\x01"                        # 10
            "\x56"                            # 12
            "\x8B\xF8"                        # 13
            "\xE8\xAB\xAB\xAB\xAB"            # 15
            "\x68" + self.exe.toHex(loadedStr, 4) +  # 20
            "\x68\xAB\xAB\xFF\xFF"            # 25
            "\x56"                            # 30
            "\xE8\xAB\xAB\xAB\xAB"            # 31
            "\x57"                            # 36
            "\x6A\x02"                        # 37
            "\x56"                            # 39
            "\xE8\xAB\xAB\xAB\xAB"            # 40
            "\x6A\xFF"                        # 45
            "\x56"                            # 47
            "\xE8\xAB\xAB\xAB\xAB"            # 48
            "\x83\xC4\x34"                    # 53
            "\x85\xC0"                        # 56
            "\x74\x2D"                        # 58
            "\x6A\xFF"                        # 60
            "\x56"                            # 62
            "\xE8\xAB\xAB\xAB\xAB"            # 63
            "\x83\xC4\x08"                    # 68
            "\x3D\xAB\xAB\xAB\xAB"            # 71
            "\x0F\x85\xAB\xAB\xAB\xAB"        # 76
            "\x57"                            # 82
            "\x68\xAB\xAB\xAB\xAB"            # 83
            "\x56"                            # 88
            "\xE8\xAB\xAB\xAB\xAB"            # 89
            "\x83\xC4\x0C"                    # 94
            "\x5F"                            # 97
            "\xB8\x01\x00\x00\x00"            # 98
            "\x5E"                            # 103
            "\xC3"                            # 104
            "\x68\xAB\xAB\xAB\xAB"            # 105
            "\x68\xAB\xAB\xFF\xFF"            # 110
            "\x56"                            # 115
            "\xE8\xAB\xAB\xAB\xAB"            # 116
            "\x6A\xFF"                        # 121
            "\x56"                            # 123
            "\xE8\xAB\xAB\xAB\xAB"            # 124
            "\x83\xC4\x14"                    # 129
        )
        luaL_checklstringOffset = 6
        lua_settopOffset = 16
        lua_tobooleanOffset = 49
        lua_touserdataOffset = 64
        luaL_errorOffset = 90
        lua_typeOffset = 125
        lua_getfieldOffsets = (32, 41, 117)
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        self.log("Error: first block in ll_require not found")
        exit(1)
    ll_requireOffset = offset
    self.luaL_checklstring = self.getAddr(offset,
                                          luaL_checklstringOffset,
                                          luaL_checklstringOffset + 4) - \
        codeSection.rawVaDiff
    lua_settop = self.getAddr(offset,
                              lua_settopOffset,
                              lua_settopOffset + 4) - \
        codeSection.rawVaDiff
    if self.lua_settop != lua_settop:
        self.log("Error: found different lua_settop")
        exit(1)
    self.lua_toboolean = self.getAddr(offset,
                                      lua_tobooleanOffset,
                                      lua_tobooleanOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_touserdata = self.getAddr(offset,
                                       lua_touserdataOffset,
                                       lua_touserdataOffset + 4) - \
        codeSection.rawVaDiff
    self.luaL_error = self.getAddr(offset,
                                   luaL_errorOffset,
                                   luaL_errorOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_type = self.getAddr(offset,
                                 lua_typeOffset,
                                 lua_typeOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_getfield = self.getAddr(offset,
                                     lua_getfieldOffsets[0],
                                     lua_getfieldOffsets[0] + 4) - \
        codeSection.rawVaDiff
    for lua_getfieldOffset in lua_getfieldOffsets:
        lua_getfield = self.getAddr(offset,
                                    lua_getfieldOffset,
                                    lua_getfieldOffset + 4) - \
            codeSection.rawVaDiff
        if self.lua_getfield != lua_getfield:
            self.log("Error: found different lua_getfield")
            exit(1)
    self.addVaFuncType("luaL_checklstring",
                       self.luaL_checklstring,
                       "const char *__cdecl luaL_checklstring"
                       "(lua_State *L, int numArg, "
                       "size_t *l)")
    self.addVaFuncType("lua_toboolean",
                       self.lua_toboolean,
                       "int __cdecl lua_toboolean(lua_State *L, int idx)")
    self.addVaFuncType("lua_touserdata",
                       self.lua_touserdata,
                       "void *__cdecl lua_touserdata(lua_State *L, int idx)")
    self.addVaFuncType("luaL_error",
                       self.luaL_error,
                       "int __cdecl luaL_error"
                       "(lua_State *L, const char *fmt, ...)")
    self.addVaFuncType("lua_type",
                       self.lua_type,
                       "int __cdecl lua_type(lua_State *L, int idx)")
    self.addVaFuncType("lua_getfield",
                       self.lua_getfield,
                       "void __cdecl lua_getfield"
                       "(lua_State *L, int idx, const char *k)")

    # 5. search second part in ll_require
    # 0  call lua_pushlightuserdata
    # 5  push edi
    # 6  push 2
    # 8  push esi
    # 9  call lua_setfield
    # 14 push edi
    # 15 push esi
    # 16 call lua_pushstring
    # 21 push 1
    # 23 push 1
    # 25 push esi
    # 26 call lua_call
    # 31 push 0FFFFFFFFh
    # 33 push esi
    # 34 call lua_type
    # 39 add esp, 30h
    # 42 pop ebx
    # 43 test eax, eax
    # 45 jz short loc_40FB14
    # 47 push edi
    # 48 push 2
    # 50 push esi
    # 51 call lua_setfield
    # 56 add esp, 0Ch
    # 59 push edi
    # 60 push 2
    # 62 push esi
    # 63 call lua_getfield
    # 68 push 0FFFFFFFFh
    # 70 push esi
    # 71 call lua_touserdata
    # 76 add esp, 14h
    # 79 cmp eax, offset sentinel
    # 84 jnz short loc_40FB4B
    # 86 push 1
    # 88 push esi
    # 89 call lua_pushboolean
    code = (
        "\xE8\xAB\xAB\xAB\xAB"            # 0
        "\x57"                            # 5
        "\x6A\x02"                        # 6
        "\x56"                            # 8
        "\xE8\xAB\xAB\xAB\xAB"            # 9
        "\x57"                            # 14
        "\x56"                            # 15
        "\xE8\xAB\xAB\xAB\xAB"            # 16
        "\x6A\x01"                        # 21
        "\x6A\x01"                        # 23
        "\x56"                            # 25
        "\xE8\xAB\xAB\xAB\xAB"            # 26
        "\x6A\xFF"                        # 31
        "\x56"                            # 33
        "\xE8\xAB\xAB\xAB\xAB"            # 34
        "\x83\xC4\x30"                    # 39
        "\x5B"                            # 42
        "\x85\xC0"                        # 43
        "\x74\xAB"                        # 45
        "\x57"                            # 47
        "\x6A\x02"                        # 48
        "\x56"                            # 50
        "\xE8\xAB\xAB\xAB\xAB"            # 51
        "\x83\xC4\x0C"                    # 56
        "\x57"                            # 59
        "\x6A\x02"                        # 60
        "\x56"                            # 62
        "\xE8\xAB\xAB\xAB\xAB"            # 63
        "\x6A\xFF"                        # 68
        "\x56"                            # 70
        "\xE8\xAB\xAB\xAB\xAB"            # 71
        "\x83\xC4\x14"                    # 76
        "\x3D\xAB\xAB\xAB\xAB"            # 79
        "\x75\xAB"                        # 84
        "\x6A\x01"                        # 86
        "\x56"                            # 88
        "\xE8\xAB\xAB\xAB\xAB"            # 89
    )
    lua_pushlightuserdataOffset = 1
    lua_setfieldOffsets = (10, 52, )
    lua_pushstringOffset = 17
    lua_callOffset = 27
    lua_typeOffset = 35
    lua_getfieldOffset = 64
    lua_touserdataOffset = 72
    lua_pushbooleanOffset = 90
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   ll_requireOffset + 0x70,
                                   ll_requireOffset + 0x300)
    if offset is False:
        self.log("Error: second block in ll_require not found")
        exit(1)
    self.lua_pushlightuserdata = \
        self.getAddr(offset,
                     lua_pushlightuserdataOffset,
                     lua_pushlightuserdataOffset + 4) - codeSection.rawVaDiff
    for lua_setfieldOffset in lua_setfieldOffsets:
        lua_setfield = self.getAddr(offset,
                                    lua_setfieldOffset,
                                    lua_setfieldOffset + 4) - \
            codeSection.rawVaDiff
        if self.lua_setfield != lua_setfield:
            self.log("Error: found different lua_setfield")
            exit(1)
    self.lua_pushstring = self.getAddr(offset,
                                       lua_pushstringOffset,
                                       lua_pushstringOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_call = self.getAddr(offset,
                                 lua_callOffset,
                                 lua_callOffset + 4) - \
        codeSection.rawVaDiff
    lua_type = self.getAddr(offset,
                            lua_typeOffset,
                            lua_typeOffset + 4) - \
        codeSection.rawVaDiff
    if self.lua_type != lua_type:
        self.log("Error: found different lua_type")
        exit(1)
    lua_getfield = self.getAddr(offset,
                                lua_getfieldOffset,
                                lua_getfieldOffset + 4) - \
        codeSection.rawVaDiff
    if self.lua_getfield != lua_getfield:
        self.log("Error: found different lua_getfield")
        exit(1)
    self.lua_touserdata = self.getAddr(offset,
                                       lua_touserdataOffset,
                                       lua_touserdataOffset + 4) - \
        codeSection.rawVaDiff
    self.lua_pushboolean = self.getAddr(offset,
                                        lua_pushbooleanOffset,
                                        lua_pushbooleanOffset + 4) - \
        codeSection.rawVaDiff
    self.addVaFuncType("lua_pushlightuserdata",
                       self.lua_pushlightuserdata,
                       "void __cdecl lua_pushlightuserdata"
                       "(lua_State *L, void *p)")
    self.addVaFuncType("lua_pushstring",
                       self.lua_pushstring,
                       "void __cdecl lua_pushstring"
                       "(lua_State *L, const char *s)")
    self.addVaFuncType("lua_call",
                       self.lua_call,
                       "void __cdecl lua_call"
                       "(lua_State *L, int nargs, int nresults)")
    self.addVaFuncType("lua_touserdata",
                       self.lua_touserdata,
                       "void *__cdecl lua_touserdata(lua_State *L, int idx)")
    self.addVaFuncType("lua_pushboolean",
                       self.lua_pushboolean,
                       "void __cdecl lua_pushboolean(lua_State *L, int b)")
