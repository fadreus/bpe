#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchItemInfo(self):
    if self.ITEM_INFO_init == 0:
        self.log("Error: skip ITEM_INFO search because "
                 "ITEM_INFO_init not found")
        if self.packetVersion < "20160000" or self.clientType in ("ruro",):
            return
        exit(1)

    # search in ITEM_INFO_init
    # 0  ret retn
    # 1  align 10h
    code = (
        "\xC3"                            # 0
        "\xCC\xCC"                        # 1
    )
    offset = self.exe.code(code,
                           self.ITEM_INFO_init,
                           self.ITEM_INFO_init + 0x150)
    if offset is False:
        # 0  ret retn
        # 1  align 10h
        code = (
            "\xC3\xCC"                        # 0
        )
        offset = self.exe.code(code,
                               self.ITEM_INFO_init,
                               self.ITEM_INFO_init + 0x150)
    if offset is False:
        # 0  ret retn
        code = (
            "\xC3"                            # 0
        )
        offset = self.exe.code(code,
                               self.ITEM_INFO_init,
                               self.ITEM_INFO_init + 0x150)
    if offset is False:
        self.log("failed in search ITEM_INFO_init end")
        exit(1)
    endOffset = offset

    # 0  mov [esi+ITEM_INFO.item_options_count], 0
    # 10 mov ecx, 19h
    # 15 lea eax, [esi+ITEM_INFO.item_options]
    # 21 mov byte ptr [eax], 0
    # 24 lea eax, [eax+1]
    # 27 dec ecx
    # 28 jnz short loc_7FA047
    # 30 mov [esi+ITEM_INFO.field_DD], cl
    # 36 pop esi
    # 37 ret retn
    code = (
        "\xC7\x86\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 0
        "\xB9\xAB\xAB\x00\x00"            # 10
        "\x8D\x86\xAB\xAB\x00\x00"        # 15
        "\xC6\x00\x00"                    # 21
        "\x8D\x40\x01"                    # 24
        "\x49"                            # 27
        "\x75\xF7"                        # 28
        "\x88\x8E\xAB\xAB\x00\x00"        # 30
        "\x5E"                            # 36
        "\xC3"                            # 37
    )
    itemOptionsCountOffset = (2, 4)
    itemOptionsSizeOffset = (11, 4)
    itemOptionsOffset = (17, 4)
    lastFieldOffset = (32, 4)
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.ITEM_INFO_init,
                                   endOffset)

    if offset is False:
        # 0  mov [esi+ITEM_INFO.item_options_count], 0
        # 10 lea eax, [esi+ITEM_INFO.item_options]
        # 16 mov ecx, 19h
        # 21 pop esi
        # 22 mov byte ptr [eax], 0
        # 25 lea eax, [eax+1]
        # 28 dec ecx
        # 29 jnz short loc_77C3C5
        # 31 ret retn
        code = (
            "\xC7\x86\xAB\xAB\x00\x00\x00\x00\x00\x00"  # 0
            "\x8D\x86\xAB\xAB\x00\x00"        # 10
            "\xB9\xAB\xAB\x00\x00"            # 16
            "\x5E"                            # 21
            "\xC6\x00\x00"                    # 22
            "\x8D\x40\x01"                    # 25
            "\x49"                            # 28
            "\x75\xF7"                        # 29
            "\xC3"                            # 31
        )
        itemOptionsCountOffset = (2, 4)
        itemOptionsSizeOffset = (17, 4)
        itemOptionsOffset = (12, 4)
        lastFieldOffset = 0
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)
    if offset is False:
        # 0  mov [esi+ITEM_INFO.item_options_count], 0
        # 7  lea eax, [esi+ITEM_INFO.item_options]
        # 10 mov ecx, 19h
        # 15 pop esi
        # 16 mov byte ptr [eax], 0
        # 19 lea eax, [eax+1]
        # 22 dec ecx
        # 23 jnz short loc_6B8194
        # 25 ret retn
        code = (
            "\xC7\x46\xAB\x00\x00\x00\x00"    # 0
            "\x8D\x46\xAB"                    # 7
            "\xB9\xAB\xAB\x00\x00"            # 10
            "\x5E"                            # 15
            "\xC6\x00\x00"                    # 16
            "\x8D\x40\x01"                    # 19
            "\x49"                            # 22
            "\x75\xF7"                        # 23
            "\xC3"                            # 25
        )
        itemOptionsCountOffset = (2, 1)
        itemOptionsSizeOffset = (11, 4)
        itemOptionsOffset = (9, 1)
        lastFieldOffset = 0
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.ITEM_INFO_init,
                                       endOffset)

    if offset is False:
        self.log("Error: failed in search ITEM_INFO_init last offset")
        exit(1)

    itemOptionsCount = self.getVarAddr(offset, itemOptionsCountOffset)
    itemOptionsSize = self.getVarAddr(offset, itemOptionsSizeOffset)
    itemOptions = self.getVarAddr(offset, itemOptionsOffset)
    if lastFieldOffset != 0:
        lastField = self.getVarAddr(offset, lastFieldOffset)
        if itemOptions + itemOptionsSize != lastField:
            self.log("Error: found wrong end field in ITEM_INFO")
            exit(1)
        haveIndex = lastField
    else:
        lastField = itemOptions + itemOptionsSize
        haveIndex = -1

    if itemOptionsCount + 4 != itemOptions:
        self.log("Error: wrong item options fields found in ITEM_INFO")
        exit(1)
    if itemOptionsSize != 25:
        self.log("Error: found unknown item options fields size in ITEM_INFO")
        exit(1)

    itemOptionsMaxCount = itemOptionsSize / 5

    # 0  push eax
    # 1  push offset g_regPath
    # 6  lea ecx, [esi+ITEM_INFO.m_itemname_IdStr.m_cstr]
    # 9  mov word ptr [esi+ITEM_INFO.identified], 0
    # 15 mov [esi+ITEM_INFO.refine_level], 0
    # 22 mov [esi+ITEM_INFO.bind_on_equip], ax
    # 26 call std_string_assign
    # 31 push 0
    # 33 push offset g_regPath
    # 38 lea ecx, [esi+ITEM_INFO.str_field_48.m_cstr]
    # 41 call std_string_assign
    code = (
        "\x50"                            # 0
        "\x68\xAB\xAB\xAB\xAB"            # 1
        "\x8D\x4E\xAB"                    # 6
        "\x66\xC7\x46\xAB\x00\x00"        # 9   unused
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 15  unused
        "\x66\x89\x46\xAB"                # 22  unused
        "\xE8\xAB\xAB\xAB\xAB"            # 26
        "\x6A\x00"                        # 31
        "\x68\xAB\xAB\xAB\xAB"            # 33
        "\x8D\x4E\xAB"                    # 38
        "\xE8\xAB\xAB\xAB\xAB"            # 41
    )
    itemname_IdStrOffset = (8, 1)
    unknown_strOffset = (40, 1)
    fromChars1Offset = 27
    fromChars2Offset = 42
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.ITEM_INFO_init,
                                   endOffset)

    if offset is False:
        self.log("Error: failed in search std_string_assign "
                 "in ITEM_INFO_init")
        if self.packetVersion > "20170000":
            exit(1)
        foundChars = False
    else:
        foundChars = True

    if foundChars is True:
        itemname_IdStr = self.getVarAddr(offset, itemname_IdStrOffset)
        unknown_str = self.getVarAddr(offset, unknown_strOffset)
        fromChars1 = self.getAddr(offset,
                                  fromChars1Offset,
                                  fromChars1Offset + 4)
        fromChars2 = self.getAddr(offset,
                                  fromChars2Offset,
                                  fromChars2Offset + 4)
        if fromChars1 != fromChars2:
            self.log("Error: found different std_string_assign")
            exit(1)
        self.std_string_assign = fromChars1
        self.addRawFuncType("std::string::assign",
                            self.std_string_assign,
                            "int __thiscall std_string_assign("
                            "struct_std_string *std_string_ptr,"
                            " char *inStr, int len)")


    # 0  push esi
    # 1  mov esi, ecx
    # 3  xor eax, eax
    # 5  xorps xmm0, xmm0
    # 8  mov dword ptr [esi+ITEM_INFO.item_type], 0
    # 14 mov [esi+ITEM_INFO.location], 0
    # 21 mov [esi+ITEM_INFO.item_index], 0
    # 28 mov [esi+ITEM_INFO.wear_location], 0
    # 35 mov [esi+ITEM_INFO.amount], 0
    # 42 mov [esi+ITEM_INFO.price], 0
    # 49 mov [esi+ITEM_INFO.real_price], 0
    # 56 movq qword ptr [esi+ITEM_INFO.card0], xmm0
    # 61 movq qword ptr [esi+ITEM_INFO.card2], xmm0
    code = (
        "\x56"                            # 0
        "\x8B\xF1"                        # 1
        "\x33\xC0"                        # 3
        "\x0F\x57\xC0"                    # 5
        "\xC7\x06\x00\x00\x00\x00"        # 8
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 14
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 21
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 28
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 35
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 42
        "\xC7\x46\xAB\x00\x00\x00\x00"    # 49
        "\x66\x0F\xD6\x46\xAB"            # 56
        "\x66\x0F\xD6\x46"                # 61
    )
    item_typeValue = 0
    locationOffset = (16, 1)
    item_indexOffset = (23, 1)
    wear_locationOffset = (30, 1)
    amountOffset = (37, 1)
    priceOffset = (44, 1)
    real_priceOffset = (51, 1)
    card1Offset = (60, 1)
    card2Offset = (65, 1)

    offset = self.exe.matchWildcardOffset(code,
                                          "\xAB",
                                          self.ITEM_INFO_init)

    if offset is not True:
        self.log("Error: failed in search cards in ITEM_INFO_init")
        exit(1)
    offset = self.ITEM_INFO_init

    if item_typeValue >= 0:
        item_type = item_typeValue
    else:
        self.log("Error: item_type not found")
        exit(1)

    location = self.getVarAddr(offset, locationOffset)
    item_index = self.getVarAddr(offset, item_indexOffset)
    wear_location = self.getVarAddr(offset, wear_locationOffset)
    amount = self.getVarAddr(offset, amountOffset)
    price = self.getVarAddr(offset, priceOffset)
    real_price = self.getVarAddr(offset, real_priceOffset)
    card1 = self.getVarAddr(offset, card1Offset)
    card2 = self.getVarAddr(offset, card2Offset)
    if location >= item_index:
        self.log("Error: found wrong location in ITEM_INFO_init")
        exit(1)
    if item_index >= wear_location:
        self.log("Error: found wrong item_index in ITEM_INFO_init")
        exit(1)
    if wear_location >= amount:
        self.log("Error: found wrong wear_location in ITEM_INFO_init")
        exit(1)
    if amount >= price:
        self.log("Error: found wrong amount in ITEM_INFO_init")
        exit(1)
    if price >= real_price:
        self.log("Error: found wrong price in ITEM_INFO_init")
        exit(1)
    if real_price >= card1:
        self.log("Error: found wrong real_price in ITEM_INFO_init")
        exit(1)
    if card1 >= card2:
        self.log("Error: found wrong cards in ITEM_INFO_init")
        exit(1)

    self.addStruct("struct_item_option")
    self.addStructMember("index", 0, 2)
    self.addStructMember("value", 2, 2)
    self.addStructMember("param", 4, 1)

    mod = lastField % 4
    if mod != 1:
        self.log("Possible error in ITEM_INFO size search")
        exit(1)
    self.ITEM_INFO_Size = lastField + 4 - mod
    self.showVaAddr("len(ITEM_INFO)", self.ITEM_INFO_Size)
    self.addStruct("ITEM_INFO")
    self.setStructComment("Size {0}".format(hex(self.ITEM_INFO_Size)))
    self.addStructMember("item_type", item_type, 4, True)
    self.addStructMember("location", location, 4, True)
    self.addStructMember("item_index", item_index, 4, True)
    self.addStructMember("wear_location", wear_location, 4, True)
    self.addStructMember("amount", amount, 4, True)
    self.addStructMember("price", price, 4, True)
    self.addStructMember("real_price", real_price, 4, True)
    self.addStructMember("cards", card1, 16, True)
    self.setStructMemberType(card1, "int[4]")
    if foundChars is True:
        self.addStructMember("item_id_str",
                             itemname_IdStr,
                             self.stdstringSize,
                             True)
        self.setStructMemberType(itemname_IdStr, "struct_std_string")
        self.addStructMember("item_name_str",
                             unknown_str,
                             self.stdstringSize,
                             True)
        self.setStructMemberType(unknown_str, "struct_std_string")
    self.addStructMember("item_options_count", itemOptionsCount, 4, True)
    self.addStructMember("item_options", itemOptions, itemOptionsSize, True)
    self.setStructMemberType(itemOptions,
                             "struct_item_option[{0}]".
                             format(itemOptionsMaxCount))
    if haveIndex != -1:
        self.addStructMember("have_index", haveIndex, 1, True)
    self.addStructMember("padding_end", self.ITEM_INFO_Size - 1, 1)
