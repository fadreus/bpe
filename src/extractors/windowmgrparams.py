#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.params import searchParams2


def searchWindowMgrParams(self, errorExit):
    vals = (
        ("m_minimapZoomWnd", "m_miniMapZoomFactor", 4),
        ("M_ISDRAGALL", "m_isDragAll", 4),
        ("M_ISDRAWCOMPASS", "m_isDrawCompass", 4),
        ("M_MINIMAPARGB", "m_miniMapArgb", 4),
        ("M_CHATWNDSTATUS", "m_chatWndStatus", 4),
        ("M_CHATWNDHEIGHT", "m_chatWndHeight", 4),
        ("M_CHATWNDX", "m_chatWndX", 4),
        ("M_CHATWNDY", "m_chatWndY", 4),
        ("M_CHATWNDSHOW", "m_chatWndShow", 4),
        ("CHATWNDSTICKON", "m_chatWndStickOn", 1),
        ("ONSTSUBCHAT", "m_onStSubChat", 1),
        ("ONBTSUBCHAT", "m_onBtSubChat", 1),
        ("ITEMSTOREWNDNUM", "m_ITEMSTOREWNDNUM", 4),
    )
    self.addStruct("UIWindowMgr")
    from src.blocks.windowmgrparams import blocks
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           True,
                           4)
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            self.addStructMember(val[1], arr[val0], val[2], True)

    props = (
        ("X", "x"),
        ("Y", "y"),
        ("W", "w"),
        ("H", "h"),
    )
    vals = (
        ("SAYDIALOGWNDINFO", "sayDialogWndInfo"),
        ("CHOOSEWNDINFO", "chooseWndInfo"),
        ("BASICINFOWNDINFO", "basicInfoWndInfo"),
        ("ITEMWNDINFO", "itemWndInfo"),
        ("QUESTWNDINFO", "questWndInfo"),
        ("STATUSWNDINFO", "statusWndInfo"),
        ("EQUIPWNDINFO", "equipWndInfo"),
        ("OPTIONWNDINFO", "optionWndInfo"),
        ("SHORTENITEMWNDINFO", "shortenItemWndInfo"),
        ("SHORTCUTWNDINFO", "shortCutWndInfo"),
        ("CHATROOMWNDINFO", "chatRoomWndInfo"),
        ("ITEMSTOREFINDWNDINFO", "itemStoreFindWndInfo"),
        ("ITEMSHOPWNDINFO", "itemShopWndInfo"),
        ("ITEMSELLWNDINFO", "itemSellWndInfo"),
        ("ITEMPURCHASEWNDINFO", "itemPurchaseWndInfo"),
        ("MESSENGERGROUPWNDINFO", "messengerGroupWndInfo"),
        ("EXCHANGEACCEPTWNDINFO", "exchangeAcceptWndInfo"),
        ("SKILLLISTWNDINFO", "skillListWndInfo"),
        ("MERCHANTITEMWNDINFO", "merchantItemWndInfo"),
        ("MERCHANTMIRRORITEMWNDINFO", "merchantMirrorItemWndInfo"),
        ("MERCHANTSHOPMAKEWNDINFO", "merchantShopMakeWndInfo"),
        ("MERCHANTITEMSHOPWNDINFO", "merchantItemShopWndInfo"),
        ("MERCHANTITEMMYSHOPWNDINFO", "merchantItemMyShopWndInfo"),
        ("MERCHANTITEMPURCHASEWNDINFO", "merchantItemPurchaseWndInfo"),
        ("ITEMCOLLECTIONWNDINFO", "itemCollectionWndInfo"),
        ("COMBINEDCARDITEMCOLLECTIONWNDINFO",
         "combinedCardItemCollectionWndInfo"),
        ("ITEMPARAMCHANGEDISPLAYWNDINFO", "itemParamChangeDisplayWndInfo"),
        ("PARTYSETTINGWNDINFO", "partySettingWndInfo"),
        ("DETAILLEVELWNDINFO", "detailLevelWndInfo"),
        ("WHISPERWNDINFO", "whisperWndInfo"),
        ("FRIENDOPTIONWNDINFO", "friendOptionWndInfo"),
        ("ITEMCOMPOSITIONWNDINFO", "itemCompositionWndInfo"),
        ("ITEMIDENTIFYWNDINFO", "itemIdentifyWndInfo"),
        ("SUBCHATWNDST", "subChatWndStInfo"),
        ("SUBCHATWNDBT", "subChatWndBtInfo"),
        ("BOOKINGLISTWNDINFO", "bookingListWndInfo"),
        ("BOOKINGMBWNDINFO", "bookingMbWndInfo"),
        ("BOOKINGWNDINFO", "bookingWndInfo"),
        ("BOOKINGMBCHECKWNDINFO", "bookingMbCheckWndInfo"),
        ("QUICKSLOTWNDINFO", "quickSlotWndInfo"),
        ("BATTLEFIELDWNDINFO", "battleFieldWndInfo"),
        ("GUILDHELPERWNDINFO", "guildHelperWndInfo"),
        ("BUYINGSTOREMAKEWNDINFO", "buyingStoreMakeWndInfo"),
        ("BUYINGSTOREMIRRORITEMWNDINFO", "buyingStoreMirrorItemWndInfo"),
        ("BUYINGSTOREITEMMYSHOPWNDINFO", "buyingStoreItemMyShopWndInfo"),
        ("BUYINGSTOREITEMSHOPWNDINFO", "buyingStoreItemShopWndInfo"),
        ("BUYINGSTOREITEMSELLWNDINFO", "buyingStoreItemSellWndInfo"),
        ("BUYINGSTOREMIRRORSELLLISTWNDINFO",
         "buyingStoreMirrorSellListWndInfo"),
        ("REPLAYRECCONTROLNWNDINFO", "replayRecControlnWndInfo"),
        ("NAVIGATIONWNDINFO", "navigationWndInfo"),
        ("MINIMAPWNDINFO", "minimapWndInfo"),
        ("ACHTRACINGWNDINFO", "achTracingWndInfo"),
        ("PREVIEWEQUIPWNDINFO", "previewEquipWndInfo"),
        ("SWAPEQUIPMENTWNDINFO", "swapEquipmentWndInfo"),
        ("ITEMSTOREWNDINFO", "itemStoreWndInfo"),
    )
    vals2 = [
        ("ITEMWNDINFO.ORGHEIGHT", "itemWndInfo.orgHeight"),
        ("ITEMWNDINFO.CURTAB", "itemWndInfo.curTab"),
        ("ITEMWNDINFO.CURRADIO1", "itemWndInfo.curRadio1"),
        ("ITEMWNDINFO.CURRADIO2", "itemWndInfo.curRadio2"),
        ("ITEMWNDINFO.CURRADIO3", "itemWndInfo.curRadio3"),
        ("ITEMWNDINFO.CURRADIO4", "itemWndInfo.curRadio4"),
        ("ITEMWNDINFO.SHOW", "itemWndInfo.show"),
        ("QUESTWNDINFO.ORGHEIGHT", "questWndInfo.orgHeight"),
        ("QUESTWNDINFO.CURTAB", "questWndInfo.curTab"),
        ("QUESTWNDINFO.SHOW", "questWndInfo.SHOW"),
        ("QUESTDISPWNDINFO.QUESTDISPLAY", "questDisplayWndInfo.questDisplay"),
        ("STATUSWNDINFO.ORGHEIGHT", "statusWndInfo.orgHeight"),
        ("STATUSWNDINFO.SHOW", "statusWndInfo.show"),
        ("EQUIPWNDINFO.ORGHEIGHT", "equipWndInfo.orgHeight"),
        ("EQUIPWNDINFO.SHOW", "equipWndInfo.show"),
        ("OPTIONWNDINFO.ORGHEIGHT", "optionWndInfo.orgHeight"),
        ("OPTIONWNDINFO.SHOW", "optionWndInfo.show"),
        ("SHORTENITEMWNDINFO.SHOW", "shortenItemWndInfo.show"),
        ("SHORTCUTWNDINFO.SHOW", "shortCutWndInfo.show"),
        ("ITEMSTOREWNDINFO.subWnd", "itemStoreWndInfo.subWnd"),
        ("ITEMSTOREWNDINFO.findWnd", "itemStoreWndInfo.findWnd"),
        ("MESSENGERGROUPWNDINFO.SHOW", "messengerGroupWndInfo.show"),
        ("MESSENGERGROUPWNDINFO.RADIO", "messengerGroupWndInfo.radio"),
        ("MESSENGERGROUPWNDINFO.SIZEMODE", "messengerGroupWndInfo.sizeMode"),
        ("SKILLLISTWNDINFO.SHOW", "skillListWndInfo.show"),
        ("MERCHANTITEMWNDINFO.SHOW", "merchantItemWndInfo.show"),
        ("NAVIGATIONWNDINFO.ISSCREEN", "navigationWndInfo.isScreen"),
        ("MINIMAPWNDINFO.SHOWQUEST", "minimapWndInfo.showQuest"),
        ("MINIMAPWNDINFO.WHOWNPC", "minimapWndInfo.whoWNpc"),
        ("MINIMAPWNDINFO.WHOWMEMBER", "minimapWndInfo.whoWMember"),
        ("BASICINFOWNDINFO.ISSHOW", "basicInfoWndInfo.isShow"),
    ]
    for val in vals:
        for prop in props:
            vals2.append(("{0}.{1}".format(val[0], prop[0]),
                          "{0}.{1}".format(val[1], prop[1])))
    vals = vals2
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           False,
                           2)
    # structName = (offset=(fieldName, flag))
    structs1 = dict()
    # structName = set(offsets)
    structs2 = dict()
    # memberName = structName
    structNames = dict()
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            val1 = val[1]
            idx = val1.find(".")
            valName1 = val1[:idx]
            valName2 = val1[idx + 1:]
            fields = structs1.get(valName1, dict())
            offset = arr[val0]
            if type(offset) is list:
                cnt = 1
                # if more than one var found, add cnt to field name
                for data in offset:
                    valSubName = "{0}_{1}".format(valName1, cnt)
                    offsets = structs2.get(valSubName, set())
                    if cnt == 1:
                        fields[data] = valName2
                    offsets.add(data)
                    structs2[valSubName] = offsets
                    structNames[valSubName] = valName1
                    cnt = cnt + 1
            else:
                fields[offset] = valName2
                offsets = structs2.get(valName1, set())
                offsets.add(offset)
                structs2[valName1] = offsets
                structNames[valName1] = valName1
            structs1[valName1] = fields
    for struct in sorted(structs1.keys()):
        fields = structs1[struct]
        firstOffset = min(fields.keys())
        structName = struct[0].upper() + struct[1:]
        self.addStruct(structName)
        for offset in sorted(fields.keys()):
            field = fields[offset]
            self.addStructMember(field,
                                 offset - firstOffset,
                                 4,
                                 False)
    self.addStruct("UIWindowMgr")
    for name in sorted(structs2.keys()):
        offset = min(structs2[name])
        name2 = structNames[name]
        structName = name2[0].upper() + name2[1:]
        self.addStructMember(name, offset, 4, True)
        self.setStructMemberType(offset, structName)
