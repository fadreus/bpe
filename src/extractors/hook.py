#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchHookAddr(self):
    addrSet = set()
    addrList = dict()
    for addr in self.allCallAddrs:
        if addr not in addrSet:
            addrSet.add(addr)
            addrList[addr] = 1
        else:
            addrList[addr] = addrList[addr] + 1
    if self.packetVersion < "20140000":
        if self.allCallAddrs[1] == self.allCallAddrs[2]:
            # detected pattern with 1 repeated function
            hookPettern = 1
        elif self.allCallAddrs[1] == self.allCallAddrs[3]:
            # detected pattern with 2 repeated functions
            hookPettern = 2
        else:
            # detected other pattern
            hookPettern = 0
    else:
        hookPettern = 0

    if hookPettern == 0:
        addr = self.allCallAddrs[2]
    else:
        addr = self.allCallAddrs[3]
    if addrList[addr] < 3:
        self.log("Cant find hook2 function")
        exit(1)

    self.hookAddr2 = addr
    self.hookAddr2Offset = 4

    # push eax
    # lea ecx, [ebp + addr1]
    # call hookaddr
    # mov [ebp + addr2], 0FFFFFFFFh
    code = (
        "\x50" +
        "\x8D\x4D\xAB" +
        "\xE8\xAB\xAB\x00\x00" +
        "\xC7\x45\xAB\xFF\xFF\xFF\xFF")
    addrOffset = 5
    self.hookAddrOffset = 8
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.hookAddr2,
                                   self.hookAddr2 + 100)
    if offset is False:
        # 2014-05-08
        # push eax
        # lea ecx, [esi + A]
        # push ecx
        # add edi, B
        # push edi
        # call hookaddr
        # add esp, C
        # mov [ebp + addr2], 0FFFFFFFFh
        code = (
            "\x50" +
            "\x8D\x4E\xAB" +
            "\x51" +
            "\x83\xAB\xAB" +
            "\xAB" +
            "\xE8\xAB\xAB\xFF\xFF" +
            "\x83\xAB\xAB" +
            "\xC7\x45\xAB\xFF\xFF\xFF\xFF")
        addrOffset = 10
        self.hookAddrOffset = 12
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.hookAddr2,
                                       self.hookAddr2 + 100)
    if offset is False and \
       self.packetVersion < "20140000" and \
       hookPettern == 1:
        # 0  push ebp
        # 1  push esi
        # 2  push 1
        # 4  push ecx
        # 5  mov ecx, edi
        # 7  call hookaddr
        # 12 mov edx, [eax]
        # 14 mov eax, [esp+10h+a2]
        code = (
            "\x55"                            # 0 push ebp
            "\x56"                            # 1 push esi
            "\x6A\x01"                        # 2 push 1
            "\x51"                            # 4 push ecx
            "\x8B\xCF"                        # 5 mov ecx, edi
            "\xE8\xAB\xAB\xFF\xFF"            # 7 call hookaddr
            "\x8B\x10"                        # 12 mov edx, [eax]
            "\x8B\x44\x24"                    # 14 mov eax, [esp+10h+a2]
        )
        addrOffset = 8
        self.hookAddrOffset = 16
        self.hookAddr2Offset = 8
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.hookAddr2,
                                       self.hookAddr2 + 100)
        if offset is False:
            # 0  push edx
            # 1  push esi
            # 2  push 1
            # 4  push ecx
            # 5  mov ecx, ebx
            # 7  call hookaddr
            # 12 mov edx, [eax]
            # 14 mov eax, [ebp+arg_0]
            code = (
                "\x52"                            # 0 push edx
                "\x56"                            # 1 push esi
                "\x6A\x01"                        # 2 push 1
                "\x51"                            # 4 push ecx
                "\x8B\xCB"                        # 5 mov ecx, ebx
                "\xE8\xAB\xAB\xFF\xFF"            # 7 call hookaddr
                "\x8B\x10"                        # 12 mov edx, [eax]
                "\x8B\x45"                        # 14 mov eax, [ebp+arg_0]
            )
            addrOffset = 8
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.hookAddr2,
                                           self.hookAddr2 + 100)

    if offset is False:
        self.log("failed in seach hook addr")
        self.showRawAddr("hookAddr2", self.hookAddr2)
        exit(1)
        return False
    self.hookAddr = self.getAddr(offset, addrOffset, addrOffset + 4)
