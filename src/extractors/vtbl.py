#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import functools

from src.section import SectionRange
from src.vtbl import ClassVtbl, readPointersArray, readVtbl, \
    isClassNameValid, demangleClassName, getBaseClassesNameStr


try:
    xrange
except NameError:
    xrange = range


def getNextPossibleVtbl(readUInt, rdataRange, rdataSection, textRange, offset):
    finish = rdataRange.finishR
    vaToRaw = rdataSection.vaToRaw
    rIsNotIn = rdataRange.isNotIn
    tIsNotIn = textRange.isNotIn
    while offset < finish:
        vtblRttiV = readUInt(offset)
        # check if first pointer to .rdata section
        if rIsNotIn(vtblRttiV):
            offset = offset + 4
            continue
        vtblRttiR = vaToRaw(vtblRttiV)
        locator_signature = readUInt(vtblRttiR)
        # check if signature in RTTICompleteObjectLocator is zero
        if locator_signature != 0:
            offset = offset + 4
            continue
        vtbl0 = readUInt(offset + 4)
        # check if first vtable members is pointer to .text section
        if tIsNotIn(vtbl0):
            offset = offset + 4
            continue
        return offset
    return -1


def searchVtbl(self, errorExit):
    exe = self.exe
    sections = exe.sections
    readUInt = exe.readUInt
    readUInt2 = exe.readUInt2
    readUInt4 = exe.readUInt4
    readInt4 = exe.readInt4
    if exe.themida is False:
        if ".rdata" not in sections:
            exe.log("Error: .rdata section not found")
            if errorExit is True:
                exit(1)
            return
        if ".data" not in sections:
            exe.log("Error: .data section not found")
            if errorExit is True:
                exit(1)
            return
        if ".text" not in sections:
            exe.log("Error: .text section not found")
            if errorExit is True:
                exit(1)
            return
    else:
        if "sect_0" not in sections:
            exe.log("Error: themida code section not found")
            if errorExit is True:
                exit(1)
            return

    if exe.themida is False:
        rdataSection = sections[".rdata"]
        dataSection = sections[".data"]
        textSection = sections[".text"]
    else:
        rdataSection = sections["sect_0"]
        dataSection = sections["sect_0"]
        textSection = sections["sect_0"]
    vtables = []

    rdataRange = SectionRange(rdataSection)
    dataRange = SectionRange(dataSection)
    textRange = SectionRange(textSection)
    offset = rdataRange.startR
    finish = rdataRange.finishR
    if exe.fastSearch is True:
        exe.module.setRange(rdataRange.startV,
                            rdataRange.finishV,
                            textRange.startV,
                            textRange.finishV,
                            rdataSection.vaToRaw(0))
        nextVtbl = exe.module.getNextPossibleVtbl
    else:
        nextVtbl = functools.partial(getNextPossibleVtbl,
                                     readUInt,
                                     rdataRange,
                                     rdataSection,
                                     textRange)

    vtblRttiV = readUInt(offset)
    vtblRttiR = rdataSection.vaToRaw(vtblRttiV)

    rVaToRaw = rdataSection.vaToRaw
    dVaToRaw = dataSection.vaToRaw
    dIsNotIn = dataRange.isNotIn
    rIsNotIn = rdataRange.isNotIn
    readStr = exe.readStr
    # basic extraction based on http://www.openrce.org/articles/full_view/23
    while offset < finish:
        offset = nextVtbl(offset)
        if offset < 0:
            break
        vtblRttiV = readUInt(offset)
        vtblRttiR = rVaToRaw(vtblRttiV)
        vtblOffset, cdOffset, typeDescriptorV, classDescriptorV = \
            readUInt4(vtblRttiR + 1 * 4)
        # check if typeDescription pointer in .data section
        if dIsNotIn(typeDescriptorV):
            offset = offset + 4
            continue
        # check if classDescriptor is pointer to .rdata section
        if rIsNotIn(classDescriptorV):
            offset = offset + 4
            continue
        typeDescriptorR = dVaToRaw(typeDescriptorV)
        typeDescriptor_vfptr = readUInt(typeDescriptorR)
        # check if typeDescriptor.vfptr is pointer to .rdata section
        if rIsNotIn(typeDescriptor_vfptr):
            offset = offset + 4
            continue
        classDescriptorR = rVaToRaw(classDescriptorV)
        classDescriptor_signature = readUInt(classDescriptorR)
        # check if signature in RTTIClassHierarchyDescriptor is zero
        if classDescriptor_signature != 0:
            offset = offset + 4
            continue
        className = readStr(typeDescriptorR + 2 * 4)
        # check class name for valid name
        if isClassNameValid(className) is False:
            offset = offset + 4
            continue
        classDescriptor_attributes, classDescriptor_numBase = \
            readUInt2(classDescriptorR + 1 * 4)
        # check base classes number between 0 to 10
        if classDescriptor_numBase < 0 or classDescriptor_numBase > 10:
            offset = offset + 4
            continue
        classDescriptor_baseArrayV = readUInt(classDescriptorR + 3 * 4)
        classDescriptor_baseArrayR = rVaToRaw(
            classDescriptor_baseArrayV)
        pointers = readPointersArray(self,
                                     classDescriptor_baseArrayR,
                                     classDescriptor_numBase,
                                     rdataSection)
        if len(pointers) != classDescriptor_numBase:
            offset = offset + 4
            continue
        failed = False
        baseClasses = []
        baseClassesOffsets = dict()
        # for over all base class descriptors
        for pointer in pointers:
            baseTypeDescriptorV = readUInt(pointer)
            if dIsNotIn(baseTypeDescriptorV):
                failed = True
                break
            baseTypeDescriptorR = dVaToRaw(baseTypeDescriptorV)
            baseTypeDescriptor_vfptr = readUInt(baseTypeDescriptorR)
            # check if baseTypeDescriptor.vfptr is pointer to .rdata section
            if rIsNotIn(baseTypeDescriptor_vfptr):
                failed = True
                break
            baseClassName = readStr(baseTypeDescriptorR + 2 * 4)
            # check class name for valid name
            if isClassNameValid(baseClassName) is False:
                failed = True
                break
            baseClasses.append(baseClassName)
            mdisp, pdisp, vdisp, _ = readInt4(pointer + 2 * 4)
            baseClassesOffsets[baseClassName] = (mdisp, pdisp, vdisp)
        if failed is True:
            offset = offset + 4
            continue
        # skip class names with complex mangling
        if className.find("@@V?$") > 0 or className.find("$char_traits") > 0:
            offset = offset + 4
            continue
        className2, flags = demangleClassName(className)
        if className2 is None:
            offset = offset + 4
            continue
        # skip some useless class names
        if className2.find("CItemInfoMgr::?1??SetItemInfo"
                           "::lcMyInsertDesc") > 0:
            offset = offset + 4
            continue
        obj = ClassVtbl(className2, offset)
        obj.offsetV = rdataSection.rawToVa(offset)
        obj.mangledClassName = className
        obj.vtblRttiR = vtblRttiR
        obj.classDescriptorR = classDescriptorR
        obj.baseClasseDescriptors = pointers
        obj.mangledBaseClassNames = baseClasses
        obj.mangledBaseClassOffsets = baseClassesOffsets
        obj.members = readVtbl(readUInt, offset + 4, textRange, textSection)
        obj.baseClassesStr = getBaseClassesNameStr(obj)
        obj.flags = flags
        obj.vtblOffset = vtblOffset
        obj.constructorOffset = cdOffset
        obj.attr = classDescriptor_attributes
        if flags[0] is True:
            obj.typeStr = "class"
        else:
            obj.typeStr = "struct"
        # debugPrint(obj, rdataSection)
        vtables.append(obj)
        offset = offset + 4 + len(obj.members) * 4
    self.vtables = vtables


def findIdx(classes, name):
    try:
        return classes.index(name)
    except ValueError:
        return -1


def vtblNameMembers(self):
    # className = classObj
    nameObjDict = dict()
    # class name
    classes = []
    # class name if have parent
    nameHaveParent = []
    # className = (cnt, [obj,])
    nameCnt = dict()
    for obj in self.vtables:
        name = obj.name
        nameObjDict[name] = obj
        classes.append(name)
        cnt, vals = nameCnt.get(name, (0, []))
        cnt = cnt + 1
        vals.append(obj)
        nameCnt[name] = (cnt, vals)
        baseSet = set()
        for baseName in obj.mangledBaseClassNames:
            name2, _ = demangleClassName(baseName)
            baseSet.add(name2)
        obj.baseClassSet = baseSet
        if len(obj.mangledBaseClassNames) > 0:
            nameHaveParent.append(name)
        obj.name2 = name
    classes = list(set(classes))
    correct = False
    # bubble sort by complex condition
    while correct is False:
        correct = True
        for checkName in nameHaveParent:
            checkIdx = findIdx(classes, checkName)
            if checkIdx == -1:
                continue
            for baseName in nameObjDict[checkName].baseClassSet:
                baseIdx = findIdx(classes, baseName)
                if baseIdx == -1:
                    continue
                if baseIdx > checkIdx:
                    classes[checkIdx] = baseName
                    classes[baseIdx] = checkName
                    correct = False
                    break
    addrNameDict = self.addrNameDict
    addVaFunc = self.addVaFunc
    addVaVar = self.addVaVar
    addVaCommentAddr = self.addVaCommentAddr
    makeUnknown = self.makeUnknown
    makeDword = self.makeDword
    paramFunctions = self.paramFunctions
    for className in classes:
        cnt, vals = nameCnt[className]
        if cnt > 1:
            idx = 0
            for obj in vals:
                obj.name2 = "{0}_{1}".format(obj.name, idx)
                idx = idx + 1
        for obj in vals:
            name = obj.name2
            namePtr = name + "*"
            offsetV = obj.offsetV
            makeUnknown(offsetV, 4)
            makeDword(offsetV)
            obj.classStr = "{0} {1}: {2}".format(obj.typeStr,
                                                 name,
                                                 obj.baseClassesStr)
            addVaCommentAddr(obj.classStr,
                             offsetV,
                             0)
            addVaVar("{0}_vptr".format(name), obj.offsetV + 4, False)
            for idx in xrange(0, len(obj.members)):
                pointer = obj.members[idx]
                if pointer not in addrNameDict:
                    memberName = "{0}_virt{1}".format(name, idx * 4)
                    addVaFunc(memberName, pointer, False)
                    paramFunctions[pointer] = (memberName, namePtr)
    self.log("Added {0} classes.".format(len(self.vtables)))


def vtblAddStruct(self):
    addStruct = self.addStruct
    addStructMember = self.addStructMember
    setStructMemberType = self.setStructMemberType
    setStructComment = self.setStructComment
    setStructMemberComment = self.setStructMemberComment
    addrNameDict = self.addrNameDict
    for obj in self.vtables:
        name = obj.name2
        vtableName = "{0}_vtable".format(name)
        obj.vtableName = vtableName
        addStruct(vtableName)
        vtblTypes = []
        for idx in xrange(0, len(obj.members)):
            pointer = obj.members[idx]
            memberName = addrNameDict[pointer][1]
            offset = idx * 4
            addStructMember(memberName, offset, 4)
            setStructMemberComment(offset, "offset {0}".format(hex(offset)))
            vtblTypes.append((offset, pointer))
        obj.vtblTypes = vtblTypes
        addStruct(name)
        setStructComment(obj.classStr)
        addStructMember("vptr", 0, 4)
        setStructMemberType(0, "{0} *".format(vtableName))


def vtblApplyMemberParams(self):
    paramFunctions = self.paramFunctions
    for addr in paramFunctions:
        param = paramFunctions[addr]
        self.setVaFuncParam(param[0], addr, param[1])


def vtblApplyVtblTypes(self):
    addStruct = self.addStruct
    setVtblMemberType = self.setVtblMemberType
    for obj in self.vtables:
        addStruct(obj.vtableName)
        for data in obj.vtblTypes:
            setVtblMemberType(data[0], data[1])
