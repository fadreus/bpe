#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import pickle


class CacheData:
    pass


class Cache:
    def __init__(self, client, size):
        self.version = "00000"
        self.size = size
        self.path = ".cache/" + client
        self.strings = self.newData()
        self.codes = self.newData()


    def loadAll(self):
        client = self.path
        if not os.path.exists(client):
            os.makedirs(client)
        self.strings = self.load(client + "/strings.cache")
        self.codes = self.load(client + "/codes.cache")


    def saveAll(self):
        self.save(self.path + "/strings.cache", self.strings)
        self.save(self.path + "/codes.cache", self.codes)


    def load(self, fileName):
        if os.path.exists(fileName) is False:
            return self.newData()
        with open(fileName, "rb") as r:
            data = pickle.load(r)
        if data.size != self.size:
            return self.newData()
        if data.version < self.version:
            return self.newData()
        return data


    def save(self, fileName, obj):
        with open(fileName, "wb") as w:
            pickle.dump(obj, w, pickle.HIGHEST_PROTOCOL)


    def newData(self):
        data = CacheData()
        data.version = self.version
        data.size = self.size
        data.arr = dict()
        return data
