#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from src.templates import Templates


class Olly:
    def addLine(self, w, text):
        w.write(text + "\n")


    def getScript(self, extractors):
        template = Templates("file.txt", "block.txt")
        for extractor in extractors:
            template.addLine(extractor.ollyScript, extractor.cleanFileName)
        template.save("output", "extract_packets_{0}_{1}.txt", "cobmined")


    @staticmethod
    def saveOllyScript(extractors):
        if extractors.hookAddr == 0 or extractors.hookAddr2 == 0:
            extractors.log("Skip ollyscript because no hooks found")
            return
        subName = os.path.basename(os.path.abspath("."))
        outDir = "output/olly"
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        extractors.cleanupName(extractors.exe.fileName)
        with open(outDir + "/extract_packets_{0}_{1}.txt".
                  format(subName,
                         extractors.exe.fileName
                         ), "wt") as w:
            print("-------------------------------")
            extractors.addScriptCommand2(w,
                                         "// packets table extractor v2")
            extractors.addScriptCommand(w, "// {0}.exe".format(
                extractors.exe.fileName))
            addr = extractors.initPacketLenWithClientFunction
            extractors.addScriptCommand(w, "mov STARTADDR, {0}".format(
                extractors.getSimpleAddr(addr)))
            extractors.addScriptCommand(w, "mov EXITADDR,  {0}".format(
                extractors.getSimpleAddr(extractors.asmEndPos)))
            extractors.addScriptCommand(w, "mov HOOKADDR1, {0}".format(
                extractors.getSimpleAddr(extractors.hookAddr)))
            extractors.addScriptCommand(w, "mov HOOKADDR2, {0}".format(
                extractors.getSimpleAddr(extractors.hookAddr2)))
            extractors.addScriptCommand(w, "mov HOOKOFFSET1, {0}".format(
                extractors.getHexAddr(extractors.hookAddrOffset)))
            extractors.addScriptCommand(w, "mov HOOKOFFSET2, {0}".format(
                extractors.getHexAddr(extractors.hookAddr2Offset)))
            if extractors.comboAddr != 0:
                extractors.addScriptCommand(w, "mov COMBOADDR, {0}".format(
                    extractors.getSimpleAddr(extractors.comboAddr)))
            else:
                extractors.addScriptCommand(w, "mov COMBOADDR, 00000000")
            extractors.addScriptCommand(w, "mov EXENAME, \"{0}\"".format(
                extractors.cleanFileName))
            extractors.addScriptCommand2(w, "#inc \"packetstable3.txt\"")
            print("-------------------------------")
