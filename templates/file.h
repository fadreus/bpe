/*
 *  Packet logger
 *  Copyright (C) 2018  Andrei Karas (4144)
 *
 *  This file is part of Packet logger.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

static const int config_data_count = {0};
static const int config_data_bytes_count = {1};

static const struct config_item config_data[{0}] =
{{
{2}}};
